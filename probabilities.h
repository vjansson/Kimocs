/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */


/* Distribution of neutral atoms with
 * dipole moments atom_dipole_moment in a electeric field F [V/m]
 */
double dipole_moment_distribution(double F);


/* Weight the probability rates for deposition events
 * according to the strenght of the electric field.
 */
void deposition_field_correlation();


/* Find the probability for an atom transition (1nn, 2nn, 3nn, or 5nn jump), defined by the Jump struct jump 
 * Could include exchange processes in the future.
 * Replaces jump_probability_nn()
 */
Jump transition_probability(Jump jump);


/* Read the nn parameters from the 4d parameterization table and calculate the probabilities 
 * for an atom jumping from q to w (in the omega direction).
 * jump_kind = 1,2,4 or 5 (nn, 2nn, 3nn, 5nn)
 * Will take the field into account if it is present.
 */
// double atom_4D_jump_probability(Pvector q, Pvector w, int omega, int jump_kind);

/* Read the nn parameters from the 26D parameterization table and calculate the probabilities 
 * from position q in direction omega
 */
double atom_26D_jump_probability(Pvector q, int omega);


// /* Calculate the probability of an adatom jumping from q to nn */ 
// double jump_probability_nn(Pvector q, int inn);
// 
// 
// /* Calculate the probability of an adatom making a nnn jump from q to w (innn next-nearest neighbour of q) */ 
// double jump_probability_nnn(Pvector q, int innn);
// 
// 
// /* Calculate the probability of an adatom making a nnn jump from q to w (omega3 3nn of q) */ 
// double jump_probability_3nn(Pvector q, int omega3);
// 
// 
// /* Calculate the probability of an adatom making a nnn jump from q to w (omega5 5nn of q) */ 
// double jump_probability_5nn(Pvector q, int omega5);



// Adds internal and external probabilities based on nr_1nn and nr_2nn to all unit cells sourrounding q, including q.
void add_probabilities_locally(Pvector q);


// Add probabilities for external events to all objects.
void add_external_probabilies();
