/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */

#include <stdio.h>	/* for fprintf and stderr */
#include <stdlib.h>	/* for exit */
#include <math.h>
#include <string.h>
#include "mt64.h"	/* Mersenne Twister random generator */
#include <time.h>	/* For timing functions */

#include "global.h"
#include "kimocs.h"
#include "neighbour_matrices.h"
#include "s_coordinate_matrices.h"


/* Global variables */
Object system_lattice[NCELLX][NCELLY][NCELLZ][NCELLP];
double simulation_time            = 0.0;            // [s]  Simulation time.
double Temperature                = 300.0;          // [K]  Simulation temperature
double cloud_temperature          = 300.0;          // [K] Average temperature of particles being deposited.
double a0                         = 3.6146e-10;     // [m]
double jump_distance_nn           = 1.0e-10;        // [m]  Distance between nearest neighbour atom positions.
double jump_distance_2nn          = 0;              // [m] 2nn jump distance.
double jump_distance_3nn          = 0;              // [m] 3nn jump distance.
double jump_distance_4nn          = 0;              // [m] 4nn jump distance.
double jump_distance_5nn          = 0;              // [m] 5nn jump distance.
double height                     = 0.0;            // [m]  Highest z coordinate of the system
double height_old                 = 0.0;            // [m]  Old height
double kmc_step                   = 0.0;            //      KMC step
double kmc_run                    = 0;              //      The KMC run number
double evaporation_rate           = 0.0;            // [adatoms/s/m^2] Rate of adatoms removed from the system.
double deposition_rate            = 0.0;            // [adatoms/s/m^2] Rate of adatoms added to the system.
double deposition_per_second      = 0.0;            // [adatoms/s] Average rate of adatoms deposited to the system          
double atomic_deposition_rate     = 0.0;            // [1/s] Probability rate for one adatom to become depositioned (at the position of a vacancy)
double atomic_evaporation_rate    = 0.0;            // [1/s] Probability rate for one adatom to evaporate

double max_simulation_time        = 1e100;          // [s]	Maximum simulation time
double max_kmc_step               = 100.0;          // 	Max number of KMC steps in the simulation.
double max_kmc_run;                                 //	Max number of KMC runs for improved statistics; eg for calculations of the diffusion coefficient.
double min_height                 = 0.0;            // [m]	Minimum height allowed before ending the simulation.
double max_height                 = 100.0;          // [m]	Maximum height allowed before ending the simulation.

double sum_probabilities_before_event = 0.0;        // Sum of fall event probabilities before any event is carried out
double sum_probabilities              = 0.0;        // Sum of all event probabilities
double sum_internal_probabilities     = 0.0;        // Sum of the internal probabilities
double sum_nn_jump_probabilities      = 0.0;        // Sum of the nn jump internal probabilities
double sum_nnn_jump_probabilities     = 0.0;        // Sum of the nnn jump internal probabilities
double sum_3nn_jump_probabilities     = 0.0;        // Sum of the 3nn jump internal probabilities
double sum_5nn_jump_probabilities     = 0.0;        // Sum of the 5nn jump internal probabilities
double sum_external_probabilities     = 0.0;        // Sum of the external probabilities 

double sum_D_coefficient              = 0.0;        // Sum of the 3D diffusion coefficients from every kmc_run.
double sum_squared_displacement       = 0.0;        // Sum of the squared displacements for calculations of the diffusion coefficient.
double sum_ca_bonds                   = 0.0;        // Cumulative sum of c-a value for all 1nn (a,b,c,d) jumps
double sum_Em                         = 0.0;        // Cumulative sum of all migration barriers [eV] for all 1nn jumps

Parameter j1nn_parameter_table[13][7][13][7];       // Probabilities for atom 1nn jumps in the 4D parameterization.
Parameter j2nn_parameter_table[13][7][13][7];       // Probabilities for atom 2nn jumps. Optimization: Could be defined dynamically to save memory
Parameter j3nn_parameter_table[13][7][13][7];       // Probabilities for atom 3nn jumps. Optimization: Could be defined dynamically to save memory
Parameter j5nn_parameter_table[13][7][13][7];       // Probabilities for atom 5nn jumps. Optimization: Could be defined dynamically to save memory
float atom_26D_parameter_table[2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2] = {};  // Probabilities for atom nn jumps in the 26D parameterization.


Pvector dimension;                                  // [unit cells]; Simulation cell dimensions.
Ivector pbc;                                        // Periodic boundary condition for x dimension (0: no, 1: yes)

int max_1nn                         = 12;                       // Max number of nn atoms. 12 for fcc and 8 for bcc.
int max_2nn                         = 6;                        // Max number of nnn atoms; 6 for both fcc and bcc.
int max_3nn                         = 12;                       // Max number of 3nn atoms; 12 for bcc.
int max_4nn                         = 12;                       // Max number of 4nn atoms; 12 for bcc.
int max_5nn                         = 8;                        // Max number of 5nn atoms; 8 for bcc.
int max_s                           = 26;                       // Max number of s-parameters; default is 26d.

double max_index                    = 0;                        // Max object index.

Nr  nr;                                                         // Namespace for counting variables
Tag tag;                                                        // Namespace for tags

int     max_nr_elements             = 0;                        // 1 for only one metal.
int     max_nr_clusters             = 1000000;                  // Max number of clusters (end condition).
int     max_nr_evaporated_atoms     = 1000000000;               // Max number of evaporated atoms (end condition).
double  max_nr_atomic_objects       = 1e100;                    // Maximum number of atoms (adatoms + atoms) allowed in the system. Possibel end condition for deposition processes.
int     min_cluster_size_limit      = 1;                        // Minimum number of atoms to be considered a cluster.
int     min_cluster_size            = 0;                        // Size of the smallest cluster in the system.
double  max_diffusion_displacement  = 1e100;                    // Maximum displacement of diffusing atom in diffusion calculations.
double  max_CPU_time                = -1;                       // Maximum CPU time (ignore if negative)

int     bulk_cluster_index          = 0;                        // The cluster index of the substrate.
int     old_nr_objects;

float   slow_down_begin             = 0.0;
float   print_out_cycle             = 0;                        // Print out data and do checks after every step cycle.
double  print_out_cycle_s           = 0;                        // Print out data and do checks after every time cycle.

int     print_missing_parameters    = 0;                        // List in a file the missing parameters and make xyz files for the barrier calculations. Turned off [0] by default.
int     colour_probabilities        = 0;                        // Colour code objects in xyz files according to their max event probability. Will slow down simulation.
int     xyz_label_colour            = 0;                        // 1: Colour code labelled objects. 0: No colouring.

int     print_xyz_info              = 1;                        // Print label, 1nn, 2nn in objects.xyz
int     print_xyz_frame_nr          = -1;                       // Count the number of frames printed to object.xyz.

int     surface_orientation         = 100;                      // 100 or 111 or 110.
int     lattice_option              = 0;                        // 0: standard; 1: second alternative for fcc110 (110b for helmod)
enum Crystal crystal                = fcc;                      // 1: fcc, 2: bcc

int     surface_z                   = 0;                        // Higest layer of unit cells.

/* Parameters */
double attempt_frequency_1nn         = 1.0e12;                   // [s^-1] The attempt frequency for 1nn jumps
double attempt_frequency_2nn         = 1.0e12;                   // [s^-1] The attempt frequency for 2nn jumps
double attempt_frequency_3nn         = 1.0e12;                   // [s^-1] The attempt frequency for 3nn jumps
double attempt_frequency_5nn         = 1.0e12;                   // [s^-1] The attempt frequency for 5nn jumps
int   vector_1nn[12][12][4]         = {};                       // Matrix for calculating distances between 1nn atoms with p = 4, p = 6, and p = 12.
int   vector_2nn[12][6][4]          = {};                       // Matrix for calculating distances between 2nn atoms with p= 4, p = 6, and p = 12.
int   vector_3nn[12][12][4]         = {};                       // Matrix for calculating distances between 3nn atoms
int   vector_5nn[12][8][4]          = {};                       // Matrix for calculating distances between 5nn atoms
int   matrix_26D_1nn[12][12][26][4] = {};                       // Matrix for 1nn jumps with the 26D parameterization. matrix_26D_1nn[omega][s][xyzp] = Em
int   matrix_72D_1nn[12][12][72][4] = {};                       // Matrix for 1nn jumps with the 72D parameterization. matrix_72D_1nn[omega][s][xyzp] = Em
int   matrix_mirror1[72][2]         = {};                       // Matrix for the mirror1 operation of the (currently, up to) 72D LAE description
int   matrix_mirror2[72][2]         = {};                       // Matrix for the mirror2 operation of the (currently, up to) 72D LAE description
int   matrix_mirror3[72][2]         = {};                       // Matrix for the mirror3 operation of the (currently, up to) 72D LAE description
unsigned long long seed[4]          = {0x12345ULL, 0x23456ULL, 0x34567ULL, 0x45678ULL};

char  atom_type1[100];                                          // Atom type for xyz files: Cu, W, Fe, etc
char  atom_type2[100];                                          // Atom type for xyz files: Cu, W, Fe, etc

Vector  unit_cell_dimension;                                    // Length of the unit cell in x, y and z direction in [a0]. 
Vector  p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11;                  // #D coordinates in the unit cell of the p atoms.
Pvector q_diffusion_adatom;                                     // Initial coordinates for adatom for diffusion calculations.
int     inverse_omega[12][12];                                  // The omega index giving the opposite jump direction for nn jumps
int     inverse_omega2[12][12];                                 // The omega index giving the opposite jump direction for 2nn jumps
int     inverse_omega3[12][12];                                 // The omega index giving the opposite jump direction for 3nn jumps
int     inverse_omega5[12][12];                                 // The omega index giving the opposite jump direction for 5nn jumps



/* Field-related variables */
double  external_field                                = 0;      // [V/m]        Applied external field.
double  field_sign                                    = 1;      // +1 or -1;    Gives the direction of the external field along z: +1 for the anod and -1 for the kathod.
double  free_polarizability                           = 0.0;    // [e m]        Polarizability of free single atoms in vacuum.
Dipole  W2232;                                                  //              Field dipole parameters for the (2,2,3,2) process on the W{110} surface.
int     global_electric_field_recalculation_frequency = 1;      // How often the electric field is recalculated for the whole system
int     electric_field_calculation_step               = 0;      // In range [0..electric_field_calculation_step), 0 mean electric field was  calculated in the previous kmc_step
double  cumulative_charge;                                      // Stores cumulative charge for the whole system
double  cumulative_charge_prev;                                 // Used to store the cumulative charge of all the atoms for the last full-system field calculations
int     aroundapx_input                               = 64;     // baibuz: read in from data.in The size of the box where the field is calculated, the center of the box is in the highest atom position (?)
double  charge_threshold;                                       // baibuz: read in from data.in The threshold for the charge.
int     n_full_field_calc                             = 0;
double  max_local_field                               = 0.0;    // [V/m] Max measured local field (Positive for anode and negative for cathode fields)
double  local_field_limit                             = 1e100;  // [V/m] The simulation will end if the local field limit is reached.
int     max_jumps_at_constant_field                   = 0;      // Maximum jumps of an idividual atom before the fields needs to be recalculated.
Pvector field_measurement_point;                                // Coordinate for measure the field.
double  nr_femocs_errors                              = 0;      // Nr of times that FEMOCS returns femocs.error != 0.
double  gradient_correction                           = 1.0;    // Correction factor for the estimation of the gradient in the direciton of the jump.
double  surface_field_measurement_distance            = 1.0e-9; // [m] Perpendicular distance from the surface where the surface field is measured for adatoms and vacancies.

     
clock_t simulation_begin, simulation_end;
clock_t simulation_cpu;
clock_t check_system_integrity_time0, check_system_integrity_time1;
clock_t calculate_sum_probabilities_time0, calculate_sum_probabilities_time1;
clock_t choose_event_time0, choose_event_time1;
clock_t print_xyz_time0, print_xyz_time1;
clock_t print_xyzp_time0, print_xyzp_time1;
clock_t make_jump_time0, make_jump_time1;
clock_t make_barrier_xyz0, make_barrier_xyz1;
clock_t deposition_time0,deposition_time1;
clock_t evaporation_time0,evaporation_time1;
clock_t helmod_time0,helmod_time1;
clock_t helmod_wrapper_time0,helmod_wrapper_time1;
clock_t count_all_bonds_time0, count_all_bonds_time1;
clock_t read_configuration_file_time0, read_configuration_file_time1;
clock_t read_in_parameter_file_time0, read_in_parameter_file_time1;
clock_t make_bulk_time0, make_bulk_time1;
clock_t check_for_evaporation_time0, check_for_evaporation_time1;
clock_t add_probabilities_locally_time0, add_probabilities_locally_time1;
clock_t update_bond_count_time0, update_bond_count_time1;
clock_t cluster_analysis_time0, cluster_analysis_time1;
clock_t kmc_step_time0, kmc_step_time1;
clock_t print_out_cycle_time0, print_out_cycle_time1;
clock_t diffusion_coefficient_calculation_analysis_time0, diffusion_coefficient_calculation_analysis_time1;

double CPU_time 			= 0.0;
double check_system_integrity_time	= 0.0; 
double calculate_sum_probabilities_time	= 0.0;
double choose_event_time		= 0.0;
double print_xyz_time			= 0.0;
double print_xyzp_time			= 0.0;
double make_jump_time			= 0.0;
double make_barrier_xyz_time		= 0.0;
double deposition_time			= 0.0;
double evaporation_time			= 0.0;
double helmod_time			= 0.0;
double helmod_wrapper_time		= 0.0;
double count_all_bonds_time		= 0.0;
double read_configuration_file_time	= 0.0;
double read_in_parameter_file_time	= 0.0;
double make_bulk_time			= 0.0;
double check_for_evaporation_time	= 0.0;
double add_probabilities_locally_time	= 0.0;
double update_bond_count_time		= 0.0;
double cluster_analysis_time		= 0.0;
double kmc_step_time			= 0.0;
double print_out_cycle_time 		= 0.0;
double diffusion_coefficient_calculation_analysis_time	= 0.0;

FILE *file_init_xyz;              // Intitial xyz input file
FILE *file_parameters;            // nn parameter file
FILE *file_data_in;               // Configuration file
FILE *file_objects_out;           // Output data
FILE *file_events_out;            // Events data
FILE *file_transitions_out;       // Indicies of all nn transitions made during the simulation.
FILE *file_end_xyz;               // Final xyz file
FILE *file_xyz;                   // XYZ output
FILE *file_parcas_xyz;            // XYZ output for PARCAS
FILE *file_xyzp;                  // XYZP file for restarts
FILE *file_in_xyzp;               // XYZP input file 
FILE *file_2nn_p0_xyz;
FILE *file_2nn_p1_xyz;
FILE *file_2nn_p2_xyz;
FILE *file_2nn_p3_xyz;
FILE *file_2nn_p4_xyz;
FILE *file_2nn_p5_xyz;
FILE *file_2nn_p6_xyz;
FILE *file_2nn_p7_xyz;
FILE *file_2nn_p8_xyz;
FILE *file_2nn_p9_xyz;
FILE *file_2nn_p10_xyz;
FILE *file_2nn_p11_xyz;
FILE *file_local;
FILE *file_parcas_mdin;
FILE *file_E_comsol_out;
FILE *file_clusters;               // File for printing out cluster statistics
FILE *file_diffusion_out;          // File for printing the diffusion coeffiecients after every KMC run.


/* Initialize the global variables defined as structs */
void initialize_global_variables() {

  /* Objects */ 
  nr.empty                  = 0;    // Total number of empty points in the system lattice.
  nr.fixed_empty            = 0;    // Total number of fixed empty points (atom sinks).
  nr.vacancies              = 0;    // Total number of vacancy objects in the system lattice.
  nr.atoms                  = 0;    // Total number of atom objects in the system lattice.
  nr.fixed_atoms            = 0;    // Total number of edge atom objects in the system lattice.
  nr.adatoms                = 0;    // Total number of adatom objects in the system lattice.
  nr.atomic_objects         = 0;    // Nr atoms + adatoms + nr_fixed_atoms
  nr.objects                = 0;    // Total number of atoms, adatoms and vacancy objects 
  nr.initial_atomic_objects = 0;    // Number of initial atoms, adatoms and fixed atoms
    
  nr.removed_atoms          = 0;    // Total number of removed atoms or adatoms during simulation
  nr.removed_vacancies      = 0;    // Total number of removed vacancies during simulation
  nr.added_fixed_atoms      = 0;
  nr.added_fixed_empty      = 0;
  nr.clusters               = 0;    // Total number of atomic clusters
    
  /* Events */
  nr.no_event               = 0.0;  // Nr of steps where no event were carried out.
  nr.jumps_1nn              = 0.0;  // Nr of 1nn jump events in the simulation.
  nr.jumps_2nn              = 0.0;  // Nr of 2nn jump events in the simulation.
  nr.jumps_3nn              = 0.0;  // Nr of 3nn jump events in the simulation.
  nr.jumps_4nn              = 0.0;  // Nr of 4nn jump events in the simulation.
  nr.jumps_5nn              = 0.0;  // Nr of 5nn jump events in the simulation.
  nr.evaporation_events     = 0.0;  // Total number of evaporated atoms or adatoms
  nr.deposition_events      = 0.0;  // Total number of depositioned atoms or adatoms
    
  nr.jumps_unstable         = 0.0;  // Nr of 1nn jumps with a < 4.
  nr.calls_field_solver     = 0.0;  // Nr of calls to the field solver (Helmod or Femocs)


  /** Tags **/

  tag.end_condition             = 0;    // End conditions are fullfilled: 1 or not fullfilled: 0
  tag.kmc_runs_end_condition    = 0;    // End conditions are fullfilled: 1 or not fullfilled: 0
  tag.focused_deposition        = 0;    // 1: Deposition is focused on a small area.
  tag.print_empty_kind          = 0;
  tag.print_femocs_field        = 0;    // 1: Print the Field for empty objects, as calculated by Femocs.
  tag.using_comsol              = 0;    // 1: Data will be exchanged with COMSOL
  tag.print_xyz_files           = 1;    // 1: Print objects.xyz and end.xyz files.
  tag.print_xyzp_files          = 1;    // 1: Print objects_*.xyzp files for restarts.
  tag.print_parcas_files        = 1;    // 1: Print md.in, mdlat.in.xyz_initial, and mdlat.in.xyz_end for Parcas.
  tag.print_par_files           = 0;    // 1: Print used_nn_parameters.par and other files with used parameters.
  tag.print_transitions         = 0;    // 1: Print the index of every nn transition.
  tag.global_attempt_frequency_1nn = 0; // 1: All 1nn jumps have the same attempt frequency read in from the par file or data.in.
  tag.global_attempt_frequency_2nn = 0;
  tag.global_attempt_frequency_3nn = 0;
  tag.global_attempt_frequency_5nn = 0;
  tag.allow_nnn_jumps           = 0;    // 1: Allow nnn jumps.
  tag.allow_3nn_jumps           = 0;    // 1: Allow 3nn jumps.
  tag.allow_5nn_jumps           = 0;    // 1: Allow 5nn jumps.
  tag.use_atomic_temperature    = 0;    // 1: Tempererature set for individual objects.
  tag.cluster_evaporation       = 0;    // 1: Clusters detached from the bulk will be removed.
  tag.calculate_diffusion_coefficient   = 0;    // 1: Calculate the diffusion coefficient with multiple KMC runs.
  tag.using_26D_parameters      = 0;    // 1: Using a 26D parameterization (instead of the 4D parameterization)
  tag.using_72D_parameters      = 0;    // 1: Using a 72D parameterization
  tag.using_ann                 = 0;    // 1: Using ANNs for barriers
  tag.using_classifier          = 0;    // 1: Using ANN classifier for barriers
  tag.debug                     = 0;    // 1: Debuggin mode
  tag.print_xyz_temperature     = 0;    // Print object temperatures in xyz files.
  tag.deposition_mode           = 3;    // 1, 2, or 3
  tag.deposition_nn_threshold   = 0;    // int; Minimum number of neighbour for a vacancy position to be valid for a deposition event.
  tag.check_neighbour_matrices  = 0;    // 1: Check that the neighbours of any atom are correctly identified.
    
  /* Field-related tags */
  tag.use_field                 = 0;    // 1: Use electric field
  tag.print_libhelmod_field     = 0;    // 1: Print the LibHelmod field
  tag.recalculate_field         = 1;    // 1: Recalculate the field.
  tag.use_libhelmod             = 0;    // 1: Use LibHelmod to calculate the field.
  tag.use_charges               = 0;    // 1: Calculate electric charges with Femocs (or Helmod)
  tag.libhelmod_field_model     = 2;    // 1: Old, 2: New far-field. Model for how the field values are transferred from LibHelmod to Kimocs

  /* Dipole parameters */
  W2232.M_sl                    = 0.0;
  W2232.A_sl                    = 0.0;
  W2232.M_sr                    = 0.0;
  W2232.A_sr                    = 0.0;
  
  
  dimension.x = 0;
  dimension.y = 0;
  dimension.z = 0;
  dimension.p = 0;
  
}


void initiate_lattice_orientation_parameters(int surface_orientation) {
  int p,q,nnn;
  int omega, omega2, omega3, omega5, s;
  
  if (crystal == fcc) {
    max_1nn = 12;
    max_2nn = 6;
    jump_distance_nn = sqrt(2.0)/2.0*a0;
    int i;
    for (i = 0; i<72; i++){
      matrix_mirror1[i][0] = matrix_fcc_mirror1[i][0];
      matrix_mirror1[i][1] = matrix_fcc_mirror1[i][1];

      matrix_mirror2[i][0] = matrix_fcc_mirror2[i][0];
      matrix_mirror2[i][1] = matrix_fcc_mirror2[i][1];

      matrix_mirror3[i][0] = matrix_fcc_mirror3[i][0];
      matrix_mirror3[i][1] = matrix_fcc_mirror3[i][1];
    }
  }
  else if (crystal == bcc) {
    max_1nn = 8;
    max_2nn = 6;
    max_3nn = 12;
    max_4nn = 12;
    max_5nn = 8;
    jump_distance_nn  = sqrt(3.0)/2.0*a0;
    jump_distance_2nn = 1.0*a0;
    jump_distance_3nn = sqrt(2.0)*a0;
//     jump_distance_4nn TODO
    jump_distance_5nn = sqrt(3.0)*a0; 
    
    if (tag.using_26D_parameters == 1) {
      printf("ERROR 26D is not yet implemented for bcc\n");
      exit(1);
    }
  }
  
  
  /* Make sure the matrix is empty */
  for (p = 0; p < 12; p++) {
    for (omega = 0; omega < 12; omega++) {
      for (q = 0; q < 4; q++) {
        vector_1nn[p][omega][q] = 0;
        if (omega < 6) vector_2nn[p][omega][q] = 0;
      }
    }
  }
  
  /* Make sure the matrix is empty */
  for (p = 0; p < 12; p++) {
    for (omega3 = 0; omega3 < max_3nn; omega3++) {
      for (q = 0; q < 4; q++) {
        vector_3nn[p][omega3][q] = 0;
      }
    }
  }
  
  /* Make sure the matrix is empty */
  for (p = 0; p < 12; p++) {
    for (omega5 = 0; omega5 < max_5nn; omega5++) {
      for (q = 0; q < 4; q++) {
        vector_5nn[p][omega5][q] = 0;
      }
    }
  }
  
  /* 26d. p is max 6 in fcc. TODO bcc (12) */
  for (p = 0; p < 12; p++) {
    for (omega = 0; omega < 12; omega++) {
      for (s = 0; s < 26; s++) {
        for (q = 0; q < 4; q++) {
          matrix_26D_1nn[p][omega][s][q] = 0;
          
        }
      }      
    }  
  }
      
  /* 72d. p is max 6 in fcc. TODO bcc (12) */
  for (p = 0; p < 12; p++) {
    for (omega = 0; omega < 12; omega++) {
      for (s = 0; s < 72; s++) {
        for (q = 0; q < 4; q++) {
          matrix_72D_1nn[p][omega][s][q] = 0;
          
        }
      }      
    }  
  }
      
      
  
  if (surface_orientation == 100 && crystal == fcc) {
    
    /* Inverse omega */
    for (p = 0; p < dimension.p; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        inverse_omega[p][omega] = inverse_omega_fcc100[p][omega];
      }
    }
    
    /* 1nn atoms */
    for (p = 0; p < 4; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        for (q = 0; q < 4; q++) {
          vector_1nn[p][omega][q] = vector_1nn_fcc100[p][omega][q];
        }
      }
    }
    
    /* 2nn atoms */
    for (p = 0; p < 4; p++) {
      for (nnn = 0; nnn < 6; nnn++) {
        for (q = 0; q < 4; q++) {
          vector_2nn[p][nnn][q] = vector_2nn_fcc100[p][nnn][q];
        }
      }
    }
    
    if (tag.allow_3nn_jumps == 1) {
      printf("ERROR 3nn jumps with fcc not implemented. Exiting.\n");
      exit(1);
    }
    if (tag.allow_5nn_jumps == 1) {
      printf("ERROR 5nn jumps with fcc not implemented. Exiting.\n");
      exit(1);
    }
	  
    // Positions of base atoms (p) in the fcc unit cell (a0 = 1.0)
    p0.x = 0.0; 
    p0.y = 0.0; 
    p0.z = 0.0;
    
    p1.x = 0.5; 
    p1.y = 0.5; 
    p1.z = 0.0;
    
    p2.x = 0.0; 
    p2.y = 0.5; 
    p2.z = 0.5;
    
    p3.x = 0.5; 
    p3.y = 0.0; 
    p3.z = 0.5;
    
    /* Unused */
    p4.x = p4.y = p4.z = 0.0;
    p5.x = p5.y = p5.z = 0.0;
    
    
    /* 26D parameterization */
    for (p = 0; p < 4; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        for (s = 0; s < 26; s++) {
          for (q = 0; q < 4; q++) {
            matrix_26D_1nn[p][omega][s][q] = matrix_72D_fcc100[p][omega][s][q];
          }
        }
      }
    }
    
    /* 72D parameterization */
    for (p = 0; p < 4; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        for (s = 0; s < 72; s++) {
          for (q = 0; q < 4; q++) {
            matrix_72D_1nn[p][omega][s][q] = matrix_72D_fcc100[p][omega][s][q];
          }
        }
      }
    }
    
    /* Unit cell is symmetrical. Also defined in main(). */
    unit_cell_dimension.x = unit_cell_dimension.y = unit_cell_dimension.z = 1.0;
    
  }
  else if (surface_orientation == 111 && crystal == fcc) {
    
    /* Inverse omega */
    for (p = 0; p < dimension.p; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        inverse_omega[p][omega] = inverse_omega_fcc111[p][omega];
      }
    }
    
    // 1nn atoms
    for (p = 0; p < 6; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        for (q = 0; q < 4; q++) {
          vector_1nn[p][omega][q] = vector_1nn_fcc111[p][omega][q];
        }
      }
    }
    
    // 2nn atoms
    for (p = 0; p < 6; p++) {
      for (nnn = 0; nnn < 6; nnn++) {
        for (q = 0; q < 4; q++) {
          vector_2nn[p][nnn][q] = vector_2nn_fcc111[p][nnn][q];
        }
      }
    }
    
    if (tag.allow_3nn_jumps == 1) {
      printf("ERROR 3nn jumps with fcc not implemented. Exiting.\n");
      exit(1);
    }
    if (tag.allow_5nn_jumps == 1) {
      printf("ERROR 5nn jumps with fcc not implemented. Exiting.\n");
      exit(1);
    }

    // Positions of base atoms (p) in the (111) unit cell (a0 = 1.0)
    p0.x = 0.0;
    p1.x = sqrt(6.0)/4.0; 
    p2.x = 2.0/3.0*sqrt(3.0/2.0); 
    p3.x = 1.0/6.0*sqrt(3.0/2.0);
    p4.x = sqrt(6.0)/6.0;
    p5.x = 5.0/6.0*sqrt(3.0/2.0);
    
    p0.y = 0.0; 
    p1.y = 1.0/(2.0*sqrt(2.0));
    p2.y = 0.0; 
    p3.y = 1.0/(2.0*sqrt(2.0));
    p4.y = 0.0;
    p5.y = 1.0/(2.0*sqrt(2.0));
    
    p0.z = 0.0;
    p1.z = 0.0;
    p2.z = 1.0/sqrt(3.0);
    p3.z = 1.0/sqrt(3.0);
    p4.z = 2.0/sqrt(3.0);
    p5.z = 2.0/sqrt(3.0);
    
    /* 26D parameterization */
    for (p = 0; p < 6; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        for (s = 0; s < 26; s++) {
          for (q = 0; q < 4; q++) {
            matrix_26D_1nn[p][omega][s][q] = matrix_72D_fcc111[p][omega][s][q];
          }
        }
      }
    }
    
    /* 72D parameterization */
    for (p = 0; p < 6; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        for (s = 0; s < 72; s++) {
          for (q = 0; q < 4; q++) {
            matrix_72D_1nn[p][omega][s][q] = matrix_72D_fcc111[p][omega][s][q];
          }
        }
      }
    }
    
    // As the unit cell is not symmetrical, define the cell dimensions in [a0]
    unit_cell_dimension.x = sqrt(6.0)/2.0;
    unit_cell_dimension.y = 1.0/sqrt(2.0);
    unit_cell_dimension.z = 3.0/sqrt(3.0);
    
  }
  else if (surface_orientation == 110 && crystal == fcc) {
    
    /* 110a */
    if (lattice_option == 0) {
      
      /* Inverse omega */
      for (p = 0; p < dimension.p; p++) {
        for (omega = 0; omega < max_1nn; omega++) {
          inverse_omega[p][omega] = inverse_omega_fcc110a[p][omega];
        }
      }
      
      // 1nn atoms
      for (p = 0; p < 6; p++) {
        for (omega = 0; omega < max_1nn; omega++) {
          for (q = 0; q < 4; q++) {
            vector_1nn[p][omega][q] = vector_1nn_fcc110a[p][omega][q];
          }
        }
      }
      
      // 2nn atoms
      for (p = 0; p < 6; p++) {
        for (nnn = 0; nnn < 6; nnn++) {
          for (q = 0; q < 4; q++) {
            vector_2nn[p][nnn][q] = vector_2nn_fcc110a[p][nnn][q];
          }
        }
      }
      
      if (tag.allow_3nn_jumps == 1) {
        printf("ERROR 3nn jumps with fcc not implemented. Exiting.\n");
        exit(1);
      }
      if (tag.allow_5nn_jumps == 1) {
        printf("ERROR 5nn jumps with fcc not implemented. Exiting.\n");
        exit(1);
      }

      // Positions of base atoms (p) in the (110) unit cell (a0 = 1.0)
      p0.x = 0.0;
      p1.x = 0.0;
      p2.x = 1.0/sqrt(3.0);
      p3.x = 1.0/sqrt(3.0);
      p4.x = 2.0/sqrt(3.0);
      p5.x = 2.0/sqrt(3.0);
      
      p0.y = 0.0;
      p1.y = sqrt(6.0)/4.0; 
      p2.y = 2.0/3.0*sqrt(3.0/2.0); 
      p3.y = 1.0/6.0*sqrt(3.0/2.0);
      p4.y = sqrt(6.0)/6.0;
      p5.y = 5.0/6.0*sqrt(3.0/2.0);
      
      p0.z = 0.0; 
      p1.z = 1.0/(2.0*sqrt(2.0));
      p2.z = 0.0; 
      p3.z = 1.0/(2.0*sqrt(2.0));
      p4.z = 0.0;
      p5.z = 1.0/(2.0*sqrt(2.0));
      
            
      /* 26D parameterization */
      if (tag.using_26D_parameters == 1) {
        for (p = 0; p < 6; p++) {
          for (omega = 0; omega < max_1nn; omega++) {
            for (s = 0; s < 26; s++) {
              for (q = 0; q < 4; q++) {
                matrix_26D_1nn[p][omega][s][q] = matrix_72D_fcc110a[p][omega][s][q];
              }
            }
          }
        }
      }
      
      /* 72D parameterization */
      if (tag.using_72D_parameters == 1) {
        for (p = 0; p < 6; p++) {
          for (omega = 0; omega < max_1nn; omega++) {
            for (s = 0; s < 72; s++) {
              for (q = 0; q < 4; q++) {
                matrix_72D_1nn[p][omega][s][q] = matrix_72D_fcc110a[p][omega][s][q];
              }
            }
          }
        }
      }
      
      
      // As the unit cell is not symmetrical, define the cell dimensions in [a0]
      unit_cell_dimension.x = 3.0/sqrt(3.0);
      unit_cell_dimension.y = sqrt(6.0)/2.0;
      unit_cell_dimension.z = 1.0/sqrt(2.0);
      
    }
    else if (lattice_option == 1) {
      /*fcc110b --- Compatible with LibHelmod.*/
      
      /* Inverse omega */
      for (p = 0; p < dimension.p; p++) {
        for (omega = 0; omega < max_1nn; omega++) {
          inverse_omega[p][omega] = inverse_omega_fcc110b[p][omega];
        }
      }
      
      // 1nn atoms
      for (p = 0; p < 2; p++) {
        for (omega = 0; omega < max_1nn; omega++) {
          for (q = 0; q < 4; q++) {
            vector_1nn[p][omega][q] = vector_1nn_fcc110b[p][omega][q];
          }
        }
      }
      
      // 2nn atoms
      for (p = 0; p < 2; p++) {
        for (nnn = 0; nnn < 6; nnn++) {
          for (q = 0; q < 4; q++) {
            vector_2nn[p][nnn][q] = vector_2nn_fcc110b[p][nnn][q];
          }
        }
      }
      
      p0.x = 0.0;
      p0.y = 0.0;
      p0.z = 0.0;
      
      p1.x = 0.5;
      p1.y = 0.5/sqrt(2.0);
      p1.z = 0.5/sqrt(2.0);
      
      /* 26D parameterization */
      for (p = 0; p < 2; p++) {
        for (omega = 0; omega < max_1nn; omega++) {
          for (s = 0; s < 26; s++) {
            for (q = 0; q < 4; q++) {
              matrix_26D_1nn[p][omega][s][q] = matrix_72D_fcc110b[p][omega][s][q];
            }
          }
        }
      }    
      
      /* 72D parameterization */
      for (p = 0; p < 2; p++) {
        for (omega = 0; omega < max_1nn; omega++) {
          for (s = 0; s < 72; s++) {
            for (q = 0; q < 4; q++) {
              matrix_72D_1nn[p][omega][s][q] = matrix_72D_fcc110b[p][omega][s][q];
            }
          }
        }
      }    
      
      /* As the unit cell is not symmetrical, define the cell dimensions in [a0] */
      unit_cell_dimension.x = 1.0;
      unit_cell_dimension.y = 1.0/sqrt(2.0);
      unit_cell_dimension.z = 1.0/sqrt(2.0);
      
    }
  }
  
  
  
  else if (surface_orientation == 100 && crystal == bcc) {
    
    /* Inverse omega */
    for (p = 0; p < dimension.p; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        inverse_omega[p][omega] = inverse_omega_bcc100[p][omega];
      }
    }
    
    // 1nn atoms
    for (p = 0; p < 2; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        for (q = 0; q < 4; q++) {
          vector_1nn[p][omega][q] = vector_1nn_bcc100[p][omega][q];
        }
      }
    }
    
    // 2nn atoms
    for (p = 0; p < 2; p++) {
      for (nnn = 0; nnn < 6; nnn++) {
        for (q = 0; q < 4; q++) {
          vector_2nn[p][nnn][q] = vector_2nn_bcc100[p][nnn][q];
        }
      }
    }
    
    /* 3nn atoms */
    for (p = 0; p < 12; p++) {
      for (omega3 = 0; omega3 < max_3nn; omega3++) {
        for (q = 0; q < 4; q++) {
          vector_3nn[p][omega3][q] = vector_3nn_bcc100[p][omega3][q]; 
        }
      }
    }
    
    if (tag.allow_5nn_jumps == 1) {
      printf("ERROR 5nn jumps with bcc100 not implemented. Exiting.\n");
      exit(1);
    }
	  
    // Position of base atoms (p) in the bcc unit cell (a0 == 1.0)
    p0.x = 0.0; 
    p0.y = 0.0; 
    p0.z = 0.0;

    p1.x = 0.5; 
    p1.y = 0.5; 
    p1.z = 0.5;
    
    // Unused.
    p2.x = p2.y = p2.z = 0.0;
    p3.x = p3.y = p3.z = 0.0;
    p4.x = p4.y = p4.z = 0.0;
    p5.x = p5.y = p5.z = 0.0;
    
    // Unit cell is symmetrical. Also defined in main().
    unit_cell_dimension.x = unit_cell_dimension.y = unit_cell_dimension.z = 1.0;
  }
  else if (surface_orientation == 110 && crystal == bcc) {
    
    /* Inverse omega */
    for (p = 0; p < dimension.p; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        inverse_omega[p][omega] = inverse_omega_bcc110[p][omega];
      }
    }
    
    /* Inverse omega for 2nn jumps*/
    for (p = 0; p < dimension.p; p++) {
      for (omega2 = 0; omega2 < max_2nn; omega2++) {
        inverse_omega2[p][omega2] = inverse_omega2_bcc110[p][omega2];
      }
    }
    
    /* Inverse omega for 3nn jumps*/
    for (p = 0; p < dimension.p; p++) {
      for (omega3 = 0; omega3 < max_3nn; omega3++) {
        inverse_omega3[p][omega3] = inverse_omega3_bcc110[p][omega3];
      }
    }
    
    /* Inverse omega for 5nn jumps*/
    for (p = 0; p < dimension.p; p++) {
      for (omega5 = 0; omega5 < max_5nn; omega5++) {
        inverse_omega5[p][omega5] = inverse_omega5_bcc110[p][omega5];
      }
    }
    
    /* 1nn atoms */
    for (p = 0; p < 4; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        for (q = 0; q < 4; q++) {
          vector_1nn[p][omega][q] = vector_1nn_bcc110[p][omega][q];
        }
      }
    }
    
    /* 2nn atoms */
    for (p = 0; p < 4; p++) {
      for (nnn = 0; nnn < 6; nnn++) {
        for (q = 0; q < 4; q++) {
          vector_2nn[p][nnn][q] = vector_2nn_bcc110[p][nnn][q]; 
        }
      }
    }
    
    /* 3nn atoms */
    for (p = 0; p < 12; p++) {
      for (omega3 = 0; omega3 < max_3nn; omega3++) {
        for (q = 0; q < 4; q++) {
          vector_3nn[p][omega3][q] = vector_3nn_bcc110[p][omega3][q]; 
        }
      }
    }
    
    /* 5nn atoms */
    for (p = 0; p < 12; p++) {
      for (omega5 = 0; omega5 < max_5nn; omega5++) {
        for (q = 0; q < 4; q++) {
          vector_5nn[p][omega5][q] = vector_5nn_bcc110[p][omega5][q]; 
        }
      }
    }
    
    // Positions of base atoms (p) in the bcc unit cell (a0 = 1.0)
    p0.x = 0.0; 
    p0.y = 0.0; 
    p0.z = 0.0;
    
    p1.x = sqrt(2.0)/2.0; 
    p1.y = 0.5; 
    p1.z = 0.0;
    
    p2.x = 0.0; 
    p2.y = 0.5; 
    p2.z = sqrt(2.0)/2.0;
    
    p3.x = sqrt(2.0)/2.0; 
    p3.y = 0.0; 
    p3.z = sqrt(2.0)/2.0;
    
    // Unused.
    p4.x = p4.y = p4.z = 0.0;
    p5.x = p5.y = p5.z = 0.0;
    
    // Unit cell is non-symmetrical; define the cell dimensions in [a0]
    unit_cell_dimension.x = sqrt(2.0);
    unit_cell_dimension.y = 1.0;
    unit_cell_dimension.z = sqrt(2.0);
    
  }
  else if (surface_orientation == 111 && crystal == bcc) {
    
    if (tag.use_field == 1) {
      /* Inverse omega */
      for (p = 0; p < dimension.p; p++) {
        for (omega = 0; omega < max_1nn; omega++) {
  //         inverse_omega[p][omega] = inverse_omega_bcc111[p][omega];
          printf("ERROR The inverse omega matrix for the bcc111 system is not yet implemented. Field can not be used. Ending.\n");
          exit(1);
        }
      }
    } 
    
    /* 1nn atoms */
    for (p = 0; p < 12; p++) {
      for (omega = 0; omega < max_1nn; omega++) {
        for (q = 0; q < 4; q++) {
          vector_1nn[p][omega][q] = vector_1nn_bcc111[p][omega][q];
        }
      }
    }
    
    /* 2nn atoms */
    for (p = 0; p < 12; p++) {
      for (nnn = 0; nnn < 6; nnn++) {
        for (q = 0; q < 4; q++) {
          vector_2nn[p][nnn][q] = vector_2nn_bcc111[p][nnn][q]; 
        }
      }
    }
    
    /* 3nn atoms */
    for (p = 0; p < 12; p++) {
      for (omega3 = 0; omega3 < 12; omega3++) {
        for (q = 0; q < 4; q++) {
          vector_3nn[p][omega3][q] = vector_3nn_bcc111[p][omega3][q]; 
        }
      }
    }
    
    /* 5nn atoms */
    for (p = 0; p < 12; p++) {
      for (omega5 = 0; omega5 < 8; omega5++) {
        for (q = 0; q < 4; q++) {
          vector_5nn[p][omega5][q] = vector_5nn_bcc111[p][omega5][q]; 
        }
      }
    }
    
  
    /* Positions of base atoms (p) in the bcc (111) unit cell (a0 = 1.0) */
    p0.x = 0.0; 
    p0.y = 0.0;
    p0.z = 0.0;
    
    p1.x = 1.0/3.0*sqrt(6); 
    p1.y = 0.0; 
    p1.z = 1.0/6.0*sqrt(3.0);
    
    p2.x = 2.0/3.0*sqrt(6.0); 
    p2.y = 0.0 ;
    p2.z = 1.0/3.0*sqrt(3.0);
    
    p3.x = 0.0;
    p3.y = 0.0;
    p3.z = 1.0/2.0*sqrt(3.0);
    
    p4.x = 1.0/3.0*sqrt(6.0); 
    p4.y = 0.0;
    p4.z = 2.0/3.0*sqrt(3.0);
    
    p5.x = 2.0/3.0*sqrt(6.0); 
    p5.y = 0.0;
    p5.z = 5.0/6.0*sqrt(3.0);

    p6.x = 1.0/2.0*sqrt(6.0); 
    p6.y = 1.0/2.0*sqrt(2.0); 
    p6.z = 0.0;
    
    p7.x = 5.0/6.0*sqrt(6.0); 
    p7.y = 1.0/2.0*sqrt(2.0); 
    p7.z = 1.0/6.0*sqrt(3.0);
    
    p8.x = 1.0/6.0*sqrt(6.0); 
    p8.y = 1.0/2.0*sqrt(2.0); 
    p8.z = 1.0/3.0*sqrt(3.0);
    
    p9.x = 1.0/2.0*sqrt(6.0); 
    p9.y = 1.0/2.0*sqrt(2.0); 
    p9.z = 1.0/2.0*sqrt(3.0);
    
    p10.x = 5.0/6.0*sqrt(6.0); 
    p10.y = 1.0/2.0*sqrt(2.0); 
    p10.z = 2.0/3.0*sqrt(3.0);
    
    p11.x = 1.0/6.0*sqrt(6.0); 
    p11.y = 1.0/2.0*sqrt(2.0); 
    p11.z = 5.0/6.0*sqrt(3.0);
    
    // Unit cell is non-symmetrical; define the cell dimensions in [a0]
    unit_cell_dimension.x = sqrt(6.0);
    unit_cell_dimension.y = sqrt(2.0);
    unit_cell_dimension.z = sqrt(3.0);
    
  }
  else {
    printf("ERROR (%i) surface undefined in initiate_lattice_orientation_parameters(); crystal = %i\n", surface_orientation,crystal);
    exit(1);
  }
  
}


/* Eulers's angle transformations.
 * Omega is a vector with the three rotation angles psi, phi and theta.
 * x is the intial xyz position and r is the returned xyz position.
 */
Vector rot(Vector x, Vector Omega){
  Vector r;
  double a11,a12,a13,a21,a22,a23,a31,a32,a33;
  double psi, phi, theta;
  
  psi   = Omega.x;
  phi   = Omega.y;
  theta = Omega.z;
  
  
  a11 = cos(psi)*cos(phi) - cos(theta)*sin(phi)*sin(psi);
  a12 = cos(psi)*sin(phi) + cos(theta)*cos(phi)*sin(psi);
  a13 = sin(psi)*sin(theta);
  a21 = -sin(psi)*cos(phi) - cos(theta)*sin(phi)*cos(psi);
  a22 = -sin(psi)*sin(phi) + cos(theta)*cos(phi)*cos(psi);
  a23 = cos(psi)*sin(theta);
  a31 = sin(theta)*sin(phi);
  a32 = -sin(theta)*cos(phi);
  a33 = cos(theta);
  
  r.x = a11*x.x+a12*x.y+a13*x.z;
  r.y = a21*x.x+a22*x.y+a23*x.z;
  r.z = a31*x.x+a32*x.y+a33*x.z;

  return r;
}


/* Function for converting 3D coordinates to internal Kimocs coordinates */
Pvector xyz_to_xyzp(Vector v, double lattice_parameter) {
  Pvector q;		// xyzp coordinate
  Vector v_unit;	// xyz coordinate within the unit cell
  double epsilon = lattice_parameter/100.0;	// Precision.
  int i;
  int nr_agreements = 0;
  Vector P[12];
  
  P[0]  = p0;
  P[1]  = p1;
  P[2]  = p2;
  P[3]  = p3;
  P[4]  = p4;
  P[5]  = p5;
  P[6]  = p6;
  P[7]  = p7;
  P[8]  = p8;
  P[9]  = p9;
  P[10] = p10;
  P[11] = p11;
  
  /* P vectors need to be be scaled correctly */
  for (i = 0; i < dimension.p; i++) {
    P[i].x = P[i].x*lattice_parameter;
    P[i].y = P[i].y*lattice_parameter;
    P[i].z = P[i].z*lattice_parameter;
  }
  
  /* Find Carthesian coordinates */
  q.x = floor(v.x/(lattice_parameter*unit_cell_dimension.x));
  q.y = floor(v.y/(lattice_parameter*unit_cell_dimension.y));
  q.z = floor(v.z/(lattice_parameter*unit_cell_dimension.z));
    
  /* If position is close to the boarder */
  if        (fabs((q.x + 1)*lattice_parameter*unit_cell_dimension.x - v.x) < epsilon) q.x++;
  else if   (fabs((q.x - 1)*lattice_parameter*unit_cell_dimension.x - v.x) < epsilon) q.x--;
  if        (fabs((q.y + 1)*lattice_parameter*unit_cell_dimension.y - v.y) < epsilon) q.y++;
  else if   (fabs((q.y - 1)*lattice_parameter*unit_cell_dimension.y - v.y) < epsilon) q.y--;
  if        (fabs((q.z + 1)*lattice_parameter*unit_cell_dimension.z - v.z) < epsilon) q.z++;
  else if   (fabs((q.z - 1)*lattice_parameter*unit_cell_dimension.z - v.z) < epsilon) q.z--;
  
  v_unit.x 	= v.x - q.x*(lattice_parameter*unit_cell_dimension.x);
  v_unit.y 	= v.y - q.y*(lattice_parameter*unit_cell_dimension.y);
  v_unit.z 	= v.z - q.z*(lattice_parameter*unit_cell_dimension.z);
  
  
  /* Find the p coordinates */
  nr_agreements = 0;
  for (i = 0; i < dimension.p; i++) {
    if (distance(v_unit, P[i]) < epsilon) {
      q.p = i;
      nr_agreements++;
    } 
  }
  
  if( nr_agreements > 1) {
    printf("# ERROR in xyz_to_xyzp(): epsilon too large\n");
  }
  else if (nr_agreements == 0) {
    printf("# ERROR n xyz_to_xyzp(): No match for p coordinate, (%lf, %lf, %lf) != (%i, %i, %i %i) \n", v_unit.x, v_unit.y, v_unit.z, q.x, q.y, q.z, q.p);
  }
  
  return q;
  
}


// Function for converting 4D coordinates to 3D
Vector xyzp_to_xyz(Pvector q, double lattice_parameter) {
  Vector v;
  
  switch(q.p) {
    case 0:
      v.x = (q.x*unit_cell_dimension.x + p0.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p0.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p0.z)*lattice_parameter;
      break;
    case 1:
      v.x = (q.x*unit_cell_dimension.x + p1.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p1.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p1.z)*lattice_parameter;
      break;
    case 2:
      v.x = (q.x*unit_cell_dimension.x + p2.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p2.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p2.z)*lattice_parameter;
      break;
    case 3:
      v.x = (q.x*unit_cell_dimension.x + p3.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p3.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p3.z)*lattice_parameter;
      break;
    case 4:
      v.x = (q.x*unit_cell_dimension.x + p4.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p4.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p4.z)*lattice_parameter;
      break;
    case 5:
      v.x = (q.x*unit_cell_dimension.x + p5.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p5.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p5.z)*lattice_parameter;
      break;
    case 6:
      v.x = (q.x*unit_cell_dimension.x + p6.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p6.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p6.z)*lattice_parameter;
      break;
    case 7:
      v.x = (q.x*unit_cell_dimension.x + p7.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p7.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p7.z)*lattice_parameter;
      break;
    case 8:
      v.x = (q.x*unit_cell_dimension.x + p8.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p8.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p8.z)*lattice_parameter;
      break;
    case 9:
      v.x = (q.x*unit_cell_dimension.x + p9.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p9.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p9.z)*lattice_parameter;
      break;
    case 10:
      v.x = (q.x*unit_cell_dimension.x + p10.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p10.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p10.z)*lattice_parameter;
      break;
    case 11:
      v.x = (q.x*unit_cell_dimension.x + p11.x)*lattice_parameter;
      v.y = (q.y*unit_cell_dimension.y + p11.y)*lattice_parameter;
      v.z = (q.z*unit_cell_dimension.z + p11.z)*lattice_parameter;
      break;
  }
  
  return v;
}


/* Count the number of adatoms in the system */
void count_number_atoms() { 
  Pvector q;
    
  nr.atomic_objects = 0;
  nr.vacancies      = 0;
  nr.adatoms        = 0;
  nr.atoms          = 0;

  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          //printf("#count_number_atoms(): %i\n",system_lattice[x][y][z][p].kind);
          if (system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind ) {
            nr.atomic_objects++;
            nr.atoms++;
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == fixed_atom_kind) {
            nr.atomic_objects++;
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
            nr.atomic_objects++;
            nr.adatoms++;
          }
                    
          /* Count nr of vacancies */
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {
            nr.vacancies++;
          }
        }
      }
    }
  }
}


/* Applying periodic boundary conditions in x, y, z directions and
 * change unit cell coorinate q accordingly for the dimension D:
 * D = 0 for x dimension
 * D = 1 for y dimension
 * D = 2 for z dimension
 * D = 3 for p dimension
 * 
 * If perodic boundary apply, the new unit cell coordinte is returned.
 * The lattice have unit cells numbered between 0 and q_max in any dimension.
 * 
 * If boundaries are not periodic in some direction, this will be handled by
 * another function. In this function pbc = 1 for all directions.
 */
int boundary_condition(int q, int D) {
  if (D < 0 || D > 3) printf("ERROR in boundary_condition(): D>2\n"); 
  
  int q_max	= 0;
  int Q		= 0;
  
  if (D < 3) {
  
    if (D == 0) {
      q_max 	= dimension.x;
    }
    else if (D == 1) {
      q_max 	= dimension.y;
    }
    else if (D == 2) {
      q_max 	= dimension.z;
    }
    
    if (q < 0) {
      q = q + q_max;
      Q = q;
    }
    else if (q >= 0 && q < q_max) Q = q;
    while (q >= q_max) {
      q = q - q_max;
      Q = q;
    }
  
  }
  else if (D == 3) {
    Q = q;		// The p coordinate (D = 3) is not Carthesian.
  }
  
  return Q;
  
}


/* Arrhenius' formula for temperature activated event probability
 * frequencies: 
 * nu: attempt frequency [s^-1] 
 * E: activation energy [eV]
 * kb: Boltzmann's contant [eV K^-1]
 * T: Atomic temperature, i.e. a temperature saved in the Object.
 */
double arrhenius_formula(double nu, double E, double T) {
  double kb = 8.6173324e-5; 	// [eV K^-1]
  double Gamma;			// [s^-1]
  
  Gamma = nu*exp(-E/(kb*T));
//   printf("Gamma %lf\n",Gamma);
  return Gamma;
}


void coordinate_check(Pvector q) {
  
  if (q.x < 0 || q.x >= dimension.x) {
    printf("ERROR x = %i outside boundary", q.x);
    exit(1);
  }
  if (q.y < 0 || q.y >= dimension.y) {
    printf("ERROR y = %i outside boundary", q.y);
    exit(1);
  }
  if (q.z < 0 || q.z >= dimension.z) {
    printf("ERROR z = %i outside boundary", q.z);
    exit(1);
  }
  if (q.p < 0 || q.p >= dimension.p) {
    printf("ERROR p = %i outside boundary", q.p);
    exit(1);
  }
  
}

/* Calculation of the distance in [m] between two Carthesian coordinates.
 * Does not take periodic boundary conditions into account.
 */
double distance(Vector v0, Vector v1) {
  double distance;
  
  distance = sqrt((v1.x - v0.x)*(v1.x - v0.x) + (v1.y - v0.y)*(v1.y - v0.y) + (v1.z - v0.z)*(v1.z - v0.z));
  
  return distance;
  
}

/* Calculation of the carthesian distance in [m] between two xyzp coordinates.
 * Does not take periodic boundary conditions into account.
 */
double qdistance(Pvector q0, Pvector q1) {
  double d;
  
  d = distance(system_lattice[q0.x][q0.y][q0.z][q0.p].position, system_lattice[q1.x][q1.y][q1.z][q1.p].position);
  
  return d;
}


/* Initialize a new adatom 
 * Probabilities are not updated nor initialized.
 */
void new_adatom(Pvector q) {
  Vector E, v;
  int omega;
  
  E.x = 0.0;
  E.y = 0.0;
  E.z = 0.0;
  
  system_lattice[q.x][q.y][q.z][q.p].nr_1nn           = 0;
  system_lattice[q.x][q.y][q.z][q.p].nr_2nn           = 0;
  system_lattice[q.x][q.y][q.z][q.p].kind             = adatom_kind;
  system_lattice[q.x][q.y][q.z][q.p].element          = lattice_element;	// Assuming the new adatom to be of lattice kind.
  system_lattice[q.x][q.y][q.z][q.p].label            = label_none;
  system_lattice[q.x][q.y][q.z][q.p].cluster_index    = 0;
  // index not changed
  system_lattice[q.x][q.y][q.z][q.p].deposition_index = nr.deposition_events;
  // probabilities are updated elsewhere
  // configuration_26d updated elsewhere
  // barrier updated elsewhere
  // field_barrier updated elsewehere
  // probabilities_nnn are updated elsewhere
  system_lattice[q.x][q.y][q.z][q.p].probability_deposition   = 0;
  system_lattice[q.x][q.y][q.z][q.p].probability_evaporation  = 0;
  system_lattice[q.x][q.y][q.z][q.p].global_xyzp      = q;
  system_lattice[q.x][q.y][q.z][q.p].initial_position = system_lattice[q.x][q.y][q.z][q.p].position;
  system_lattice[q.x][q.y][q.z][q.p].electric_field   = E;
  system_lattice[q.x][q.y][q.z][q.p].scalar_field     = 0.0;
  system_lattice[q.x][q.y][q.z][q.p].average_field    = 0.0;
  for (omega = 0; omega < max_1nn; omega++) {
    system_lattice[q.x][q.y][q.z][q.p].field_gradient[omega] = 0.0;
  }
  system_lattice[q.x][q.y][q.z][q.p].max_gradient     = 0.0;
  system_lattice[q.x][q.y][q.z][q.p].electric_charge  = 0.0;
  system_lattice[q.x][q.y][q.z][q.p].nr_jumps         = 0.0;
  system_lattice[q.x][q.y][q.z][q.p].Temperature      = Temperature;
  system_lattice[q.x][q.y][q.z][q.p].nr_jumps_with_constant_field = 0;
  
  
  tag.recalculate_field = 1;  // Recalculation of the field is needed.
  
  

/* Probabilities should not be updated here, as add_probabilities_locally() will do it and
 * calculate the difference against the previous sum_probabilities. If probabilites are changed here
 * and error will emerge in the probability calculations.
 *   system_lattice[q.x][q.y][q.z][q.p].probability_deposition	= 0.0;
 *   system_lattice[q.x][q.y][q.z][q.p].probability_evaporation	= 0.0;
 *   for (i = 0; i < max_1nn; i++){
 *     system_lattice[q.x][q.y][q.z][q.p].probability_nn_jump[omega]	= 0.0;
 *   }
 *   
 *   for (i = 0; i < 6; i++){
 *     system_lattice[q.x][q.y][q.z][q.p].probability_nnn_jump[i]	= 0.0;
 *   }
 */  
  
 /* The following variables are set at initialization and will not change during the simulation
  * system_lattice[q.x][q.y][q.z][q.p].position
  * system_lattice[q.x][q.y][q.z][q.p].index
  */
}


/* Initialize a new vacancy (or empty)
 * Probabilities are not updated nor initialized.
 */
void new_vacancy(Pvector q) {
  Object point;
  Vector E, v;
  int i;
  int omega;
  
  E.x = 0.0;
  E.y = 0.0;
  E.z = 0.0;
  
  v.x = 0.0;
  v.y = 0.0;
  v.z = 0.0;
  
  system_lattice[q.x][q.y][q.z][q.p].nr_1nn           = 0;
  system_lattice[q.x][q.y][q.z][q.p].nr_2nn           = 0;
  system_lattice[q.x][q.y][q.z][q.p].kind             = vacancy_kind;
  system_lattice[q.x][q.y][q.z][q.p].element          = vacancy_element;
  system_lattice[q.x][q.y][q.z][q.p].label            = label_none;
  system_lattice[q.x][q.y][q.z][q.p].cluster_index    = 0;
  // index not changed   
  // probabilities are updated elsewhere
  // configuration_26d updated elsewhere
  // barrier updated elsewhere
  // field_barrier updated elsewehere
  // probabilities_nnn are updated elsewhere
  // probabilities_3nn are updated elsewhere
  // probabilities_5nn are updated elsewhere
  // probability_deposition
  // probability_evaporation
  system_lattice[q.x][q.y][q.z][q.p].global_xyzp      = q; 
  // helmod_xyz not changed
  // position not changed
  system_lattice[q.x][q.y][q.z][q.p].initial_position = system_lattice[q.x][q.y][q.z][q.p].position;
  system_lattice[q.x][q.y][q.z][q.p].electric_field   = E;
  system_lattice[q.x][q.y][q.z][q.p].scalar_field     = 0.0;
  system_lattice[q.x][q.y][q.z][q.p].average_field    = 0.0;
  system_lattice[q.x][q.y][q.z][q.p].max_gradient     = 0.0;
  
  system_lattice[q.x][q.y][q.z][q.p].electric_charge  = 0.0;
  system_lattice[q.x][q.y][q.z][q.p].nr_jumps         = 0.0;
  system_lattice[q.x][q.y][q.z][q.p].Temperature      = Temperature;
  system_lattice[q.x][q.y][q.z][q.p].nr_jumps_with_constant_field = 0;
  

  tag.recalculate_field = 1;  // Recalculation of the field is needed.

/* Probabilities should not be updated here, as add_probabilities_locally() will do it and
 * calculate the difference against the previous sum_probabilities. If probabilites are changed here
 * an error will emerge in the probability calculations.
 *   system_lattice[q.x][q.y][q.z][q.p].probability_deposition	= 0.0;
 *   system_lattice[q.x][q.y][q.z][q.p].probability_evaporation	= 0.0;
 *   for (i = 0; i < max_1nn; i++){
 *     system_lattice[q.x][q.y][q.z][q.p].probability_nn_jump[i]	= 0.0;
 *   }
 *   
 *   for (i = 0; i < 6; i++){
 *     system_lattice[q.x][q.y][q.z][q.p].probability_nnn_jump[i]	= 0.0;
 *   }
 */
  
 /* The following variables are set at initialization and will not change during the simulation
  * system_lattice[q.x][q.y][q.z][q.p].position
  * system_lattice[q.x][q.y][q.z][q.p].index
  */
 
}


/* Find the 1nn coordinates q of position v, numbered 0 to omega. Boundary conditions are taken into account. */
Pvector omega_position_of_1nn(Pvector v, int omega) { 
  Pvector q;
  q.x = boundary_condition(v.x + vector_1nn[v.p][omega][0], 0); 
  q.y = boundary_condition(v.y + vector_1nn[v.p][omega][1], 1); 
  q.z = boundary_condition(v.z + vector_1nn[v.p][omega][2], 2);
  q.p = boundary_condition(v.p + vector_1nn[v.p][omega][3], 3);
  
  return q;
}


/* Find the 1nn coordinates q of position v, numbered 0 to omega. Boundary conditions are not taken into account. */
Pvector omega_position_of_1nn_with_no_PBC(Pvector v, int omega) { 
  Pvector q;
  
  q.x = v.x + vector_1nn[v.p][omega][0]; 
  q.y = v.y + vector_1nn[v.p][omega][1]; 
  q.z = v.z + vector_1nn[v.p][omega][2];
  q.p = v.p + vector_1nn[v.p][omega][3];
    
  return q;
}


/* Find the 2nn coordinates q of position v, numbered 0 to omega. Boundary conditions are taken into account. */
Pvector omega_position_of_2nn(Pvector v, int omega) { 
  Pvector q;
  q.x = boundary_condition(v.x + vector_2nn[v.p][omega][0], 0); 
  q.y = boundary_condition(v.y + vector_2nn[v.p][omega][1], 1); 
  q.z = boundary_condition(v.z + vector_2nn[v.p][omega][2], 2);
  q.p = boundary_condition(v.p + vector_2nn[v.p][omega][3], 3);
  
  return q;
}


/* Find the 2nn coordinates q of position v, numbered 0 to omega. Boundary conditions are not taken into account. */
Pvector omega_position_of_2nn_with_no_PBC(Pvector v, int omega) { 
  Pvector q;
  q.x = v.x + vector_2nn[v.p][omega][0]; 
  q.y = v.y + vector_2nn[v.p][omega][1]; 
  q.z = v.z + vector_2nn[v.p][omega][2];
  q.p = v.p + vector_2nn[v.p][omega][3];
  
  return q;
}  


/* Find the 3nn coordinates q of position v, numbered 0 to omega. Boundary conditions are taken into account. */
Pvector omega_position_of_3nn(Pvector v, int omega) { 
  Pvector q;
  q.x = boundary_condition(v.x + vector_3nn[v.p][omega][0], 0);
  q.y = boundary_condition(v.y + vector_3nn[v.p][omega][1], 1);
  q.z = boundary_condition(v.z + vector_3nn[v.p][omega][2], 2);
  q.p = boundary_condition(v.p + vector_3nn[v.p][omega][3], 3);
  
  return q;
}


/* Find the 3nn coordinates q of position v, numbered 0 to omega. Boundary conditions are not taken into account. */
Pvector omega_position_of_3nn_with_no_PBC(Pvector v, int omega) { 
  Pvector q;
  q.x = v.x + vector_3nn[v.p][omega][0];
  q.y = v.y + vector_3nn[v.p][omega][1];
  q.z = v.z + vector_3nn[v.p][omega][2];
  q.p = v.p + vector_3nn[v.p][omega][3];
  
  return q;
}


/* Find the 5nn coordinates q of position v, numbered 0 to omega. Boundary conditions are taken into account. */
Pvector omega_position_of_5nn(Pvector v, int omega) { 
  Pvector q;
  q.x = boundary_condition(v.x + vector_5nn[v.p][omega][0], 0);
  q.y = boundary_condition(v.y + vector_5nn[v.p][omega][1], 1);
  q.z = boundary_condition(v.z + vector_5nn[v.p][omega][2], 2);
  q.p = boundary_condition(v.p + vector_5nn[v.p][omega][3], 3);
  
  return q;
}


/* Find the 5nn coordinates q of position v, numbered 0 to omega. Boundary conditions are not taken into account. */
Pvector omega_position_of_5nn_with_no_PBC(Pvector v, int omega) { 
  Pvector q;
  q.x = v.x + vector_5nn[v.p][omega][0];
  q.y = v.y + vector_5nn[v.p][omega][1];
  q.z = v.z + vector_5nn[v.p][omega][2];
  q.p = v.p + vector_5nn[v.p][omega][3];
  
  return q;
}

/* 26d: Find the coordinate q of the s neighbour of the 1nn omega neighbour of v. */
Pvector omega_26d_s_configuration(Pvector v, int omega, int s) { 
  Pvector q;
  q.x = boundary_condition(v.x + matrix_26D_1nn[v.p][omega][s][0],0);
  q.y = boundary_condition(v.y + matrix_26D_1nn[v.p][omega][s][1],1);
  q.z = boundary_condition(v.z + matrix_26D_1nn[v.p][omega][s][2],2);
  q.p = boundary_condition(v.p + matrix_26D_1nn[v.p][omega][s][3],3);

  return q;
}

/* 26d: Find the coordinate q of the s neighbour of the 1nn omega neighbour of v. No periodic boundary condition */
Pvector omega_26d_s_configuration_with_no_PBC(Pvector v, int omega, int s) { 
  Pvector q;
  q.x = v.x + matrix_26D_1nn[v.p][omega][s][0];
  q.y = v.y + matrix_26D_1nn[v.p][omega][s][1];
  q.z = v.z + matrix_26D_1nn[v.p][omega][s][2];
  q.p = v.p + matrix_26D_1nn[v.p][omega][s][3];

  return q;
}

/* 72d: Find the coordinate q of the s neighbour of the 1nn omega neighbour of v. */
Pvector omega_72d_s_configuration(Pvector v, int omega, int s) { 
  Pvector q;
  q.x = boundary_condition(v.x + matrix_72D_1nn[v.p][omega][s][0],0);
  q.y = boundary_condition(v.y + matrix_72D_1nn[v.p][omega][s][1],1);
  q.z = boundary_condition(v.z + matrix_72D_1nn[v.p][omega][s][2],2);
  q.p = boundary_condition(v.p + matrix_72D_1nn[v.p][omega][s][3],3);

  return q;
}

/* 72d: Find the coordinate q of the s neighbour of the 1nn omega neighbour of v. No periodic boundary condition */
Pvector omega_72d_s_configuration_with_no_PBC(Pvector v, int omega, int s) { 
  Pvector q;
  q.x = v.x + matrix_72D_1nn[v.p][omega][s][0];
  q.y = v.y + matrix_72D_1nn[v.p][omega][s][1];
  q.z = v.z + matrix_72D_1nn[v.p][omega][s][2];
  q.p = v.p + matrix_72D_1nn[v.p][omega][s][3];

  return q;
}


/* Definition of the global variable sum_probabilities */
double definition_sum_probabilities() {  
  sum_probabilities = sum_internal_probabilities + sum_external_probabilities;
  return sum_probabilities;
}


/* Definition of the global variable sum_internal_probabilities */
double definition_sum_internal_probabilities() {
  sum_internal_probabilities = sum_nn_jump_probabilities + sum_nnn_jump_probabilities + sum_3nn_jump_probabilities + sum_5nn_jump_probabilities;
  return sum_internal_probabilities;
}



/* Check the neighbour matrices for 4d */
int check_4d_neighbours(int nn) {
  int omega;
  FILE *pxyz;
  int max_nn = 0;
  double jump_distance = 0;
  int i;
  double adimx,adimy,adimz;
  Vector v,v1;
  Pvector q,q1;
  float xtmp,ytmp,ztmp;
  double d;
  int nr_errors = 0;
  int nr_distance_errors = 0;
  int nr_atom_count_errors = 0;
  
  Pvector zero;
  int n,j,k;
  int check_nr_atoms = 0;

  zero.x = 0;
  zero.y = 0;
  zero.z = 0;
  zero.p = 0;
  
  switch (nn) {
    case 1:
      pxyz = fopen("./p_1nn.xyz","w");
      max_nn = max_1nn;
      jump_distance =jump_distance_nn;
      break;
    case 2:
      pxyz = fopen("./p_2nn.xyz","w");
      max_nn = max_2nn;
      break;
    case 3:
      pxyz = fopen("./p_3nn.xyz","w");
      max_nn = max_3nn;
      break;
    case 4:
      printf("ERROR 4nn not implemented.\n");
      pxyz = fopen("./p_4nn.xyz","w");
      max_nn = max_4nn;
      break;
    case 5:
      pxyz = fopen("./p_5nn.xyz","w");
      max_nn = max_5nn;
      break;
    default:
      printf("ERROR Only up to 5nn implemented.\n");
      break;
  }
  
  Pvector atoms[max_nn + 1];
  
  
  printf("Checking %inn neighbours\n", nn);
  
  adimx = dimension.x*a0*unit_cell_dimension.x/1.0e-10; // [Å]
  adimy = dimension.y*a0*unit_cell_dimension.y/1.0e-10; //
  adimz = dimension.z*a0*unit_cell_dimension.z/1.0e-10; //
  
  for (i = 0; i < dimension.p; i++) {
    
    printf("p = %i\n",i);
    
    fprintf(pxyz,"%i\n#\n",max_nn + 1);
    
    /* Print comment line in Ovito Extended xyz format */
    fprintf(file_xyz,"Lattice=\"%f %f %f %f %f %f %f %f %f\" ", adimx, 0.0, 0.0,  0.0, adimy, 0.0,  0.0, 0.0, adimz); // Simulation box base vectors
    fprintf(file_xyz,"Properties=species:S:1");   // Kind
    fprintf(file_xyz,":pos:R:3");     // Position
    fprintf(file_xyz,":omega:I:1");     // Label
    fprintf(file_xyz," "); 
    fprintf(file_xyz,"\n");
    
    
    /* Initial position */
    q.x = 10;
    q.y = 10;
    q.z = 10;
    q.p = i;
    n = 0; atoms[n] = q;
    
    v = xyzp_to_xyz(q,a0/1.0e-10); 
    fprintf(pxyz,"In %f %f %f  %i %i %i %i 1000\n",v.x,v.y,v.z, q.x,q.y,q.z,q.p);
    
    /* NN atoms */
    
    // NB! omega for 2nn (and others) is not the same as for 1nn. At the moment 2nn omegas are arbitrarily defined and not based on Euler rotations. TODO
    
    for (omega = 0; omega < max_nn; omega++) {
      switch (nn) {
        case 1:
          q1 = omega_position_of_1nn_with_no_PBC(q, omega);
          break;
        case 2:
          q1 = omega_position_of_2nn_with_no_PBC(q, omega);
          break;
        case 3:
          q1 = omega_position_of_3nn_with_no_PBC(q, omega);
          break;
        case 4:
             q1 = zero;
//           q1 = omega_position_of_4nn_with_no_PBC(q, omega);
          break;
        case 5:
          q1 = omega_position_of_5nn_with_no_PBC(q, omega);
          break;
        default:
          printf("ERROR Only up to 5nn implemented.\n");
        break;
      }
      
      v1 = xyzp_to_xyz(q1,a0/1.0e-10);
      fprintf(pxyz,"nn %f %f %f  %i %i %i %i  %i\n", v1.x,v1.y,v1.z, q1.x,q1.y,q1.z,q1.p, omega); 
      
      /* Check the distances between q and q1 */
      d = qdistance(q,q1);
      if (d >= jump_distance - 0.01*a0 || d <= jump_distance + 0.01*a0) {
//         printf("The %inn distances are correct.\n", nn);
      }
      else {
        printf("ERROR Distances are not correct. d = %le m != %le m\n", d, jump_distance);
        nr_errors++;
        nr_distance_errors++;
      }
      
      n++; atoms[n] = q1;
      
    }
    
    /* Checking that the considered atoms are unique and the correct number */
    j = k = check_nr_atoms = 0;
    for (j = 0; j < max_nn + 1; j++) {
      for (k = 0; k < max_nn + 1; k++) {
        if (j != k){
          if ( atoms[j].x == atoms[k].x 
            && atoms[j].y == atoms[k].y
            && atoms[j].z == atoms[k].z
            && atoms[j].p == atoms[k].p) {
//             printf("ERROR Atoms are identical\n");
            nr_errors++;
            nr_atom_count_errors++;
          }
          else {
            check_nr_atoms++;
          }
        }
      }
    }
    
    /* Initialization */
    for (j = 0; j < max_nn + 1; j++) {
      atoms[j] = zero;
    }
    
    check_nr_atoms = sqrt(check_nr_atoms + max_nn + 1);
    
    if (nr_distance_errors == 0) {
      printf("Distances       ok\n");
    }
    else {
      printf("Distances       ERROR\n");
    }
    
    if (check_nr_atoms == max_nn + 1) {
      printf("Nr neighbours   ok\n");
    }
    else {
      printf("Nr neighbours   ERROR\n");
    }
    
  }
  
  fclose(pxyz);

  printf("p_%inn.xyz printed.\n\n\n",nn);
  
  return nr_errors;
  
}  



/* Check matrix_26D_1nn or matrix_72D_1nn*/
int check_1nn_bonds() {
  int omega;
  FILE *pxyz;
  Vector P[12];
  int i,s;
  double adimx,adimy,adimz;
  Vector v,v1,vs;
  Pvector q,q1,qs;
  float xtmp,ytmp,ztmp;
  if (max_s == 26){
    pxyz = fopen("./p_26d.xyz","w");
  }else if(max_s == 72){
    pxyz = fopen("./p_72d.xyz","w");
  }
  Pvector atoms[max_s + 2];
  int n = -1;
  int j,k;
  double d,d1;
  int nr_errors = 0;
  int nr_distance_errors = 0;
  int nr_atom_count_errors = 0;
  int check_nr_atoms = 0;
  Pvector zero;
  
  zero.x = 0;
  zero.y = 0;
  zero.z = 0;
  zero.p = 0;
  
  for (j = 0; j < max_s + 2; j++) {
    atoms[j] = zero;
  }
   
  adimx = dimension.x*a0*unit_cell_dimension.x/1.0e-10; // [Å]
  adimy = dimension.y*a0*unit_cell_dimension.y/1.0e-10; //
  adimz = dimension.z*a0*unit_cell_dimension.z/1.0e-10; //
  
  for (i = 0; i < dimension.p; i++) {
    for (omega = 0; omega < max_1nn; omega++) {
      fprintf(pxyz,"%d\n",max_s+2);
      
      /* Print comment line in Ovito Extended xyz format */
      fprintf(pxyz,"Lattice=\"%f %f %f %f %f %f %f %f %f\" ", adimx, 0.0, 0.0,  0.0, adimy, 0.0,  0.0, 0.0, adimz); // Simulation box base vectors
      fprintf(pxyz,"Properties=species:S:1");   // Kind
      fprintf(pxyz,":pos:R:3");     // Position
      fprintf(pxyz,":omega:I:1");     // omega
      fprintf(pxyz,":s:I:1");     // s
      fprintf(pxyz,":q.x:I:1");
      fprintf(pxyz,":q.y:I:1");
      fprintf(pxyz,":q.z:I:1");
      fprintf(pxyz,":q.p:I:1");
      fprintf(pxyz," "); 
      fprintf(pxyz,"p=%i omega=%i ",i,omega); 
      fprintf(pxyz,"\n");

      
      
      /* Initial position */
      q.x = 10;
      q.y = 10;
      q.z = 10;
      q.p = i;
      n = 0; atoms[n] = q;
      
      v = xyzp_to_xyz(q,a0/1.0e-10); 
      fprintf(pxyz,"i %f %f %f -1 -1  %i %i %i %i\n",v.x,v.y,v.z,  q.x,q.y,q.z,q.p);
      
      /* Final position */
      q1 = omega_position_of_1nn_with_no_PBC(q,omega);
      n = 1; atoms[n] = q1;
      v1 = xyzp_to_xyz(q1,a0/1.0e-10);
      fprintf(pxyz,"f %f %f %f %i -2  %i %i %i %i\n", v1.x,v1.y,v1.z, omega,  q1.x,q1.y,q1.z,q1.p); 
      
      /* Check the distance */
      d = qdistance(q,q1);
      if (d >= jump_distance_nn - 0.01*a0 || d <= jump_distance_nn + 0.01*a0) {
//         printf("Jump distance ok\n");
      }
      else {
        printf("Jump distance ERROR\n");
        nr_distance_errors++; 
        nr_errors++;
      }
      
      /* 26d or 72d atoms */
      for (s = 0; s < max_s; s++) {
        if (max_s == 26){
          qs = omega_26d_s_configuration_with_no_PBC(q, omega, s);
        }else if(max_s == 72){
          qs = omega_72d_s_configuration_with_no_PBC(q, omega, s);
        }
        n++; atoms[n] = qs;
//         printf("qs %i %i %i %i\n", qs.x, qs.y, qs.z, qs.p);
        
        vs = xyzp_to_xyz(qs,a0/1.0e-10);
        fprintf(pxyz,"s %f %f %f %i %i  %i %i %i %i\n", vs.x,vs.y,vs.z, omega, s,  qs.x,qs.y,qs.z,qs.p); 
        
        
        /* Check distances */
        for (j = 0; j < max_s; j++) {
          d  = qdistance(q,qs);
          d1 = qdistance(q1,qs);
          
          if ( (d  >= jump_distance_nn  - 0.01*a0 || d  <= jump_distance_nn  + 0.01*a0)
            || (d  >= jump_distance_2nn - 0.01*a0 || d  <= jump_distance_2nn + 0.01*a0)
            || (d1 >= jump_distance_nn  - 0.01*a0 || d1 <= jump_distance_nn  + 0.01*a0)
            || (d1 >= jump_distance_2nn - 0.01*a0 || d1 <= jump_distance_2nn + 0.01*a0)) {
//             printf("Neighbour distance ok\n");
          }
          else {
            printf("Neighbour distance ERROR\n");
            nr_distance_errors++;
            nr_errors++;
          }
        }
      }
      
      
      /* Check the number of atoms */
      j = k = check_nr_atoms = 0;
      for (j = 0; j < max_s + 2; j++) {
        for (k = 0; k < max_s + 2; k++) {
          if (j != k){
            if ( atoms[j].x == atoms[k].x 
              && atoms[j].y == atoms[k].y
              && atoms[j].z == atoms[k].z
              && atoms[j].p == atoms[k].p) {
//                 printf("ERROR Atoms are identical\n");
              nr_errors++;
              nr_atom_count_errors++;
            }
            else {
              check_nr_atoms++;
            }
          }
        }
      }
      
  
      /* Initialization */
      for (j = 2; j < max_s + 2; j++) {
        atoms[j] = zero;
//         printf("%i %i %i %i\n", atoms[j].x, atoms[j].y, atoms[j].z, atoms[j].p);
      }
      
      
      if(nr_distance_errors == 0) {
        printf("p = %i, omega = %i, Distances     ok      ",i, omega);
      }
      else {
        printf("p = %i, omega = %i, Distances     ERROR   ",i, omega);
      }
      
      if(nr_atom_count_errors == 0) {
        printf("Nr atoms     ok      \n");
      }
      else {
        printf("Nr atoms     ERROR   \n");
      }
      
    }
  }
  
  fclose(pxyz);

  if (max_s == 26){
    printf("p_26.xyz printed.\n");  
  }else if (max_s == 72){
    printf("p_72.xyz printed.\n");  
  }
  return nr_errors;
  
}


void check_local_configuration() {
  Pvector i,f,q;
  int nn, nnn;
  
  // Atom of interest at initial site
  i.x = 10;
  i.y = 10;
  i.z = 10;
  i.p = 0;
  
  // Final site
  f = omega_position_of_1nn_with_no_PBC(i,0);
  
  for (nnn = 0; nnn < 6; nnn++) {
    q = omega_position_of_2nn_with_no_PBC(i, nnn);
    system_lattice[q.x][q.y][q.z][q.p].kind = atom_kind;
    system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;
  }
  
  for (nnn = 0; nnn < 6; nnn++) {
    q = omega_position_of_2nn_with_no_PBC(f, nnn);
    system_lattice[q.x][q.y][q.z][q.p].kind = atom_kind;
    system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;
  }
  
  // 1nn for i nn = 0 is the final site.
  for (nn = 0; nn < max_1nn; nn++) {
    q = omega_position_of_1nn_with_no_PBC(i,nn);
    system_lattice[q.x][q.y][q.z][q.p].kind = adatom_kind;
    system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;
  }
  
  // f 1nn
  for (nn = 0; nn < max_1nn; nn++) {
    q = omega_position_of_1nn_with_no_PBC(f,nn);
    system_lattice[q.x][q.y][q.z][q.p].kind = adatom_kind;
    system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;
  }
  

  // Mark initial and final site
  system_lattice[i.x][i.y][i.z][i.p].kind   = atom_kind;
  system_lattice[i.x][i.y][i.z][i.p].element  = lattice_element;
  system_lattice[f.x][f.y][f.z][f.p].kind   = vacancy_kind;
  system_lattice[f.x][f.y][f.z][f.p].element  = vacancy_element;
  
  print_frame_xyz(1);
  printf("Check finished. Ending.\n");
  exit(1);
}
  
  
void check_random_generator() {
  int i;
  for (i=0;i<100;i++) printf("%llu\n",random_int());
  for (i=0;i<100;i++) printf("%lf\n",random_double1());
  for (i=0;i<100;i++) printf("%lf\n",random_double2());
  for (i=0;i<100;i++) printf("%lf\n",random_double3());
  for (i=0;i<100;i++) printf("%i\n",random_n(10));
  
}


void check_neighbour_matrices() {
  int nr_errors = 0;
  int nn = 0;
  printf("Checking the neighbour matrices\n");
  
  if (tag.using_26D_parameters == 0 && tag.using_72D_parameters == 0) { 
    
    /* 4d parameters */
    printf("Checking 4d neighbours\n\n");
    for (nn = 1; nn <= 5; nn++) {
      nr_errors = nr_errors + check_4d_neighbours(nn);
    }
    
  }
  else if (tag.using_26D_parameters == 1 || tag.using_72D_parameters == 1) { 
    
    /* 26d parameters */
    printf("Checking 26d/72d neighbours\n\n");
    nr_errors = nr_errors + check_1nn_bonds();
  
  } 
  
  if (nr_errors == 0) {
    printf("The distances and number of neighbour atoms are correct, no errors found\n");
  }
  else {
    printf("ERROR Some distances or neighbour counts were found to be wrong\n");
  }
  
  printf("Check finished. Ending\n");
  exit(0);
}
