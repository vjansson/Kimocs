
/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */


/** Inverse omega matrices **/

extern int inverse_omega_fcc100[4][12];                 // The omega index giving the opposite jump direction for fcc (100)
extern int inverse_omega_fcc111[6][12];                 // The omega index giving the opposite jump direction for fcc (111)
extern int inverse_omega_fcc110a[6][12];                // The omega index giving the opposite jump direction for fcc (110-0)
extern int inverse_omega_fcc110b[2][12];                // The omega index giving the opposite jump direction for fcc (110-1)
extern int inverse_omega_bcc100[2][8];                  // The omega index giving the opposite jump direction for bcc (100)
extern int inverse_omega_bcc110[4][8];                  // The omega index giving the opposite jump direction for bcc (110)
extern int inverse_omega2_bcc110[4][6];                 // The omega index giving the opposite jump direction for 2nn jumps in the bcc(110) system
extern int inverse_omega3_bcc110[4][12];                // The omega index giving the opposite jump direction for 3nn jumps in the bcc(110) system
extern int inverse_omega5_bcc110[4][8];                 // The omega index giving the opposite jump direction for 5nn jumps in the bcc(110) system
extern int inverse_omega_bcc111[12][8];                 // The omega index giving the opposite jump direction for bcc (111)


/** Neighbour matrices **/

/* 4d neigbour vectors. Format: [p_init][omega][x,y,z,p] */
extern int vector_1nn_fcc100[4][12][4];
extern int vector_2nn_fcc100[4][6][4];

extern int vector_1nn_fcc110a[6][12][4];
extern int vector_2nn_fcc110a[6][6][4];

extern int vector_1nn_fcc110b[2][12][4];
extern int vector_2nn_fcc110b[2][6][4];

extern int vector_1nn_fcc111[6][12][4];
extern int vector_2nn_fcc111[6][6][4];

extern int vector_1nn_bcc100[2][8][4];
extern int vector_2nn_bcc100[2][6][4];
extern int vector_3nn_bcc100[2][12][4];

extern int vector_1nn_bcc110[4][8][4];
extern int vector_2nn_bcc110[4][6][4];
extern int vector_3nn_bcc110[4][12][4];
extern int vector_5nn_bcc110[4][8][4];
      
extern int vector_1nn_bcc111[12][8][4];
extern int vector_2nn_bcc111[12][6][4];
extern int vector_3nn_bcc111[12][12][4];
extern int vector_5nn_bcc111[12][8][4];
