\documentclass[a4paper,12pt,hidelinks]{article}
\usepackage{fancyhdr}
\usepackage{multirow}
\usepackage{times}

\usepackage[T1]{fontenc}
\usepackage[utf8x]{inputenc}
\usepackage[british]{babel}

% \usepackage{ae,aecompl}

\usepackage[margin=2cm]{geometry}

% For links
\usepackage{url}
\usepackage{hyperref}

% References
\usepackage[numbers]{natbib}                                
%\usepackage{hypernat}  %Makes citations like [1-3] as links

\usepackage{longtable}
\usepackage{booktabs}
\usepackage{rotating}
\usepackage{multirow}

\usepackage{wrapfig}

\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{graphicx}
\usepackage{color}

\parskip=12pt
\parindent=0pt


\begin{document}
\frenchspacing

\title{Kimocs manual}
\author{Ville Jansson, PhD\\ {\tt ville.b.c.jansson@gmail.com}}

\maketitle

\tableofcontents

\section{The Atomistic Kinetic Monte Carlo code Kimocs}

Kimocs \cite{jansson2016long} is an open source atomistic Kinetic Monte Carlo code that is freely available at \url{https://gitlab.com/vjansson/Kimocs}. 
We will in the following give a brief overview in how to use this code to simulate the long-term evolution of atomic-scale surface structures. 

Some example animations of Kimocs simulations can be found on Dr Jansson's homepage: \url{https://sites.google.com/site/villebcjansson/}.

\subsection{How to cite}

If you are publishing results using Kimocs, please cite the following paper, where the key features of the code are described:

{\em
V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
}


\subsection{Licence}

Kimocs is licensed under the termes of GPLv3:

Copyright (C) 2014 Ville Jansson, PhD, \url{ville.b.c.jansson@gmail.com}

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see \url{https://www.gnu.org/licenses/}.



\section{How to download and compile Kimocs}
 
 Kimocs is downloaded using the version control system {\bf Git}.

 \subsection{Stable version}
 
 The stable branch is meant for new users and is supposed to be well-tested and easy to install.
 
 On your local computer, run the command 
 
 '{\tt git clone --recursive https://gitlab.com/vjansson/Kimocs.git -b stable}'.
 
 This will download the 'stable' branch of Kimocs and create a new directory {\tt Kimocs}. When is asking for username and password for the libhelmod repository, interrupt it with the command '{\tt Ctrl-c}'. The LibHelmod module is not needed with the Stable branch.
 
 If you are on the Kale.grid.helsinki.fi cluster, you'll need to load the right compilation environment before compiling using the command '{\tt module load CMake/3.5.2-foss-2016b}'.
 
 In the {\tt Kimocs} directory, compile the code with the command '{\tt make stable}'. This should produce an executable {\tt kimocs}.
 
You can run a test case in the {\tt Kimocs/test} directory. Just cd it and run {\tt ../kimocs} and it should work. If you get segmentation fault, it probably means that you are using more RAM than your laptop has. You can make Kimocs use less RAM by changing the NCELLX, NCELLY, NCELLZ, and NCELLP parameters in the file global.h. 
For our systems (and fcc), good values should NCELLX = NCELLY = NCELLZ $\geq$ 40 and NCELLP = 12. These parameters set the maximum size in unit cells (compare the {\tt box\_dimensions} option in data.in. If you try to set a system that too large in data.in, it will complain and ask you to increase the NCELL parameters and recompile. 


\subsection{Master version --- for developers}

The master branch is the main development branch. This code should be stable, but not necessary yet fine-tuned for an end-user. From time to time, when the master branch is considered tested enough, it will be merged with the stable branch.

\begin{enumerate}
 \item Make an account on gitlab.com.

 \item Ask for access to LibHelmod from Dr Jansson, {\tt ville.b.c.jansson@gmail.com}

 \item To download Kimocs, run the command
\begin{verbatim}
 git clone --recursive https://gitlab.com/vjansson/Kimocs.git -b master
\end{verbatim}

\item If making a fresh installation (will download dependencies and compile them):
\begin{itemize}
      \item Ubuntu: {\tt make install}
   \begin{itemize}
      \item If you are using \textbf{Ubuntu 16}: In the file {\tt Kimocs/femocs/share/makefile.femocs}, remove {\verb -lstdc++ } from the lines with {\verb FEMOCS_LIB } and {\verb FEMOCS_DLIB }. For Ubuntu 14, ignore.
   \end{itemize}
      
      \item taito.csc.fi: {\tt make install\_taito}
      
      \item alcyone.grid.helsinki.fi: {\tt make install\_alcyone }
\end{itemize}

\item Every time (also recompiling):
\begin{itemize}
 \item Ubuntu 16: {\tt make}
 
 \item Ubuntu 14: {\tt make ubuntu14}
 
 \item taito.csc.fi: {\tt make taito}
 
 \item alcyone.grid.helsinki.fi: {\tt make alcyone}
\end{itemize}

\end{enumerate}

During this process, it will ask you for your usuername and password for the LibHelmod repository at gitlab.com.

You will need the "install" commands only when you are compiling the Femocs libraries for the first time!


\subsection{Other make options}

To clean, use the command:
{\tt make clean}

To also clean submodule builds, use
{\tt make clean-all}



\subsection{Git version control}
 
 Git is a open source version control system developed by Linus Torvalds. More detailed information on how to use Git can be found here: \url{https://git-scm.com/book/en/v2}. For us, the useful commands (to be run in a Git repository, such as e.g. the {\tt Kimocs} directory) are:
 \begin{itemize}
 \item {\tt git clone [address] } Clones a Git project from the source address
 \item {\tt git log } Gives the version history
 \item {\tt git status } Gives the status of the project (if any changes have happened since the last commit
 \item {\tt git diff} Shows the difference in the files since the last commit
 \item {\tt git pull [address or just 'origin'} Downloads the latest commits from the source address.
 \end{itemize}

A commit in Git looks like this:
{\tiny
\begin{verbatim}
commit 2e5569c18c957423f8d49b3362d2f07ad420958f
Author: Ville Jansson <ville.b.c.jansson@gmail.com>
Date:   Fri Aug 10 14:52:18 2018 +0300

    Added a deposition index to every atom.
    
    * Deposited atoms will be numbered
    * This can be visualized with Ovito in in the xyz files.

\end{verbatim}
}
% \begin{figure}[h!]
% \includegraphics[width=8cm]{git_commit.png}
% \end{figure}

Note the commit number, which is actually a cryptographic hash. When referring to the commit number, it is enough to only use a few of the starting characters, like {\tt 2e55}.


\section{To run Kimocs}
 
 In order the run Kimocs, two input files are needed: the configuration file {\tt data.in} and a parameter file, e.g. {\tt Cu.par}. If these are present in the directory, just run the executable, '{\tt ./kimocs}'.
 
\subsection{Parameterizations}
 
 The parameter file (definded by the {\tt parameter\_file} command in data.in, usually ending with ``{\tt .par}``, gives the migration barriers needed for all atom (first nearest neighbour) jump events. It has the structure:
 \begin{figure}[h!]
  \includegraphics[width=\textwidth]{par.png}
 \end{figure}
 
 {\tt \#version, \#a0, \#fcc} and {\tt \#attempt\_frequency} are key-words that specifies the parameter set version, the lattice parameter, the lattice structure (fcc), and the attempt frequency for every jump, respectively.

 \subsubsection{4d parameterizations}
 Currenty the follwing metals have been parameterized (in the 4d format):
 \begin{itemize}
  \item Cu \cite{baibuz2018data}
  \item Fe (nn and nnn) \cite{baibuz2018dataFe}
  \item Au \cite{vigonski2018au}
 \end{itemize}

 Three different types of parameterization formats can be used with Kimocs. The above listed are all of the 4d kind, which uses the number of first and second nearest neighbours of the initial and final position of the jump to define a jump and its migration energy. This parameterization is extensively described in \cite{baibuz2018migration}. 

 \subsubsection{26d}
 The other parameterization is using the precise positions of all 26 neighbours to define a jump and this parameterization is called 26d. This parameterization is also described in \cite{baibuz2018migration}. 
 
 
 \subsubsection{ANN}
 The 26d parameterization is also used for training artificial neural networks (ANNs), which is the third kind. Currently, only one ANN for Cu exist and is described here \cite{lahtinen2018artificial,lahtinen2018data}.
 
 
 
 \subsection{The data.in file}
 
 The {\tt data.in} file has a structure with a keyword followed by a number of values to be read in. Lines starting with \# are comments. As the format of the file changes as Kimocs is developed, the date of the latest format update is given on the first line of the file. The file has input options grouped as follows:
 %\begin{figure}[h!]
 % \includegraphics[width=\textwidth]{datain_1.png}
 %\end{figure}
 
 \subsubsection{General}
 
 {\tiny
 \begin{verbatim}
## General
seed                          123252532734 245762515456 3445363567 45678  # Four integers
Temperature                   1000.0                                      # [K]
time                          0.0e0                                       # [s];    Starting time
kmc_step                      0e0                                         # Starting KMC step
 \end{verbatim}
 }

 
 The important options here are the seed (four integers needed by Mersenne Twister) and Temperature (in kelvin).

 
 
 \subsubsection{Material-dependent parameters}
 
 {\tiny
 \begin{verbatim}
parameter_file                ../parameters/Cu.par      # 4d barrier set
 \end{verbatim}
 }

 Specify the parameter set file, normally ending with ''.par``. Some set files are gathered in the directory {\verb Kimocs/parameters }.
 
 \subsubsection{System dimensions and orientation}
%   \begin{figure}[h!]
%   \includegraphics[width=\textwidth]{datain_2.png}
%  \end{figure}
 {\tiny
 \begin{verbatim}
## System dimensions and orientation
surface                       100 0                     # 100, 110 or 111; gives the z lattice orientation. Second option is 0 or 1 (only with fcc110).
box_dimensions                32 32 32                  # [unit cell]; three integers.
# box_dimensions_m            1e-8 1e-8 1e-8            # [m]
 \end{verbatim}
 }
  
 This section defines the system we want to simulate. Whether the lattice should be fcc or bcc, should be already specified in the parameter file. The possible surfaces are (at the moment)
 \begin{itemize}
  \item fcc
  \begin{itemize}
   \item {\tt 100}
   \item {\tt 110 0 }
   \item {\tt110 1 } (turned 45\textdegree around the z-axis)
   \item {\tt 111 }
  \end{itemize}
  \item bcc
  \begin{itemize}
   \item {\tt 100 }
   \item {\tt 110 }
   \item {\tt 111 }
  \end{itemize}

 \end{itemize} 

The {\tt box\_dimensions} gives the system in terms of unit cells. The unit cell depends on what surface is used and does  not need to be cubic (but always cuboid). It might be easier to ue the {\tt box\_dimensions\_m} command instead that defines the system dimensions in metres(!) instead.


\subsubsection{Substrate and boundaries}

{\tiny
\begin{verbatim}
## Substrate and boundaries
# add_xyz                     objects.xyz At 3.6356     # file, element, a0; a0 = 1 standard.
# add_xyzp                    objects.xyzp              # Read in atom/atoms from a xyzp file.
make_bulk                     0 9                       # [z]; create bulk layers between z-coordinates (z=0 lowest layer)
# make_bulk_m                 0.0 2e-9                  # [m]
pbc                           1 1 0                     # 1: periodic boundaries, 0: off. Three values for x y z. 
\end{verbatim}
}

So far, the system does not have any atoms. These are either read in from xyz or xyzp files ({\tt add\_xyz} and {\tt add\_xyzp}, respectively) or introduced by option commands in data.in, such as {\tt make\_bulk}, which creates a substrate (infinite flat surface) of desired thickness. 

Reading in xyz files may be tricky as the lattice need to be perfect (not relaxed) and have origo in the lower left corner of the system. Some trial and errors may be need. xyzp is the internal coordinate system of Kimocs. Files with atoms in these coordinates are printed during simulations and may be used to restart a simulations for a intermediate KMC step.

{\tt PBC} defines the periodic boundary conditions in the x y and z direction. If it is set to 0 in the z direction, the lowest layer of the atoms will be fixed. These fixed atoms should always be covered with many layers of atoms so that they will not come in contact with the surface where the normal jumps happens. This, since they are artificially fixed and normally not part of the system that we really want to simulate. If we want to simulate a cluster, no bulk is needed and we should have periodic boundaries also in z direction. The same if The whole system is bulk (and only vacancies moving).

\subsubsection{Make structures on the surface}

% \begin{figure}[h!]
%   \includegraphics[width=\textwidth]{datain_3.png}
%  \end{figure}

{\tiny
\begin{verbatim}
## Make structures on the surface
make_cube                     5 5 5                     # [unit cell]
# make_cylinder               2e-9 4e-9 0               # [m], [m], i; radius, and height of a cylinder, 0 = vertical, 1 = horizontal orientation.
# make_sphere                 4e-9 2e-9                 # [m]; diameter of sphere or hemisphere and height above surface
# make_ridge                  4e-9                      # [m]; Height of a ridge along the x axis with 45 degree slopes.
# make_cubic_void             5 5 5                     # [unit cell] Make cuboid void in the centre of the system.
# add_adatoms                 100                       # Nr of adatoms to add randomly on top of bulk.
# add_adatom                  10 7 6 1  0               # [xyzp, label] Add single adatom at xyzp coordinate. Label == 1 for diffusing adatom.
# add_vacancies               1000                      # Add random vacancies into the bulk. 
\end{verbatim}
}

 
In this section, we can add structures, such as cuboid, cylindrical tips or spheres or ridges, on the substrate. If no bulk is present, these structures will placed in the centre of the system. If a tip is tall enough, it becomes a nanowire, which with periodic boundary conditions becomes infinitely long. We can also add voids into the bulk. {\tt add\_adatoms} will randomly add atoms on the surface; {\tt add\_adatom} will place an atom at a specific xyzp coordinate. 

These command can be repeated as many times one likes. They will overwrite previous atom structures if they overlap.

\subsubsection{External events}
% \begin{figure}[h!]
%   \includegraphics[width=\textwidth]{datain_4.png}
%  \end{figure}
{\tiny
\begin{verbatim}
## External events
evaporation_rate              0.0e0                     # [adatoms/s/m^2]; Rate of adatoms randomly removed.
deposition_rate               0.0e0                     # [adatoms/s/m^2]; Rate of adatoms randomly added.
cluster_evaporation           0                         # 1: Clusters detached from the bulk will be removed.
mark_free_clusters_as_bulk    0                         # 1: Initial free clusters will not be removed by the cluster_evaporation function.
\end{verbatim}
}
 
 Evaporation means that atoms are randomly removed from the surface with a certain average rate. Deposition means that they are added.
 
 Cluster evaporation, if having value 1, will remove all atoms not connected to the substrate. {\bf NB If there is no substrate, it will remove all atoms from the system!} The idea of the function is to deal with atoms whose neighbour have disappeared for some reason. Without any neighbours, no jump is defined in Kimocs, so the atom is at this point an artefact.

 \subsubsection{xyz parameters, output files, end conditions and standard output}
%  \begin{figure}[h!]
%   \includegraphics[width=\textwidth]{datain_5.png}
% \end{figure}
{\tiny
\begin{verbatim}
## xyz parameters
xyz_empty_objects             0                         # 1: Print empty objects into objects.xyz (makes the file larger)

## Output files
print_xyz_files               1                         # 1: Print objects.xyz and end.xyz files.
print_xyzp_files              1                         # 1: Print objects_*.xyzp files for restarts.
print_parcas_files            0 Cu Cr                   # 1, [atom type]: Print md.in, mdlat.in.xyz_initial, and mdlat.in.xyz_end for Parcas.

## End conditions and standard output
print_cycle                   1e3                       # [steps] The number of steps between output to std. Cluster analysis, 
                                                          system integrity check (end condition check)
print_cycle_s                 1e-10                     # [s] Time between output to files (and std). If <= 0, print-out every print_cycle. 
max_kmc_step                  1e5                       # Maximum number of steps
max_simulation_time           1e-8                      # [s]; maximum simulated time
# min_height                    0.0                     # [unit cell]; stop simulation if heighest atom has z <= min_height. Need bulk.
# min_height_m                  2e-9                    # [m]; Need bulk.
# max_height                    100.0                   # [unit cell]; stop simulation if heighest atom has z >= max_height. Need bulk.
# max_height_m                  20e-9                   # [m]; Need bulk.
# max_nr_clusters               100                     # [nr clusters]; stop if more than maxium nr of clusters (incl. bulk) form
# max_nr_evaporated             10                      # Max nr of evaporated atoms
# max_CPU_time                  1209600                 # [s]; Maximum CPU time (30d = 2591000, 14d = 1209000, 7d = 604400, 3d = 259000). 
                                                          Ending time might takes some additional 400s if field is used.
\end{verbatim}
}

 The xyz parameters defines what should be printed in the objects.xyz file, which is a set of xyz frames that can be read by e.g. Ovito \url{https://ovito.org/} as a movie of the simulation. If {\tt xyz\_empty\_objects} has value 1, besides all atoms, all empty lattice point will be visualized in objects.xyz, which makes it very large, but the option might be useful when simulating voids. The objects.xyz, initial.xyz and end.xyz and .xyzp files are printed if specified in the 'Output files' section. It is also possible to produce xyz files in a format that is readable by the PARCAS Molecular Dynamics code. 
  
 The end conditions defines when the simulation should stop. In this section, it is also defined how often the output is printed (on screen or into files. The {\tt print\_cycle} defines after how many steps some information about the system should be printed on-screen (standard output). 

 The printed information looks like follows:
 
 Format:
 {\tt \#0:Step 1:Time[s] 2:AOBj 3:at 4:ad 5:rm\_ad 6:rm\_V 7:height[uc] \\	
 8:height[m] 9:dep 10:dep[1/s] 11:ev 12:ev[1/s] \\
 13:max\_Efield[V/m] 14:max\_charge[e] 15:err[pc] 16:Frame \#d}
 
 Output:
 {\tt 3.000000e+00  1.550983e+03 29740 23431 3429 4 0 10 \\
 4.45e-09 5 3.22e-03 0 0.00e+00 2.12e+10 -0.13 9.12e-18 32 \#d}

 \begin{itemize}
 \item KMC step and time are self-explanatory
 \item 'AOBj' nr of atomic objects, i.e. atoms + adatoms + fixed atoms in the system
 \item 'At', atoms, are defined as (physical) atoms with no nearest neighbour vacancy
 \item 'Ad', adatoms, are defined as (physical) atoms with at least one nearest neighbour vacancy (e.g. at the surface)
 \item rm\_ad and rm\_V, counts cumulatively how many (ad)atoms and vacancies have been removed.
 \item height gives the z coordinate in unit cells and in metres of the highest (ad)atom in the system.
 \item 'dep' and 'ev' give the actual number of atoms deposited and evaporated and their rates. Since these events are random, the actual value given here may vary from the one specified in data.in.
 \item max\_Efield and max\_charge gives the the maxim electric field and charge if field is included in the simulation (not in the Stable branch)
 \item 'err' gives the error of the summation of all probabilities. If it gets above 1e-14, there might be a bug in the code.
 \item 'frame' tells which frame was printed last in objects.xyz
\end{itemize}
 
 
 {\tt print\_cycle\_s} defines in seconds how often
 \begin{itemize}
  \item A new frame in objects.xyz should be printed (slow for large systems)
  \item The cluster analysis should be done (heavy; needed for e.g. the cluster\_evaporation option above)
  \item Check the summation of probabilities (zeros the err mentioned above). 
  \item Prints the objects\_*.xyzp files, which can be used for restarts. If they are more than 10, the oldest will be deleted.
 \end{itemize}

If {\tt print\_cycle\_s} is set negative, the actions above will happens every {\tt print\_cycle}

{\tt max\_kmc\_step} and {\tt max\_simulation\_time} define the maximum number of steps and time, respectively.

The other end conditions consider the maximum or minimum z coordinate of the system or the number of clusters. 

{\tt max\_CPU\_time} might be useful on when running Kimocs on a computer cluster which have a time limit. If Kimocs is interrupted prematurely, the end analysis will not be printed. This command may prevent this.


\subsubsection{Special calculations}

{\tiny
\begin{verbatim}
## Specific calculations
calculate_diffusion           0  0 0 0 0                # [Nr of KMC runs; x y z p] Will ad an adatom at xyzp 
                                                          (0 0 0 0 = random) and calculate its 3D diffusion and velocity for every KMC run.
max_diffusion_displacement    73e-10                    # [m] Maximum displacement of the diffusing atom in diffusion calculations.
\end{verbatim}
}

{\tt calculate\_diffusion} will add a atom and calculate the diffusion constant. The simulations is repeated as many times as specified.




 \section{The output files}
 
 The main output files of kimocs are
 \begin{itemize}
  \item objects.xyz: Can be read as a movie in Ovito.
  \item initial.xyz: The initial frame; can be read in Ovito. 
  \item end.xyz: A snapshot frame of the simulation, printed every hour (CPU time) or at the end of the simulation. Good for monitoring long simulations.
  \item objects\_01.xyzp and objects\_end.xyzp: snapshots of the system in internal coordinates. Can be used to restart the simulation.
  \item clusters.out: Statistics about free clusters in the system
  \item diffusion\_coefficient.out: Printed only for diffusion calculations
 \end{itemize}

\subsection{Object types}

In the xyz files, the following object types may be printed out:
\begin{itemize}
 \item \textbf{Adatom (Ad):} An (physical) atom with with at least one vacancy nearest neighbour. The adatoms will constitute the surfaces in the system.
 \item \textbf{Atom (At):} An (physical) atom with only atoms (or adatoms or fixed atoms) as nearest neighbours (no vacancies).
 \item \textbf{Fixed atom (Fx):} Immobile atoms, used at the system boundaries with non-periodic conditions.
 \item \textbf{Vacancy (V):} A lattice point with at least one adatom as first-nearest neighbour.
 \item \textbf{Empty (Em):} A lattice point with all first-nearest neighbours either empty, vacancies, or sinks.
 \item \textbf{Sink (S):} An empty lattice point which will remove all atoms or adatoms that end the turn as first nearest neighbours; used to account for the free vacuum boundary when non-periodic boundaries are used.
\end{itemize}

 

\section{Features}

\subsection{Automatic removal of atoms}

An atom found in the end of a KMC step to have zero first and second nearest neighbours will be removed and counted as evaporated. 
This is done in the function {\tt check\_for\_evaporation()}. 
The reason for this is that isolated atoms can not move in Kimocs and the situation is unphysical. 
A good parameterization should normally not produce systems with isolated atoms in the first place. 

If a group of two or more atoms are isolated, they will only be removed if the cluster evaporation feature is activated in data.in ({\tt cluster\_evaporation 1}). 
This is only checked for every {\tt print\_cycle}, as the cluster analysis is quite CPU-demanding.



 
\section{Using the computer cluster Kale}

 Kale is a computer cluster only available within University of Helsinki, but other clusters work in a similar way.

 For many long simulations, it is convenient to use a computer cluster. We will only need to run serial code, since KMC codes are very hard to parallelize.
 
 To log in to Kale with {\tt ssh [username]@kale.grid.helsinki.fi}
 
 In your home directory, download Kimocs using Git and compile it, like described before. On Kale, you'll need to run the command {\tt module load CMake GCC/5.3.0-2.26} before you compile Kimocs. This command installs CMake and the correct compiler.
 
 Upload from your local machine the directory (let's call it 'dir') with the data.in and parameter files (and possible scripts) to Kale using scp: {\tt scp -r dir kale.grid.helsinki.fi:} Notice the ':', you can specify a directory after the colon, but leaving it empty means your home directory. 'dir' is now your submission directory.

 
 You still need to include into dir the submission script:
{\tiny
\begin{verbatim}
#!/bin/bash -l
#SBATCH -J MCP
#SBATCH -o data.out
#SBATCH -e std.err
#SBATCH -n 1
#SBATCH -t 0-06:00:0
#SBATCH --mem-per-cpu=2000
#SBATCH --mail-type=END
#SBATCH --mail-user=youremail@helsinki.fi
#SBATCH -p short

# commands to manage the batch script
#   submission command
#     sbatch [script-file]
#   status command
#     squeue -u username
#   termination command
#     scancel [jobid]

# For more information
#   http://docs.physics.helsinki.fi/kale.html

# Commands
sd=`pwd`

cd $sd 
cp $HOME/Kimocs/kimocs .
srun kimocs
\end{verbatim}
} 

This script will submit a job named 'MCP' on one node. It will stop at the latest after after 6 h and not use more than 2 GB RAM. When it stops, it will send an email to the defined email address.

To submit, give the command {\tt sbatch submit\_kale.sh}, where the later is the name of your script. You can now check if your jobs are running by the command {\tt squeue -u [username]} They might be in the queue, which you can check the length of with just {\tt squeue}. You can cancel your job with {\tt scancel [jobid]} or all your jobs with {\tt scancel -u [username]}.

There are many queues on Kale. You can see them all with the command {\tt sinfo}. The command also shows the different time limitations for the different queues. Different nodes are associated with different queues. If you just specify how much memory (RAM) and time you need, the system will decide which queue, but you can also specify with the {\tt \#SBATCH -p} command in the submission script.


\section{Developer guide}


Anybody is welcome to contribute to the development of Kimocs, but in order to keep the code
at high quality and avoid forking, developers are asked to follow the following guidelines:

\begin{enumerate}
\item The master branch is the main branch and should only contain well-written tested code. Too keep it this way,
new code submitted or merged into this branch should be reviewed by Ville Jansson <ville.b.c.jansson@gmail.com>.
If you want to merge a branch with master, make sure that there are no conflicts by making the merge on you local mashine
and then upload it to Gitlab as a new temporary branch and make a merge request to the master branch.

\item The stable branch is intended for new users of Kimocs. The intention is that this branch should be easy to install
and run and test out by any user. The stable branch should thus be even more tested than the master branch. The stable
branch should otherwise be just a snapshot of the master branch when the master branch is considered to be stable enough
to show to new users.

\item Developers are allowed to make as many temporary branches on Gitlab as they want (as long as there is space). When
they want to merge their work with the master branch, they may just make a merge request in Gitlab.

\item Please try to keep a coding style consistent with the exisiting code in Kimocs. Make sure that variables are not
confused with global variables.

\item Use SI units in all calculations, inputs and outputs and don't use prefixes. If values are printed in the scientific
notation (e.g.  4.0e-9 m), the order of magnitude is easy to see anyway. Electronvolt (eV) is however used for energies.
xyz files use Ångström per convention.
\end{enumerate}


\bibliographystyle{model1a-num-names}
%\bibliographystyle{plainnat}
%\bibliographystyle{ieeetr}
\bibliography{pub/vjansson.bib,pub/vjansson_publications.bib}

\end{document}
