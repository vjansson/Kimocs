const float classifier_sum_tol;

/* Creates the reflection of the local atomic environment desc
   and saves it to mir1. The migration direction is preserved.
   Plane of reflection is parallel to a {100} surface.*/
void mirror1 (int *mir1, int desc[]);

/* Creates the reflection of the local atomic environment desc
   and saves it to mir2. The migration direction is preserved.
   Plane of reflection is perpendicular to a {100} surface.*/
void mirror2 (int *mir2, int desc[]);

/* Creates the reflection of the local atomic environment desc
   and saves it to mir3. The migration direction is REVERSED.
   Plane of reflection is perpendicular to the migration direction.*/
void mirror3 (int *mir3, int desc[]);

/* Compares descriptions desc1 and desc2 element by element
   and returns the smaller one (first smaller element). If the
   descriptions are exactly the same, returns the (pointer to the)
   first description*/
int *mindesc(int desc1[], int desc2[]);

/* Reads in the the ANN for the correct element from file */
void read_in_ann_file(int element, char * file_name);

/* Returns the barrier for jump s of the correct element with ANN */
float get_ann_barrier(int element, int * s);

/* Reads in the the ANN regressor for the correct element and class from file.
   Returns the input dimensionality of the network. */
int read_in_ann_regressor(int element, int ann_class, int committee_member, char * file_name);

/* Reads in the the ANN classifier for the correct element from file.
   Returns the input dimensionality of the network. */
int read_in_ann_classifier(int element, char * file_name);

/* Allocates the arrays needed for finding the minimum
   description in get_ann_barrier.*/
void allocate_desc_arrays();

/* Initializes the regressor network array */
void initiate_regressor(int element, int classes, int committee_size);
