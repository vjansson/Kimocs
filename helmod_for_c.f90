!
! Created 2015 by Ekaterina Baibuz
!
!

module helmod_for_c
USE , intrinsic::iso_c_binding
use helmod, only : charge_and_field_from_atoms, helError

implicit none

! The subroutine links c and fortran codes, getting variables values from C, 
! sending them to fortran and returning the pointers to the arrays back to C.

contains
subroutine helmod_wrapper_for_c(applied_field, x0, ncells, grid_spacing, xq, atype, iabove, ptr, full_solve, aroundapx, &
  debug, evpatype, skiplimcheck, myatoms, error) bind(C,name="helmod_wrapper_for_c")
  implicit none
  integer, intent(in) :: myatoms
  double precision, intent(in) :: applied_field !< Current applied electric field (V/Å)
  double precision, intent(in) :: x0(myatoms*3) !< Atom coordinates (parcas units)
  integer, intent(in) :: ncells(3) !< Nox size in unit cells
  double precision, intent(in) :: grid_spacing(3) !< Grid spacing
  double precision, intent(out) :: xq(myatoms*4) !< Atom charges (e)
  integer, intent(inout) :: atype(myatoms) !< Atom types
  integer, intent(in) :: iabove !< How high above the surface to calculate the electric field (gridpoints)
  type(c_ptr), intent(out) :: ptr
  logical, intent(in) :: full_solve !< Whether to solve full system, or smaller system given by aroundapx
  integer, intent(in) :: aroundapx !< Size of smaller system when not solving the full system (gridpoints)
  logical, intent(in) :: debug !< Whether to print debug information
  integer, intent(in) :: evpatype !< Type of evaporated atoms
  logical, intent(in) :: skiplimcheck !< Whether to skip checking if the boundary conditions have changed
  integer, intent(out) :: error
  double precision, dimension(:,:,:,:),allocatable,target , save :: Efield
  integer, dimension(1) :: shape_e_one_dim

  integer :: i, j, k, l, index
  integer :: isize(3)
  
  error = helError
  

  call charge_and_field_from_atoms(applied_field, x0, ncells, grid_spacing, xq, atype, iabove, Efield, full_solve, &
    aroundapx, debug, evpatype, skiplimcheck)
  ptr = c_loc(Efield(1,1,1,1))
  
  
end subroutine
end module
