# Makefile for installing Kimocs on an Ubuntu system

#
#   Kimocs - Kinetic Monte Carlo for Surfaces
# 
#   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
#   Cite as:
# 
#      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
#      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
# 

include build/makefile.defs
include femocs/share/makefile.femocs
LDFLAGSFEMOCS = $(patsubst -I%, -Ifemocs/%, $(FEMOCS_HEADPATH) $(FEMOCS_LIB))  
LIFLAGSFEMOCS = $(patsubst -L%, -Lfemocs/%, $(FEMOCS_LIBPATH)) 

CCMODFLAGS= -DFIELD=1

all: $(EXE1)

mt19937-64.o:		mt19937-64.c mt64.h
global.o:		global.c global.h kimocs.h neighbour_matrices.h s_coordinate_matrices.h
probabilities.o:	probabilities.c global.h probabilities.h ann.h
helmod_interface.o:	helmod_for_c.o helmod_interface.c global.h kimocs.h helmod_interface.h
efield.o:		efield.c global.h probabilities.h kimocs.h helmod_interface.h efield.h
kimocs.o:		kimocs.c mt64.h global.h helmod_interface.h efield.h kimocs.h ann.h
neighbour_matrices.o:	neighbour_matrices.c	
ann.o:			ann.c ann.h global.h ./fann/install/lib/libfann.a

# LibHelmod
helmod_for_c.o: 	helmod.o helmod_for_c.f90
helmod.o:		ppm.o emission.o evaporation.o multigrid.o jouleheat.o libhelmod/helmod.f90
evaporation.o:		random.o libhelmod/evaporation.f90
random.o:		libhelmod/random.f90
emission.o:		libhelmod/emission.f90
jouleheat.o:		libhelmod/jouleheat.f90
multigrid.o:		libhelmod/multigrid.f90
ppm.o:			libhelmod/ppm.f90
qforces.o:		libhelmod/qforces.f90


.PHONY: $(EXE1)

$(EXE1): $(OBJS) $(OBJSFANN) $(OBJSLIBHELMOD)
	$(CC) $(CCFLAGS) $(CCMODFLAGS) $(CCFLAGSFEMOCS) $(LIFLAGSFEMOCS) $(OBJS) $(OBJSFANN) $(OBJSLIBHELMOD) -o $@ $(LIBSFANN) $(LIBS) $(LDFLAGSFEMOCS)

%.o: libhelmod/%.f90
	$(FC) $(FCFLAGS) -c $< -o $@

helmod_for_c.o: helmod_for_c.f90
	$(FC) $(FCFLAGS) -c $< -o $@	

%.o: %.c
	$(CC) $(CCFLAGS) $(CCMODFLAGS) $(CCFLAGSFEMOCS) -c $< -o $@ $(LIBSFANN) $(LIBS)

./fann/install/lib/libfann.a:
	cd fann && mkdir build install
	cd fann/build && cmake -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=c++ -DCMAKE_INSTALL_PREFIX=../install ..
	cd fann/build && make && make install

clean:
	rm -f $(EXE1) a.out *.o *.mod
	rm -rf fann/build

clean-all:
	rm -f $(EXE1) a.out *.o *.mod
	rm -rf fann/build fann/install

.ALWAYSEXEC:
