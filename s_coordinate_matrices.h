
/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */



/* 72D neighbour matrices. Format: matrix_72D[p_init][omega][72D_parameter_index == s][xyzp] */
extern int matrix_72D_fcc100[4][12][72][4];

extern int matrix_72D_fcc110a[6][12][72][4];
extern int matrix_72D_fcc110b[2][12][72][4];

extern int matrix_72D_fcc111[6][12][72][4];

/** Mirror matrices for (up to) 72D descriptors**/

extern int matrix_fcc_mirror1[72][2];

extern int matrix_fcc_mirror2[72][2];

extern int matrix_fcc_mirror3[72][2];

