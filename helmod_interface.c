/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */

#ifdef FIELD

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>	/* For timing functions */

#include "global.h"
#include "kimocs.h"
#include "helmod_interface.h"
#include "efield.h"

  
/** Global variables for integration with Helmod **/

Vector libhelmod_unit_cell_dimension;
Vector p0_libhelmod,p1_libhelmod,p2_libhelmod,p3_libhelmod,p4_libhelmod,p5_libhelmod;	// #D coordinates in the unit cell of the p atoms for LibHelmod
Vector p6_libhelmod,p7_libhelmod,p8_libhelmod,p9_libhelmod,p10_libhelmod,p11_libhelmod;

/* Input for LibHelmod */
int LH_iabove	= 0;			// [uc] How high above surface E is calculated
double LH_elfield	= 0; 		// Applied electric field
int LH_myatoms	= 0;
int LH_error	= 0;			// 1: stopping simulation due to error in LibHelmod.
int LH_ncells[3];
int LH_ncellp[3];
double LH_pbc[3];
double * LH_x0; 			// Coordinates of atoms in Parcas coordinates.
int * LH_atype;				// Array of types of atoms listed in x0 [type_of_atom1, type_of_atom2,...]

/* LibHelmod output */
double * LH_xq; 			// Charges of atoms [charge1, x1 side,y1 side ,z1 side,...]

int LH_isize[3];  			// ncells*ncellp
int LH_evpatype;			// Atom type for evaporated atoms
int LH_nfullsolve;			// How often to evaluate E for full system
int LH_full_solve;
int LH_aroundapx;			// Solve E around apex (probably highest point)
int LH_debug = 0;
int LH_skiplimcheck = 1;		// Tells LibHelmod that it is called by a KMC code

double *LH_E;


/* Applying periodic boundary conditions in the x and y dimensions for the Ivector h 
 * in the LibHelmod ijk field coordinate system. 
 * 
 */
Ivector ijk_pbc(Ivector h) {
  int x_max = 0;
  int y_max = 0;
  Ivector h_pbc;
  
  h_pbc.x  = 0;
  h_pbc.y  = 0;
  
  if ((crystal == fcc && (surface_orientation == 100 || surface_orientation == 110)) 
    || crystal == bcc && surface_orientation == 100) {
    x_max   = dimension.x*2;
    y_max   = dimension.y*2;
  }
  else {
    printf("ERROR ijk_pbc() Field only implemented for fcc100, fcc110 and bcc100 with LibHelmod\n");
    exit(1);
  }
  
  /* x direction */  
  while (h.x < 0) {
    h.x = h.x + x_max;
    h_pbc.x = h.x;
  }

  while (h.x >= x_max) {
    h.x = h.x - x_max;
    h_pbc.x = h.x;
  }
  
  if (h.x >= 0 && h.x < x_max) h_pbc.x = h.x;
  else {
    printf("ERROR ijk_pbc() in the x dimension\n");
    exit(1);
  }
  
  
  /* y direction */  
  while (h.y < 0) {
    h.y = h.y + y_max;
    h_pbc.y = h.y;
  }

  while (h.y >= y_max) {
    h.y = h.y - y_max;
    h_pbc.y = h.y;
  }
  
  if (h.y >= 0 && h.y < x_max) h_pbc.y = h.y;
  else {
    printf("ERROR ijk_pbc() in the y dimension\n");
    exit(1);
  }
  
  /* z direction */
  h_pbc.z = h.z; // Periodic boundary conditions not applied in the z direction.
  
  return h_pbc;
  
}


/* Calculates the distance in [m] between two field points, h0 and h1, 
 * in LibHelmod's ijk coordinate system 
 * h0 and h1 are assumed to have been corrected for periodic boundary conditions.
 * Periodic bounaries are considered so that the shortest distance is returned.
 */
double ijk_distance(Ivector h0, Ivector h1) {
  Vector v0,v1;
  double d;
  Ivector ijk_dimension, H1;
  int hdist, hdist_tmp;
  
  if (crystal == bcc && surface_orientation == 100) {
  
    ijk_dimension.x = 2*dimension.x; // Dimensions of the ijk space
    ijk_dimension.y = 2*dimension.y;
    ijk_dimension.z = 2*dimension.z;
    
    H1 = h1;
    
    /* Finding shortest distance in x dimension in ijk space; keeping h0 fixed */
    hdist = abs(H1.x - h0.x);
    
    hdist_tmp = abs((h1.x + ijk_dimension.x) - h0.x);
    if (hdist_tmp < hdist) {
      hdist = hdist_tmp;
      H1.x = h1.x + ijk_dimension.x;
    }
    
    hdist_tmp = abs((h1.x - ijk_dimension.x) - h0.x);
    if (hdist_tmp < hdist) {
      hdist = hdist_tmp;
      H1.x = h1.x - ijk_dimension.x;
    }
    
    /* y dimension */
    hdist = abs(H1.y - h0.y);
    
    hdist_tmp = abs((h1.y + ijk_dimension.y) - h0.y);
    if (hdist_tmp < hdist) {
      hdist = hdist_tmp;
      H1.y = h1.y + ijk_dimension.y;
    }
    
    hdist_tmp = abs((h1.y - ijk_dimension.y) - h0.y);
    if (hdist_tmp < hdist) {
      hdist = hdist_tmp;
      H1.y = h1.y - ijk_dimension.y;
    }
    
    /* z dimension not pbc */
    H1.z = h1.z;
    
    
    /* Converting to Carthesian space */  
    v0 = libhelmod_field_ijk_to_xyz(h0); // [Å]
    v1 = libhelmod_field_ijk_to_xyz(H1); // [Å]
    
    d = distance(v0, v1)*1e-10; // [m]
  
  }
  else {
    printf("ERROR ijk_distance() Only tested for bcc(100)\n");
    exit(1);
  }
  
  return d;
}


/* Initialize system variables needed for LibHelmod */
void initialize_libhelmod() {
  int i;
  
  if (crystal == fcc && surface_orientation == 110 && lattice_option == 1) {
    /* LibHelmod only works well if the system is perfectly cubic, why the fcc100(1) unit cell 
     * needs to be truncated in the x and z direction before sent to LibHelmod.
     */
    libhelmod_unit_cell_dimension.x = 1.0;
    libhelmod_unit_cell_dimension.y = 1.0;
    libhelmod_unit_cell_dimension.z = 1.0;
    
    p0_libhelmod.x = 0.0;
    p0_libhelmod.y = 0.0;
    p0_libhelmod.z = 0.0;
    
    p1_libhelmod.x = 0.5;	// Coordinates for truncated fcc110(1) unit cells
    p1_libhelmod.y = 0.5;	//
    p1_libhelmod.z = 0.5;	//
    
    /* Not used */
    p2_libhelmod = p3_libhelmod = p4_libhelmod = p5_libhelmod  = p6_libhelmod 	= p0_libhelmod;	// 0.0
    p7_libhelmod = p8_libhelmod = p9_libhelmod = p10_libhelmod = p11_libhelmod	= p0_libhelmod;	// 0.0
    
  }
  else {
    libhelmod_unit_cell_dimension.x = unit_cell_dimension.x;
    libhelmod_unit_cell_dimension.y = unit_cell_dimension.y;
    libhelmod_unit_cell_dimension.z = unit_cell_dimension.z;
    
    p0_libhelmod = p0;
    p1_libhelmod = p1;
    p2_libhelmod = p2;
    p3_libhelmod = p3;
    p4_libhelmod = p4;
    p5_libhelmod = p5;
    p6_libhelmod = p6;
    p7_libhelmod = p7;
    p8_libhelmod = p8;
    p9_libhelmod = p9;
    p10_libhelmod = p10;
    p11_libhelmod = p11;
    
  }
  
  LH_evpatype	= 2;							// Atom type for evaporated atoms
  LH_debug 	= 0;
  LH_ncells[0] 	= dimension.x;
  LH_ncells[1] 	= dimension.y;
  LH_ncells[2] 	= dimension.z;
  LH_ncellp[0] 	= 2;
  LH_ncellp[1] 	= 2;
  LH_ncellp[2] 	= 2;
  LH_pbc[0] 	= (double) pbc.x;
  LH_pbc[1] 	= (double) pbc.y;
  LH_pbc[2] 	= (double) pbc.z;
  
  for (i = 0; i < 3; ++i) {
      LH_isize[i]= LH_ncells[i]*LH_ncellp[i];
  }
  LH_iabove =  LH_isize[2];
}


/* Calculate cartheisian libhelmod atom coordinates (Parcas coordinates)
 * These coordinates are normalized between -0,5 and 0.5 in all directions.
 */
Vector xyzp_to_libhelmod_coordinates(Pvector q) {
  Vector v;
  Vector box_dimension;
  
  if (tag.use_field == 1) {
    switch (crystal) {
      case 1:
    /* fcc */
    switch(surface_orientation) {
      case 100:
        /* Give every point in libhelmod atom coordinates (Parcas coordinates) */
        box_dimension.x = dimension.x*libhelmod_unit_cell_dimension.x;
        box_dimension.y = dimension.y*libhelmod_unit_cell_dimension.y;
        box_dimension.z = dimension.z*libhelmod_unit_cell_dimension.z;
        v = xyzp_to_truncated_xyz(q);	
        v.x = v.x/box_dimension.x - 1.0/2.0 + 1.0/(4.0*box_dimension.x);	// Ekaterina's definitions [dimensionless Parcas coordinates, between -0.5 and 0.5]
        v.y = v.y/box_dimension.y - 1.0/2.0 + 1.0/(4.0*box_dimension.y);
        v.z = v.z/box_dimension.z - 1.0/2.0 + 1.0/(4.0*box_dimension.z);
        break;
      case 110:
        /* Give every point in libhelmod atom coordinates (Parcas coordinates) */
        box_dimension.x = dimension.x*libhelmod_unit_cell_dimension.x;	// Box width in uc
        box_dimension.y = dimension.y*libhelmod_unit_cell_dimension.y;
        box_dimension.z = dimension.z*libhelmod_unit_cell_dimension.z;
        v = xyzp_to_truncated_xyz(q);	// Lattice parameter = 1.0 (arbitrary)
        v.x = (v.x + 0.25)/box_dimension.x - 1.0/2.0;	// [dimensionless LibHelmod (Parcas) coordinates, between -0.5 and 0.5]
        v.y = (v.y + 0.25)/box_dimension.y - 1.0/2.0;
        v.z = (v.z + 0.25)/box_dimension.z - 1.0/2.0;
        break;
      default:
        printf("ERROR Only implemented for fcc(100) and fcc(110)\n");
        exit(1);
        break;
	  
    } 
    break;
	
    case 2:
      /* bcc */
      if (surface_orientation == 100) {
        /* Give every point in libhelmod coordinates (Parcas coordinates) */
        box_dimension.x = dimension.x*libhelmod_unit_cell_dimension.x;	// Box width in uc
        box_dimension.y = dimension.y*libhelmod_unit_cell_dimension.y;
        box_dimension.z = dimension.z*libhelmod_unit_cell_dimension.z;
        v = xyzp_to_truncated_xyz(q);	// Lattice parameter = 1.0 (arbitrary)
        v.x = v.x/box_dimension.x - 1.0/2.0 + 1.0/(4.0*box_dimension.x);	// Ekaterina's definitions [dimensionless Parcas coordinates, between -0.5 and 0.5]
        v.y = v.y/box_dimension.y - 1.0/2.0 + 1.0/(4.0*box_dimension.y);
        v.z = v.z/box_dimension.z - 1.0/2.0 + 1.0/(4.0*box_dimension.z);
      }
      else {
        printf("ERROR Only implemented for bcc(100)\n");
        exit(1);
      }
      break;
	
      default:
      printf("ERROR Only implemented for fcc\n");
      exit(1);
    }   
  }
  else {
    v = xyzp_to_xyz(q,1.0);
  }
  
  return v;
}


/* Function for converting xyzp coordinates q to truncated (i.e. cubic) xyz coordinates  
 * Important for the unit cell of fcc110(1), that needs to be cubic before passed to LibHelmod
 * 
 * Resulting xyz coordinates have origo in the same place, but the unit cells have been scaled
 * to be cubic with the sides of lattice_parameter.
 */
Vector xyzp_to_truncated_xyz(Pvector q) {
  Vector lattice_parameter;
  Vector v;
  
  lattice_parameter.x = 1.0;	// Arbitrary units
  lattice_parameter.y = 1.0;
  lattice_parameter.z = 1.0;
  
  switch(q.p) {
    case 0:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p0_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p0_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p0_libhelmod.z)*lattice_parameter.z;
      break;
    case 1:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p1_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p1_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p1_libhelmod.z)*lattice_parameter.z;
      break;
    case 2:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p2_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p2_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p2_libhelmod.z)*lattice_parameter.z;
      break;
    case 3:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p3_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p3_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p3_libhelmod.z)*lattice_parameter.z;
      break;
    case 4:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p4_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p4_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p4_libhelmod.z)*lattice_parameter.z;
      break;
    case 5:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p5_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p5_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p5_libhelmod.z)*lattice_parameter.z;
      break;
    case 6:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p6_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p6_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p6_libhelmod.z)*lattice_parameter.z;
      break;
    case 7:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p7_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p7_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p7_libhelmod.z)*lattice_parameter.z;
      break;
    case 8:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p8_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p8_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p8_libhelmod.z)*lattice_parameter.z;
      break;
    case 9:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p9_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p9_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p9_libhelmod.z)*lattice_parameter.z;
      break;
    case 10:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p10_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p10_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p10_libhelmod.z)*lattice_parameter.z;
      break;
    case 11:
      v.x = (q.x*libhelmod_unit_cell_dimension.x + p11_libhelmod.x)*lattice_parameter.x;
      v.y = (q.y*libhelmod_unit_cell_dimension.y + p11_libhelmod.y)*lattice_parameter.y;
      v.z = (q.z*libhelmod_unit_cell_dimension.z + p11_libhelmod.z)*lattice_parameter.z;
      break;
  }
  
  return v;
}


/* Convert LibHelmod field coordinates h = (i,j,k) to xyz [Å] coordinates */
Vector libhelmod_field_ijk_to_xyz(Ivector h) {
  Vector v;
  Vector libhelmod_field_lattice_parameter; // [Å]
  
  libhelmod_field_lattice_parameter.x = unit_cell_dimension.x*a0/1.0e-10/2.0; // [Å]
  libhelmod_field_lattice_parameter.y = unit_cell_dimension.y*a0/1.0e-10/2.0;
  libhelmod_field_lattice_parameter.z = unit_cell_dimension.z*a0/1.0e-10/2.0;
  
  v.x = h.x*libhelmod_field_lattice_parameter.x;
  v.y = h.y*libhelmod_field_lattice_parameter.y;
  v.z = h.z*libhelmod_field_lattice_parameter.z;
  
  return v;
  
}

/* Convert cartheisian xyz [Å] coordinates to LibHelmod field coordinates h = (i,j,k) */
Ivector xyz_to_libhelmod_field_ijk(Vector v) {
  Ivector h;
  Vector libhelmod_field_lattice_parameter; // [Å]

  libhelmod_field_lattice_parameter.x = unit_cell_dimension.x*a0/1.0e-10/2.0; // [Å]
  libhelmod_field_lattice_parameter.y = unit_cell_dimension.y*a0/1.0e-10/2.0;
  libhelmod_field_lattice_parameter.z = unit_cell_dimension.z*a0/1.0e-10/2.0; 
  
  h.x = floor(v.x/libhelmod_field_lattice_parameter.x);
  if (h.x - floor(v.x/libhelmod_field_lattice_parameter.x) >= 0.5) h.x++; // Rounding to nearest ijk coordinate.
  
  h.y = floor(v.y/libhelmod_field_lattice_parameter.y);
  if (h.y - floor(v.y/libhelmod_field_lattice_parameter.y) >= 0.5) h.y++;
  
  h.z = floor(v.z/libhelmod_field_lattice_parameter.z);
  if (h.z - floor(v.z/libhelmod_field_lattice_parameter.z) >= 0.5) h.z++;
  
  return h;
}


/* Check if the position h in the libhelmod field lattice is a fcc or bcc position.
 * Returns 1 if yes and 0 otherwise.
 * Needed as the libhelmod lattice can not be mapped one-to-one with the 
 * Kimocs lattice.
 */
int check_for_kimocs_lattice_position_in_libhelmod_lattice(Ivector h) {
  Pvector q;
  int kimocs_lattice = 0;
  
  /* Transform LibHelmod h coordinates to Kimocs coordinates q. */
  
  /* LibHelmod field coordinates i,j,k are defined as
   * 0 <= i < dimension.x*2
   * 0 <= j < dimension.y*2
   * 0 <= k < dimension.z*2
   * with origo in the same position as in the Kimocs xyzp coordinates.
   */
 
  q.x = floor(h.x/2.0);	// Kimocs coordinates are not spatial, but only tells which unit cell the atom belong to; p atoms per unit cell.
  q.y = floor(h.y/2.0);
  q.z = floor(h.z/2.0);
  
  if (crystal == fcc && surface_orientation == 100) {
    if (	(h.x - 2*q.x == 0) && (h.y - 2*q.y == 0) && (h.z - 2*q.z == 0)){
      q.p = 0;
      kimocs_lattice = 1;
    }
    else if ( 	(h.x - 2*q.x == 1) && (h.y - 2*q.y == 1) && (h.z - 2*q.z == 0)){
      q.p = 1;
      kimocs_lattice = 1;
    }
    else if ( 	(h.x - 2*q.x == 0) && (h.y - 2*q.y == 1) && (h.z - 2*q.z == 1)){
      q.p = 2;
      kimocs_lattice = 1;
    }
    else if ( 	(h.x - 2*q.x == 1) && (h.y - 2*q.y == 0) && (h.z - 2*q.z == 1)){
      q.p = 3;
      kimocs_lattice = 1;
    }
  }
  else if (crystal == fcc && surface_orientation == 110 && lattice_option == 1) {
    if ( 	(h.x - 2*q.x == 0) && (h.y - 2*q.y == 0) && (h.z - 2*q.z == 0)){
      q.p = 0;
      kimocs_lattice = 1;
    }
    else if ( 	(h.x - 2*q.x == 1) && (h.y - 2*q.y == 1) && (h.z - 2*q.z == 1)){
      q.p = 1;
      kimocs_lattice = 1;
    }
  }
  else if (crystal == bcc && surface_orientation == 100 && lattice_option == 0) {
    if (        (h.x - 2*q.x == 0) && (h.y - 2*q.y == 0) && (h.z - 2*q.z == 0)){
      q.p = 0;
      kimocs_lattice = 1;
    }
    else if (   (h.x - 2*q.x == 1) && (h.y - 2*q.y == 1) && (h.z - 2*q.z == 1)){
      q.p = 1;
      kimocs_lattice = 1;
    }
  }
  else {
    printf("ERROR Only fcc100, fcc110(1), and bcc100 implemented with electric field.\n");
    exit(1);
  }
  
  
  return kimocs_lattice;	// 1: Kimocs lattice position; 0: Not.
  
}


/* Convert LibHelmod field coordinates h = (i,j,k) to Kimocs coordinates q. 
 * NOTE: h -> q is not a one-to-one mapping as the h lattice has more points 
 * than the q lattice.
 */
Pvector libhelmod_field_ijk_to_xyzp(Ivector h) {
  Pvector q;
  
  /* Transform LibHelmod h coordinates to Kimocs coordinates q. */
  
  /* LibHelmod field coordinates i,j,k are defined as
   * 0 <= i < dimension.x*2
   * 0 <= j < dimension.y*2
   * 0 <= k < dimension.z*2
   * with origo in the same position as in the Kimocs xyzp coordinates.
   */
  

  
  q.x = floor(h.x/2.0);	// Kimocs coordinates are not spatial, but only tells which unit cell the atom belong to; p atoms per unit cell.
  q.y = floor(h.y/2.0);
  q.z = floor(h.z/2.0);
  

  
  if (crystal == fcc && surface_orientation == 100) {
    if (	(h.x - 2*q.x == 0) && (h.y - 2*q.y == 0) && (h.z - 2*q.z == 0)){
      q.p = 0;
    }
    else if ( 	(h.x - 2*q.x == 1) && (h.y - 2*q.y == 1) && (h.z - 2*q.z == 0)){
      q.p = 1;
    }
    else if ( 	(h.x - 2*q.x == 0) && (h.y - 2*q.y == 1) && (h.z - 2*q.z == 1)){
      q.p = 2;
    }
    else if ( 	(h.x - 2*q.x == 1) && (h.y - 2*q.y == 0) && (h.z - 2*q.z == 1)){
      q.p = 3;
    }
    else {
      printf("ERROR libhelmod_field_ijk_to_xyzp() (%i %i %i) -> (%i %i %i ?)\n", h.x, h.y, h.z, q.x, q.y, q.z);
      printf("ERROR h-2q = [%i, %i, %i]\n", (h.x - 2*q.x), (h.y - 2*q.y), (h.z - 2*q.z));
      exit(1);
    }
  }
  else if (crystal == fcc && surface_orientation == 110 && lattice_option == 1) {
    if ( 	(h.x - 2*q.x == 0) && (h.y - 2*q.y == 0) && (h.z - 2*q.z == 0)){
      q.p = 0;
    }
    else if ( 	(h.x - 2*q.x == 1) && (h.y - 2*q.y == 1) && (h.z - 2*q.z == 1)){
      q.p = 1;
    }
    else {
      printf("ERROR libhelmod_field_ijk_to_xyzp() (%i %i %i) -> (%i %i %i ?)\n", h.x, h.y, h.z, q.x, q.y, q.z); 
      printf("ERROR h-2q = [%i, %i, %i]\n", (h.x - 2*q.x), (h.y - 2*q.y), (h.z - 2*q.z));
      exit(1);
    }
  }
  else if (crystal == bcc && surface_orientation == 100 && lattice_option == 0) {
    if (        (h.x - 2*q.x == 0) && (h.y - 2*q.y == 0) && (h.z - 2*q.z == 0)){
      q.p = 0;
    }
    else if (   (h.x - 2*q.x == 1) && (h.y - 2*q.y == 1) && (h.z - 2*q.z == 1)){
      q.p = 1;
    }
    else {
      printf("ERROR libhelmod_field_ijk_to_xyzp() (%i %i %i) -> (%i %i %i ?)\n", h.x, h.y, h.z, q.x, q.y, q.z);
      printf("ERROR h-2q = [%i, %i, %i]\n", (h.x - 2*q.x), (h.y - 2*q.y), (h.z - 2*q.z));
      exit(1);
    }
  }
  else {
    printf("ERROR Only fcc100, fcc110(1), and bcc100 implemented with electric field.\n");
    exit(1);
  }
  
  
  return q;
  
}


/* Convert Kimocs coordinates q to LibHelmod field coordinates i,j,k */
Ivector xyzp_to_libhelmod_field_ijk(Pvector q) {
  Ivector h;
  Vector v;
  
  
 /* LibHelmod (field) coordinates h = (i,j,k)
  * The unit cells in LibHelmod are not cubic for fcc(110), 
  * i.e. the LibHelmod lattice constant is different is different in 
  * the x, y, and z direction.
  */

 /* Cuboid unit cells 
  * Compare definition if grid_spacing = { libhelmod_unit_cell_dimension.x*a0/1e-10/2.0 ... } 
  * in the Helmod subroutine above.
  */
    
  if (crystal == fcc && surface_orientation == 100) {
    switch(q.p) {
      case 0:
	h.x = q.x*2 + p0_libhelmod.x*2;
	h.y = q.y*2 + p0_libhelmod.y*2;
	h.z = q.z*2 + p0_libhelmod.z*2;
	break;
      case 1:
	h.x = q.x*2 + p1_libhelmod.x*2;
	h.y = q.y*2 + p1_libhelmod.y*2;
	h.z = q.z*2 + p1_libhelmod.z*2;
	break;
      case 2:
	h.x = q.x*2 + p2_libhelmod.x*2;
	h.y = q.y*2 + p2_libhelmod.y*2;
	h.z = q.z*2 + p2_libhelmod.z*2;
	break;
      case 3:
	h.x = q.x*2 + p3_libhelmod.x*2;
	h.y = q.y*2 + p3_libhelmod.y*2;
	h.z = q.z*2 + p3_libhelmod.z*2;
	break;
    }
  }
  else if (crystal == fcc && surface_orientation == 110 && lattice_option == 1) {
    switch(q.p) {
      case 0:
	h.x = q.x*2 + p0_libhelmod.x*2;
	h.y = q.y*2 + p0_libhelmod.y*2;
	h.z = q.z*2 + p0_libhelmod.z*2;
	break;
      case 1:
	h.x = q.x*2 + p1_libhelmod.x*2;
	h.y = q.y*2 + p1_libhelmod.y*2;
	h.z = q.z*2 + p1_libhelmod.z*2;
	break;
    }
  }
  else if (crystal == bcc && surface_orientation == 100) {
    switch(q.p) {
      case 0:
	h.x = q.x*2 + p0_libhelmod.x*2;
	h.y = q.y*2 + p0_libhelmod.y*2;
	h.z = q.z*2 + p0_libhelmod.z*2;
	break;
      case 1:
	h.x = q.x*2 + p1_libhelmod.x*2;
	h.y = q.y*2 + p1_libhelmod.y*2;
	h.z = q.z*2 + p1_libhelmod.z*2;
	break;
    }  
  }
  else {
    printf("ERROR xyzp_to_libhelmod_field_ijk() Lattice not supported by LibHelmod\n");
    exit(1);
  }
  
  return h;
}


/* Read in atoms from Kimocs to LibHelmod */
void kimocs_lattice_to_x0() {
  int x,y,z,p;
  int atomN = 0;
  for (x = 0; x < dimension.x; x++) {
    for (y = 0; y < dimension.y; y++) {
      for (z = 0; z < dimension.z; z++) {
        for (p = 0; p < dimension.p; p++) {
          if ((system_lattice[x][y][z][p].kind == atom_kind) ||
            (system_lattice[x][y][z][p].kind == fixed_atom_kind) ||
            (system_lattice[x][y][z][p].kind == adatom_kind)) {
	  
//          system_lattice_xyz[atomN] = system_lattice[q.x][q.y][q.z][q.p].helmod_xyz;	// [LibHelmod units]
            LH_x0[atomN*3]	= system_lattice[x][y][z][p].helmod_xyz.x; 		// [LibHelmod units]		
            LH_x0[atomN*3+1]	= system_lattice[x][y][z][p].helmod_xyz.y; 		// [LibHelmod units]
            LH_x0[atomN*3+2]	= system_lattice[x][y][z][p].helmod_xyz.z; 		// [LibHelmod units]
	    
            atomN++;
            if (atomN > LH_myatoms) {
              printf("ERROR something wrong in kimocs_lattice_to_x0()");
              exit(1);
              
            }  
          }
          
          /* Reset variable for the field solver optimization (Field will only be 
           * calculated only after any atom have jumped a certain number of steps).
           */
          system_lattice[x][y][z][p].nr_jumps_with_constant_field = 0;  
          
        }
      }
    }
  }
}





/* Update in-variables, such as atom positions, needed before calling LibHelmod */
void update_libhelmod_variables() {
  int x,y,z,p;
  int i;
      
  LH_elfield = external_field/1e10; 	// [V/ang]	Applied external electic field in V/ang 

  LH_aroundapx = aroundapx_input;
  if (electric_field_calculation_step == 1 || electric_field_calculation_step == 0) {
    LH_full_solve = 1; //      1: Solve the full system every step
    n_full_field_calc++; //for the statistics
  }
  else {
    LH_full_solve = 0;		
  }
  
  count_number_atoms();
  LH_myatoms = nr.atomic_objects;				// Number of atomic objects in the system. Might change every kmc step.

  
  /* Memoray allocations. Done every kmc step as the number of atoms may change */
  LH_x0 = malloc(sizeof(double) * 3*LH_myatoms);			// List of atom coordinates in Helmod: [x1,y1,z1, x2,y2,z3,...]. Might change every kmc step.
  memset(LH_x0, 0.0, sizeof(double) * 3*LH_myatoms);
        
  LH_xq = malloc(sizeof(double) * 4*LH_myatoms);
  memset(LH_xq, 0.0, sizeof(double) * 4*LH_myatoms);
  
  LH_atype = malloc(sizeof(int) * LH_myatoms);
  memset(LH_atype, 1, sizeof(int) * LH_myatoms);
  
  if (!LH_x0 || !LH_xq || !LH_atype) { 			/* If data == 0 after the call to malloc, allocation failed for some reason */
      perror("Error allocating memory");
      abort();
  }
  
  kimocs_lattice_to_x0();
    
}

/* Call LibHelmod and give the atom positions. 
 * Get field and charges
 * Translate the LibHelmod data to Kimocs formats
 */ 
void helmod() {
  helmod_time0 = clock();
  int i,j,k,l;
  double grid_spacing[3] = { libhelmod_unit_cell_dimension.x*a0/1e-10/2.0, libhelmod_unit_cell_dimension.y*a0/1e-10/2.0, libhelmod_unit_cell_dimension.z*a0/1e-10/2.0 }; // SP: Divide lattice constant by 2 to get grid spacing. BUG: not correct for rectangular cells!
  int index_xq = 0;
  int x,y,z,p;
  int iterator = 0;
  int ih,jh,kh,ik,jk,kk,pk;
  Pvector q;
  int N;
  Vector F,v,w, Fsum;
  Vector Fsurf;
  double Fsurf_scalar;
  Vector F_removed;         // [V/m]
  double F_removed_scalar;  // [V/m]
  Ivector h;
  Ivector hnn;
  Ivector h_removed;
  Ivector H_removed; // No pbc
  Ivector h_displacement;
  Vector v_displacement;    // [Å]
  double field;
  double Fsum_scalar;
  double charge_difference;
  double scalar_field_tmp;
  Vector Fmin;
  
  cumulative_charge = 0;
  
  update_libhelmod_variables();

  /* Call Helmod */
  helmod_wrapper_time0 = clock();

  static int step = 0; // SP
  //int full_solve = 1; // (step % nfullsolve == 0); // SP: TODO: add other logic for when to do full solve and when not
  step += 1; // SP

  helmod_wrapper_for_c(&LH_elfield, LH_x0, LH_ncells, grid_spacing, LH_xq, LH_atype, &LH_iabove, &LH_E, &LH_full_solve, &LH_aroundapx, &LH_debug, &LH_evpatype, &LH_skiplimcheck, &LH_myatoms, &LH_error);
  
  if (LH_error != 0) {
    /* The field could not be calculated and the simulation should end */
    printf("LH ERROR: Field could not be calculated. Ending the simulation\n");
    tag.end_condition = 1;
  }
  
  helmod_wrapper_time1 = clock();
  helmod_wrapper_time += (double) (helmod_wrapper_time1 - helmod_wrapper_time0) / CLOCKS_PER_SEC;

  
  /*** Analyze output from Helmod and convert the data for Kimocs ***/
    
  if (LH_error == 0) {
    // Only do the analysis if the field was properly calculated.
    
    /** Get the charge from Helmod **/
    index_xq = 0;
    for (x = 0; x < dimension.x; x++) {
      for (y = 0; y < dimension.y; y++) {
        for (z = 0; z < dimension.z; z++) {
          for (p = 0; p < dimension.p; p++) {
            if ((system_lattice[x][y][z][p].kind == atom_kind) ||
              (system_lattice[x][y][z][p].kind == fixed_atom_kind) ||
              (system_lattice[x][y][z][p].kind == adatom_kind)) {
        
              system_lattice[x][y][z][p].electric_charge = -field_sign*LH_xq[index_xq]; // The charge will be opposit to the field sign. LibHelmod gives negative charges by default.
              //            system_lattice[x][y][z][p].charge_x = xq[index_xq +1];	// [e], Not needed, right?
              //            system_lattice[x][y][z][p].charge_y = xq[index_xq +2];
              //            system_lattice[x][y][z][p].charge_z = xq[index_xq +3];

              /* Add charge of the atom to the cumulutive charge array */      
              index_xq +=4;
              cumulative_charge += system_lattice[x][y][z][p].electric_charge;
              }
            } 
          }                       
        }
      }
      
      /** Check the cumulative charge difference needed for the subsystem field calculation optimization **/
      //printf("# electric_field_calculation_step %i \n", electric_field_calculation_step);
      if (electric_field_calculation_step > 1) {
        charge_difference = fabs(cumulative_charge_prev - cumulative_charge);
        if (charge_difference > charge_threshold) {
          electric_field_calculation_step = 0;

          printf("%e cumulative charge difference %le\n",kmc_step,charge_difference);
        }
      }
      else {
        cumulative_charge_prev = cumulative_charge; //used to compare the full system field calculations with subsystem
      }

    
      /** Get the field from Helmod. The field is only calculated for z >= surface_z (Kimocs coordinates). **/
      /* Remember: iabove = 2*dimension.z */
      for (l = 0; l < 4; l++) {  
        for (k = surface_z*2; k < surface_z*2+LH_iabove; k++) {
          for(j = 0; j < LH_isize[1]; j++) {
            for (i = 0; i < LH_isize[0]; i++) {       
              field_grid[i][j][k][l] = *(LH_E+iterator);			// LibHelmod coordinates (i,j,k). Contains also lattice points between the atoms.
              
              /* Correct the field signs and unit */
              /*LibHelmod always calculates the components for a negative (cathode) field, 
               *but gives always gives a positive magnitude 
               */
              if (l < 3) {
                if (field_sign > 0) {
                  /* Anode field */
                  field_grid[i][j][k][l] = -field_grid[i][j][k][l]*1e10; // [V/m]; x,y,z components opposite to what LibHelmod assumes
                }
                else if (field_sign < 0) {
                  /* Cathode */
                  field_grid[i][j][k][l] = +field_grid[i][j][k][l]*1e10; // [V/m]; x,y,z components correct

                }
              }
              if (l == 3) {
                if (field_sign > 0) {
                  /* Anode field */
                  field_grid[i][j][k][l] = +field_grid[i][j][k][l]*1e10; // [V/m]; magnitude correct.
                }
                else if (field_sign < 0) {
                  /* Cathode */
                  field_grid[i][j][k][l] = -field_grid[i][j][k][l]*1e10; // [V/m]; magnitude opposite to what LibHelmod assumes. 

                }
              }
              
              iterator++; 
            } 
          }
        }
      }    

      if (tag.libhelmod_field_model == 1) {
        /** Assign the field to the V and empty lattice points in Kimocs. q are Kimocs and h LibHelmod coordinates. **/
        for (h.x = 0; h.x < LH_isize[0]; h.x++) {
          for (h.y = 0; h.y < LH_isize[1]; h.y++) {
            for (h.z = 0; h.z < LH_isize[2]; h.z++) {
              
              if( check_for_kimocs_lattice_position_in_libhelmod_lattice(h) == 1) {
                q = libhelmod_field_ijk_to_xyzp(h);
                
                system_lattice[q.x][q.y][q.z][q.p].electric_field.x =  field_grid[h.x][h.y][h.z][0];
                system_lattice[q.x][q.y][q.z][q.p].electric_field.y =  field_grid[h.x][h.y][h.z][1];
                system_lattice[q.x][q.y][q.z][q.p].electric_field.z =  field_grid[h.x][h.y][h.z][2];
                system_lattice[q.x][q.y][q.z][q.p].scalar_field     =  field_grid[h.x][h.y][h.z][3];		// Assuming the field to be negative for a cathode. LibHelmod gives out positive values 
                
                system_lattice[q.x][q.y][q.z][q.p].average_field	= system_lattice[q.x][q.y][q.z][q.p].scalar_field;	// Average field and sum of field only make sense for adatoms.
                system_lattice[q.x][q.y][q.z][q.p].sum_field	= system_lattice[q.x][q.y][q.z][q.p].scalar_field;
                
              }
            }
          }
        }
        
        /** Assign field to adatoms in Kimocs (LibHelmod does not assign field to atoms explicitly) **/
        for (q.x = 0; q.x < dimension.x; q.x++) {
          for (q.y = 0; q.y < dimension.y; q.y++) {
            for (q.z = 0; q.z < dimension.z; q.z++) {
              for (q.p = 0; q.p < dimension.p; q.p++) {
                if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
            
                  /* Find the adatom in the LibHelmod lattice */
                  
                  h = xyzp_to_libhelmod_field_ijk(q);
                  
                  /* Find the field from the surrounding vacancies (x,y,z) in the LibHelmod lattice */
                  N = 0;
                  Fsum.x = Fsum.y = Fsum.z = 0.0;
                  for (x = -1; x <= 1; x++) {
                    for (y = -1; y <= 1; y++) {
                      for (z = -1; z <= 1; z++) {
                        hnn.x = h.x + x;
                        hnn.y = h.y + y;
                        hnn.z = h.z + z;
                        hnn = ijk_pbc(hnn); // Periodic boundary conditions in x and y directions.
                        F.x = field_grid[hnn.x][hnn.y][hnn.z][0];	// [V/m]
                        F.y = field_grid[hnn.x][hnn.y][hnn.z][1];
                        F.z = field_grid[hnn.x][hnn.y][hnn.z][2];
                        scalar_field_tmp = field_grid[hnn.x][hnn.y][hnn.z][3];	// [V/m] 
                        if (fabs(scalar_field_tmp) > 0) {
                          N++; // Counting all surrounding non-zero field-vectors
                          Fsum.x += F.x;
                          Fsum.y += F.y;
                          Fsum.z += F.z;
                        }
                      }
                    }
                  }
                  
                  Fsurf.x = Fsum.x/N;
                  Fsurf.y = Fsum.y/N;
                  Fsurf.z = Fsum.z/N;
                  
                  /* Alternative field values of the adatoms */
                  Fsurf_scalar = field_sign*sqrt(Fsurf.x*Fsurf.x + Fsurf.y*Fsurf.y + Fsurf.z*Fsurf.z);
                  
                  if (N > 0) {
                    system_lattice[q.x][q.y][q.z][q.p].average_field	    = Fsurf_scalar;
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.x   = Fsurf.x;	// [V/m];
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.y   = Fsurf.y;	// [V/m];
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.z   = Fsurf.z;	// [V/m];
                    system_lattice[q.x][q.y][q.z][q.p].scalar_field       = Fsurf_scalar;	// [V/m]
                  }
                  else {
                    system_lattice[q.x][q.y][q.z][q.p].average_field      = 0.0;
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.x   = 0.0;	// [V/m];
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.y   = 0.0;	// [V/m];
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.z   = 0.0;	// [V/m];
                    system_lattice[q.x][q.y][q.z][q.p].scalar_field       = 0.0;	// [V/m]
                  }
                  system_lattice[q.x][q.y][q.z][q.p].remote_field_position = h;
                  
                  system_lattice[q.x][q.y][q.z][q.p].sum_field		= Fsum_scalar;
                }
              }
            }
          }
        }
      }
      
      
      
      
      
      
      /*** The surface fields are measured ~ 1 nm above the adatoms or vacancies. ***/
      else if (tag.libhelmod_field_model == 2) {
        
        /** Assign field to empy objects **/
        for (h.x = 0; h.x < LH_isize[0]; h.x++) {
          for (h.y = 0; h.y < LH_isize[1]; h.y++) {
            for (h.z = 0; h.z < LH_isize[2]; h.z++) {
              
              if( check_for_kimocs_lattice_position_in_libhelmod_lattice(h) == 1) {
                q = libhelmod_field_ijk_to_xyzp(h);
                
                if (system_lattice[q.x][q.y][q.z][q.p].kind == empty_kind) { 
                  system_lattice[q.x][q.y][q.z][q.p].electric_field.x =  field_grid[h.x][h.y][h.z][0];
                  system_lattice[q.x][q.y][q.z][q.p].electric_field.y =  field_grid[h.x][h.y][h.z][1];
                  system_lattice[q.x][q.y][q.z][q.p].electric_field.z =  field_grid[h.x][h.y][h.z][2];
                  system_lattice[q.x][q.y][q.z][q.p].scalar_field     =  field_grid[h.x][h.y][h.z][3];    
                  
                  system_lattice[q.x][q.y][q.z][q.p].average_field    = 0.0;  // Average field and sum of field only make sense for adatoms.
                  system_lattice[q.x][q.y][q.z][q.p].sum_field        = 0.0;
                  system_lattice[q.x][q.y][q.z][q.p].remote_field_position = h; // Field measured at the empty objects own position.
                }
                
              }
            }
          }
        }

        
        /** Assign remote field to adatom and vacancy objects **/
        for (q.x = 0; q.x < dimension.x; q.x++) {
          for (q.y = 0; q.y < dimension.y; q.y++) {
            for (q.z = 0; q.z < dimension.z; q.z++) {
              for (q.p = 0; q.p < dimension.p; q.p++) {
                if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind || system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {
            
                  /* Find the coordinates in the LibHelmod lattice */
                  h = xyzp_to_libhelmod_field_ijk(q); 
                  
                  
                  
                  
                  /* Find the Adatom field at position q by summing surrounding field points */
                  if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
                    N = 0;
                    Fsum.x = Fsum.y = Fsum.z = 0.0;
                    for (x = -1; x <= 1; x++) {
                      for (y = -1; y <= 1; y++) {
                        for (z = -1; z <= 1; z++) {
                          hnn.x = h.x + x; 
                          hnn.y = h.y + y;
                          hnn.z = h.z + z;
                          hnn = ijk_pbc(hnn); // Periodic boundary conditions in x and y direction.
                          F.x = field_grid[hnn.x][hnn.y][hnn.z][0]; // [V/m]
                          F.y = field_grid[hnn.x][hnn.y][hnn.z][1];
                          F.z = field_grid[hnn.x][hnn.y][hnn.z][2];
                          scalar_field_tmp = field_grid[hnn.x][hnn.y][hnn.z][3];  // [V/m] LibHelmod gives out positive values for both cathode and anode.
                          if (fabs(scalar_field_tmp) > 0) {
                            N++; // Counting all surrounding non-zero field-vectors
                            Fsum.x += F.x;
                            Fsum.y += F.y;
                            Fsum.z += F.z;
                          }
                        }
                      }
                    }
                    
                    Fsurf.x = Fsum.x/N;
                    Fsurf.y = Fsum.y/N;
                    Fsurf.z = Fsum.z/N;
                    Fsurf_scalar = sqrt(Fsurf.x*Fsurf.x + Fsurf.y*Fsurf.y + Fsurf.z*Fsurf.z); // Positive, pointing out from the surface
                  }
                  
                  
                  
                  
                  /* Find the field for surface vacancies the same way as for adatoms, but Fsurf is directly taken 
                   * from LibHelmod instead of summing the surrounding field points */
                  else if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {
                    Fsurf.x      = field_grid[h.x][h.y][h.z][0]; // [V/m] 
                    Fsurf.y      = field_grid[h.x][h.y][h.z][1];
                    Fsurf.z      = field_grid[h.x][h.y][h.z][2];
                    Fsurf_scalar = field_grid[h.x][h.y][h.z][3];
                    N = 1; // Only relevant for adatoms; here just put to non-zero.
                  }
                  
                  
                  
                  
                  if (N > 0 && (Fsurf_scalar > 0.0 || Fsurf_scalar < 0.0)) {
                  
                    /* Find the LibHelmod field value at the surface_field_measurement_distance along the direction of the Fsurf vector, which is assumed to be perpendicular to the surface */
                    
                    v_displacement.x = Fsurf.x/Fsurf_scalar * surface_field_measurement_distance*1e10; // [Å]
                    v_displacement.y = Fsurf.y/Fsurf_scalar * surface_field_measurement_distance*1e10; // [Å]
                    v_displacement.z = Fsurf.z/Fsurf_scalar * surface_field_measurement_distance*1e10; // [Å]
                                        
                    h_displacement = xyz_to_libhelmod_field_ijk(v_displacement); // LibHelmod field vector
                                        
                    H_removed.x = h.x + h_displacement.x;
                    H_removed.y = h.y + h_displacement.y;
                    H_removed.z = h.z + h_displacement.z;
                    h_removed   = ijk_pbc(H_removed); // Periodic boundary conditions in x and y direction.
                    
                    /* F_removed is the field vector at the libhelmod ijk point at the surface_field_measurement_distance away from the adatom. */
                    F_removed.x       = field_grid[h_removed.x][h_removed.y][h_removed.z][0]; // [V/m]
                    F_removed.y       = field_grid[h_removed.x][h_removed.y][h_removed.z][1];
                    F_removed.z       = field_grid[h_removed.x][h_removed.y][h_removed.z][2];
                    F_removed_scalar  = field_grid[h_removed.x][h_removed.y][h_removed.z][3];
                    
//                     F_removed_scalar = field_sign*sqrt(F_removed.x*F_removed.x + F_removed.y*F_removed.y + F_removed.z*F_removed.z);
                    
                    /* Using the removed field as the adatom or vacancy field scalar. Direction of Fsurf. */
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.x   = Fsurf.x/Fsurf_scalar * F_removed_scalar; // [V/m]; // Direction taken as the average field vector given by LibHelmod and magnitude given by the remote field vector.
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.y   = Fsurf.y/Fsurf_scalar * F_removed_scalar; // [V/m];
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.z   = Fsurf.z/Fsurf_scalar * F_removed_scalar; // [V/m];
                    system_lattice[q.x][q.y][q.z][q.p].scalar_field       = F_removed_scalar; // The adatom field
                    system_lattice[q.x][q.y][q.z][q.p].remote_field_position      = h_removed; // pbc

                  
                    
                    
                  }
                  else {
                    // Adatoms with no surrounding field.
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.x   = 0.0; // [V/m]; 
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.y   = 0.0; // [V/m];
                    system_lattice[q.x][q.y][q.z][q.p].electric_field.z   = 0.0; // [V/m];
                    system_lattice[q.x][q.y][q.z][q.p].scalar_field       = 0.0; // [V/m];
                    
                    system_lattice[q.x][q.y][q.z][q.p].remote_field_position.x = 0;
                    system_lattice[q.x][q.y][q.z][q.p].remote_field_position.y = 0;
                    system_lattice[q.x][q.y][q.z][q.p].remote_field_position.z = 0;
                  }
                  
                  /*********** Not used ***********/
                  system_lattice[q.x][q.y][q.z][q.p].average_field        = 0.0;                                 
                  system_lattice[q.x][q.y][q.z][q.p].sum_field            = 0.0;
                  
                  /********************************/

                }
              }
            }
          }
        }
        
      }      
    }
    
    free(LH_x0);
    free(LH_xq);
    free(LH_atype);

    helmod_time1 = clock();
    helmod_time += (double) (helmod_time1 - helmod_time0) / CLOCKS_PER_SEC;
}



#endif // FIELD

