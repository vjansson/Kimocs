
/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */

#include <stdio.h>	/* for fprintf and stderr */
#include <stdlib.h>	/* for exit */
#include <math.h>
#include <string.h>
#include <time.h>	/* For timing functions */

#include "mt64.h"	/* Mersenne Twister random generator */
#include "global.h"
#include "probabilities.h"

#ifdef FIELD
#include "helmod_interface.h"
#endif

#include "efield.h"


#include "kimocs.h"
#include "ann.h"

/***** General functions *****/

// Extra calculations
void estimate_memory_usage() {
  
  double lattice_point_mem, tot_memory,parameter_memory; 
//   lattice_point_mem = sizeof(Vector);
  
//   printf("Memory required for one lattice point: %lf bytes\n", lattice_point_mem);
//   tot_memory = 4.0e6*lattice_point_mem/1024.0/1024.0/1024.0;
//   printf("Memory required for 4e6 lattice points: %f GiB\n",tot_memory);
  
//   parameter_memory = 12*6*12*6*2*sizeof(float)/1024.0/1024.0;
  parameter_memory = sizeof(Object);
 
  printf("Memory required for one lattice point: %f bytes\n",parameter_memory);

  
}


// Calculates the 3D height (z coordinate) in [m] for a 4D coordinate q.
double height_3d(Pvector q) {
  Vector v;
  double calculated_height;
  v = xyzp_to_xyz(q,a0);
  calculated_height = v.z;	// Height in [m]
  return calculated_height;
}


/* Read in parameters for nn atom jumps */
void read_in_parameter_file(char * file_name){
  read_in_parameter_file_time0 = clock();
  
  char file_line[200];
  char keyword[200];
  int A1nn,A2nn,B1nn,B2nn;
  double atom_nu = 0.0;
  float atom_M	= 100.0;
  int tag_using_zero_nu = 0;
  double zero_attempt_frequency = 0.0;
  double transition_index = -1;
  
  if ((file_parameters = fopen(file_name,"r")) == NULL) {
    printf("ERROR Could not open parameter file: %s\n",file_name);
    exit(1); 
  }
  
  while (fgets(file_line,200,file_parameters) != NULL) {
    strcpy(keyword," ");		// Initialization of string
    sscanf(file_line,"%s", keyword);	// Finding the first word on the line
    if (strcmp(keyword,"#version") == 0) {
      printf("# Parameter %s\n",file_line);
    }
    else if (strcmp(keyword,"#a0") == 0) {
      sscanf(file_line,"%s %le", keyword, &a0);
      printf("# a0 = %e m read from par-file.\n",a0);
    }
    else if (strcmp(keyword,"#fcc") == 0) {
      printf("# Using fcc structure (read from par-file)\n");
      crystal = fcc;
    }
    
    else if (strcmp(keyword,"#bcc") == 0) {
      printf("# Using bcc structure (read from par-file)\n");
      crystal = bcc;
    }
    else if (strcmp(keyword,"#attempt_frequency") == 0) {
      sscanf(file_line,"%s %le", keyword, &attempt_frequency_1nn);
      tag.global_attempt_frequency_1nn = 1;
      attempt_frequency_5nn = attempt_frequency_3nn = attempt_frequency_2nn = attempt_frequency_1nn; // Default: all attempt frequencies are identical. May be changed when other parameter files are read.
      printf("# Global 1nn attempt frequency = %le s^-1 (read from par-file)\n",attempt_frequency_1nn);
    }
    else if (strcmp(keyword,"#zero_attempt_frequency") == 0) {
      sscanf(file_line,"%s %lg", keyword, &zero_attempt_frequency);
      tag_using_zero_nu = 1;
      printf("# Zero attempt frequency = %le s^-1 (read from par-file)\n",zero_attempt_frequency);
    }
    else if (strcmp(keyword,"#free_polarizability") == 0) {
      sscanf(file_line,"%s %le", keyword, &free_polarizability);
      printf("# Free polarizability = %le em^2V^-1 (read from par-file)\n",free_polarizability);
    }
    else if (keyword[0] != '#' && keyword[0] != ' ' && keyword[0] != '\n') {
      // Format:	# 1nn, 2nn, 1nn, 2nn, M [eV], attemt_frequency [s^{-1}]
      // Newer parameters will overwrite already existing parameters
      sscanf(file_line,"%i %i	%i %i	%g %le",&A1nn,&A2nn, &B1nn,&B2nn, &atom_M, &atom_nu);
      
      if ( atom_M >= 1000.0 ) {
        printf("ERROR: A 4D barrier larger than 1000.0 eV is found, which Kimocs is not designed to handle.\n");
        printf("       This large barriers are used in the code to mark forbidden processes.\n");
        exit(1);
      }
      j1nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.a = A1nn;
      j1nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.b = A2nn;
      j1nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.c = B1nn;
      j1nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.d = B2nn;
      
      j1nn_parameter_table[A1nn][A2nn][B1nn][B2nn].intrinsic_barrier = atom_M;
      transition_index++;
      j1nn_parameter_table[A1nn][A2nn][B1nn][B2nn].index = transition_index;
      if (tag.global_attempt_frequency_1nn == 0) {
        if (atom_nu > 0){
          j1nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = atom_nu;
        }
        else if (atom_nu == 0.0 && tag_using_zero_nu == 1){
          j1nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = zero_attempt_frequency;
        }
      }
      else {
        j1nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = attempt_frequency_1nn;
      }
    }
  }
  
  fclose(file_parameters);
  
  read_in_parameter_file_time1 = clock();
  read_in_parameter_file_time += (double) (read_in_parameter_file_time1 - read_in_parameter_file_time0) / CLOCKS_PER_SEC;
}




/* Read in parameters for nnn atom jumps */
void read_in_nnn_parameter_file(char * file_name){
  char file_line[200];
  char keyword[200];
  int A1nn,A2nn,B1nn,B2nn;
  double atom_nu = 0.0;
  float atom_M	= 100.0;
  int tag_using_zero_nu = 0;
  double zero_attempt_frequency = 0.0;
  double transition_index = -1;
  
  
  if ((file_parameters = fopen(file_name,"r")) == NULL) {
    printf("ERROR Could not open nnn parameter file: %s\n",file_name);
    exit(1); 
  }
  
  while (fgets(file_line,200,file_parameters) != NULL) {
    strcpy(keyword," ");		// Initialization of string
    sscanf(file_line,"%s", keyword);	// Finding the first word on the line
    if (strcmp(keyword,"#version") == 0) {
      printf("# Parameter %s\n",file_line);
    }
    else if (strcmp(keyword,"#attempt_frequency") == 0) {
      sscanf(file_line,"%s %le", keyword, &attempt_frequency_2nn);
      tag.global_attempt_frequency_2nn = 1;
      printf("# Global 2nn attempt frequency = %e s^-1 (read from 2nn par-file)\n",attempt_frequency_2nn);
    }
    else if (strcmp(keyword,"#zero_attempt_frequency") == 0) {
      sscanf(file_line,"%s %lg", keyword, &zero_attempt_frequency);
      tag_using_zero_nu = 1;
      printf("# Zero attempt frequency = %le s^-1 (read from 2nn par-file)\n",zero_attempt_frequency);
    }
    else if (keyword[0] != '#' && keyword[0] != ' ' && keyword[0] != '\n') {
      // Format: 1nn 2nn 1nn 2nn Em[eV]
      // Newer parameters will overwrite already existing parameters
      sscanf(file_line,"%i %i	%i %i	%g %le",&A1nn,&A2nn, &B1nn,&B2nn, &atom_M, &atom_nu);

      if ( atom_M >= 1000.0 ) {
        printf("ERROR: A 4D 2nn barrier larger than 1000.0 eV is found, which Kimocs is not designed to handle.\n");
        printf("       This large barriers are used in the code to mark forbidden processes.\n");
        exit(1);
      }
      
      j2nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.a = A1nn;
      j2nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.b = A2nn;
      j2nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.c = B1nn;
      j2nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.d = B2nn;
      
      j2nn_parameter_table[A1nn][A2nn][B1nn][B2nn].intrinsic_barrier = atom_M;
      transition_index++;
      j2nn_parameter_table[A1nn][A2nn][B1nn][B2nn].index = transition_index;
      if (tag.global_attempt_frequency_2nn == 0) {
        if (atom_nu > 0){
          j2nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = atom_nu;
        }
        else if (atom_nu == 0.0 && tag_using_zero_nu == 1){
          j2nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = zero_attempt_frequency;
        }
      }
      else if (tag.global_attempt_frequency_2nn == 1) {
        j2nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = attempt_frequency_2nn;
      }
    }
    
  }
  
  fclose(file_parameters);
}


/* Read in parameters for 3nn atom jumps */
void read_in_3nn_parameter_file(char * file_name){
  char file_line[200];
  char keyword[200];
  int A1nn,A2nn,B1nn,B2nn;
  double atom_nu = 0.0;
  float atom_M  = 100.0;
  int tag_using_zero_nu = 0;
  double zero_attempt_frequency = 0.0;
  double transition_index = -1;
  
  
  if ((file_parameters = fopen(file_name,"r")) == NULL) {
    printf("ERROR Could not open nnn parameter file: %s\n",file_name);
    exit(1); 
  }
  
  while (fgets(file_line,200,file_parameters) != NULL) {
    strcpy(keyword," ");    // Initialization of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line
    if (strcmp(keyword,"#version") == 0) {
      printf("# Parameter %s\n",file_line);
    }
    else if (strcmp(keyword,"#attempt_frequency") == 0) {
      sscanf(file_line,"%s %le", keyword, &attempt_frequency_3nn);
      tag.global_attempt_frequency_3nn = 1;
      printf("# Global 3nn attempt frequency = %e s^-1 (read from 3nn par-file)\n",attempt_frequency_3nn);
    }
    else if (strcmp(keyword,"#zero_attempt_frequency") == 0) {
      sscanf(file_line,"%s %lg", keyword, &zero_attempt_frequency);
      tag_using_zero_nu = 1;
      printf("# Zero attempt frequency = %le s^-1 (read from 3nn par-file)\n",zero_attempt_frequency);
    }
    else if (keyword[0] != '#' && keyword[0] != ' ' && keyword[0] != '\n') {
      // Format: 1nn 2nn 1nn 2nn Em[eV]
      // Newer parameters will overwrite already existing parameters
      sscanf(file_line,"%i %i %i %i %g %le",&A1nn,&A2nn, &B1nn,&B2nn, &atom_M, &atom_nu);

      if ( atom_M >= 1000.0 ) {
        printf("ERROR: A 4D 2nn barrier larger than 1000.0 eV is found, which Kimocs is not designed to handle.\n");
        printf("       This large barriers are used in the code to mark forbidden processes.\n");
        exit(1);
      }
      
      j3nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.a = A1nn;
      j3nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.b = A2nn;
      j3nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.c = B1nn;
      j3nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.d = B2nn;
      
      j3nn_parameter_table[A1nn][A2nn][B1nn][B2nn].intrinsic_barrier = atom_M;
      transition_index++;
      j3nn_parameter_table[A1nn][A2nn][B1nn][B2nn].index = transition_index;
      if (tag.global_attempt_frequency_3nn == 0) {
        if (atom_nu > 0){
          j3nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = atom_nu;
        }
        else if (atom_nu == 0.0 && tag_using_zero_nu == 1){
          j3nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = zero_attempt_frequency;
        }
      }
      else if (tag.global_attempt_frequency_3nn == 1) {
        j3nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = attempt_frequency_3nn;
      }
    }
    
  }
  
  fclose(file_parameters);
}


/* Read in parameters for 5nn atom jumps */
void read_in_5nn_parameter_file(char * file_name){
  char file_line[200];
  char keyword[200];
  int A1nn,A2nn,B1nn,B2nn;
  double atom_nu = 0.0;
  float atom_M  = 100.0;
  int tag_using_zero_nu = 0;
  double zero_attempt_frequency = 0.0;
  double transition_index = -1;
  
  
  if ((file_parameters = fopen(file_name,"r")) == NULL) {
    printf("ERROR Could not open nnn parameter file: %s\n",file_name);
    exit(1); 
  }
  
  while (fgets(file_line,200,file_parameters) != NULL) {
    strcpy(keyword," ");    // Initialization of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line
    if (strcmp(keyword,"#version") == 0) {
      printf("# Parameter %s\n",file_line);
    }
    else if (strcmp(keyword,"#attempt_frequency") == 0) {
      sscanf(file_line,"%s %le", keyword, &attempt_frequency_5nn);
      tag.global_attempt_frequency_5nn = 1;
      printf("# Global 5nn attempt frequency = %e s^-1 (read from 5nn par-file)\n",attempt_frequency_5nn);
    }
    else if (strcmp(keyword,"#zero_attempt_frequency") == 0) {
      sscanf(file_line,"%s %lg", keyword, &zero_attempt_frequency);
      tag_using_zero_nu = 1;
      printf("# Zero attempt frequency = %le s^-1 (read from 5nn par-file)\n",zero_attempt_frequency);
    }
    else if (keyword[0] != '#' && keyword[0] != ' ' && keyword[0] != '\n') {
      // Format: 1nn 2nn 1nn 2nn Em[eV]
      // Newer parameters will overwrite already existing parameters
      sscanf(file_line,"%i %i %i %i %g %le",&A1nn,&A2nn, &B1nn,&B2nn, &atom_M, &atom_nu);

      if ( atom_M >= 1000.0 ) {
        printf("ERROR: A 4D 2nn barrier larger than 1000.0 eV is found, which Kimocs is not designed to handle.\n");
        printf("       This large barriers are used in the code to mark forbidden processes.\n");
        exit(1);
      }
      
      j5nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.a = A1nn;
      j5nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.b = A2nn;
      j5nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.c = B1nn;
      j5nn_parameter_table[A1nn][A2nn][B1nn][B2nn].signum_4d.d = B2nn;
      
      j5nn_parameter_table[A1nn][A2nn][B1nn][B2nn].intrinsic_barrier = atom_M;
      transition_index++;
      j5nn_parameter_table[A1nn][A2nn][B1nn][B2nn].index = transition_index;
      if (tag.global_attempt_frequency_5nn == 0) {
        if (atom_nu > 0){
          j5nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = atom_nu;
        }
        else if (atom_nu == 0.0 && tag_using_zero_nu == 1){
          j5nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = zero_attempt_frequency;
        }
      }
      else if (tag.global_attempt_frequency_5nn == 1) {
        j5nn_parameter_table[A1nn][A2nn][B1nn][B2nn].attempt_frequency = attempt_frequency_5nn;
      }
    }
    
  }
  
  fclose(file_parameters);
}


/* Read in 26D parameters for nn atom jumps */
void read_in_26D_parameter_file(char * file_name){
  read_in_parameter_file_time0 = clock();
  
  char file_line[200];
  char keyword[200];
  float atom_nu = 0.0;
  float atom_M	= 100.0;
  int s[26];	// The 26D parameters
  
  if ((file_parameters = fopen(file_name,"r")) == NULL) {
    printf("ERROR Could not open parameter file: %s\n",file_name);
    exit(1); 
  }
  
  while (fgets(file_line,200,file_parameters) != NULL) {
    strcpy(keyword," ");		// Initialization of string
    sscanf(file_line,"%s", keyword);	// Finding the first word on the line
    if (strcmp(keyword,"#version") == 0) {
      printf("# Parameter %s\n",file_line);
    }
    else if (strcmp(keyword,"#a0") == 0) {
      sscanf(file_line,"%s %le", keyword, &a0);
      printf("# a0 = %e m read from par-file.\n",a0);
    }
    else if (strcmp(keyword,"#fcc") == 0) {
      printf("# Using fcc structure (read from par-file)\n");
      crystal = fcc;
    }
    
    else if (strcmp(keyword,"#bcc") == 0) {
      printf("# Using bcc structure (read from par-file)\n");
      crystal = bcc;
    }
    else if (strcmp(keyword,"#attempt_frequency") == 0) {
      sscanf(file_line,"%s %le", keyword, &attempt_frequency_1nn);
      printf("# Attempt frequency = %e s^-1 (read from par-file)\n",attempt_frequency_1nn);
    }
    else if (keyword[0] != '#' && keyword[0] != ' ' && keyword[0] != '\n') {
      // Format:	# 1nn, 2nn --> 1nn, 2nn	attempt_frequency_1nn [s^-1]	Cu_M [eV]
      // Newer parameters will overwrite already existing parameters
      sscanf(file_line,"%i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i %i	%g",&s[0],&s[1],&s[2],&s[3],&s[4],&s[5],&s[6],&s[7],&s[8],&s[9],&s[10],&s[11],&s[12],&s[13],&s[14],&s[15],&s[16],&s[17],&s[18],&s[19],&s[20],&s[21],&s[22],&s[23],&s[24],&s[25], &atom_M);
//       printf("%s",file_line);
//       printf("param %lf %lf\n",atom_nu,atom_M);
      if ( atom_M >= 1000.0 ) {
        printf("ERROR: A 26D 1nn barrier larger than 1000.0 eV is found, which Kimocs is not designed to handle.\n");
        printf("       This large barriers are used in the code to mark forbidden processes.\n");
        exit(1);
      }
      atom_26D_parameter_table[s[0]][s[1]][s[2]][s[3]][s[4]][s[5]][s[6]][s[7]][s[8]][s[9]][s[10]][s[11]][s[12]][s[13]][s[14]][s[15]][s[16]][s[17]][s[18]][s[19]][s[20]][s[21]][s[22]][s[23]][s[24]][s[25]] = atom_M;
    }
  }
  
  fclose(file_parameters);
  
  read_in_parameter_file_time1 = clock();
  read_in_parameter_file_time += (double) (read_in_parameter_file_time1 - read_in_parameter_file_time0) / CLOCKS_PER_SEC;
}


/* Print nn atom jump parameters */
void print_1nn_parameters() {
  int i,j,k,l,m;
  int nr_parameters = 0;
  FILE *file_used_parameters;
  
  file_used_parameters = fopen("./used_1nn.par","w");
  fprintf(file_used_parameters,"# (1nn,2nn) --> (1nn,2nn)	E_m[eV] nu[s^-1]  Index\n");
  fprintf(file_used_parameters,"# ---------------------------------------\n");
  for (i = 0; i <= max_1nn; i++){
    for (j = 0;j <= max_2nn; j++){
      for (k = 0; k<= max_1nn; k++){
        for (l = 0; l <= max_2nn; l++){
          if (j1nn_parameter_table[i][j][k][l].intrinsic_barrier < 69.0) {
            fprintf(file_used_parameters,"%i %i  %i %i  %f %le  %5.0lf\n",i,j,k,l,j1nn_parameter_table[i][j][k][l].intrinsic_barrier,j1nn_parameter_table[i][j][k][l].attempt_frequency, j1nn_parameter_table[i][j][k][l].index);
          }
          if (j1nn_parameter_table[i][j][k][l].intrinsic_barrier < 49.0)
            nr_parameters++;
        }
      }
    }
  }
  printf("# Number of nn parameters used (Em < 49 eV): %i\n", nr_parameters);
  fflush(file_used_parameters);
}

// Print nnn atom jump parameters
void print_2nn_parameters() {
  if (tag.allow_nnn_jumps == 1) {
    int i,j,k,l,m;
    int nr_parameters = 0;
    FILE *file_used_parameters;
    
    file_used_parameters = fopen("./used_2nn.par","w");
    fprintf(file_used_parameters,"# (1nn,2nn) --> (1nn,2nn)	E_m [eV] nu[s^-1]  Index\n");
    fprintf(file_used_parameters,"# ---------------------------------------\n");
    for (i = 0;i <= max_1nn;i++){
      for (j = 0;j <= max_2nn;j++){
        for (k = 0;k <= max_1nn;k++){
          for (l = 0;l <= max_2nn;l++){
            if (j2nn_parameter_table[i][j][k][l].intrinsic_barrier < 69.0) {
              fprintf(file_used_parameters,"%i %i  %i %i  %f %le  %5.0lf\n",i,j,k,l,j2nn_parameter_table[i][j][k][l].intrinsic_barrier,j2nn_parameter_table[i][j][k][l].attempt_frequency, j2nn_parameter_table[i][j][k][l].index);
            }
            if (j2nn_parameter_table[i][j][k][l].intrinsic_barrier < 49.0)
              nr_parameters++;
          }
        }
      }
    }
    printf("# Number of nnn parameters used (Em < 49 eV): %i\n", nr_parameters);
    fflush(file_used_parameters);
    fclose(file_used_parameters);
  }
}

// Print 3nn atom jump parameters
void print_3nn_parameters() {
  if (tag.allow_nnn_jumps == 1) {
    int i,j,k,l,m;
    int nr_parameters = 0;
    FILE *file_used_parameters;
    
    file_used_parameters = fopen("./used_3nn.par","w");
    fprintf(file_used_parameters,"# (1nn,2nn) --> (1nn,2nn) E_m [eV] nu[s^-1]  Index\n");
    fprintf(file_used_parameters,"# ---------------------------------------\n");
    for (i = 0;i <= max_1nn;i++){
      for (j = 0;j <= max_2nn;j++){
        for (k = 0;k <= max_1nn;k++){
          for (l = 0;l <= max_2nn;l++){
            if (j3nn_parameter_table[i][j][k][l].intrinsic_barrier < 69.0) {
              fprintf(file_used_parameters,"%i %i  %i %i  %f %le  %5.0lf\n",i,j,k,l,j3nn_parameter_table[i][j][k][l].intrinsic_barrier,j3nn_parameter_table[i][j][k][l].attempt_frequency, j3nn_parameter_table[i][j][k][l].index);
            }
            if (j3nn_parameter_table[i][j][k][l].intrinsic_barrier < 49.0)
              nr_parameters++;
          }
        }
      }
    }
    printf("# Number of nnn parameters used (Em < 49 eV): %i\n", nr_parameters);
    fflush(file_used_parameters);
    fclose(file_used_parameters);
  }
}

// Print 5nn atom jump parameters
void print_5nn_parameters() {
  if (tag.allow_nnn_jumps == 1) {
    int i,j,k,l,m;
    int nr_parameters = 0;
    FILE *file_used_parameters;
    
    file_used_parameters = fopen("./used_5nn.par","w");
    fprintf(file_used_parameters,"# (1nn,2nn) --> (1nn,2nn) E_m [eV] nu[s^-1]  Index\n");
    fprintf(file_used_parameters,"# ---------------------------------------\n");
    for (i = 0;i <= max_1nn;i++){
      for (j = 0;j <= max_2nn;j++){
        for (k = 0;k <= max_1nn;k++){
          for (l = 0;l <= max_2nn;l++){
            if (j5nn_parameter_table[i][j][k][l].intrinsic_barrier < 69.0) {
              fprintf(file_used_parameters,"%i %i  %i %i  %f %le  %5.0lf\n",i,j,k,l,j5nn_parameter_table[i][j][k][l].intrinsic_barrier,j5nn_parameter_table[i][j][k][l].attempt_frequency, j5nn_parameter_table[i][j][k][l].index);
            }
            if (j5nn_parameter_table[i][j][k][l].intrinsic_barrier < 49.0)
              nr_parameters++;
          }
        }
      }
    }
    printf("# Number of nnn parameters used (Em < 49 eV): %i\n", nr_parameters);
    fflush(file_used_parameters);
    fclose(file_used_parameters);
  }
}


// Read in the parameters for all different objects.
// Parameter files have names of the form *.par.


/* Initiate nn atom jump parameter table */
void initiate_nn_parameter_table() {
  int i,j,k,l,m;
  
  for (i = 0; i <= max_1nn; i++){
    for (j = 0; j <= max_2nn; j++){
      for (k = 0; k <= max_1nn; k++){
        for (l = 0; l <= max_2nn; l++){
          j1nn_parameter_table[i][j][k][l].intrinsic_barrier = 100000.0; // M = 100000 being near infinity.
          j1nn_parameter_table[i][j][k][l].attempt_frequency = 0;
          j1nn_parameter_table[i][j][k][l].index = -1;
        }
      }
    }
  }
  
  /* Check */
  if (j1nn_parameter_table[2][2][2][2].intrinsic_barrier < 100000.0) printf("ERROR: Parameter table value not 100.00, but %f\n",j1nn_parameter_table[2][2][2][2].intrinsic_barrier);
}


/* Initiate nnn atom jump parameter table */
void initiate_nnn_parameter_table() {
  int i,j,k,l;
  
  for (i = 0; i <= max_1nn; i++){
    for (j = 0; j <= 6; j++){
      for (k = 0;k <= max_1nn; k++){
        for (l = 0; l <= 6; l++){
          j2nn_parameter_table[i][j][k][l].intrinsic_barrier = 100000.0; // M = 100000 being near infinity.
          j2nn_parameter_table[i][j][k][l].attempt_frequency = 0;
          j2nn_parameter_table[i][j][k][l].index = -1;
        }
      }
    }
  }
}


/* Initiate 3nn atom jump parameter table */
void initiate_3nn_parameter_table() {
  int i,j,k,l;
  
  for (i = 0; i <= max_1nn; i++){
    for (j = 0; j <= max_2nn; j++){
      for (k = 0; k <= max_1nn; k++){
        for (l = 0; l <= max_2nn; l++){
          j3nn_parameter_table[i][j][k][l].intrinsic_barrier = 100000.0; // M = 100000 being near infinity.
          j3nn_parameter_table[i][j][k][l].attempt_frequency = 0;
          j3nn_parameter_table[i][j][k][l].index = -1;
        }
      }
    }
  }
}


/* Initiate 5nn atom jump parameter table */
void initiate_5nn_parameter_table() {
  int i,j,k,l;
  
  for (i = 0; i <= max_1nn; i++){
    for (j = 0; j <= max_2nn; j++){
      for (k = 0; k <= max_1nn; k++){
        for (l = 0; l <= max_2nn; l++){
          j5nn_parameter_table[i][j][k][l].intrinsic_barrier = 100000.0; // M = 100000 being near infinity.
          j5nn_parameter_table[i][j][k][l].attempt_frequency = 0;
          j5nn_parameter_table[i][j][k][l].index = -1;
        }
      }
    }
  }
}



/* Initiate nn atom jump 26D-parameter table */
void initiate_26D_parameter_table() {
  int s00,s01,s02,s03,s04,s05,s06,s07,s08,s09,s10,s11,s12,s13,s14,s15,s16,s17,s18,s19,s20,s21,s22,s23,s24,s25; 
  int nr_elements_table = 2;	// Max number of elements (including vacancies) allwed in atom_26D_parameter_table.
  
  for (s00 = 0; s00 < nr_elements_table; s00++){
    for (s01 = 0; s01 < nr_elements_table; s01++){
      for (s02 = 0; s02 < nr_elements_table; s02++){
        for (s03 = 0; s03 < nr_elements_table; s03++){
          for (s04 = 0; s04 < nr_elements_table; s04++){
            for (s05 = 0; s05 < nr_elements_table; s05++){
              for (s06 = 0; s06 < nr_elements_table; s06++){
                for (s07 = 0; s07 < nr_elements_table; s07++){
                  for (s08 = 0; s08 < nr_elements_table; s08++){
                    for (s09 = 0; s09 < nr_elements_table; s09++){
                      for (s10 = 0; s10 < nr_elements_table; s10++){
                        for (s11 = 0; s11 < nr_elements_table; s11++){
                          for (s12 = 0; s12 < nr_elements_table; s12++){
                            for (s13 = 0; s13 < nr_elements_table; s13++){
                              for (s14 = 0; s14 < nr_elements_table; s14++){
                                for (s15 = 0; s15 < nr_elements_table; s15++){
                                  for (s16 = 0; s16 < nr_elements_table; s16++){
                                    for (s17 = 0; s17 < nr_elements_table; s17++){
                                      for (s18 = 0; s18 < nr_elements_table; s18++){
                                        for (s19 = 0; s19 < nr_elements_table; s19++){
                                          for (s20 = 0; s20 < nr_elements_table; s20++){
                                            for (s21 = 0; s21 < nr_elements_table; s21++){
                                              for (s22 = 0; s22 < nr_elements_table; s22++){
                                                for (s23 = 0; s23 < nr_elements_table; s23++){
                                                  for (s24 = 0; s24 < nr_elements_table; s24++){
                                                    for (s25 = 0; s25 < nr_elements_table; s25++){
                                                      atom_26D_parameter_table[s00][s01][s02][s03][s04][s05][s06][s07][s08][s09][s10][s11][s12][s13][s14][s15][s16][s17][s18][s19][s20][s21][s22][s23][s24][s25] = 100000.0; // M = 100 being near infinity.
                                                    }                               
                                                  }
                                                }
                                              }
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      } 
                    }
                  } 
                } 
              } 
            } 
          } 
        } 
      }
    }
  }
  
  /* Check */
  if (atom_26D_parameter_table[1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1] < 100000.0) {
    printf("ERROR: Parameter table value not 100.00, but %f\n",atom_26D_parameter_table[1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1][1]);
    exit(1);
  }
}


/* Define random number genrator functions. The idea is that the generators should be easy to change in this function,
 * so that not the wholse code need to be altered. These functions are thus interfaces between the generator and the simulation
 * code. As generator, the Mersenne Twister will be used.
 */

/* initialization of the random number generator with seed
 */
void random_generator_initialization() {
//   unsigned long long init[4]={0x12345ULL, 0x23456ULL, 0x34567ULL, 0x45678ULL}, 	// A "random" seed
  int length=4;	
  init_by_array64(seed, length);	// Initialization of the Mersenne Twister
  printf("# Random generator initialized with seed = %llu %llu %llu %llu\n",seed[0],seed[1],seed[2],seed[3]);
}

/* Generates random integer [0, 2^63-1]
 */
long long random_int() {
  return (long long) genrand64_int63();
}

// Generates random double on [0,1]-real-interval
double random_double1() {
  return (double) genrand64_real1();
}

// Generates random double on [0,1)-real-interval
double random_double2() {
  return (double) genrand64_real2();
}

// Generates random double on (0,1)-real-interva
double random_double3() {
  return (double) genrand64_real3();
}

// Generate integers on interval [0,N]
int random_n(int N) {
  int tmp;
  tmp = (int) (N+1)*random_double2();
  return tmp;
}

/*
 * Reads in atoms from a xyz file with the format
 * At x y z
 * 
 * Where At is the atom kind that will be read in.
 * 
 * in_xyz_file_name       = file name
 * xyz_lattice_parameter  = lattice parameter in the same units as in the xyz file.
 * xyz_atom_type          = atom name (e.g. 'At', 'Cu')
 * 
 * disp a the displacement Pvector, [0,0,0,0] by default.
 */
void read_initial_xyz_file(char in_xyz_file_name[100], double xyz_lattice_parameter, char xyz_atom_type[100], Pvector disp){
  char    file_line[200];
  char    keyword[100];
  float   X, Y, Z;
  int     N_obj         = 0;
  int     N_atoms       = 0;
  int     N_read        = 0;
  int     counter_line  = 0;
  double  lattice_parameter;
  Pvector q;
  Vector  v;
  
  if (xyz_lattice_parameter==0){
    lattice_parameter = 1.0;}
  else {
    lattice_parameter = xyz_lattice_parameter;
  }

  
  sprintf(in_xyz_file_name,"%s",in_xyz_file_name);
  printf("# Initial xyz positions read from %s\n",in_xyz_file_name);
  if ((file_init_xyz = fopen(in_xyz_file_name,"r")) == NULL) {
    printf("ERROR Could not open parameter file: %s\n",in_xyz_file_name);
    exit(1);
    
  }
  
  while ( fgets(file_line,200,file_init_xyz) != NULL) {
    
    if (counter_line == 0) {
      sscanf(file_line,"%i",&N_obj);
      printf("# Number of atoms read in from xyz file: %i\n",N_obj);  
    }
    
    counter_line++;
    strcpy(keyword," ");            // Initiation of string
    sscanf(file_line,"%s",keyword); // Finding the first word on the line 
  
    /* Read in atoms of specified kind and ssave them as atom_kind */
    if (strcmp(keyword,xyz_atom_type) == 0) {
      sscanf(file_line,"%s %g %g %g",keyword, &X,&Y,&Z);
      v.x = X;
      v.y = Y;
      v.z = Z;
      q = xyz_to_xyzp(v,lattice_parameter);
      if ( q.x < 0 || q.y < 0 || q.z < 0 || q.p < 0 ) {
        printf("ERROR Negative xyzp coordinates read, try change box_dimensions in data.in\n");
        exit(1);
	
      }
      
      if (q.x + disp.x < 0 || q.y + disp.y < 0|| q.z + disp.z < 0) {
          printf("Read in xyz atoms with diplacement give negative coordinates. Should all be positive.\n");
          exit(1);
      }

      // The atom will be displaced by a Pvector disp, defined by the user. (disp.p = 0)
      system_lattice[q.x + disp.x][q.y + disp.y][q.z + disp.z][q.p].kind		= atom_kind;   
      system_lattice[q.x + disp.x][q.y + disp.y][q.z + disp.z][q.p].element 	= lattice_element;	// Assuming only one element in the system.
      N_atoms++;
      
    }
  }
 
  // Check
  if (N_read!=0) {
    printf("error: 0 atoms read in \n");
    exit(1);
  }
  
  N_read = N_atoms;
  if (N_obj != N_read) {
    printf("WARNING Only %i of %i objects read from intitial xyz file\n",N_read,N_obj);
  }
  
  fclose(file_init_xyz);
  count_all_bonds();
}


/* Print start or end xyz file. 
 * Only print the positions of atoms and adatoms.
 * Coordinates in [a0].
 * stage = 0 Initial system
 * stage = 1 Final system
 * stage = 2 Intermediate 
 */
void print_frame_xyz(int stage) {
  if (tag.print_xyz_files == 1) {
    //  int sum_atoms   = 0;
    //  int sum_adatoms = 0;
    //  int sum_objects = 0;
    int nr_sites = 0;
    int x,y,z,p;
    int nr_points;
    double charge, field;
    enum Kind obj_kind;
    enum Label obj_label;
    int obj_1nn,obj_2nn,obj_cluster;
    int obj_element;
    double obj_index,obj_depid;
    double obj_jumps;
    double obj_gradient;
    double obj_T;
    double obj_depprob;
    Pvector q;
    Vector v;
    Vector F;
    Ivector h;
    double index;
    double adimx,adimy,adimz;
    int print_object = 0;
    Ivector obj_ijk, obj_ijkp;
    adimx = dimension.x*a0*unit_cell_dimension.x/1.0e-10; // [Å]
    adimy = dimension.y*a0*unit_cell_dimension.y/1.0e-10; //
    adimz = dimension.z*a0*unit_cell_dimension.z/1.0e-10; //
    if (stage == 0) {
      file_end_xyz    = fopen("./initial.xyz","w");
    }
    else if (stage == 1 || stage == 2) {
      file_end_xyz    = fopen("./end.xyz","w");
    }

    printf("# Print xyz file\n");
    
    if (stage == 2) {
      nr_sites = nr.objects;
    }
    if (stage == 0 || stage == 1) {
      nr_sites = dimension.x*dimension.y*dimension.z*dimension.p;
    }
    
#ifdef FIELD
    if (tag.use_field == 1 && tag.use_libhelmod == 1 && (stage == 0 || stage == 1) ) {
        /* Add nr of LibHelmod lattice points */
        nr_sites += dimension.x*dimension.y*dimension.z*2*2*2;
    }
#endif // FIELD

    fprintf(file_end_xyz,"%i\n",nr_sites);
    
    /* Print comment line in Ovito Extended xyz format */
    fprintf(file_end_xyz,"Lattice=\"%f %f %f %f %f %f %f %f %f\" ", adimx, 0.0, 0.0,  0.0, adimy, 0.0,  0.0, 0.0, adimz); // Simulation box base vectors
    fprintf(file_end_xyz,"Properties=species:S:1"); // Kind
    fprintf(file_end_xyz,":pos:R:3");               // Position
    fprintf(file_end_xyz,":cluster:I:1");           // Cluster index
    fprintf(file_end_xyz,":field:R:1");             // Field
    fprintf(file_end_xyz,":charge:R:1");            // Charge
    fprintf(file_end_xyz,":njumps:R:1");            // Nr of jumps
    fprintf(file_end_xyz,":id:I:1");                // Object index
    fprintf(file_end_xyz,":q.x:I:1");               // Kimocs coordinate
    fprintf(file_end_xyz,":q.y:I:1");               // Kimocs coordinate
    fprintf(file_end_xyz,":q.z:I:1");               // Kimocs coordinate
    fprintf(file_end_xyz,":q.p:I:1");               // Kimocs coordinate
    fprintf(file_end_xyz,":elm:I:1");               // Element
    fprintf(file_end_xyz,":gradient:R:1");          // Max field gradient
    fprintf(file_end_xyz,":depid:I:1");             // Deposition index
    fprintf(file_end_xyz,":logdepprob:R:1");        // log(Deposition rate)
    fprintf(file_end_xyz,":nnn:I:1");               // NNN
    fprintf(file_end_xyz,":nn:I:1");                // NN
    if (tag.print_xyz_temperature == 1) fprintf(file_end_xyz,":temperature:R:1");
    fprintf(file_end_xyz,":F.x:R:1");     
    fprintf(file_end_xyz,":F.y:R:1");
    fprintf(file_end_xyz,":F.z:R:1");
    fprintf(file_end_xyz,":ijk.x:I:1");
    fprintf(file_end_xyz,":ijk.y:I:1");
    fprintf(file_end_xyz,":ijk.z:I:1");
    fprintf(file_end_xyz," ");
    fprintf(file_end_xyz,"Step=%e ",      kmc_step);
    fprintf(file_end_xyz,"Time=%e ",  simulation_time);  
    fprintf(file_end_xyz,"Nobj=%i ",  nr_sites);  
    fprintf(file_end_xyz,"\n");
    
    for (x = 0; x < dimension.x; x++) {
      for (y = 0; y < dimension.y; y++) {
        for (z = 0; z < dimension.z; z++) {
          for (p = 0; p < dimension.p; p++) {
            q.x = x;
            q.y = y;
            q.z = z;
            q.p = p;
            
            /* Converting to xyz coordinates with correct lattice parameter in [Å] */
            v = xyzp_to_xyz(q,a0/1.0e-10);
            
            
            obj_kind      = system_lattice[x][y][z][p].kind;
            obj_element   = system_lattice[x][y][z][p].element;
            obj_label     = system_lattice[x][y][z][p].label;
            obj_cluster   = system_lattice[x][y][z][p].cluster_index;
            obj_1nn       = system_lattice[x][y][z][p].nr_1nn;
            obj_2nn       = system_lattice[x][y][z][p].nr_2nn;
            field         = system_lattice[x][y][z][p].scalar_field;
            charge        = system_lattice[x][y][z][p].electric_charge;
            obj_jumps     = system_lattice[x][y][z][p].nr_jumps;
            obj_index     = system_lattice[x][y][z][p].index;
            F             = system_lattice[x][y][z][p].electric_field;
            obj_gradient  = system_lattice[x][y][z][p].max_gradient;
            obj_depid     = system_lattice[x][y][z][p].deposition_index;
            obj_ijk       = system_lattice[x][y][z][p].remote_field_position;
            
            
            obj_depprob   = log(system_lattice[x][y][z][p].probability_deposition);
            if (obj_depprob < 1e-100) {
              obj_depprob = 0;
            }
            else {
              obj_depprob = log(obj_depprob);
            }
            
            /* Temperature regime */
            if (tag.use_atomic_temperature == 1) {
              obj_T = system_lattice[q.x][q.y][q.z][q.p].Temperature; // Global temperature not used.
            }
            else {
              obj_T = Temperature;  // Global temperature is used.
              
            }
            print_object = 0;
            switch (obj_kind) {
            case atom_kind:
              fprintf(file_end_xyz,"At");
              print_object = 1;
              break;
            case fixed_atom_kind:
              fprintf(file_end_xyz,"Fx");
              print_object = 1;
              break;
            case adatom_kind:
              fprintf(file_end_xyz,"Ad");
              print_object = 1;
              break;
            case vacancy_kind:
              fprintf(file_end_xyz,"V");
              print_object = 1;
              break;
            case empty_kind:
              if (stage != 2) {
                fprintf(file_end_xyz,"Em");
                print_object = 1;
              }
              break;
            case fixed_empty_kind:
              if (stage != 2) {
                fprintf(file_end_xyz,"S");
                print_object = 1;
              }
              break;
            default:
              printf("ERROR Unknown object kind.\n");
              print_object = 0;
              exit(1);
              break;
            }
            if(print_object == 1) {
              fprintf(file_end_xyz," %f %f %f ",        v.x, v.y, v.z);
        //    fprintf(file_end_xyz," %i",               obj_label); 
              fprintf(file_end_xyz," %i",               obj_cluster);
              fprintf(file_end_xyz," %le %lf",          field, charge);
              fprintf(file_end_xyz," %e",               obj_jumps);
              fprintf(file_end_xyz," %7.0lf",           obj_index);
              fprintf(file_end_xyz,"  %i %i %i %i ",    q.x, q.y, q.z, q.p);
              fprintf(file_end_xyz," %i",               obj_element);
              fprintf(file_end_xyz,"  %le",             obj_gradient);
              fprintf(file_end_xyz," %7.0lf",           obj_depid);
              fprintf(file_end_xyz,"  %le",             obj_depprob);
              fprintf(file_end_xyz,"  %i %i",           obj_2nn, obj_1nn);
              if (tag.print_xyz_temperature == 1) fprintf(file_end_xyz," %lf", obj_T);
              fprintf(file_end_xyz,"  %e %e %e",        F.x, F.y, F.z);
              fprintf(file_end_xyz,"  %i %i %i",        obj_ijk.x, obj_ijk.y, obj_ijk.z);
              fprintf(file_end_xyz,"\n");
            }
          }
        }
      }
    }
    
#ifdef FIELD
    /* Print the field given by LibHelmod, using the LibHelmod field lattice */
    index = max_index;  // Starting particle index for LibHelmod grid points.
    if (tag.use_field == 1 && tag.use_libhelmod == 1 && stage != 2) {
      for (h.x = 0; h.x < LH_isize[0]; h.x++) {
        for (h.y = 0; h.y < LH_isize[1]; h.y++) {
          for (h.z = 0; h.z < LH_isize[2]; h.z++) {
            if (print_xyz_info == 1) {
              v = libhelmod_field_ijk_to_xyz(h);
              field = field_grid[h.x][h.y][h.z][3]; // [V/m]; Libhelmod field scalar always positive.
              F.x = field_grid[h.x][h.y][h.z][0];   // [V/m]; Libhelmod field components are always for a cathode field, and thus negative.
              F.y = field_grid[h.x][h.y][h.z][1];
              F.z = field_grid[h.x][h.y][h.z][2];
              fprintf(file_end_xyz,"H %f %f %f ", v.x, v.y, v.z);
              fprintf(file_end_xyz," 0");                                           // obj_cluster);
              fprintf(file_end_xyz," %le 0.0", field);                              // charge
              fprintf(file_end_xyz," 0e0");                                         // obj_jumps
              obj_index++;
              fprintf(file_end_xyz," %7.0lf",obj_index);
              fprintf(file_end_xyz,"  0 0 0 0");                                    // q.x, q.y, q.z, q.p
              fprintf(file_end_xyz,"  0");                                          // obj_element
              fprintf(file_end_xyz,"  0");                                          // obj_gradient
              fprintf(file_end_xyz,"  0");                                          // obj_depid
              fprintf(file_end_xyz,"  0");                                          // obj_depprob
              fprintf(file_end_xyz,"  0 0");                                        // obj_1nn, obj_2nn
              if (tag.print_xyz_temperature == 1) fprintf(file_end_xyz," 0.0");
              fprintf(file_end_xyz,"  %e %e %e", F.x, F.y, F.z);
              fprintf(file_end_xyz,"  %i %i %i", h.x, h.y, h.z);                    // ijk coordinates, sharing column with the hjk remote positions for Ad and V.
              fprintf(file_end_xyz,"\n");
              
            }
          }
        }
      }
    }
#endif
    
    fclose(file_end_xyz);
    
  }
}


/* Print xyz frames for animations. 
 * Print all information.
 * Output coordinates in [ang].
 */
void print_xyz() {
  print_xyz_time0 = clock();
  if (tag.print_xyz_files == 1) {
    int x,y,z,p;
    enum Kind obj_kind;
    enum Label obj_label; 
    int obj_1nn, obj_2nn, obj_cluster;
    int obj_element;
    double obj_index, obj_depid, obj_depprob;
    double obj_jumps;
    double obj_gradient;
    double obj_nn_gradient;
    double obj_T;
    int nr_sites = 0;
    Pvector q,G;
    Vector F;	// Electric field
    double charge, field;
    Vector v,V;
    Ivector h;
    double adimx,adimy,adimz;
    adimx = dimension.x*a0*unit_cell_dimension.x/1.0e-10;	// [Å]
    adimy = dimension.y*a0*unit_cell_dimension.y/1.0e-10;	//
    adimz = dimension.z*a0*unit_cell_dimension.z/1.0e-10;	//
    if (tag.print_empty_kind == 0) {
      nr_sites = nr.objects;
      if (tag.print_libhelmod_field == 1) {
        nr_sites += dimension.x*dimension.y*dimension.z*2*2*2;
      }
      fprintf(file_xyz,"%i\n",nr_sites);
    }
    else {
      nr_sites = dimension.x*dimension.y*dimension.z*dimension.p;
      if (tag.print_libhelmod_field == 1) {
        nr_sites += dimension.x*dimension.y*dimension.z*2*2*2;
      }

      fprintf(file_xyz,"%i\n",nr_sites);
    }
    
    /* Print comment line in Ovito Extended xyz format */
    fprintf(file_xyz,"Lattice=\"%f %f %f %f %f %f %f %f %f\" ", adimx, 0.0, 0.0,  0.0, adimy, 0.0,  0.0, 0.0, adimz);	// Simulation box base vectors
    fprintf(file_xyz,"Properties=species:S:1"); // Kind
    fprintf(file_xyz,":pos:R:3");               // Position
    fprintf(file_xyz,":cluster:I:1");           // Cluster index
    fprintf(file_xyz,":field:R:1");             // Field
    fprintf(file_xyz,":charge:R:1");            // Charge
    fprintf(file_xyz,":njumps:R:1");            // Nr of jumps
    fprintf(file_xyz,":id:I:1");                // Object index
    fprintf(file_xyz,":gpos:R:3");              // Global position (xyz)
    fprintf(file_xyz,":elm:I:1");               // Element
    fprintf(file_xyz,":label:I:1");             // Label
    fprintf(file_xyz,":q.x:I:1");               // Kimocs coordinate
    fprintf(file_xyz,":q.y:I:1");               // Kimocs coordinate
    fprintf(file_xyz,":q.z:I:1");               // Kimocs coordinate
    fprintf(file_xyz,":q.p:I:1");               // Kimocs coordinate
    fprintf(file_xyz,":gradient:R:1");          // Max field gradient
    fprintf(file_xyz,":nn_grad:R:1");           // nn gradient
    fprintf(file_xyz,":depid:I:1");             // Deposition index
    fprintf(file_xyz,":logdepprob:R:1");        // log(Deposition rate)
    fprintf(file_xyz,":nnn:I:1");               // NNN
    fprintf(file_xyz,":nn:I:1");                // NN
    if (tag.print_xyz_temperature == 1) fprintf(file_xyz,":temperature:R:1");
    fprintf(file_xyz," ");
    fprintf(file_xyz,"Step=%e ",  kmc_step);
    fprintf(file_xyz,"Time=%e ",	simulation_time);  
    fprintf(file_xyz,"Nobj=%i ",	nr_sites);  
    fprintf(file_xyz,"Frame=%i ",	print_xyz_frame_nr);
    fprintf(file_xyz,"\n");
    
    for (x = 0; x < dimension.x; x++) {
      for (y = 0; y < dimension.y; y++) {
        for (z = 0; z < dimension.z; z++) {
          for (p = 0; p < dimension.p; p++) {
            q.x = x;
            q.y = y;
            q.z = z;
            q.p = p;

            
            /* Converting to xyz coordinates with correct lattice parameter in [Å] */
            v = xyzp_to_xyz(q,a0/1.0e-10);
            
            obj_kind      = system_lattice[x][y][z][p].kind;
            obj_element   = system_lattice[x][y][z][p].element;
            obj_label     = system_lattice[x][y][z][p].label;
            obj_cluster   = system_lattice[x][y][z][p].cluster_index;
            obj_1nn       = system_lattice[x][y][z][p].nr_1nn;
            obj_2nn       = system_lattice[x][y][z][p].nr_2nn;
            field         = system_lattice[x][y][z][p].scalar_field;
            charge        = system_lattice[x][y][z][p].electric_charge;
            obj_index     = system_lattice[x][y][z][p].index;
            obj_jumps     = system_lattice[x][y][z][p].nr_jumps;
            F             = system_lattice[x][y][z][p].electric_field;
            G             = system_lattice[x][y][z][p].global_xyzp;
            V             = xyzp_to_xyz(G,a0*1e10); // [Angstrom]
            obj_gradient  = system_lattice[x][y][z][p].max_gradient;
            obj_nn_gradient = system_lattice[x][y][z][p].nn_gradient;
            obj_depid     = system_lattice[x][y][z][p].deposition_index;
            
            obj_depprob   = system_lattice[x][y][z][p].probability_deposition;
            if (obj_depprob < 1e-100) {
              obj_depprob = 0;
            }
            else {
              obj_depprob = log(obj_depprob);
            }
            
            

            /* Temperature regime */
            if (tag.use_atomic_temperature == 1) {
              obj_T = system_lattice[q.x][q.y][q.z][q.p].Temperature; // Global temperature not used.
            }
            else {
              obj_T = Temperature;	// Global temperature is used.
            }
            
            switch (obj_kind) {
            case atom_kind:
              fprintf(file_xyz,"At");
              break;
            case fixed_atom_kind:
              fprintf(file_xyz,"Fx");
              break;
            case adatom_kind:
              fprintf(file_xyz,"Ad");
              break;
            case vacancy_kind:
              fprintf(file_xyz,"V");
              break;
            case empty_kind:
              if (tag.print_empty_kind == 1) fprintf(file_xyz,"Em");
              break;
            case fixed_empty_kind:
              if (tag.print_empty_kind == 1) fprintf(file_xyz,"S");
              break;
            default:
              printf("ERROR Unknown object kind.\n");
              exit(1);
              break;
            }

            if (((obj_kind != empty_kind && obj_kind != fixed_empty_kind) && tag.print_empty_kind == 0) || tag.print_empty_kind == 1) {
              fprintf(file_xyz," %f %f %f  ",       v.x, v.y, v.z);
              if (print_xyz_info == 1) { 
                fprintf(file_xyz," %i",             obj_cluster);
                fprintf(file_xyz," %le %lf ",       field, charge);
                fprintf(file_xyz," %le",            obj_jumps); 
                fprintf(file_xyz," %7.0lf ",        obj_index);
                fprintf(file_xyz,"  %lf %lf %lf ",  V.x, V.y, V.z);			// Global position xyz [Ang]
                fprintf(file_xyz," %i",             obj_element);
                fprintf(file_xyz," %i",             obj_label);
                fprintf(file_xyz,"  %i %i %i %i ",  q.x, q.y, q.z, q.p);
                fprintf(file_xyz,"  %le",           obj_gradient);
                fprintf(file_xyz,"  %le",           obj_nn_gradient);
                fprintf(file_xyz," %7.0lf ",        obj_depid);
                fprintf(file_xyz," %le",            obj_depprob);
                fprintf(file_xyz,"  %i %i",         obj_2nn, obj_1nn);  // Leave NN last to make default in Ovito.
                if (tag.print_xyz_temperature == 1) fprintf(file_xyz," %lf", obj_T);
                // 		fprintf(file_xyz,"  %e", system_lattice[x][y][z][p].sum_field);
                // 		fprintf(file_xyz," %e", system_lattice[x][y][z][p].average_field);
            
              }
              fprintf(file_xyz,"\n");
            }
          }
        }
      }
    }
    
#ifdef FIELD
    /* Print the field given by LibHelmod, using the LibHelmod field lattice */
    obj_index = max_index;	// Starting particle index for LibHelmod grid points.
    if (tag.print_libhelmod_field == 1) {
      for (h.x = 0; h.x < LH_isize[0]; h.x++) {
        for (h.y = 0; h.y < LH_isize[1]; h.y++) {
          for (h.z = 0; h.z < LH_isize[2]; h.z++) {
            if (print_xyz_info == 1) {
              v = libhelmod_field_ijk_to_xyz(h);
              field = field_grid[h.x][h.y][h.z][3]*1.0e10; // [V/m]
              F.x = field_grid[h.x][h.y][h.z][0];
              F.y = field_grid[h.x][h.y][h.z][1];
              F.z = field_grid[h.x][h.y][h.z][2];
              fprintf(file_xyz,"H %f %f %f ", v.x, v.y, v.z);
              fprintf(file_xyz," 0");	// obj_cluster);
              fprintf(file_xyz,"  0 0");	// obj_1nn, obj_2nn
              fprintf(file_xyz," %le 0.0", field);	// charge
              fprintf(file_xyz," 0e0");	// obj_jumps
              obj_index++;
              fprintf(file_xyz," %lf",obj_index);
              fprintf(file_xyz,"  0 0 0 0");	// q.x, q.y, q.z, q.p
              if (tag.print_xyz_temperature == 1) fprintf(file_xyz," 0.0");
              fprintf(file_xyz,"  %e %e %e", F.x, F.y, F.z);
              fprintf(file_xyz,"\n");
              
            }
          }
        }
      }
    }
#endif
    
    fflush(file_xyz);
  }
  
  print_xyz_time1 = clock();
  print_xyz_time += (double) (print_xyz_time1 - print_xyz_time0) / CLOCKS_PER_SEC;
}


/* Print xyz files in the LAMMPS atom dump format */
void print_lammps_xyz() {
  int nr_sites = 0;
  int x,y,z,p;
  Pvector q;
  Vector v;
  int obj_kind;
  double obj_index;
  int print_object;
  int lammps_atom_type, lammps_atom_index = 0;
  
  FILE * f;
  
  double adimx,adimy,adimz;
  adimx = dimension.x*a0*unit_cell_dimension.x/1.0e-10; // [Å]
  adimy = dimension.y*a0*unit_cell_dimension.y/1.0e-10; //
  adimz = dimension.z*a0*unit_cell_dimension.z/1.0e-10; //
  
  f = fopen("./initial_lammps.xyz","w");
  
  printf("# Print LAMMPS atom dump file\n");
  
  nr_sites = nr.atoms + nr.adatoms + nr.fixed_atoms;
  
  fprintf(f,"# LAMMPS Data file created by Kimocs. Atoms of type 2 should be fixed.\n");
  fprintf(f,"\n");
  fprintf(f,"%i atoms\n", nr_sites);
  fprintf(f,"\n");
  fprintf(f,"1 atom types\n"); 
  fprintf(f,"\n");
  fprintf(f,"0 %f xlo xhi\n", adimx);
  fprintf(f,"0 %f ylo yhi\n", adimy);
  fprintf(f,"0 %f zlo zhi\n", adimz);
  fprintf(f,"\n");
  fprintf(f,"Atoms # atomic\n");
  fprintf(f,"\n");
 
  for (x = 0; x < dimension.x; x++) {
    for (y = 0; y < dimension.y; y++) {
      for (z = 0; z < dimension.z; z++) {
        for (p = 0; p < dimension.p; p++) {
          q.x = x;
          q.y = y;
          q.z = z;
          q.p = p;
          
          /* Converting to xyz coordinates with correct lattice parameter in [Å] */
          v = xyzp_to_xyz(q,a0/1.0e-10);
          
          /* Displacing the atomic coordinates with 0.25a0 to avoid atoms on the exact boarder */
//           v.x = v.x + 0.25*a0;
//           v.y = v.y + 0.25*a0;
//           v.z = v.z + 0.25*a0;
          
          obj_kind  = system_lattice[x][y][z][p].kind;
          obj_index = system_lattice[x][y][z][p].index;
          if (obj_index == 0) obj_index = nr_sites;     // LAMMPS can not have an atom index 0.
          
          
          print_object = 0;
          switch (obj_kind) {
          case atom_kind:
            lammps_atom_type = 1;
            lammps_atom_index++;
            print_object = 1;
            break;
          case fixed_atom_kind:
            lammps_atom_type = 1;
            lammps_atom_index++;
            print_object = 1;
            break;
          case adatom_kind:
            lammps_atom_type = 1;
            lammps_atom_index++;
            print_object = 1;
            break;
          case vacancy_kind:
            print_object = 0;
            break;
          case empty_kind:
            print_object = 0;
            break;
          case fixed_empty_kind:
              print_object = 0;
            break;
          default:
            printf("ERROR Unknown object kind.\n");
            print_object = 0;
            exit(1);
            break;
          }
          
          if(print_object == 1) {
            fprintf(f,"%i %i %f %f %f ", lammps_atom_index, lammps_atom_type, v.x, v.y, v.z);
            fprintf(f,"\n");
          }
        }
      }
    }
  }
  
  fflush(f);
  fclose(f);
}



/* Print out the final state xyz file for NEB calculation of the migration
 * barrier
 */
void print_parcas_xyz_final_missing_barrier(char * file_name, Pvector qi, Pvector qf) {
  int x,y,z,p;
  int nr_atoms_adatoms;
  Pvector q;
  Vector v;
  file_parcas_xyz   = fopen(file_name,"w");
  double adimx,adimy,adimz;
  adimx = dimension.x*unit_cell_dimension.x*a0/1.0e-10; // [Å]
  adimy = dimension.y*unit_cell_dimension.y*a0/1.0e-10; //
  adimz = dimension.z*unit_cell_dimension.z*a0/1.0e-10; //
  nr_atoms_adatoms = nr.atoms + nr.adatoms + nr.fixed_atoms;
  fprintf(file_parcas_xyz,"%i\n",nr_atoms_adatoms);
  fprintf(file_parcas_xyz," # %e s, boxsize %lf %lf %lf ang\n", simulation_time,adimx,adimy,adimz);

  //   printf("Dimensions: %i %i %i %i\n",dimension.x,dimension.y,dimension.z,dimension.p);
  for (x=0; x<dimension.x; x++) {
    for (y=0; y<dimension.y; y++) {
      for (z=0; z<dimension.z; z++) {
        for (p=0; p<dimension.p; p++) {
          if (x == qi.x && y == qi.y && z == qi.z && p == qi.p) {
            // Change coordinates for the jumping atom
            q.x = qf.x;
            q.y = qf.y;
            q.z = qf.z;
            q.p = qf.p;
          
          }
          else {
            // Coordinates for the other atoms.
            q.x = x;
            q.y = y;
            q.z = z;
            q.p = p;
          }
          
          // Converting to xyz coordinates with lattice parameter 1.0
          v = xyzp_to_xyz(q,a0/1.0e-10);
          
          // Transfirming xyz system to comply with Parcas standard. 
          v.x = v.x - dimension.x*unit_cell_dimension.x/2.0*(a0/1.0e-10) + 0.001; // Adding 0.001 to avoid atoms to be exactly on the border.
          v.y = v.y - dimension.y*unit_cell_dimension.y/2.0*(a0/1.0e-10) + 0.001;
          v.z = v.z - (dimension.z*unit_cell_dimension.z/2.0 - 1.0/4.0)*a0/1.0e-10; // z_{min} = -height/2 +a/4 for Parcas clic
          
          // Added "1" in last column for PARCAS xyz format.
          // The lattice is centered at (0 0).
          // All atoms need to be called "Cu".
          
          if (system_lattice[x][y][z][p].kind == atom_kind ) {
            fprintf(file_parcas_xyz,"Cu %f %f %f 1\n",v.x, v.y, v.z);
          }
          else if (system_lattice[x][y][z][p].kind == fixed_atom_kind) {
            fprintf(file_parcas_xyz,"Cu %f %f %f -1\n",v.x, v.y, v.z);
          }
          else if (system_lattice[x][y][z][p].kind == adatom_kind) {
            fprintf(file_parcas_xyz,"Cu %f %f %f 1\n",v.x, v.y, v.z);
          }
        }
      }
    }
  }
  fclose(file_parcas_xyz);  
  
}


void print_parcas_xyz(char * file_name) {
  int x,y,z,p;
  int nr_atoms_adatoms;
  Pvector q;
  Vector v;
  char atom_type[100];
  file_parcas_xyz   = fopen(file_name,"w");
  double adimx,adimy,adimz;
  adimx = dimension.x*unit_cell_dimension.x*a0/1.0e-10; // [Å]
  adimy = dimension.y*unit_cell_dimension.y*a0/1.0e-10; //
  adimz = dimension.z*unit_cell_dimension.z*a0/1.0e-10; //
  nr_atoms_adatoms = nr.atoms + nr.adatoms + nr.fixed_atoms;
  fprintf(file_parcas_xyz,"%i\n",nr_atoms_adatoms);
  fprintf(file_parcas_xyz," # %e s, boxsize %lf %lf %lf ang\n", simulation_time,adimx,adimy,adimz);
  //   printf("Dimensions: %i %i %i %i\n",dimension.x,dimension.y,dimension.z,dimension.p);
  for (x=0; x<dimension.x; x++) {
    for (y=0; y<dimension.y; y++) {
      for (z=0; z<dimension.z; z++) {
        for (p=0; p<dimension.p; p++) {
          q.x = x;
          q.y = y;
          q.z = z;
          q.p = p;
          
          // Converting to xyz coordinates with choosen lattice parameter
          v = xyzp_to_xyz(q, a0/1.0e-10);
          
          // Transforming xyz system to comply with Parcas standard. 
          v.x = v.x - dimension.x*unit_cell_dimension.x/2.0*(a0/1.0e-10) + 0.001; // Adding 0.001 to avoid atoms to be exactly on the border.
          v.y = v.y - dimension.y*unit_cell_dimension.y/2.0*(a0/1.0e-10) + 0.001;
          v.z = v.z - (dimension.z*unit_cell_dimension.z/2.0 - 1.0/4.0)*a0/1.0e-10; // z_{min} = -height/2 +a/4 for Parcas clic
          
          // Added "1" in last column for PARCAS xyz format.
          // The lattice is centered at (0 0).
          // All atoms need to be called "Cu".
          
          if    (system_lattice[x][y][z][p].element == 1) strcpy(atom_type, atom_type1);
          else if   (system_lattice[x][y][z][p].element == 2) strcpy(atom_type, atom_type2);
          
          if (system_lattice[x][y][z][p].kind == atom_kind ) {
            fprintf(file_parcas_xyz,"%s %f %f %f 1\n",atom_type, v.x, v.y, v.z);
          }
          else if (system_lattice[x][y][z][p].kind == fixed_atom_kind) {
            fprintf(file_parcas_xyz,"%s %f %f %f -1\n",atom_type,v.x, v.y, v.z);
          }
          else if (system_lattice[x][y][z][p].kind == adatom_kind) {
            fprintf(file_parcas_xyz,"%s %f %f %f 1\n",atom_type,v.x, v.y, v.z);
          }
        }
      }
    }
  }
  fclose(file_parcas_xyz);
}


/* Make xyz file for Parcas for initial, qi, and final state, qf, of a jump, 
 * for which the migration energy is missing. */
void make_barrier_xyz(Pvector qi, Pvector qf, int qi_nn1, int qi_2nn, int qf_1nn, int qf_2nn) {
  make_barrier_xyz0 = clock();
  char init_file_name[100], final_file_name[100];
  int tmp_kind;
  
  // Make variable file names
  snprintf(init_file_name, sizeof(init_file_name), "./ini_%i_%i__%i_%i.xyz",qi_nn1,qi_2nn,qf_1nn,qf_2nn);
  snprintf(final_file_name, sizeof(final_file_name), "./fin_%i_%i__%i_%i.xyz",qi_nn1,qi_2nn,qf_1nn,qf_2nn);
  
  printf("%e Making xyz files for (%i,%i)->(%i,%i)\n",kmc_step, qi_nn1,qi_2nn,qf_1nn,qf_2nn);
  print_parcas_xyz(init_file_name);
  print_parcas_xyz_final_missing_barrier(final_file_name, qi, qf);

  make_barrier_xyz1 = clock();
  make_barrier_xyz_time += (double) (make_barrier_xyz1 - make_barrier_xyz0) / CLOCKS_PER_SEC;
}



/* Print file for export to COMSOL.
 */
void print_comsol_xyz() {
  int nr_sites = 0;
  int x,y,z,p;
  int nr_points;
  enum Kind obj_kind; 
  enum Label obj_label;
  Vector obj_E;
  Pvector q;
  Vector v;
  double T;
  FILE *file_E_kimocs_out;
  file_E_kimocs_out = fopen("./comsol.ckx","w");

  printf("# Print comsol.ckx file\n");
  
  fprintf(file_E_kimocs_out, "# %e s\n", simulation_time);
  
  for (x = 0; x < dimension.x; x++) {
    for (y = 0; y < dimension.y; y++) {
      for (z = 0; z < dimension.z; z++) {
        for (p = 0; p < dimension.p; p++) {
          q.x = x;
          q.y = y;
          q.z = z;
          q.p = p;
          
          // Converting to xyz coordinates with correct lattice parameter in [Å]
          v = xyzp_to_xyz(q,2);
          
          
          obj_kind = system_lattice[x][y][z][p].kind;
          obj_label = system_lattice[x][y][z][p].label;
          
          // Temperature regime
          if (tag.use_atomic_temperature == 1) {
            T = system_lattice[q.x][q.y][q.z][q.p].Temperature; // Global temperature not used.
          }
          else {
            T = Temperature;	// Global temperature is used.
          }
          
          if (obj_kind == adatom_kind || obj_kind == atom_kind) {
            fprintf(file_E_kimocs_out,"%i %f %f %f %le %le %le %lf\n",obj_kind, v.x, v.y, v.z, obj_E.x, obj_E.y, obj_E.z, T);
          }
        }
      }
    }
  }
  
  fclose(file_E_kimocs_out);
  
}



/* Function to save the state of the simulation
 * in terms of objects positions.
 */
void print_xyzp(char * file_name) {
  if (tag.print_xyzp_files == 1) {
    print_xyzp_time0 = clock();
    int x,y,z,p;
    int obj_element;
    file_xyzp = fopen(file_name,"w");
    fprintf(file_xyzp,"# %e %e %i %i\n",kmc_step, simulation_time,surface_orientation,nr.objects);
    for (x = 0; x < dimension.x; x++) {
      for (y = 0; y < dimension.y; y++) {
        for (z = 0; z < dimension.z; z++) {
          for (p = 0; p < dimension.p; p++) {
            obj_element = system_lattice[x][y][z][p].element;
            if (obj_element > 0) {
              fprintf(file_xyzp,"%i %i %i %i %i\n", x,y,z,p,obj_element);
            }
          }
        }
      }
    }
    
    fflush(file_xyzp);
    fclose(file_xyzp);
    
    print_xyzp_time1 = clock();
    print_xyzp_time += (double) (print_xyzp_time1 - print_xyzp_time0) / CLOCKS_PER_SEC;
  }
}


/* Read in atom, adatom and vacancy objects from a xyzp file.
 * The objects will overwrite old objects at the same positions.
 */ 
void read_xyzp_file(char * file_name){
  int x,y,z,p;
  int obj_element;
  int nn,nnn;
  double step_tmp = 0.0;
  double time_tmp = 0.0;
  int surface_orientation_tmp,nr_objects_tmp;;
  char file_line[200];
  char keyword[200];
  int omega;
  Jump jump;
  Pvector q;

  if ((file_in_xyzp = fopen(file_name,"r")) == NULL) {
    printf("ERROR Could not open xyzp file: %s\n",file_name);
    exit(1); 
  }
  while ( fgets(file_line,200,file_in_xyzp) != NULL) {
    strcpy(keyword," ");		// Initialization of string
    sscanf(file_line,"%s", keyword);	// Finding the first word on the line
    if (keyword[0] == '#') {
      sscanf(file_line,"%s %le %le %i %i", keyword, &step_tmp, &time_tmp, &surface_orientation_tmp, &nr_objects_tmp);
      if ( surface_orientation != surface_orientation_tmp) {
        printf("ERROR: Objects read in from xyzp file with wrong orientation: (%i) != (%i)\n",surface_orientation_tmp,surface_orientation);
        exit(1);
      }
      kmc_step		= step_tmp;
      simulation_time	= time_tmp;
      printf("# Starting step: %le s and time: %le read from a xyzp-file.\n", kmc_step, simulation_time );
      
      
    }
    else if (keyword[0] != '#' && keyword[0] != ' ' && keyword[0] != '\n') {
      // Format:	# x y z p obj_kind
      sscanf(file_line,"%i %i %i %i %i", &x, &y, &z, &p, &obj_element);
      
      if (x < dimension.x && y < dimension.y && z < dimension.z && p < dimension.p) {
      // 	printf("# added %i %i %i %i %i \n",x,y,z,p,obj_kind);
        if (obj_element == 0) {
          system_lattice[x][y][z][p].kind	= vacancy_kind;
        }
        else {
          system_lattice[x][y][z][p].kind	= atom_kind;	//  Will be changed elsewhere.
        }
        system_lattice[x][y][z][p].element	= obj_element;
        system_lattice[x][y][z][p].label	= 0;
        system_lattice[x][y][z][p].nr_1nn	= 0;
        system_lattice[x][y][z][p].nr_2nn	= 0;

      // All bonds and probabilities will be recounted later in main().
      }
      else {
        printf("# WARNING: (%i %i %i %i) is outside the box_dimensions. Object ignored.\n",x,y,z,p);
      }
    }
  }
}


/* Read in atom, adatom and vacancy objects from a xyzp file.
 * The objects will overwrite old objects at the same positions.
 */ 
void read_comsol_xyz_file(char * file_name){
  int x,y,z,p;
  int obj_kind = 0;
  int nn,nnn;
  double step_tmp,time_tmp;
  int surface_orientation_tmp,nr_objects_tmp;;
  char file_line[200];
  char keyword[200];
  Vector obj_E, v;
  Pvector q;
  double T;

  if ((file_E_comsol_out = fopen(file_name,"r")) == NULL) {
    printf("comsol.ckx file not found\n");
  }
  else {
    while ( fgets(file_line,200,file_E_comsol_out) != NULL) {
      strcpy(keyword," ");		// Initialization of string
      sscanf(file_line,"%s", keyword);	// Finding the first word on the line
      if (keyword[0] == '#') {
        continue;
        //       sscanf(file_line,"%s %le %le %i %i", keyword, &step_tmp, &time_tmp, &surface_orientation_tmp, &nr_objects_tmp);
        //       if ( surface_orientation != surface_orientation_tmp) {
        // 	printf("ERROR: Objects read in from xyzp file with wrong orientation: (%i) != (%i)\n",surface_orientation_tmp,surface_orientation);
        // 	exit(1);
        //       }
      }
      else if (keyword[0] != '#' && keyword[0] != ' ' && keyword[0] != '\n') {
        // Format:	# x y z p obj_kind
        sscanf(file_line,"%i %lf %lf %lf %le %le %le %lf\n",&obj_kind, &v.x, &v.y, &v.z, &obj_E.x, &obj_E.y, &obj_E.z, &T);
        
        q = xyz_to_xyzp(v,2.0);
        
        if (x < dimension.x && y < dimension.y && z < dimension.z && p < dimension.p) {
          printf("# Added %i %i %i %i %i \n",x,y,z,p,obj_kind);
          system_lattice[q.x][q.y][q.z][q.p].kind     = obj_kind;
          system_lattice[q.x][q.y][q.z][q.p].element  = lattice_element;	// Assuming only one element in the system.
          system_lattice[q.x][q.y][q.z][q.p].label		= 0;
          system_lattice[q.x][q.y][q.z][q.p].nr_1nn		= 0;
          system_lattice[q.x][q.y][q.z][q.p].nr_2nn		= 0;
          system_lattice[q.x][q.y][q.z][q.p].electric_field	= obj_E;
          system_lattice[q.x][q.y][q.z][q.p].Temperature	= T;

          // All bonds and probabilities will be recounted later in main().
        }
        else {
          printf("# WARNING: (%i %i %i %i) outside the box_dimensions. Object ignored.\n",x,y,z,p);
        }
      }
    }
  }
}




/* Make empty lattice with the dimensions 
 * specified in the configuration file
 * The a0 and Temperature need to be known.
 */
void initialize_lattice(){
  int x,y,z,p;
  int omega,omega3,omega5;
  double N = 0;
  Object point;
  Vector F;
  Vector v;
  Ivector iv;
  Pvector q;
  Jump jump;
  Signum_4d S;
  
  printf("# Initialize lattice, p = %i \n",dimension.p);
  
  if (dimension.x == 0 || dimension.y == 0 || dimension.z == 0 || dimension.p == 0) {
    printf("ERROR System dimensions = (%i,%i,%i,%i). Maybe box_dimensions is in the wrong place in data.in?\n", dimension.x,dimension.y,dimension.z,dimension.p);
    exit(1);
  }
  
  F.x = 0.0;
  F.y = 0.0;
  F.z = 0.0;
  
  v.x = 0.0;
  v.y = 0.0;
  v.z = 0.0;
  
  v.x = 0.0;
  v.y = 0.0;
  v.z = 0.0;
  
  iv.x = 0;
  iv.y = 0;
  iv.z = 0;
  
  q.x = 0;
  q.y = 0;
  q.z = 0;
  q.p = 0;
  
  S.a = 0;
  S.b = 0;
  S.c = 0;
  S.d = 0;
  
  jump.signum_4d    =  S;
  jump.initial      =  q;
  jump.final        =  q;
  jump.omega        = -1;
  jump.kind         =  1;
  
  jump.attempt_frequency  = 0.0;      // [s^{-1}]
  jump.effective_barrier  = 100000.0;  // [eV]; Barrier used in arrhenius formula
  jump.intrinsic_barrier  = 100000.0; // [eV]; The barrier in the absence of a field 
  jump.field_barrier      = 0.0;      // [eV]; The change of the barrier due to a field. (may be negtive)
  jump.probability        = 0.0;
  jump.index              = -1;
  
  point.kind                          = empty_kind;
  point.element                       = vacancy_element;
  point.label                         = 0;
  point.cluster_index                 = 0;
  point.nr_1nn                        = 0;
  point.nr_2nn                        = 0;
  point.deposition_index              = 0;

  point.probability_deposition        = 0.0;
  point.probability_evaporation       = 0.0;
  point.nr_jumps                      = 0.0;
  point.Temperature                   = Temperature;
  
  for (omega = 0; omega < max_1nn; omega++){
    jump.omega                          = omega;
    jump.kind                           = 1;
    point.event_1nn[omega]              = jump;
    
    point.configuration_26d[omega]      = 0;
    point.barrier[omega]                = 100000.0;
    point.field_gradient_barrier[omega] = 0.0;
    point.field_gradient[omega]         = 0.0;
  }
  
  for (omega = 0; omega < max_2nn; omega++){
    jump.omega                          = omega;
    jump.kind                           = 2;
    point.event_2nn[omega]              = jump;
  }
  for (omega = 0; omega < max_3nn; omega++){
    jump.omega                          = omega;
    jump.kind                           = 3;
    point.event_3nn[omega]              = jump;   
  }
  for (omega = 0; omega < max_5nn; omega++){
    jump.omega                          = omega;
    jump.kind                           = 5;
    point.event_5nn[omega]              = jump;     
  }
  
  point.max_gradient                    = 0.0;
  point.nn_gradient                     = 0.0;
  
  point.last_transition               = jump;
  
  point.electric_charge               = 0.0;
  point.electric_field                = F;
  point.scalar_field                  = 0.0;
  point.average_field                 = 0.0;
  point.sum_field                     = 0.0;
  point.nr_jumps_with_constant_field  = 0;
  point.remote_field_position         = iv;
  
  /* Sanity check */
  if (dimension.x == 0 || dimension.y == 0 || dimension.z == 0) {
    printf("ERROR System dimensions are not set properly (%i, %i, %i, %i)\n", dimension.x,dimension.y,dimension.z,dimension.p);
    printf("Maybe, box_dimensions option is in the wrong place in data.in\n");
    exit(1);
  }
  
  N = 0;
  for (q.z = 0; q.z < dimension.z; q.z++) {
    for (q.x = 0; q.x < dimension.x; q.x++) {
      for (q.y = 0; q.y < dimension.y; q.y++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          
          /* Object-related features (will follow the objects) */
          system_lattice[q.x][q.y][q.z][q.p]                  = point;
          system_lattice[q.x][q.y][q.z][q.p].index            = N;
          system_lattice[q.x][q.y][q.z][q.p].global_xyzp      = q;
          system_lattice[q.x][q.y][q.z][q.p].initial_position = system_lattice[q.x][q.y][q.z][q.p].position;
          
          /* Lattice-related features (will not change) */
          system_lattice[q.x][q.y][q.z][q.p].position         = xyzp_to_xyz(q,a0);
          
          /* Field-related (will not change) */
          system_lattice[q.x][q.y][q.z][q.p].helmod_xyz       = v;

          N++;
        }
      }
    }
  }
  max_index = N--;
  
}


/* For an object at v, count and return the number of all NN atoms and adatoms. */
int object_1nn_bonds(Pvector v) {
  int sum_1nn = 0;
  int nn;  
  Pvector q;
  
  /* 1nn = 0 for fixed_empty_kind; 1nn = max_1nn for fixed_atom_kind */
  if (system_lattice[v.x][v.y][v.z][v.p].kind == atom_kind || 
      system_lattice[v.x][v.y][v.z][v.p].kind == adatom_kind || 
      system_lattice[v.x][v.y][v.z][v.p].kind == vacancy_kind ||
      system_lattice[v.x][v.y][v.z][v.p].kind == empty_kind) {
    for (nn = 0; nn < max_1nn; nn++) {
      q = omega_position_of_1nn(v, nn);
      if (
        system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind || 
        system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind || 
        system_lattice[q.x][q.y][q.z][q.p].kind == fixed_atom_kind) 
        sum_1nn += 1;
    }
  }
  else if (system_lattice[v.x][v.y][v.z][v.p].kind == fixed_atom_kind) {
    sum_1nn = max_1nn;
  }
  else if (system_lattice[v.x][v.y][v.z][v.p].kind == fixed_empty_kind) {
    sum_1nn = 0;
  }
  
  if (sum_1nn <= 0 && system_lattice[v.x][v.y][v.z][v.p].kind == adatom_kind) {
//     printf("%e WARNING sum_1nn = %i for adatom at (%i,%i,%i,%i). Label = 1\n",kmc_step, sum_1nn,v.x,v.y,v.z,v.p);
    system_lattice[v.x][v.y][v.z][v.p].label = label_free_floating_atom;
//     print_xyz();
  }
//   else if (sum_1nn <= 0 && system_lattice[v.x][v.y][v.z][v.p].kind == adatom_kind) {
//     // Remove label = 1.
//     system_lattice[v.x][v.y][v.z][v.p].label = 0;
//   }

  return sum_1nn;
  
}


/* 
 * For an object at (x,y,z,p), count and return the number of all 2nn atoms and adatoms in a fcc lattice.
 * Should work for both (100), (111) and (110).
 */
int object_2nn_bonds(Pvector v) {
  int sum_2nn = 0;
  int sum_2nn_old = 0;
  int k0[6];
  int k1[6];
  int k2[6];
  int i;
  int nnn;
  Pvector q;
  
  /* 2nn = 0 for fixed_empty_kind; 2nn = 6 for fixed_atom_kind */
  if (system_lattice[v.x][v.y][v.z][v.p].kind == atom_kind || 
      system_lattice[v.x][v.y][v.z][v.p].kind == adatom_kind || 
      system_lattice[v.x][v.y][v.z][v.p].kind == vacancy_kind ||
      system_lattice[v.x][v.y][v.z][v.p].kind == empty_kind) {
    for (nnn = 0; nnn < max_2nn; nnn++) {
      q = omega_position_of_2nn(v, nnn);
      if (q.x == -1 || q.x == -1 || q.x == -1) {
        sum_2nn += 0;	// Might need to be treated differently, depending on the considered problem
        printf("ERROR Unexpected behaviour in object_2nn_bonds().\n");
        exit(1);
      }
      else if ( 
        system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind || 
        system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind || 
        system_lattice[q.x][q.y][q.z][q.p].kind == fixed_atom_kind) {
        
          sum_2nn += 1;
      }
    }
  }
  
//   if (sum_2nn <= 0 && system_lattice[v.x][v.y][v.z][v.p].kind == adatom_kind) {
//     printf("%e WARNING sum_2nn = %i for adatom at (%i,%i,%i,%i). Label = 1\n",kmc_step, sum_2nn,v.x,v.y,v.z,v.p);
//     system_lattice[v.x][v.y][v.z][v.p].label = 1;
//     print_xyz();
//   }
//   else if (sum_2nn <= 0 && system_lattice[v.x][v.y][v.z][v.p].kind == adatom_kind) {
//     // Remove label = 1.
//     system_lattice[v.x][v.y][v.z][v.p].label = 0;
//   }
  return sum_2nn;
  
}


/* Make changes adatom <-> atom and vacancy <-> empty, depending on the nr_1nn value */
void update_object_kind(Pvector q) {
  
  /* Dynamic number of atoms and adatoms */
  if (
    system_lattice[q.x][q.y][q.z][q.p].kind   == atom_kind && 
    system_lattice[q.x][q.y][q.z][q.p].nr_1nn < max_1nn) {
    system_lattice[q.x][q.y][q.z][q.p].kind   = adatom_kind;
  }
  else if (
    system_lattice[q.x][q.y][q.z][q.p].kind   == adatom_kind && 
    system_lattice[q.x][q.y][q.z][q.p].nr_1nn == max_1nn) {
    system_lattice[q.x][q.y][q.z][q.p].kind   = atom_kind;
  }
  
  /* Dynamic number of vacancies and empty kinds */
  if (
    system_lattice[q.x][q.y][q.z][q.p].kind   == empty_kind && 
    system_lattice[q.x][q.y][q.z][q.p].nr_1nn > 0) {
    system_lattice[q.x][q.y][q.z][q.p].kind   = vacancy_kind;
  }
  else if (
    system_lattice[q.x][q.y][q.z][q.p].kind   == vacancy_kind && 
    system_lattice[q.x][q.y][q.z][q.p].nr_1nn == 0) {
    system_lattice[q.x][q.y][q.z][q.p].kind   = empty_kind;
  }
  
  
  /* Update the cluster index if the adatom has no neighbours */
  if (system_lattice[q.x][q.y][q.z][q.p].kind   == adatom_kind && 
      system_lattice[q.x][q.y][q.z][q.p].nr_1nn == 0) {
    
    /* The adatom belongs to no cluster and will be assigned a 
     * cluster index next time the system is analyzed
     */
    system_lattice[q.x][q.y][q.z][q.p].cluster_index = 0;	
    
  }
  
}


// Update the bond count in the unit cells next to q0 and including q0.
void update_bond_count(Pvector q) {  
  update_bond_count_time0 = clock();
  
  int x,y,z,p;
  Pvector nn;
  
  for (x = -2; x <= 2; x++) {
    for (y = -2; y <= 2; y++) {
      for (z = -2; z <= 2; z++) {
        for (p = 0; p < dimension.p; p++) {
          nn.x = boundary_condition(q.x + x, 0);
          nn.y = boundary_condition(q.y + y, 1);
          nn.z = boundary_condition(q.z + z, 2);
          nn.p = p;
          system_lattice[nn.x][nn.y][nn.z][nn.p].nr_1nn = object_1nn_bonds(nn);
          system_lattice[nn.x][nn.y][nn.z][nn.p].nr_2nn = object_2nn_bonds(nn);
          update_object_kind(nn);
        }
      }
    }
  }
  
  update_bond_count_time1 = clock();
  update_bond_count_time = (double) (update_bond_count_time1 - update_bond_count_time0) / CLOCKS_PER_SEC;
}


/* For every lattice point, count the number of 1nn and 2nn bonds 
 * to neigbour objects.
 */
void count_all_bonds() {
  count_all_bonds_time0 = clock();
  Pvector q;
  printf("# Count all bonds\n");
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          system_lattice[q.x][q.y][q.z][q.p].nr_1nn = object_1nn_bonds(q);
          system_lattice[q.x][q.y][q.z][q.p].nr_2nn = object_2nn_bonds(q);
          
          update_object_kind(q);
        }
      }
    }
  }
  
  count_all_bonds_time1 = clock();
  count_all_bonds_time += (double) (count_all_bonds_time1 - count_all_bonds_time0) / CLOCKS_PER_SEC;
}


/* Filling the plane of unit cells at z = 0,1 with atoms*/
void make_plane(){
  int x,y,z,p;
  int added_atoms = 0;
  int added_fixed_atoms = 0;
  surface_z = 3;
  printf("# Make plane\n");
  
  for (z=0; z<surface_z; z++) {
    for (x=0; x<dimension.x; x++) {
      for (y=0; y<dimension.y; y++) {
        for (p=0; p<dimension.p; p++) {
          if (z == 0 && (surface_orientation == 100 || surface_orientation == 111) && (p == 0 || p == 1) ) {
            system_lattice[x][y][0][p].kind = fixed_atom_kind;
            system_lattice[x][y][0][p].element = lattice_element;
            system_lattice[x][y][0][p].nr_1nn = max_1nn;		// Edge atoms are fully bonded.
            system_lattice[x][y][0][p].nr_2nn = 5;
            added_fixed_atoms++;
          }
          else if (z == 0 && surface_orientation == 110 ) {
            system_lattice[x][y][0][p].kind = fixed_atom_kind;
            system_lattice[x][y][0][p].element = lattice_element;
            system_lattice[x][y][0][p].nr_1nn = max_1nn;		// Edge atoms are fully bond.
            system_lattice[x][y][0][p].nr_2nn = 5;
            added_fixed_atoms++;
          }
          else {
            system_lattice[x][y][z][p].kind = atom_kind;
            system_lattice[x][y][z][p].element = lattice_element;
            added_atoms++;
          }
        // 	printf("kind: %i\n",system_lattice[x][y][0][p].kind);
        }
      }
    }
  }
  printf("# Added atoms: %i\n",added_atoms);
  printf("# Added edge atoms: %i\n",added_fixed_atoms);
  
//   count_all_bonds();
  
}


/* Function to fill the system with atoms between z_min and z_max */
void make_bulk(int z_min, int z_max) {
  make_bulk_time0 = clock();
  
  Pvector q;
  int added_atoms = 0;
  int added_fixed_atoms = 0;
  surface_z = z_max;
  double max_bulk_z = 0.0;	// [uc]
  double bulk_height = 0.0;	// [m]
  
  if (z_max > dimension.z) z_max = dimension.z;
  if (z_min < 0) z_min = 0;
  
  for (q.z = z_min; q.z <= surface_z; q.z++) {
    for (q.x = 0; q.x < dimension.x; q.x++) {
      for (q.y = 0; q.y < dimension.y; q.y++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          system_lattice[q.x][q.y][q.z][q.p].kind 	= atom_kind;
          system_lattice[q.x][q.y][q.z][q.p].element 	= lattice_element;
          added_atoms++;
          
          if (height_3d(q) >= max_bulk_z) {
            max_bulk_z = height_3d(q);	// Top layer z of the bulk in [m]
            bulk_height = q.z;
          }
        }
      }
    }
  }
  printf("# Bulk height: %lf uc = %e m\n",bulk_height, max_bulk_z);
  printf("# Added atoms: %i\n",added_atoms);
  
  make_bulk_time1 = clock();
  make_bulk_time += (double) (make_bulk_time1 - make_bulk_time0) / CLOCKS_PER_SEC;
}


/* Function to fill the system with atoms between z_min and z_max in metres. */
void make_bulk_m(double min, double max) {
  make_bulk_time0 = clock();
  
  Pvector q;
  int added_atoms = 0;
  int added_fixed_atoms = 0;
  double max_bulk_z = 0.0;	// [uc]
  double bulk_height = 0.0;	// [m]
  int z_min,z_max;
  
  z_min = floor(min/(unit_cell_dimension.z*a0));
  z_max = floor(max/(unit_cell_dimension.z*a0));
  
  // Check
  min = z_min*unit_cell_dimension.z*a0;
  max = z_max*unit_cell_dimension.z*a0;
  
  surface_z = z_max;
  
  if (z_max > dimension.z) z_max = dimension.z;
  if (z_min < 0) z_min = 0;
  
  for (q.z = z_min; q.z <= surface_z; q.z++) {
    for (q.x = 0; q.x < dimension.x; q.x++) {
      for (q.y = 0; q.y < dimension.y; q.y++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          system_lattice[q.x][q.y][q.z][q.p].kind 	= atom_kind;
          system_lattice[q.x][q.y][q.z][q.p].element 	= lattice_element;
          added_atoms++;
          
          if (height_3d(q) >= max_bulk_z) {
            max_bulk_z = height_3d(q);	// Top layer z of the bulk in [m]
            bulk_height = q.z;
          }
        }
      }
    }
  }
  printf("# Bulk height: %lf uc = %e m\n",bulk_height, max_bulk_z);
  printf("# Added atoms: %i\n",added_atoms);
  
  make_bulk_time1 = clock();
  make_bulk_time += (double) (make_bulk_time1 - make_bulk_time0) / CLOCKS_PER_SEC;
}


void fix_bottom_layer() {
  int  x,y,z,p;
  z = 0;
  for (x = 0; x < dimension.x; x++) {
    for (y = 0; y < dimension.y; y++) {
      for (p = 0; p < dimension.p; p++) {
        if (crystal == fcc) {
          // p = 0 and 1 are bottom atoms for both (100) and (111) systems.
          if (z == 0 && (surface_orientation == 100 || surface_orientation == 111) &&  (p == 0 || p == 1) ) {
            system_lattice[x][y][0][p].kind     = fixed_atom_kind;
            system_lattice[x][y][0][p].element  = lattice_element;
            system_lattice[x][y][0][p].nr_1nn   = max_1nn;		// Fixed atoms are fully bonded.
            system_lattice[x][y][0][p].nr_2nn   = 5;
          }
          else if (z == 0 && surface_orientation == 110) {
            system_lattice[x][y][0][p].kind     = fixed_atom_kind;
            system_lattice[x][y][0][p].element  = lattice_element;
            system_lattice[x][y][0][p].nr_1nn   = max_1nn;		// Fixed atoms are fully bonded.
            system_lattice[x][y][0][p].nr_2nn   = 5;
          }
        }
        else if (crystal == bcc) {
          if (z == 0 && surface_orientation == 100 &&  p == 0 ) {
            system_lattice[x][y][0][p].kind     = fixed_atom_kind;
            system_lattice[x][y][0][p].element  = lattice_element;
            system_lattice[x][y][0][p].nr_1nn   = max_1nn;		// Fixed atoms are fully bonded.
            system_lattice[x][y][0][p].nr_2nn = 5;
          }
          else if (z == 0 && surface_orientation == 110 && (p == 0 || p == 1 )) {
            system_lattice[x][y][0][p].kind     = fixed_atom_kind;
            system_lattice[x][y][0][p].element  = lattice_element;
            system_lattice[x][y][0][p].nr_1nn   = max_1nn;		// Fixed atoms are fully bonded.
            system_lattice[x][y][0][p].nr_2nn   = 5;
          }
          else if (z == 0 && surface_orientation == 111 && (p == 0 || p == 1 || p == 7 || p == 2 || p == 6 || p == 8) ) {
            system_lattice[x][y][0][p].kind     = fixed_atom_kind;
            system_lattice[x][y][0][p].element  = lattice_element;
            system_lattice[x][y][0][p].nr_1nn   = max_1nn;		// Fixed atoms are fully bonded.
            system_lattice[x][y][0][p].nr_2nn   = 5;
          }
        }
      }
    }
  }
}


/* Make top empty layer account for non-periodic boundary. */
void fix_top_empty_layer() {
  Pvector q,v;
  int nn;
  q.z = dimension.z - 1;
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.p = 0; q.p < dimension.p; q.p++) {
        if (crystal == fcc) {
          if (surface_orientation == 100 && (q.p == 2 || q.p == 3) ) {
            system_lattice[q.x][q.y][q.z][q.p].kind     = fixed_empty_kind;
            system_lattice[q.x][q.y][q.z][q.p].element  = vacancy_element; 
            system_lattice[q.x][q.y][q.z][q.p].nr_1nn   = 0;
            system_lattice[q.x][q.y][q.z][q.p].nr_2nn   = 0;
            nr.added_fixed_empty++;
          }
          else if (surface_orientation == 111 && (q.p == 4 || q.p == 5) ) {
            system_lattice[q.x][q.y][q.z][q.p].kind     = fixed_empty_kind;
            system_lattice[q.x][q.y][q.z][q.p].element  = vacancy_element;
            system_lattice[q.x][q.y][q.z][q.p].nr_1nn   = 0;
            system_lattice[q.x][q.y][q.z][q.p].nr_2nn   = 0; 
            nr.added_fixed_empty++;
          }
          else if (surface_orientation == 110) {
            system_lattice[q.x][q.y][q.z][q.p].kind     = fixed_empty_kind;
            system_lattice[q.x][q.y][q.z][q.p].element  = vacancy_element;
            system_lattice[q.x][q.y][q.z][q.p].nr_1nn   = 0;
            system_lattice[q.x][q.y][q.z][q.p].nr_2nn   = 0;
            nr.added_fixed_empty++;
          }
        }
        else if (crystal == bcc) {
          if (surface_orientation == 100 && q.p == 1) {
            system_lattice[q.x][q.y][q.z][q.p].kind     = fixed_empty_kind;
            system_lattice[q.x][q.y][q.z][q.p].element  = vacancy_element;
            system_lattice[q.x][q.y][q.z][q.p].nr_1nn   = 0;
            system_lattice[q.x][q.y][q.z][q.p].nr_2nn   = 0;
            nr.added_fixed_empty++;
          }
          else if (surface_orientation == 110 && (q.p == 2 || q.p == 3)) {
            system_lattice[q.x][q.y][q.z][q.p].kind     = fixed_empty_kind;
            system_lattice[q.x][q.y][q.z][q.p].element  = vacancy_element;
            system_lattice[q.x][q.y][q.z][q.p].nr_1nn   = 0;
            system_lattice[q.x][q.y][q.z][q.p].nr_2nn   = 0;
            nr.added_fixed_empty++;
          }
          else if (surface_orientation == 111 && (q.p == 3 || q.p == 4 || q.p == 5 || q.p == 9 || q.p == 10 || q.p == 11)) {
            system_lattice[q.x][q.y][q.z][q.p].kind     = fixed_empty_kind;
            system_lattice[q.x][q.y][q.z][q.p].element  = vacancy_element;
            system_lattice[q.x][q.y][q.z][q.p].nr_1nn   = 0;
            system_lattice[q.x][q.y][q.z][q.p].nr_2nn   = 0;
            nr.added_fixed_empty++;
          }
        }
	
        // Check for NN vacancies of the sinks to be removed
        // These vacancies have been created before the non-PBC were applied.
        if ( system_lattice[q.x][q.y][q.z][q.p].kind == fixed_empty_kind ) {
          for (nn = 0; nn < max_1nn; nn++) {
            v   = omega_position_of_1nn(q,nn);
            v.z = q.z + vector_1nn[q.p][nn][2];      // No periodic boundary condition in z direction.
            if (v.z < dimension.z) {
              if (system_lattice[v.x][v.y][v.z][v.p].kind == vacancy_kind) {
                system_lattice[v.x][v.y][v.z][v.p].kind     = empty_kind;
                system_lattice[v.x][v.y][v.z][v.p].element  = vacancy_element; 
              }   
            }
          }
        }
      }
    }
  }
}



/* Make cube of dimension (X,Y,Z)*a^3, first layer at z = 4 */
void make_cube(int X, int Y, int Z) {
  int cube_x0, cube_y0, cube_z0;
  int tmp_x;
  int added_adatoms = 0;
  int top_layer;
  double max_cube_z = 0.0;
  Pvector q,q_tmp;
  Vector v;
  
//   float p0[] = {0.0,0.0,0.0};
//   float p1[] = {0.5,0.5,0.0};
//   float p2[] = {0.0,0.5,0.5};
//   float p3[] = {0.5,0.0,0.5};
  
  // Corner cube at coordinates
  cube_x0 = dimension.x/2 - X/2;
  cube_y0 = dimension.y/2 - Y/2;
  
  if (surface_z == 0) {
    cube_z0 = dimension.z/2 - Z/2;
  }
  else {
    // If there is a surface, put the cube on the surface.
    cube_z0 = surface_z+1;
  }
  
  printf("# Cube corner at (%i,%i,%i)\n",cube_x0,cube_y0,cube_z0);
  
  // Check dimensions of the cube and system
  tmp_x = cube_x0 + X-1;
  if (tmp_x > dimension.x) {
    printf("ERROR Cube X too large. X_max = %i > %i\n",tmp_x, dimension.x);
    exit(1);
  }
  if (cube_y0 + Y-1 > dimension.y) {
    printf("ERROR Cube Y too large\n");
    exit(1);
  }  
  if (cube_z0 + Z-1 > dimension.z) {
    printf("ERROR Cube Z too large\n");
    exit(1);
  }
  
  for (q.x=cube_x0; q.x<cube_x0+X; q.x++) {
    for (q.y=cube_y0; q.y<cube_y0+Y; q.y++) {
      for (q.z=cube_z0; q.z<cube_z0+Z; q.z++) {
        for (q.p=0; q.p<dimension.p; q.p++) {
      // 	  if (y-cube_y0 > x - cube_x0) {
            if (system_lattice[q.x][q.y][q.z][q.p].kind == empty_kind) {
              system_lattice[q.x][q.y][q.z][q.p].kind = adatom_kind;
              system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;
              added_adatoms++;
              if (height_3d(q) >= max_cube_z) {
                max_cube_z = height_3d(q);	// Top layer z of cube in [m]
                top_layer = q.z;
              }
            }
      // 	    else if(system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind){
      //   // 	    system_lattice[x][y][z][2].kind = adatom_kind;
      // 	      system_lattice[q.x][q.y][cube_z0+Z][q.p].kind = adatom_kind;
      // 	      added_adatoms++;
      // 	      q_tmp.x = q.x;
      // 	      q_tmp.y = q.y;
      // 	      q_tmp.z = cube_z0+Z;
      // 	      v = xyzp_to_xyz(q_tmp,1.0);
      // 	      if (v.z >= max_cube_z) {
      // 		max_cube_z = v.z*unit_cell_dimension.z*a0;	// Top layer z of cube + adatom in [m]
      // 	      }
      // 	    }
      // 	  }
        }
      }
    }
  }
  
  printf("# Added atoms to a cube: %i; Top layer at %i uc = %le m.\n",added_adatoms,top_layer,max_cube_z);
}


/* Make an octahedron with a "diameter" d [m], truncated to diameter t<=d [m], and height h [m] above the surface */
void make_truncated_octahedron(double d, double t, double h) {
  Pvector q;
  Vector v;
  int Rx,Ry,Rz, H;
  int tmp_x;
  int added_adatoms = 0;
  Pvector octahedron_centre;
  Vector v_octahedron_centre;
  double r = d/2.0;
  double rt = t/2.0;
  int Z;	// Lowest xyzp coordinate of the sphere.
  
  octahedron_centre.x = dimension.x/2;
  octahedron_centre.y = dimension.y/2;
  octahedron_centre.p = 0;	// Arbitrary
  
  Rx = ceil(r/(unit_cell_dimension.x*a0));
  Ry = ceil(r/(unit_cell_dimension.y*a0));
  Rz = ceil(r/(unit_cell_dimension.z*a0));
  H  = ceil(h/(unit_cell_dimension.z*a0));
  
  if (surface_z == 0) {
    octahedron_centre.z = dimension.z/2;
    Z = octahedron_centre.z - Rz;	// Full free suspended octahedron
  }
  else {
    // If there is a surface, put the "hemi-octahedron" on the surface.
    octahedron_centre.z = surface_z + 1 - (Rz - H);
    Z = octahedron_centre.z;	// "Hemi-octahedron" on the surface.
  }
  
  
  
  // Check
  if (Rx > dimension.x/2 || Ry > dimension.y/2 || Rz > dimension.z/2) {
    printf("WARNING Octahedron larger than box size\n");
  }
  
  v_octahedron_centre = xyzp_to_xyz(octahedron_centre,a0); // Octahedron centre in 3D coordinates
  
  // Create cylinder in 4D
  for (q.x = fmax(octahedron_centre.x - Rx, 0); q.x <= fmin(octahedron_centre.x + Rx, dimension.x); q.x++) {
    for (q.y = fmax(octahedron_centre.y - Ry, 0); q.y <= fmin(octahedron_centre.y + Ry, dimension.y); q.y++) {
      for (q.z = fmax(Z - Rz, 0); q.z <= fmin(octahedron_centre.z + Rz, dimension.z); q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          v =  xyzp_to_xyz(q,a0);
          /* Only create atoms inside a octahedron with radius r; calculated in 3D */

          if (  fabs(v.x - v_octahedron_centre.x)
              + fabs(v.y - v_octahedron_centre.y)
              + fabs(v.z - v_octahedron_centre.z) <= r
              && fabs(v.x - v_octahedron_centre.x) <= rt
              && fabs(v.y - v_octahedron_centre.y) <= rt
              && fabs(v.z - v_octahedron_centre.z) <= rt) {
            
            /* Print atoms in 4D */
            if (system_lattice[q.x][q.y][q.z][q.p].kind == empty_kind) {
              system_lattice[q.x][q.y][q.z][q.p].kind = adatom_kind;
              system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;
              added_adatoms++;
            }
            else if(system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind){
              system_lattice[q.x][q.y][octahedron_centre.z + Rz][q.p].kind = adatom_kind;
              system_lattice[q.x][q.y][octahedron_centre.z + Rz][q.p].element = lattice_element;
              added_adatoms++;
            }
          }
        }
      }
    }
  }
  printf("# Added atoms to a truncated octahedron: %i\n",added_adatoms);
}

/* Make cube of dimension (X,Y,Z)*a^3, first layer at z = 4 */
void make_cube_m(double x, double y, double z) {
  int cube_x0, cube_y0, cube_z0;
  int tmp_x;
  int added_adatoms = 0;
  int top_layer;
  double max_cube_z = 0.0;
  Pvector q,q_tmp;
  Vector v	;
  int X, Y, Z;
  
  X = floor(x/(unit_cell_dimension.x*a0));
  Y = floor(y/(unit_cell_dimension.y*a0));
  Z = floor(z/(unit_cell_dimension.z*a0));
  
  // Check
  x = X*unit_cell_dimension.x*a0;
  y = Y*unit_cell_dimension.y*a0;
  z = Z*unit_cell_dimension.z*a0;
  
  printf("# Cube_m: %i %i %i [uc] = %le %le %le [m]\n", X,Y,Z, x,y,z);
  
//   float p0[] = {0.0,0.0,0.0};
//   float p1[] = {0.5,0.5,0.0};
//   float p2[] = {0.0,0.5,0.5};
//   float p3[] = {0.5,0.0,0.5};
  
  // Corner cube at coordinates
  cube_x0 = dimension.x/2 - X/2;
  cube_y0 = dimension.y/2 - Y/2;
  
  if (surface_z == 0) {
    cube_z0 = dimension.z/2 - Z/2;
  }
  else {
    // If there is a surface, put the cube on the surface.
    cube_z0 = surface_z+1;
  }
  
  printf("# Cube corner at (%i,%i,%i)\n",cube_x0,cube_y0,cube_z0);
  
  // Check dimensions of the cube and system
  tmp_x = cube_x0 + X-1;
  if (tmp_x > dimension.x) {
    printf("ERROR Cube X too large. X_max = %i > %i\n",tmp_x, dimension.x);
    exit(1);
  }
  if (cube_y0 + Y-1 > dimension.y) {
    printf("ERROR Cube Y too large\n");
    exit(1);
  }  
  if (cube_z0 + Z-1 > dimension.z) {
    printf("ERROR Cube Z too large\n");
    exit(1);
  }
  
  for (q.x = cube_x0; q.x < cube_x0 + X; q.x++) {
    for (q.y = cube_y0; q.y < cube_y0 + Y; q.y++) {
      for (q.z = cube_z0; q.z < cube_z0 + Z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          if (system_lattice[q.x][q.y][q.z][q.p].kind == empty_kind) {
            system_lattice[q.x][q.y][q.z][q.p].kind = adatom_kind;
            system_lattice[q.x][q.y][q.z][q.p].element = lattice_element; 
            added_adatoms++;
            if (height_3d(q) >= max_cube_z) {
              max_cube_z = height_3d(q);	// Top layer z of cube in [m]
              top_layer = q.z;
              
            }
          }
        }
      }
    }
  }
  
  printf("# Added atoms to a cube: %i; Top layer at %i uc = %le m.\n",added_adatoms,top_layer,max_cube_z);
}


/* Make a V cube of dimension (X,Y,Z)*a^3 */
void make_cubic_void(int X, int Y, int Z) {
  int cube_x0, cube_y0, cube_z0;
  int x,y,z,p;
  int tmp_x;
  int removed_atoms = 0;
  
//   float p0[] = {0.0,0.0,0.0};
//   float p1[] = {0.5,0.5,0.0};
//   float p2[] = {0.0,0.5,0.5};
//   float p3[] = {0.5,0.0,0.5};
  
  // Corner cube at coordinates
  cube_x0 = dimension.x/2 - X/2;
  cube_y0 = dimension.y/2 - Y/2;
  cube_z0 = dimension.z/2 - Z/2;
  printf("# Cube corner at (%i,%i,%i)\n",cube_x0,cube_y0,cube_z0);
  
  // Check dimensions of the cube and system
  tmp_x = cube_x0 + X-1;
  if (tmp_x > dimension.x) {
    printf("ERROR Cube X too large. X_max = %i > %i\n",tmp_x, dimension.x);
    exit(1);
  }
  if (cube_y0 + Y-1 > dimension.y) {
    printf("ERROR Cube Y too large\n");
    exit(1);
  }  
  if (cube_z0 + Z-1 > dimension.z) {
    printf("ERROR Cube Z too large\n");
    exit(1);
  }
  
  for (x=cube_x0; x<cube_x0+X; x++) {
    for (y=cube_y0; y<cube_y0+Y; y++) {
      for (z=cube_z0; z<cube_z0+Z; z++) {
        for (p=0; p<dimension.p; p++) {
            if (system_lattice[x][y][z][p].kind == adatom_kind || system_lattice[x][y][z][p].kind == atom_kind) {
              system_lattice[x][y][z][p].kind = empty_kind;
              removed_atoms++;
            }
            else {
              system_lattice[x][y][cube_z0+Z][p].kind		= empty_kind;
              system_lattice[x][y][cube_z0+Z][p].element 	= vacancy_element;
            }
        }
      }
    }
  }
  printf("# Removed atoms in a cubic void: %i\n",removed_atoms);
}



/* Make a cubic void with dimensions [m] x,y,z */
void make_cubic_void_m(double x, double y, double z) {
  int cube_x0, cube_y0, cube_z0;
  int tmp_x;
  int top_layer;
  double max_cube_z = 0.0;
  Pvector q,q_tmp;
  Vector v	;
  int X, Y, Z;
  int removed_atoms = 0;
  
  X = floor(x/(unit_cell_dimension.x*a0));
  Y = floor(y/(unit_cell_dimension.y*a0));
  Z = floor(z/(unit_cell_dimension.z*a0));
  
  // Check
  x = X*unit_cell_dimension.x*a0;
  y = Y*unit_cell_dimension.y*a0;
  z = Z*unit_cell_dimension.z*a0;
  
  printf("# Cube_m: %i %i %i [uc] = %le %le %le [m]\n", X,Y,Z, x,y,z);
  
  // Corner cube at coordinates
  cube_x0 = dimension.x/2 - X/2;
  cube_y0 = dimension.y/2 - Y/2;
  cube_z0 = dimension.z/2 - Z/2;

  printf("# Cube corner at (%i,%i,%i)\n",cube_x0,cube_y0,cube_z0);
  
  // Check dimensions of the cube and system
  tmp_x = cube_x0 + X-1;
  if (tmp_x > dimension.x) {
    printf("ERROR Cube X too large. X_max = %i > %i\n",tmp_x, dimension.x);
    exit(1);
  }
  if (cube_y0 + Y-1 > dimension.y) {
    printf("ERROR Cube Y too large\n");
    exit(1);
  }  
  if (cube_z0 + Z-1 > dimension.z) {
    printf("ERROR Cube Z too large\n");
    exit(1);
  }
  
  for (q.x = cube_x0; q.x < cube_x0+X; q.x++) {
    for (q.y = cube_y0; q.y < cube_y0+Y; q.y++) {
      for (q.z = cube_z0; q.z < cube_z0+Z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind ||
              system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind) {
            system_lattice[q.x][q.y][q.z][q.p].kind	= empty_kind;
            system_lattice[q.x][q.y][q.z][q.p].element 	= vacancy_element;
            removed_atoms++;
          }
        }
      }
    }
  }
  
  printf("# Added V to a cube: %i.\n",removed_atoms);
}


/* Make a cylinder with a radius r [m] and height h [m] */
void make_cylinder(double r, double h, int axis) {
  int cube_x0, cube_y0, cube_z0;
  Pvector q;
  Vector v;
  int Rx, Ry, Rz, Y, Z;
  int tmp_x;
  int added_adatoms = 0;
  Pvector cylinder_centre;
  Vector v_cube_corner, v_cylinder_centre;
  
  cylinder_centre.x = dimension.x/2;
  cylinder_centre.y = dimension.y/2;
  cylinder_centre.p = 0;	// Arbitrary
  
  /* Vertical cylinder */
  if (axis == 0) {
    if (surface_z == 0) {
      cylinder_centre.z = 0;
    }
    else {
      // If there is a surface, put the cube on the surface.
      cylinder_centre.z = surface_z + 1;
    }
  }
  
  /* Longitudinal cylinder */
  else if (axis == 1) {
    cylinder_centre.z = dimension.z/2;
    cylinder_centre.y = 0;
  }
    
  if (axis == 0) {
    Rx = ceil(r/(unit_cell_dimension.x*a0));
    Ry = ceil(r/(unit_cell_dimension.y*a0));
    Z  = ceil(h/(unit_cell_dimension.z*a0));
    printf("# h = %e, Z = %i\n",h,Z);
  }
  else if (axis == 1) {
    Rx = ceil(r/(unit_cell_dimension.x*a0));
    Y = ceil(h/(unit_cell_dimension.y*a0));
    Rz  = ceil(r/(unit_cell_dimension.z*a0));
    printf("# h = %e, Y = %i\n",h,Z);
    
    // Offset of y wire from the z wire
    cylinder_centre.x = cylinder_centre.x + ceil(2*r/(unit_cell_dimension.x*a0));
  }
  
  
  // Check
  if (axis == 0) {
    if (cylinder_centre.x + Rx > dimension.x || cylinder_centre.y + Ry > dimension.y) {
      printf("ERROR Cylinder larger than box size\n");
      exit(1);
    }
  }
  else if (axis == 1) {
    if (cylinder_centre.x + Rx > dimension.x || cylinder_centre.z + Rz > dimension.z) {
      printf("ERROR Cylinder larger than box size\n");
      exit(1);
    }
  }
  
  v_cylinder_centre = xyzp_to_xyz(cylinder_centre,a0); // Cylinder centre in 3D coordinates
  
  // Create cylinder in 4D
  if (axis == 0) {
    for (q.x = cylinder_centre.x - Rx; q.x <= cylinder_centre.x + Rx; q.x++) {
      for (q.y = cylinder_centre.y - Ry; q.y <= cylinder_centre.y + Ry; q.y++) {
        for (q.z = cylinder_centre.z; q.z <= cylinder_centre.z + Z; q.z++) {
          for (q.p = 0; q.p < dimension.p; q.p++) {
            v =  xyzp_to_xyz(q,a0);
            // Only create atoms inside a cylinder of radius r; calculated in 3D
            if ( sqrt((v.x - v_cylinder_centre.x)*(v.x - v_cylinder_centre.x) + (v.y - v_cylinder_centre.y)*(v.y - v_cylinder_centre.y)) <= r) {
              
              /* Print atoms in 4D */
              if (system_lattice[q.x][q.y][q.z][q.p].kind == empty_kind) {
                system_lattice[q.x][q.y][q.z][q.p].kind = adatom_kind;
                system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;
                added_adatoms++;
              }
              else if(system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind){
                system_lattice[q.x][q.y][cylinder_centre.z+Z][q.p].kind = adatom_kind;
                system_lattice[q.x][q.y][cylinder_centre.z+Z][q.p].element = lattice_element;
                added_adatoms++;
              }
            }
          }
        }
      }
    }
  }
  else if (axis == 1) {
    for (q.z = cylinder_centre.z - Rz; q.z <= cylinder_centre.z + Rz; q.z++) {
      for (q.x = cylinder_centre.x - Rx; q.x <= cylinder_centre.x + Rx; q.x++) {
        for (q.y = cylinder_centre.y; q.y <= cylinder_centre.y + Y; q.y++) {
          for (q.p = 0; q.p < dimension.p; q.p++) {
            v =  xyzp_to_xyz(q,a0);
            // Only create atoms inside a cylinder of radius r; calculated in 3D
            if ( sqrt((v.x - v_cylinder_centre.x)*(v.x - v_cylinder_centre.x) + (v.z - v_cylinder_centre.z)*(v.z - v_cylinder_centre.z)) <= r) {
              
              /* Print atoms in 4D */
              if (system_lattice[q.x][q.y][q.z][q.p].kind == empty_kind) {
                system_lattice[q.x][q.y][q.z][q.p].kind = adatom_kind;
                system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;
                added_adatoms++;
              }
              else if(system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind){
                system_lattice[q.x][cylinder_centre.y+Y][q.z][q.p].kind = adatom_kind;
                system_lattice[q.x][cylinder_centre.y+Y][q.z][q.p].element = lattice_element;
                added_adatoms++;
              }
            }
          }
        }
      }
    }
  }
  printf("# Added atoms to a cylinder: %i\n",added_adatoms);
}




/* Make a sphere with a diameter d [m] and height h [m] above surface */
void make_sphere(double d, double h) {
  int cube_x0, cube_y0, cube_z0;
  Pvector q;
  Vector v;
  int Rx,Ry,Rz, H;
  int tmp_x;
  int added_adatoms = 0;
  Pvector sphere_centre;
  Vector v_sphere_centre;
  double r = d/2.0;
  int Z;	// Lowest xyzp coordinate of the sphere.
  
  sphere_centre.x = dimension.x/2;
  sphere_centre.y = dimension.y/2;
  sphere_centre.p = 0;	// Arbitrary
  
  Rx = ceil(r/(unit_cell_dimension.x*a0));
  Ry = ceil(r/(unit_cell_dimension.y*a0));
  Rz = ceil(r/(unit_cell_dimension.z*a0));
  H  = ceil(h/(unit_cell_dimension.z*a0));
  
  if (surface_z == 0) {
    sphere_centre.z = dimension.z/2;
    Z = sphere_centre.z - Rz;	// Full free suspended sphere
  }
  else {
    // If there is a surface, put the hemisphere on the surface.
    sphere_centre.z = surface_z + 1 - (Rz - H);
    Z = sphere_centre.z;	// Hemisphere on the surface.
  }
  
  
  
  // Check
  if (Rx > dimension.x/2 || Ry > dimension.y/2 || Rz > dimension.z/2) {
    printf("WARNING Sphere larger than box size\n");
  }
  
  v_sphere_centre = xyzp_to_xyz(sphere_centre,a0); // Cylinder centre in 3D coordinates
  
  // Create cylinder in 4D
  for (q.x = fmax(sphere_centre.x - Rx, 0); q.x <= fmin(sphere_centre.x + Rx, dimension.x); q.x++) {
    for (q.y = fmax(sphere_centre.y - Ry, 0); q.y <= fmin(sphere_centre.y + Ry, dimension.y); q.y++) {
      for (q.z = fmax(Z - Rz, 0); q.z <= fmin(sphere_centre.z + Rz, dimension.z); q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          v =  xyzp_to_xyz(q,a0);
          /* Only create atoms inside a sphere with radius r; calculated in 3D */

          if (  (v.x - v_sphere_centre.x)*(v.x - v_sphere_centre.x)
              + (v.y - v_sphere_centre.y)*(v.y - v_sphere_centre.y)
              + (v.z - v_sphere_centre.z)*(v.z - v_sphere_centre.z) <= r*r) {
            
            /* Print atoms in 4D */
            if (system_lattice[q.x][q.y][q.z][q.p].kind == empty_kind) {
              system_lattice[q.x][q.y][q.z][q.p].kind = adatom_kind;
              system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;
              added_adatoms++;
            }
            else if(system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind){
              system_lattice[q.x][q.y][sphere_centre.z + Rz][q.p].kind = adatom_kind;
              system_lattice[q.x][q.y][sphere_centre.z + Rz][q.p].element = lattice_element;
              added_adatoms++;
            }
          }
        }
      }
    }
  }
  printf("# Added atoms to a sphere: %i\n",added_adatoms);
}


/* Make a ridge with height h [m] */
void make_ridge(double h) {
  int cube_x0, cube_y0, cube_z0;
  int tmp_x;
  int top_layer;
  double max_cube_z = 0.0;
  Pvector q,q_tmp;
  Vector v,s;
  int X, Y, Z;
  int added_adatoms = 0;
  
  q.x = dimension.x;
  q.y = 0;
  q.z = surface_z + 1;
  q.p = 0;
  s = xyzp_to_xyz(q,a0);	// s.x  = surface height in [m], s.x = box width in [m]

  printf("# Cube corner at (%i,%i,%i)\n",cube_x0,cube_y0,cube_z0);
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = surface_z + 1; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          v =  xyzp_to_xyz(q,a0);
          if (v.z <= -v.x*unit_cell_dimension.z + s.z + h + s.x/2.0*unit_cell_dimension.z && v.z <= v.x*unit_cell_dimension.z + s.z + h - s.x/2.0*unit_cell_dimension.z) {
            if (system_lattice[q.x][q.y][q.z][q.p].kind == empty_kind) {
              system_lattice[q.x][q.y][q.z][q.p].kind = adatom_kind;
              system_lattice[q.x][q.y][q.z][q.p].element = lattice_element; 
              added_adatoms++;
            }
          }
        }
      }
    }
  }
  
  printf("# Added atoms to a ridge: %i.\n",added_adatoms);
}


/* Make cubic island of dimension (X,Y)*a^2; first layer at z = 4 */
void make_island(int X, int Y) {
  int cube_x0, cube_y0;
  int x,y,z,p;
  int tmp_x;
  int added_adatoms = 0;
  
//   float p0[] = {0.0,0.0,0.0};
//   float p1[] = {0.5,0.5,0.0};
//   float p2[] = {0.0,0.5,0.5};
//   float p3[] = {0.5,0.0,0.5};
  
  // Corner cube at coordinates
  cube_x0 = dimension.x/2 - X/2;
  cube_y0 = dimension.y/2 - Y/2;
//   printf("island corner at (%i,%i,%i)\n",cube_x0,cube_y0,cube_z0);
  
  // Check dimensions of the cube and system
  tmp_x = cube_x0 + X-1;
  if (tmp_x > dimension.x) {
    printf("ERROR island X too large. X_max = %i > %i\n",tmp_x, dimension.x);
    exit(1);
  }
  if (cube_y0 + Y-1 > dimension.y) {
    printf("ERROR island Y too large\n");
    exit(1);
  }  
  
  
  
  
  for (x = cube_x0; x < cube_x0 + X; x++) {
    for (y = cube_y0; y < cube_y0 + Y; y++) {
      if (surface_orientation == 100 || surface_orientation == 111) {
        for (p = 0; p < 2; p++) {
          if (y - cube_y0 > x - cube_x0) { 
            if (system_lattice[x][y][surface_z+1][p].kind == empty_kind) {
              system_lattice[x][y][surface_z+1][p].kind = adatom_kind;
              system_lattice[x][y][surface_z+1][p].element = lattice_element;
              added_adatoms++;
            }
            else if(system_lattice[x][y][surface_z+1][p].kind == adatom_kind){
              system_lattice[x][y][surface_z][2].kind = adatom_kind;
              system_lattice[x][y][surface_z][2].element = lattice_element;
              added_adatoms++;
            }
          }
        }
      }
      else {
        printf("ERROR the island structure is not implemented for (110) surface\n");
        exit(1);
      }
    }
  }
  printf("# Added atoms to an island: %i\n",added_adatoms);
}


/* Function to add N adatoms on the surface.
 */
void add_adatoms(int N) {
  int i;
  Pvector q, q0;
  int event_found = 0;
  int point_counter = 0;
  int nr_surface_vacancies = 0;
  Vector E;
  
  E.x = 0.0;
  E.y = 0.0;
  E.z = 0.0;
  
  while (N > 0) {
    event_found = 0;
    // Count all surface vacancies
    nr_surface_vacancies = 0;
    for (q.x = 0; q.x < dimension.x; q.x++) {
      for (q.y = 0; q.y < dimension.y; q.y++) {
        for (q.z = 0; q.z < dimension.z; q.z++) {
          for (q.p = 0; q.p < dimension.p; q.p++) {
            if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {
              nr_surface_vacancies++;
            }
          }
        }
      }
    }
  
    i = random_n(nr_surface_vacancies-1)+1; // Change vacancy i into adatom	
    point_counter = 0;
    q0.x = q0.y = q0.z = q0.p = 0;
    while (q0.x < dimension.x && event_found == 0) {
      q0.y = 0;
      while (q0.y < dimension.y && event_found == 0) {
        q0.z = 0;
        while (q0.z < dimension.z && event_found == 0) {
          q0.p = 0;
          while (q0.p < dimension.p && event_found == 0) {
            if (system_lattice[q0.x][q0.y][q0.z][q0.p].kind == vacancy_kind) {
              point_counter++;
              if (point_counter == i) {
                event_found = 1;	// Will stop the search for events.
            
                /* Convert vacancy into adatom */
                new_adatom(q0);
                // 		printf("# New adatom at (%i %i %i %i)\n",q0.x,q0.y,q0.z,q0.p);
                if (tag.calculate_diffusion_coefficient == 1) {
                  system_lattice[q0.x][q0.y][q0.z][q0.p].label = label_diffusion;
                  // 		  printf("Adding diffusion label\n");
                }
                
                update_bond_count(q0); // Needed before updating the field
#ifdef FIELD
                /* Recalculate the field*/
                if (tag.use_field == 1 && tag.calculate_diffusion_coefficient == 1){ 
                  /* The field needs to be recalculated if it is a diffusion calculation, but not in a normal calcaulation
                   * where the funciton is used to add random atoms and the field will be calculated later before the KMC simualtion starts.
                   */
                  electric_field_calculation_step++;
                  
                  /* Optimization for the apex (Only works with LibHelmod solver)*/
                  if (electric_field_calculation_step > global_electric_field_recalculation_frequency) {
                    electric_field_calculation_step = 1;  // The electric field will be calculated for the whole system
                  }
                  
                  calculate_field_for_full_system();
                  
                }
                
                /* Make sure the gradients around the last event is always updated */
                if (tag.use_field == 1) {
                  add_gradient(q0);
                }
#endif // FIELD
                
                add_probabilities_locally(q0);
              }
            }
            q0.p++;
          }
          q0.z++;
        }
        q0.y++;
      }
      q0.x++;
    }
    N--;
  }
}


/* Add adatom at coordinate (x,y,z,p) */
void add_adatom(Pvector q, int label) {
  
  /* Print the field at the vacancy before adding the adatom (only for diffusion simulations */
  if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {
    if (tag.calculate_diffusion_coefficient == 1) {
      printf("%e Initial field at the diffusion starting point: %e V/m\n",kmc_run,system_lattice[q.x][q.y][q.z][q.p].scalar_field);
    }
  }
  
  new_adatom(q);
  printf("# Added adatom at (%i,%i,%i,%i)\n",q.x,q.y,q.z,q.p);
  
  if (tag.calculate_diffusion_coefficient == 1 || label == 1) {
    system_lattice[q.x][q.y][q.z][q.p].label = label_diffusion;
  }
  else if (label > 0) {
    system_lattice[q.x][q.y][q.z][q.p].label = label;
  }
  
  update_bond_count(q); // Needed to be updated before calculating the field.
  
#ifdef FIELD
  /* Recalculate the field*/
  if (tag.use_field == 1 && tag.calculate_diffusion_coefficient == 1){ 
    /* The field needs to be recalculated if it is a diffusion calculation, but not in a normal calcaulation
     * where the funciton is used to add random atoms and the field will be calculated later bfore the KMC simulation starts.
     */
    electric_field_calculation_step++;
    
    /* Optimization for the apex (Only works with LibHelmod solver)*/
    if (electric_field_calculation_step > global_electric_field_recalculation_frequency) {
      electric_field_calculation_step = 1;  // The electric field will be calculated for the whole system
    }
    
    calculate_field_for_full_system();
    
  }
  
  
  /* Make sure the gradients around the last event is always updated */
  if (tag.use_field == 1) {
    add_gradient(q);
  }   
#endif // FIELD
  
  
  
  add_probabilities_locally(q);
}


/* Randomly add N vacancies.
 */ 
void add_vacancies(int N) {
  int i;
  Pvector q, q0;
  int event_found = 0;
  int point_counter = 0;
  int nr_atom_objects = 0; // Both adatoms and adatoms
  
  // Count all atoms and adatoms.
  nr_atom_objects = 0;
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          if (system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind || system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
            nr_atom_objects++;
          }
        }
      }
    }
  }
  
//   printf("Nr of atom objects: %i\n",nr_atom_objects);
  
  while (N > 0) {
    event_found = 0;
    i = random_n(nr_atom_objects-1)+1; // Change object i into a vacancy.	
    point_counter = 0;
    q0.x = q0.y = q0.z = q0.p = 0;
    while (q0.x < dimension.x && event_found == 0) {
      q0.y = 0;
      while (q0.y < dimension.y && event_found == 0) {
        q0.z = 0;
        while (q0.z < dimension.z && event_found == 0) {
          q0.p = 0;
          while (q0.p < dimension.p && event_found == 0) {
            if (system_lattice[q0.x][q0.y][q0.z][q0.p].kind == atom_kind || system_lattice[q0.x][q0.y][q0.z][q0.p].kind == adatom_kind) {
              point_counter++;
              if (point_counter == i) {
                event_found = 1;	// Will stop the search for events.
                // 		printf("pc ef %i %i\n",point_counter,event_found);
          
                new_vacancy(q0);  // Convert atom into vacancy
          
                update_bond_count(q0);
                add_probabilities_locally(q0);
              }
            }
            q0.p++;
          }
          q0.z++;
        }
        q0.y++;
      }
      q0.x++;
    }
    N--;
    nr_atom_objects--;
  }
}

/* Function checking the system after the data.in file has been read */
void check_initial_configuration() {
  Pvector q;
  Vector v;
  int index = 0;
  nr.fixed_empty = 0;
  
#ifdef FIELD
  if(tag.use_field == 1) {
   initialize_libhelmod();
  }
#endif // FIELD
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
      
#ifdef FIELD
          if (tag.use_libhelmod == 1) {
            /* Give every point in LibHelmod coordinates (Parcas coordinates) */
            system_lattice[q.x][q.y][q.z][q.p].helmod_xyz = xyzp_to_libhelmod_coordinates(q);
          }
#endif
          
          if (system_lattice[q.x][q.y][q.z][q.p].kind == fixed_empty_kind) {
            nr.fixed_empty++;
          }
          
        }
      }
    }
  }
  
  if (nr.fixed_empty != nr.added_fixed_empty) {
    printf("ERROR Nr of fixed empty object are not the same as nr of added: %i != %i\n",nr.fixed_empty,nr.added_fixed_empty);
//     exit(1);
  }
  
  /* Print par files with the used barriers and possibly the attempt frequencies */
  if (tag.print_par_files == 1) {
    print_1nn_parameters();
    print_2nn_parameters();
    print_3nn_parameters();
    print_5nn_parameters();
  }
  
}

/* mark all atoms, adatoms and fixed atoms in the systems as bulk, which will 
 * not be removed as detached clusters in cluster analysis. */
void mark_free_clusters_as_bulk(){
  Pvector q;
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          if (system_lattice[q.x][q.y][q.z][q.p].element >= 0) { // Atomic elements 
            system_lattice[q.x][q.y][q.z][q.p].label = label_bulk_cluster;
          }
        }
      }
    }
  }
}


/* Read in general parameters from data.in */
void read_general_input() {
  char file_line[500];
  char keyword[500];
  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }
    
    else if (strcmp(keyword,"seed") == 0) {
      sscanf(file_line,"%s %llu %llu %llu %llu", keyword, &seed[0],&seed[1],&seed[2],&seed[3]);
      random_generator_initialization();
    }
    
    else if (strcmp(keyword,"Temperature") == 0) {
      sscanf(file_line,"%s %lf", keyword, &Temperature);
      printf("# Temperature = %lf K\n",Temperature);
    }
    
    else if (strcmp(keyword,"time") == 0) {
      sscanf(file_line,"%s %le", keyword, &simulation_time);
      printf("# Starting time = %g s\n",simulation_time);
    }
    
    else if (strcmp(keyword,"kmc_step") == 0) {
      sscanf(file_line,"%s %le", keyword, &kmc_step);
      printf("# Starting kmc step = %g\n",kmc_step);
    }
  }
}


/* Read in material-specific parameters from data.in */
void read_material_specific_input() {
  char file_line[500];
  char keyword[500];
  char parameter_file_name[100];
  char ann_file_name[100];
  int element;
  int classes;
  int ann_class;
  int committee_member;
  int committee_size;
  int ann_dimension;
  int i,j,k,l;
  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }
    
    else if (strcmp(keyword,"parameter_file") == 0) {
      sscanf(file_line,"%s %s", keyword, parameter_file_name);
      printf("# Reading parameters from %s\n",parameter_file_name);
      initiate_nn_parameter_table();
      read_in_parameter_file(parameter_file_name);
      max_nr_elements++;
      printf("# Nr of elements: %i\n",max_nr_elements);
    }
    
    else if (strcmp(keyword,"parameter_2nn_file") == 0 || strcmp(keyword,"parameter_nnn_file") == 0) {
      sscanf(file_line,"%s %s", keyword, parameter_file_name);
      printf("# Reading 2nn parameters from %s\n",parameter_file_name);
      tag.allow_nnn_jumps = 1;
      initiate_nnn_parameter_table();
      read_in_nnn_parameter_file(parameter_file_name);
    }
    
    else if (strcmp(keyword,"parameter_3nn_file") == 0) {
      sscanf(file_line,"%s %s", keyword, parameter_file_name);
      printf("# Reading 3nn parameters from %s\n",parameter_file_name);
      tag.allow_3nn_jumps = 1;
      initiate_3nn_parameter_table();
      read_in_3nn_parameter_file(parameter_file_name);
    }
    
    // TODO 4nn parameters
    
    else if (strcmp(keyword,"parameter_5nn_file") == 0) {
      sscanf(file_line,"%s %s", keyword, parameter_file_name);
      printf("# Reading 5nn parameters from %s\n",parameter_file_name);
      tag.allow_5nn_jumps = 1;
      initiate_5nn_parameter_table();
      read_in_5nn_parameter_file(parameter_file_name);
    }
    
    else if (strcmp(keyword,"parameter_26D_file") == 0 || strcmp(keyword,"parameter_26d_file") == 0) {
      sscanf(file_line,"%s %s", keyword, parameter_file_name);
      printf("# Reading parameters from %s\n",parameter_file_name);
      tag.using_26D_parameters = 1;
      initiate_26D_parameter_table();
      read_in_26D_parameter_file(parameter_file_name);
      max_nr_elements++;
      printf("# Nr of elements: %i\n",max_nr_elements);
    }
    
    else if (strcmp(keyword,"ann_structure") == 0) {
      sscanf(file_line,"%s %d %d %d %d", keyword, &element, &classes, &committee_size, &max_s);
      if (max_s == 26){
        tag.using_26D_parameters = 1;
      }else if (max_s == 72){
        tag.using_72D_parameters = 1;
      }else{
        printf("ERROR ANN input dimensionality %d not implemented.\n",max_s);
        exit(1);
      }
      if (tag.using_ann == 0){
        allocate_desc_arrays();
      }
      tag.using_ann = 1;
      initiate_regressor(element, classes, committee_size);
      max_nr_elements++;
      printf("# Nr of elements: %i\n",max_nr_elements);
    }

    else if (strcmp(keyword,"ann_classifier_file") == 0) {
      sscanf(file_line,"%s %d %s", keyword, &element, ann_file_name);
      printf("# Reading ANN from %s\n",ann_file_name);
      tag.using_classifier = 1;
      read_in_ann_classifier(element, ann_file_name);
    }
    
    else if (strcmp(keyword,"ann_regressor_file") == 0) {
      sscanf(file_line,"%s %d %d %d %s", keyword, &element, &ann_class, &committee_member, ann_file_name);
      printf("# Reading ANN from %s\n",ann_file_name);
      ann_dimension = read_in_ann_regressor(element, ann_class, committee_member, ann_file_name);
      if (ann_dimension != max_s){
        printf("ERROR ANN regessor dimensionality %d doesn't match %d\n",ann_dimension,max_s);
        exit(1);
      }
    }
    
    else if (strcmp(keyword,"fcc") == 0) {
      printf("# Using fcc structure\n");
      crystal = fcc;
    }
    
    else if (strcmp(keyword,"bcc") == 0) {
      printf("# Using bcc structure\n");
      crystal = bcc;
    }   
    
    /* The lattice parameter. */
    else if (strcmp(keyword,"a0") == 0 || strcmp(keyword,"lattice_parameter") == 0 ) {
      sscanf(file_line,"%s %le", keyword, &a0);
      printf("# a0 = %e m\n",a0);
    }
    
    else if (strcmp(keyword,"attempt_frequency") == 0 || strcmp(keyword,"attempt_frequency_1nn") == 0) {
      sscanf(file_line,"%s %le", keyword, &attempt_frequency_1nn);
      tag.global_attempt_frequency_1nn = 1;
      printf("# Global attempt frequency = %e s^-1\n",attempt_frequency_1nn);
      
      for (i = 0; i <= max_1nn; i++){
        for (j = 0;j <= max_2nn; j++){
          for (k = 0; k<= max_1nn; k++){
            for (l = 0; l <= max_2nn; l++){
              j1nn_parameter_table[i][j][k][l].attempt_frequency = attempt_frequency_1nn;
            }
          }
        }
      }
    }
    
    else if (strcmp(keyword,"attempt_frequency_2nn") == 0) {
      sscanf(file_line,"%s %le", keyword, &attempt_frequency_2nn);
      tag.global_attempt_frequency_2nn = 1;
      printf("# Global attempt frequency = %e s^-1\n",attempt_frequency_2nn);
      
      for (i = 0; i <= max_1nn; i++){
        for (j = 0;j <= max_2nn; j++){
          for (k = 0; k<= max_1nn; k++){
            for (l = 0; l <= max_2nn; l++){
              j2nn_parameter_table[i][j][k][l].attempt_frequency = attempt_frequency_2nn;
            }
          }
        }
      }
    }
    
    else if (strcmp(keyword,"attempt_frequency_3nn") == 0) {
      sscanf(file_line,"%s %le", keyword, &attempt_frequency_3nn);
      tag.global_attempt_frequency_3nn = 1;
      printf("# Global attempt frequency = %e s^-1\n",attempt_frequency_3nn);
      
      for (i = 0; i <= max_1nn; i++){
        for (j = 0;j <= max_2nn; j++){
          for (k = 0; k<= max_1nn; k++){
            for (l = 0; l <= max_2nn; l++){
              j3nn_parameter_table[i][j][k][l].attempt_frequency = attempt_frequency_3nn;
            }
          }
        }
      }
    }
    
    else if (strcmp(keyword,"attempt_frequency_4nn") == 0) {
      printf("# ERROR 4nn jumps are not yet implemented\n");
      exit(1);
    }
    
    else if (strcmp(keyword,"attempt_frequency_5nn") == 0) {
      sscanf(file_line,"%s %le", keyword, &attempt_frequency_5nn);
      tag.global_attempt_frequency_5nn = 1;
      printf("# Global attempt frequency = %e s^-1\n",attempt_frequency_5nn);
      
      for (i = 0; i <= max_1nn; i++){
        for (j = 0;j <= max_2nn; j++){
          for (k = 0; k<= max_1nn; k++){
            for (l = 0; l <= max_2nn; l++){
              j5nn_parameter_table[i][j][k][l].attempt_frequency = attempt_frequency_5nn;
            }
          }
        }
      }
    }
    
    else if (strcmp(keyword,"dipole_par") == 0) {
      sscanf(file_line,"%s %le %le %le %le", keyword, &W2232.M_sl, &W2232.A_sl, &W2232.M_sr, &W2232.A_sr);
      printf("# Jump system dipole parameters read in:\n");
      printf("#   M_sl = %le em\n"      , W2232.M_sl);
      printf("#   A_sl = %le em^2V^-1\n", W2232.A_sl);
      printf("#   M_sr = %le em\n"      , W2232.M_sr);
      printf("#   A_sr = %le em^2V^-1\n", W2232.A_sr);
    }
  }
}


/* Read in surface parameters from data.in */
void read_system_input() {
  char file_line[500];
  char keyword[500];
  double x,y,z;
  char in_xyz_file_name[100];
  char in_xyzp_file_name[100];
  char xyz_atom_type[100];
  double xyz_lattice_parameter = 0.0;
  double min,max;
  int z_min, z_max;
  
  if (a0 == 0 || Temperature == 0) {
    printf("ERROR a0 or Temperature need to be defined before system parameters are defined.\n");
    exit(1);
  }
  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }
    
    else if (strcmp(keyword,"surface") == 0) {
      // Needs to be after box_dimensions command (to get dimenson.x,y,z,p)
      sscanf(file_line,"%s %i %i", keyword, &surface_orientation, &lattice_option);
      if (surface_orientation == 111) {
        if (crystal == fcc) dimension.p = 6;
        else if (crystal == bcc) dimension.p = 12;
        printf("# Surface of (%i) kind\n",surface_orientation);
        initiate_lattice_orientation_parameters(surface_orientation);
      }
      else if (surface_orientation == 110) {
        if (crystal == fcc) {
          if      (lattice_option == 0) dimension.p = 6;
          else if (lattice_option == 1) dimension.p = 2;
        }
        else if (crystal == bcc)  dimension.p = 4;
        printf("# Surface of (%i) kind.\n",surface_orientation);
        initiate_lattice_orientation_parameters(surface_orientation);
      }
      else if (surface_orientation == 100) {
        if  (crystal == fcc)  dimension.p = 4;
        else if (crystal == bcc)  dimension.p = 2;
        printf("# Surface of (%i) kind\n",surface_orientation);
        initiate_lattice_orientation_parameters(surface_orientation);
      }
      else {
        printf("ERROR Unknown surface: (%i)\n",surface_orientation);
        exit(1);
      }
      
      /* Check */
      if (dimension.p > NCELLP) {
        printf("ERROR Increase NCELLP and recompile\n");
        exit(1);
      }
      
    }

    // System dimensions (atoms and empty space)
    else if (strcmp(keyword,"box_dimensions") == 0) {
      sscanf(file_line,"%s %i %i %i", keyword, &dimension.x, &dimension.y, &dimension.z);
      // Check dimensions
      if (dimension.x > NCELLX || dimension.y > NCELLY || dimension.z > NCELLZ) {
        printf("ERROR Desired dimensions > [%i,%i,%i]\n.", NCELLX,NCELLY,NCELLZ);
        exit(1);
      }
      
      /* Check */
      if (dimension.x > NCELLX) {
        printf("ERROR Increase NCELLX and recompile\n");
        exit(1);
      }
      if (dimension.y > NCELLY) {
        printf("ERROR Increase NCELLY and recompile\n");
        exit(1);
      }
      if (dimension.z > NCELLZ) {
        printf("ERROR Increase NCELLZ and recompile\n");
        exit(1);
      }
        
      
      printf("# System dimensions are %i*%i*%i [uc]\n",dimension.x, dimension.y, dimension.z);
    }
    
    else if (strcmp(keyword,"box_dimensions_m") == 0) {
      sscanf(file_line,"%s %le %le %le", keyword, &x, &y, &z);  // Dimensions in [m]
      
      dimension.x = floor(x/(unit_cell_dimension.x*a0));  // Needs initiate_lattice_orientation_parameters(surface_orientation), ie. the "surface" command needs to be run before "box_dimensions_m".
      dimension.y = floor(y/(unit_cell_dimension.y*a0));
      dimension.z = floor(z/(unit_cell_dimension.z*a0));
      
      // Check
      x = dimension.x*unit_cell_dimension.x*a0;
      y = dimension.y*unit_cell_dimension.y*a0;
      z = dimension.z*unit_cell_dimension.z*a0;
      
      // Check dimensions
      if (dimension.x > NCELLX || dimension.y > NCELLY || dimension.z > NCELLZ) {
        printf("ERROR Desired dimensions > [%i,%i,%i]\n.", NCELLX,NCELLY,NCELLZ);
        exit(1);
      }
      printf("# System dimensions are %i*%i*%i*a0^3 [uc] = %le * %le * %le [m]\n",dimension.x, dimension.y, dimension.z, x,y,z);
    }
  }
  
  initialize_lattice(); // Make empty lattice with correct dimensions. a0 and Temperature need to be known
}
    
    
/* Read in substrate and boundary parameters from data.in */
void read_substrate_input() {
  char file_line[500];
  char keyword[500];
  double x,y,z;
  char in_xyz_file_name[100];
  char in_xyzp_file_name[100];
  char xyz_atom_type[100];
  double xyz_lattice_parameter = 0.0;
  double min,max;
  int z_min, z_max;
  Pvector disp;
  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }    
    
    /* Read in positions from a xyz file. */
    else if (strcmp(keyword,"add_xyz") == 0) {
      disp.x = 0; disp.y = 0; disp.z = 0; disp.p = 0;
      sscanf(file_line,"%s %s %s %le %i %i %i", keyword, in_xyz_file_name,xyz_atom_type,&xyz_lattice_parameter, &disp.x, &disp.y, &disp.z);
      printf("# Reading in atoms from the xyz file: %s\n",in_xyz_file_name);
      read_initial_xyz_file(in_xyz_file_name, xyz_lattice_parameter, xyz_atom_type, disp);
    }
    
    // Read in objects from a xyzp file.
    else if (strcmp(keyword,"add_xyzp") == 0) {
      sscanf(file_line,"%s %s", keyword, in_xyzp_file_name);
      printf("# Adding objects from the xyzp file: %s\n",in_xyzp_file_name);
      read_xyzp_file(in_xyzp_file_name);
    }
    
    else if (strcmp(keyword,"make_bulk") == 0) {
      sscanf(file_line,"%s %i %i", keyword, &z_min, &z_max);
      printf("# Making bulk up %i <= z <= %i [unit cells]\n", z_min,z_max);
      make_bulk(z_min,z_max);
    }
    
    else if (strcmp(keyword,"make_bulk_m") == 0) {
      sscanf(file_line,"%s %le %le", keyword, &min, &max);
      printf("# Making bulk up %le <= z <= %le [m]\n", min,max);
      make_bulk_m(min,max);
    }
    
    /* Periodic bounday conditions 
     * 1: yes, 0: no
     */
    else if (strcmp(keyword,"pbc") == 0) {
      sscanf(file_line,"%s %i %i %i", keyword, &pbc.x, &pbc.y, &pbc.z);
      printf("# Periodic boundary conditions:\n# x: %i\n# y: %i\n# z: %i\n",pbc.x,pbc.y,pbc.z);
    }
    
    
  }
  
  /* Execute desired boundary conditions (needs to be done after the substrate atoms are introduced into the system */
  if (pbc.z == 0) {
    printf("# Bottom layer atoms are fixed\n");
    fix_bottom_layer();
    fix_top_empty_layer();
    printf("# Top empty layer is a sink for atoms\n");
  }
  else if (pbc.z == 1){
    printf("# PBC in z\n");
  }
  
  if (pbc.x == 1){
    printf("# PBC in x\n");
  }
  else if (pbc.x == 0){
    printf("# No PBC in x\n");
  }
  
  if (pbc.y == 1){
    printf("# PBC in y\n");
  }
  else if (pbc.y == 0){
    printf("# No PBC in y\n");
  }
}


/* Read in from data.in what structures to contruct in the system*/
void read_structures_input() {
  char file_line[500];
  char keyword[500];
  int cube_x,cube_y,cube_z;
  double cube_mx,cube_my,cube_mz;
  double cylinder_r, cylinder_h;
  double sphere_d = 2e-9;
  double ridge_h = 0.0;
  double sphere_h = 1e-9;
  double octahedron_d = 2e-9;
  double octahedron_t = 1.5e-9;
  double octahedron_h = 1e-9;
  int nr_added_adatoms;
  int added_V;
  int axis;
  Pvector q;
  int label;
  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }
    
    else if (strcmp(keyword,"make_cube") == 0) {
      sscanf(file_line,"%s %i %i %i", keyword, &cube_x, &cube_y, &cube_z);
      printf("# Making a cube of adatoms with dimensions %i %i %i\n", cube_x,cube_y,cube_z);
      make_cube(cube_x,cube_y,cube_z);
    }
    
    else if (strcmp(keyword,"make_cube_m") == 0) {
      sscanf(file_line,"%s %le %le %le", keyword, &cube_mx, &cube_my, &cube_mz);
      printf("# Making a cube of adatoms with dimensions %le %le %le\n", cube_mx,cube_my,cube_mz);
      make_cube_m(cube_mx,cube_my,cube_mz);
    }
    
    else if (strcmp(keyword,"make_cylinder") == 0) {
      sscanf(file_line,"%s %le %le %i", keyword, &cylinder_r, &cylinder_h, &axis);
      printf("# Making a cylinder of adatoms with radius %le m and height %le m\n", cylinder_r, cylinder_h);
      make_cylinder(cylinder_r,cylinder_h,axis);
    }
    
    
    else if (strcmp(keyword,"make_sphere") == 0) {
      sscanf(file_line,"%s %le %le", keyword, &sphere_d, &sphere_h);
      printf("# Making a sphere of adatoms with diameter %le m\n", sphere_d);
      make_sphere(sphere_d,sphere_h);
    }
    
    else if (strcmp(keyword,"make_truncated_octahedron") == 0) {
      sscanf(file_line,"%s %le %le %le", keyword, &octahedron_d, &octahedron_t, &octahedron_h);
      printf("# Making a truncated octahedron of adatoms with \"diameter\" %le m, truncated to %le m\n", octahedron_d, octahedron_t);
      make_truncated_octahedron(octahedron_d,octahedron_t,octahedron_h);
    }
    
    else if (strcmp(keyword,"make_ridge") == 0) {
      sscanf(file_line,"%s %le", keyword, &ridge_h);
      printf("# Making a ridge with height %le m\n", ridge_h);
      make_ridge(ridge_h);
    }
    
    else if (strcmp(keyword,"make_cubic_void") == 0) {
      sscanf(file_line,"%s %i %i %i", keyword, &cube_x, &cube_y, &cube_z);
      printf("# Making a cube of vacancies with dimensions %i %i %i\n", cube_x,cube_y,cube_z);
      make_cubic_void(cube_x,cube_y,cube_z);
    }
    
    else if (strcmp(keyword,"make_cubic_void_m") == 0) {
      sscanf(file_line,"%s %le %le %le", keyword, &cube_mx, &cube_my, &cube_mz);
      printf("# Making a cube of vacancies with dimensions [m] %le %le %le\n", cube_mx,cube_my,cube_mz);
      make_cubic_void_m(cube_mx,cube_my,cube_mz);
    }
        
    else if (strcmp(keyword,"make_island") == 0) {
      sscanf(file_line,"%s %i %i", keyword, &cube_x, &cube_y);
      printf("# Making a cube of adatoms with dimensions %i %i\n", cube_x,cube_y);
      make_island(cube_x,cube_y);
    }
    
    /* Add a population of adatoms */
    else if (strcmp(keyword,"add_adatoms") == 0) {
      sscanf(file_line,"%s %i", keyword, &nr_added_adatoms);
      printf("# Adding %i adatoms on the surface plane.\n", nr_added_adatoms);
      count_all_bonds(); // Make sure that objects have right kind; needed for add_adatoms().
      add_adatoms(nr_added_adatoms);
    }
    
    /* Add an adatom */
    else if (strcmp(keyword,"add_adatom") == 0) {
      label = 0;  // Default (no label)
      sscanf(file_line,"%s %i %i %i %i  %i", keyword, &q.x, &q.y, &q.z, &q.p, &label);
      add_adatom(q,label);
    }
    
    /* Add a population of vacancies */
    else if (strcmp(keyword,"add_vacancies") == 0) {
      sscanf(file_line,"%s %i", keyword, &added_V);
      printf("# Adding %i vacancies.\n", added_V);
      add_vacancies(added_V);
    }
  }
}


/* Read in parameters for external events from data.in */
void read_external_events_input() {
  char file_line[500];
  char keyword[500];
  int choose_field_solver = 0;
  int free_clusters_are_bulk = 0;

  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }
    
    else if (strcmp(keyword,"evaporation_rate") == 0) {
      sscanf(file_line,"%s %le", keyword, &evaporation_rate);
      printf("# Evaporation rate = %e s^-1 m^-2\n",evaporation_rate);
    }
    
    else if (strcmp(keyword,"deposition_rate") == 0) {
      sscanf(file_line,"%s %le", keyword, &deposition_rate);
      printf("# Deposition rate = %e s^-1 m^-2\n",deposition_rate);
    }
    
    else if (strcmp(keyword,"deposition_per_second") == 0) {
      sscanf(file_line,"%s %le", keyword, &deposition_per_second);
      printf("# Deposition rate = %e s^-1\n",deposition_per_second);
    }
    
    else if (strcmp(keyword,"deposition_mode") == 0) {
      sscanf(file_line,"%s %i", keyword, &tag.deposition_mode);
      if (tag.deposition_mode > 0) {
        printf("# Deposition mode: %i\n",tag.deposition_mode);
      }
      else if (tag.deposition_mode < 0.0) {
        printf("# Deposition mode should be > 0. Re-set to 3\n");
        tag.deposition_mode = 3;
      }
    }
    
    else if (strcmp(keyword,"deposition_nn_threshold") == 0) {
      sscanf(file_line,"%s %i", keyword, &tag.deposition_nn_threshold);
      printf("# Deposition will only happend at vacancy sites with at least %i neighbours.\n",tag.deposition_nn_threshold);
    }
    
    else if (strcmp(keyword,"cloud_temperature") == 0) {
      sscanf(file_line,"%s %lf", keyword, &cloud_temperature);
      printf("# Average temperature of deposited atoms: %lf K\n",cloud_temperature);
    }
    
    else if (strcmp(keyword,"focused_deposition") == 0) {
      sscanf(file_line,"%s %i", keyword, &tag.focused_deposition);
      if (tag.focused_deposition == 1) {
        printf("# The deposition only happen on a 5 5 uc^2 area.\n");
      }
    }
    
    else if (strcmp(keyword,"cluster_evaporation") == 0) {
      sscanf(file_line,"%s %i", keyword, &tag.cluster_evaporation);
      if (tag.cluster_evaporation == 1) printf("# Detached clusters will be removed.\n");
    }
    
    else if (strcmp(keyword,"mark_free_clusters_as_bulk") == 0) {
      sscanf(file_line,"%s %i", keyword, &free_clusters_are_bulk);
      if (free_clusters_are_bulk == 1) mark_free_clusters_as_bulk();
    }
    
#ifdef FIELD
    else if (strcmp(keyword,"external_field") == 0) {
      sscanf(file_line,"%s %le %i", keyword, &external_field, &choose_field_solver);
      tag.use_field = 1;
      if (external_field < 0) {
        field_sign = -1;
        printf("# Applying an external cathode field %e V/m\n", external_field);
      }
      else if (external_field > 0) {
        printf("# Applying an external anode field %e V/m\n", external_field);
        field_sign = 1;
        external_field = -external_field;      // LibHelmod assumes a negative (cathode) field.
      }
      
      if (choose_field_solver == 1) {
        printf("# Using the LibHelmod field solver\n");
        tag.use_libhelmod = 1;                 // Using LibHelmod as field solver
      }
      else if (choose_field_solver == 2) {
        printf("# Using the FEMOCS field solver\n");
        intialize_femocs_variables();
        femocs.tag_use_femocs = 1;             // Using FEMOCS as field solver
      }
    }
    
    else if (strcmp(keyword,"surface_field_distance") == 0) {
      sscanf(file_line,"%s %le", keyword, &surface_field_measurement_distance); 
      // For now only used with LibHelmod. If 0, the old field model will be used.
      if (surface_field_measurement_distance == 0) {
        tag.libhelmod_field_model = 1;
        printf("# Field model 1 will be used\n");
      }
      else if( surface_field_measurement_distance > 0.0) {
        tag.libhelmod_field_model = 2; // Default
        printf("# Field model 2 will be used with surface field measured %le m above the surface.\n", surface_field_measurement_distance);
      }
    }
    
    else if (strcmp(keyword,"max_jumps_before_field_calc") == 0) {
      sscanf(file_line,"%s %i", keyword, &max_jumps_at_constant_field);
      if (tag.use_field == 1) {
        printf("# The field solver will be called if any atom has jumped more than %i times\n",max_jumps_at_constant_field);
      }
    }
    
    else if (strcmp(keyword,"gradient_correction") == 0) {
      sscanf(file_line,"%s %lg", keyword, &gradient_correction);
      if (tag.use_field == 1) {
        printf("# Gradient correction factor: %lg\n",gradient_correction);
      }
    }
    
#endif // FIELD

  }
}


/* Read in parameters for apex subsystem field calculation (only with Helmod) from data.in */
void read_libhelmod_optimization_input() {
  char file_line[500];
  char keyword[500];

  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }
    
    else if (strcmp(keyword,"calculate_field") == 0) {
      sscanf(file_line,"%s %i", keyword, &global_electric_field_recalculation_frequency);
      printf("# Helmod calculated field for the whole system every : %i steps \n",global_electric_field_recalculation_frequency);
    }
    
    else if (strcmp(keyword,"aroundapx") == 0) {
      sscanf(file_line,"%s %i", keyword, &aroundapx_input);
      printf("# Helmod subsystem dimensions: %ix%ix%i helmod grid points\n",aroundapx_input,aroundapx_input,aroundapx_input);
    }

    else if (strcmp(keyword,"charge_threshold") == 0) {
      sscanf(file_line,"%s %le", keyword, &charge_threshold);
      printf("# Charge threshold : %le \n",charge_threshold);
    } 
  }
}

/* Read in parameters how to print xyz files from data.in */
void read_xyz_parameter_input() {
  char file_line[500];
  char keyword[500];

  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }
    
    else if (strcmp(keyword,"xyz_empty_objects") == 0) { 
      sscanf(file_line,"%s %i", keyword, &tag.print_empty_kind);
      if (tag.print_empty_kind == 1) printf("# Empty objects will be visualized in xyz files.\n");
      if (tag.print_empty_kind >1 || tag.print_empty_kind <0) {
        printf("ERROR: print_empty_objects needs to be 0 or 1\n");
        exit(1);
      }
    }
    
    else if (strcmp(keyword,"xyz_libhelmod_lattice") == 0) { 
      sscanf(file_line,"%s %i", keyword, &tag.print_libhelmod_field);
      if (tag.print_libhelmod_field == 1) printf("# The LibHelmod lattice will be visualized in xyz files.\n");
      if (tag.print_libhelmod_field > 1 || tag.print_libhelmod_field < 0) {
        printf("ERROR: xyz_libhelmod_lattice needs to be 0 or 1\n");
        exit(1);
      }
    }
    
    else if (strcmp(keyword,"xyz_femocs_field") == 0) { 
      sscanf(file_line,"%s %i", keyword, &tag.print_femocs_field);
      if (tag.print_femocs_field == 1) {
        printf("# The FEMOCS field will be visualized in xyz files.\n");
        tag.print_empty_kind = 1;
      }
      else if (tag.print_femocs_field > 1 || tag.print_femocs_field < 0) {
        printf("ERROR: xyz_femocs_field needs to be 0 or 1\n");
        exit(1);
      }
    }
    
    else if (strcmp(keyword,"xyz_info") == 0) { 
      sscanf(file_line,"%s %i", keyword, &print_xyz_info);
      if (print_xyz_info == 0) printf("# No extra info will be added to objects.xyz.\n");
      if (print_xyz_info >1 || print_xyz_info <0) {
        printf("# Extra info will be added to objects.xyz (standard).\n");
      }
    }
    
    else if (strcmp(keyword,"xyz_temperature") == 0) { 
      sscanf(file_line,"%s %i", keyword, &tag.print_xyz_temperature);
      if (tag.print_xyz_temperature == 1) {
        printf("# Print object temperatures in objects.xyz.\n");
      }
    }
    
  }
}


/* Read in parameters how to print xyz files from data.in */
void read_output_files_input() {
  char file_line[500];
  char keyword[500];

  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }
    
    else if (strcmp(keyword,"print_xyz_files") == 0) { 
      sscanf(file_line,"%s %i", keyword, &tag.print_xyz_files);
      if (xyz_label_colour == 1) printf("# Print xyz files.\n");
    }
    
    else if (strcmp(keyword,"print_xyzp_files") == 0) { 
      sscanf(file_line,"%s %i", keyword, &tag.print_xyzp_files);
      if (xyz_label_colour == 1) printf("# Print xyzp files.\n");
    }
    
    else if (strcmp(keyword,"print_parcas_files") == 0) { 
      sscanf(file_line,"%s %i %s %s", keyword, &tag.print_parcas_files, atom_type1, atom_type2);
      if (xyz_label_colour == 1) printf("# Print Parcas files.\n");
    }
    
    else if (strcmp(keyword,"print_lammps_files") == 0) { 
      sscanf(file_line,"%s %i", keyword, &tag.print_lammps_files);
      printf("# Print LAMMPS atom dump files.\n");
    }    
    
    else if (strcmp(keyword,"print_par_files") == 0) { 
      sscanf(file_line,"%s %i", keyword, &tag.print_par_files);
      printf("# Print par files\n");
    }
    
    else if (strcmp(keyword,"print_transitions_file") == 0) { 
      sscanf(file_line,"%s %i", keyword, &tag.print_transitions);
      printf("# Print transitions.out\n");
    }
    
    else if (strcmp(keyword,"print_missing_parameters") == 0) {
      sscanf(file_line,"%s %i", keyword, &print_missing_parameters);
      if (print_missing_parameters == 1) {
        printf("# Make xyz files for missing processes.\n");
      }
    }
    
  }
}


/* Read in parameters for end conditions and standard output from data.in */
void read_end_conditions_input() {
  char file_line[500];
  char keyword[500];
  int min_height_uc;
  int max_height_uc;
  Pvector q;

  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }
    
    else if (strcmp(keyword,"print_cycle") == 0 || strcmp(keyword,"short_cycle") == 0) {
      sscanf(file_line,"%s %g", keyword, &print_out_cycle);
      printf("# Print out data every %g step\n",print_out_cycle);
    }
    
    else if (strcmp(keyword,"print_cycle_s") == 0 || strcmp(keyword,"long_cycle") == 0) {
      sscanf(file_line,"%s %le", keyword, &print_out_cycle_s);
      printf("# Print out data every %le s\n",print_out_cycle_s);
    }
    
    else if (strcmp(keyword,"max_kmc_step") == 0) {
      sscanf(file_line,"%s %lf", keyword, &max_kmc_step);
      printf("# Max KMC steps : %g\n",max_kmc_step);
    }
    
    else if (strcmp(keyword,"max_simulation_time") == 0) {
      sscanf(file_line,"%s %le", keyword, &max_simulation_time);
      printf("# Max simulation time: %g\n",max_simulation_time);
    }
    
    else if (strcmp(keyword,"min_height") == 0) {
      sscanf(file_line,"%s %i", keyword, &min_height_uc);
      q.x = q.y = 0;
      q.z = min_height_uc;
      if (surface_orientation == 100 ) {
        q.p = 3;  // Atom in the highest layer of the unit cell
      }
      else if ( surface_orientation == 110 ) {
        q.p = 5;
      }
      else if ( surface_orientation == 111 ) {
        q.p = 5;
      }
      min_height = height_3d(q);  // Height in [m]
      
      printf("# Minimum height allowed [m]: %le = %i uc\n",min_height,min_height_uc);
    }
    
    else if (strcmp(keyword,"min_height_m") == 0) {
      sscanf(file_line,"%s %le", keyword, &min_height);
      printf("# Minimum height allowed [m]: %le\n",min_height);
    }
    
    else if (strcmp(keyword,"max_height") == 0) {
      sscanf(file_line,"%s %i", keyword, &max_height_uc);
      q.x = q.y = 0;
      q.z = max_height_uc;
      if (surface_orientation == 100 ) {
        q.p = 3;  // Atom in the highest layer of the unit cell
      }
      else if ( surface_orientation == 110 ) {
        q.p = 5;
      }
      else if ( surface_orientation == 111 ) {
        q.p = 5;
      }
      max_height = height_3d(q);  // Height in [m]
      
      printf("# Maximum height allowed [m]: %le = %i uc\n",max_height,max_height_uc);
    }
    
    else if (strcmp(keyword,"max_height_m") == 0) {
      sscanf(file_line,"%s %le", keyword, &max_height);      
      printf("# Maximum height allowed [m]: %le\n",max_height);
    }
    
    else if (strcmp(keyword,"max_nr_clusters") == 0) {
      sscanf(file_line,"%s %i %i", keyword, &max_nr_clusters, &min_cluster_size_limit);      
      printf("# Maximum nr of clusters (size >= %i) allowed: %i\n",min_cluster_size_limit,max_nr_clusters);
    }
    
    else if (strcmp(keyword,"max_field") == 0) {
      sscanf(file_line,"%s %le", keyword, &local_field_limit);      
      printf("# Maximum local field allowed: %le\n",local_field_limit);
    }
    
    else if (strcmp(keyword,"max_nr_evaporated") == 0) {
      sscanf(file_line,"%s %i", keyword, &max_nr_evaporated_atoms);      
      printf("# Maximum nr evaporated atoms allowed: %i\n", max_nr_evaporated_atoms);
    }
    else if (strcmp(keyword,"max_nr_atoms") == 0) {
      sscanf(file_line,"%s %le", keyword, &max_nr_atomic_objects);      
      printf("# Maximum nr of atoms allowed: %le\n", max_nr_atomic_objects);
    }
    else if (strcmp(keyword,"max_CPU_time") == 0) {
      sscanf(file_line,"%s %le", keyword, &max_CPU_time);
      printf("# Max CPU time: %g\n",max_CPU_time);
    }
    
  }
}


/* Read in parameters for end conditions and standard output from data.in */
void read_specific_calculations_input() {
  char file_line[500];
  char keyword[500];
  char E_comsol_out_file_name[100];

  
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
  }
  
  while (fgets(file_line,500,file_data_in) != NULL) {
    strcpy(keyword," ");              // Initiation of string
    sscanf(file_line,"%s", keyword);  // Finding the first word on the line 
    if (keyword[0]=='#') {
      continue;
    }
    
    else if (strcmp(keyword,"calculate_diffusion") == 0) {
      sscanf(file_line,"%s %lg  %i %i %i %i", keyword, &max_kmc_run, &q_diffusion_adatom.x, &q_diffusion_adatom.y, &q_diffusion_adatom.z, &q_diffusion_adatom.p);
      if (max_kmc_run > 0) {
        tag.calculate_diffusion_coefficient = 1;
        printf("# The diffusion coefficient will be calculated over %lg runs.\n", max_kmc_run);
      }
    }
    
    else if (strcmp(keyword,"measure_field") == 0) {
      sscanf(file_line,"%s %i %i %i %i", keyword, &field_measurement_point.x, &field_measurement_point.y, &field_measurement_point.z, &field_measurement_point.p);      
      printf("# Maximum local field allowed: %le\n",local_field_limit);
    }
    
    else if (strcmp(keyword,"max_diffusion_displacement") == 0) {
      sscanf(file_line,"%s %lg", keyword, &max_diffusion_displacement);
      printf("# Maximum displacement of the diffusing atom in diffusion calculations: %lg m.\n",max_diffusion_displacement);
    }
    
    else if (strcmp(keyword,"using_comsol") == 0) {
      sscanf(file_line,"%s %i", keyword, &tag.using_comsol);
      if (tag.using_comsol == 1) {
        snprintf(E_comsol_out_file_name, sizeof(E_comsol_out_file_name), "./comsol.ckx");
        read_comsol_xyz_file(E_comsol_out_file_name);
        tag.use_atomic_temperature = 1;
      }
    }
    
    else if (strcmp(keyword,"check_neighbour_matrices") == 0) {
      sscanf(file_line,"%s %i", keyword, &tag.check_neighbour_matrices);
    }
    

  }
}

/* Every particular simulation setup is defined
 * in a configuration file called data.in.
 * Rows starting with "#" are not read.
 */
void read_configuration_file() {
  read_configuration_file_time0 = clock();
  
  printf("# Input configuration read from data.in\n");
  if ((file_data_in = fopen("data.in","r")) == NULL) {
    printf("ERROR Could not open parameter file: data.in\n");
    exit(1);
    
  }
 
  read_general_input();
  read_material_specific_input();
  read_system_input();
  read_substrate_input();
  read_structures_input();
  read_external_events_input();
  read_libhelmod_optimization_input();
  read_xyz_parameter_input();
  read_output_files_input();
  read_end_conditions_input();
  read_specific_calculations_input();

  check_initial_configuration();
  
  
  read_configuration_file_time1 = clock();
  read_configuration_file_time += (double) (read_configuration_file_time1 - read_configuration_file_time0) / CLOCKS_PER_SEC;
}


/* Count nr.objects, nr.atomic_objects, nr.atoms, nr.adatoms, nr.fixed_atoms,
 * nr.adatoms, nr.empty, nr.fixed_empty, and nr.vacancies.
 */
void count_nr_objects() {
  Pvector q; 
  // Count number of different objects and empty lattice points
  nr.empty	= 0;
  nr.vacancies	= 0;
  nr.atoms	= 0;
  nr.fixed_atoms = 0;
  nr.fixed_empty = 0;
  nr.adatoms	= 0;
  nr.objects	= 0;
  
    
  /* Analyze system */
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          if (system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind) {
            nr.atoms++;
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == fixed_atom_kind) {
            nr.fixed_atoms++;
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
            nr.adatoms++;
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == empty_kind) {
            nr.empty++;
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == fixed_empty_kind) {
            nr.fixed_empty++;
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {
            nr.vacancies++;
          }
        }
      } 
    } 
  }
  nr.atomic_objects = nr.atoms + nr.adatoms + nr.fixed_atoms;
  nr.objects = nr.atoms + nr.fixed_atoms + nr.adatoms + nr.vacancies;
    
}




/* Check that the system is correct 
 * objects_out == 1: Print to objects.out and std
 * objects_out == 0: Print only to std
 */

void check_system_integrity(int objects_out) {
  check_system_integrity_time0 = clock();
//   printf("Check system integrity\n");
  int nn, nnn, omega3, omega5;
  int nr_points 	= 0;
  int tot_points	= 0;
  int nr_1nn_bonds	= 0;
  int nr_2nn_bonds	= 0;
  double deposition_rate_check = 0.0;
  double evaporation_rate_check = 0.0;
  int i;
  Pvector q;
  Vector v;
  double check_nn_probability_sum       = 0.0;
  double check_nnn_probability_sum      = 0.0;
  double check_3nn_probability_sum      = 0.0;
  double check_5nn_probability_sum      = 0.0;
  double check_internal_probability_sum = 0.0;
  double check_external_probability_sum = 0.0;
  double check_probability_sum          = 0.0;
  
  double nn_probability_sum_delta       = 0.0;
  double nnn_probability_sum_delta      = 0.0;
  double nn3_probability_sum_delta      = 0.0;
  double nn5_probability_sum_delta      = 0.0;
  double internal_probability_sum_delta = 0.0;
  double external_probability_sum_delta = 0.0;
  double probability_sum_delta          = 0.0;
  
  int element_tmp;
  
  int height_uc           = 0;
  double area             = 0.0e0;  // [m^2]; used only for focused deposition and assumes a relatively flat surface.
  double total_area       = 0.0;    // [m^2]; The total area of the system estimated from the number of adatoms (more precise for arbitrary surfaces, like clusters)
  double max_charge       = 0.0e0;  // [e]
  height                  = 0.0;    // [m]
  max_local_field         = 0.0;    // [V/m]
  double area_per_adatom  = 0.0;    // [m^2]
  
  // Count number of different objects and empty lattice points
  nr.empty        = 0;
  nr.vacancies    = 0;
  nr.atoms        = 0;
  nr.fixed_atoms  = 0;
  nr.fixed_empty  = 0;
  nr.adatoms      = 0;
  nr.objects      = 0;
  
    
  /* Analyze system */
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
      // 	  printf("Check: %i (%i,%i,%i,%i) %i %i \n", system_lattice[x][y][z][p].kind,x,y,z,p,system_lattice[x][y][z][p].nr_1nn,system_lattice[x][y][z][p].nr_2nn);
      // 	  for (i=0;i<max_1nn;i++) printf("1nn %lf\n",system_lattice[x][y][z][p].probability_nn_jump[i]);
          if (system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind) {
            nr.atoms++;
            nr_1nn_bonds += system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
            nr_2nn_bonds += system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
            
      // 	    if (system_lattice[q.x][q.y][q.z][q.p].element == 0) {
      // 		system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;	// Assuming only one element in the system.
      // 		printf("%e WARNING Atom with wrong element value found\n",kmc_step);
      // 	    }
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == fixed_atom_kind) {
            nr.fixed_atoms++;
            nr_1nn_bonds += system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
            nr_2nn_bonds += system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
            
      // 	    if (system_lattice[q.x][q.y][q.z][q.p].element == 0) {
      // 		system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;	// Assuming only one element in the system.
      // 		printf("%e WARNING Fixed atom with wrong element value found\n",kmc_step);
      // 	    }
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
            nr.adatoms++;
            nr_1nn_bonds += system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
            nr_2nn_bonds += system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
            
            // Find maximum charge (in absolute values)
            if (fabs(system_lattice[q.x][q.y][q.z][q.p].electric_charge) > fabs(max_charge)) {
              max_charge = system_lattice[q.x][q.y][q.z][q.p].electric_charge;
            }
          
      // 	    if (system_lattice[q.x][q.y][q.z][q.p].element == 0) {
      // 		system_lattice[q.x][q.y][q.z][q.p].element = lattice_element;	// Assuming only one element in the system.
      // 		printf("%e WARNING Adatom with wrong element value found\n",kmc_step);
      // 	    }
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == empty_kind) {
            nr.empty++;
      // 	    if (system_lattice[q.x][q.y][q.z][q.p].element > 0) {
      // 		element_tmp = system_lattice[q.x][q.y][q.z][q.p].element;
      // 		system_lattice[q.x][q.y][q.z][q.p].element = vacancy_element;
      // 		printf("%e WARNING Empty with wrong element value found: %i\n",kmc_step,element_tmp);
      // 	    }
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == fixed_empty_kind) {
            nr.fixed_empty++;
      // 	    if (system_lattice[q.x][q.y][q.z][q.p].element > 0) {
      // 		element_tmp = system_lattice[q.x][q.y][q.z][q.p].element;
      // 		system_lattice[q.x][q.y][q.z][q.p].element = vacancy_element;
      // 		printf("%e WARNING Fixed empty with wrong element value found: %i\n",kmc_step, element_tmp);
      // 	    }
          }
          else if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {
            nr.vacancies++;
            
            if (fabs(system_lattice[q.x][q.y][q.z][q.p].scalar_field) > fabs(max_local_field)) {
              max_local_field = system_lattice[q.x][q.y][q.z][q.p].scalar_field;
            }
            
      // 	    if (system_lattice[q.x][q.y][q.z][q.p].element > 0) {
      // 		element_tmp = system_lattice[q.x][q.y][q.z][q.p].element;
      // 		system_lattice[q.x][q.y][q.z][q.p].element = vacancy_element;
      // 		printf("%e WARNING Vacancy with wrong element value found: %i\n",kmc_step, element_tmp);
      // 	    }
          }
          
          /* Calculate the probability sums of the system */
          for (nn = 0; nn < max_1nn; nn++) {
            check_nn_probability_sum        += system_lattice[q.x][q.y][q.z][q.p].event_1nn[nn].probability;
            check_internal_probability_sum  += system_lattice[q.x][q.y][q.z][q.p].event_1nn[nn].probability;
            check_probability_sum           += system_lattice[q.x][q.y][q.z][q.p].event_1nn[nn].probability;
          }
          if (tag.allow_nnn_jumps == 1) {
            for (nnn = 0; nnn < max_2nn; nnn++) {
              check_nnn_probability_sum       += system_lattice[q.x][q.y][q.z][q.p].event_2nn[nnn].probability;
              check_internal_probability_sum  += system_lattice[q.x][q.y][q.z][q.p].event_2nn[nnn].probability;
              check_probability_sum           += system_lattice[q.x][q.y][q.z][q.p].event_2nn[nnn].probability;
            }
          }
          if (tag.allow_3nn_jumps == 1) {
            for (omega3 = 0; omega3 < max_3nn; omega3++) {
              check_3nn_probability_sum       += system_lattice[q.x][q.y][q.z][q.p].event_3nn[omega3].probability;
              check_internal_probability_sum  += system_lattice[q.x][q.y][q.z][q.p].event_3nn[omega3].probability;
              check_probability_sum           += system_lattice[q.x][q.y][q.z][q.p].event_3nn[omega3].probability;
            }
          }
          if (tag.allow_5nn_jumps == 1) {
            for (omega5 = 0; omega5 < max_5nn; omega5++) {
              check_5nn_probability_sum       += system_lattice[q.x][q.y][q.z][q.p].event_5nn[omega5].probability;
              check_internal_probability_sum  += system_lattice[q.x][q.y][q.z][q.p].event_5nn[omega5].probability;
              check_probability_sum           += system_lattice[q.x][q.y][q.z][q.p].event_5nn[omega5].probability;
            }
          }
          
          
          check_external_probability_sum	+= system_lattice[q.x][q.y][q.z][q.p].probability_deposition;
          check_external_probability_sum	+= system_lattice[q.x][q.y][q.z][q.p].probability_evaporation;
          
          check_probability_sum 		+= system_lattice[q.x][q.y][q.z][q.p].probability_deposition;
          check_probability_sum 		+= system_lattice[q.x][q.y][q.z][q.p].probability_evaporation;
          
          /* Determine the height of any adatom/atom structure; ingoring free atoms with no 1nn and 2nn atoms */
          if ((system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind || system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind) 
            && (system_lattice[q.x][q.y][q.z][q.p].nr_1nn > 0 || system_lattice[q.x][q.y][q.z][q.p].nr_2nn > 0)) {
            /* Only consider bulk atoms or adatoms */
            if (system_lattice[q.x][q.y][q.z][q.p].cluster_index == bulk_cluster_index || bulk_cluster_index == -1) {
              // bulk_cluster_index == -1 if no bulk cluster exists, ie if no fixed atoms are used (as with bulk simulations).
              if (height_3d(q) >= height) {
                height = height_3d(q);	// Height in [m]
                height_uc = q.z;
                
              }
            }
          }
          
          if (height != height_old) {
            tag.recalculate_field = 1;
            height_old = height;
          }
 
        }
      } 
    } 
  }

  nr.atomic_objects = nr.atoms + nr.adatoms + nr.fixed_atoms;
//   if (nr_initial_atomic_objects != nr_atomic_objects + nr_removed_atoms) {
//   }
  
  nr.objects = nr.atoms + nr.fixed_atoms + nr.adatoms + nr.vacancies;
//   if (nr_objects != old_nr_objects && kmc_step > 0) {
//     printf("%e WARNING Nr of object has changed %i -> %i\n",kmc_step,old_nr_objects,nr_objects);
//     exit(1);
//   }
  nr_points  = nr.objects + nr.empty + nr.fixed_empty;
  
  
  /* Estimate the total system area */
  switch(crystal) {
    case fcc:
      area_per_adatom = a0*a0/2.0;  // Area per adatom; exactly true for a perfect fcc{100} surface, approximatly true for other
      break;
    case bcc:
      area_per_adatom = a0*a0;      // Area per adatom; exactly true for a perfect bcc{100} surface, approximatly true for other
      break;
    default:
      printf("ERROR Only fcc and bcc lattices are implemented, not crystal = %i. Ending\n", crystal);
      exit(1);
  }
  
  total_area = nr.adatoms*area_per_adatom;
  
  if (tag.focused_deposition == 1) {
    area= 5*5*unit_cell_dimension.x*unit_cell_dimension.y*a0*a0; // [m^2]; the area estimaterd for a flat system
  }
  else {
    area = dimension.x*dimension.y*unit_cell_dimension.x*unit_cell_dimension.y*a0*a0; // [m^2]
  }
  


  
  
  deposition_rate_check = (nr.deposition_events +nr.deposition_reactions)/simulation_time;
  evaporation_rate_check = (nr.removed_atoms)/simulation_time; // Effective evaporation (events + removal)
  
  
  /* Check the probability sum */
  probability_sum_delta = (check_probability_sum - sum_probabilities)/(sum_probabilities*100.0); 
  sum_probabilities = check_probability_sum;	// Correcting the sum.
  
  nn_probability_sum_delta		= (check_nn_probability_sum  - sum_nn_jump_probabilities)/(sum_nn_jump_probabilities*100.0);
  nnn_probability_sum_delta		= (check_nnn_probability_sum - sum_nnn_jump_probabilities)/(sum_nnn_jump_probabilities*100.0);
  nn3_probability_sum_delta   = (check_3nn_probability_sum - sum_3nn_jump_probabilities)/(sum_3nn_jump_probabilities*100.0);
  nn5_probability_sum_delta   = (check_5nn_probability_sum - sum_5nn_jump_probabilities)/(sum_5nn_jump_probabilities*100.0);
  internal_probability_sum_delta	= (check_internal_probability_sum - sum_internal_probabilities)/(sum_internal_probabilities*100.0);
  external_probability_sum_delta	= (check_external_probability_sum - sum_external_probabilities)/(sum_external_probabilities*100.0);
  
  if (sum_nn_jump_probabilities == 0.0)   nn_probability_sum_delta = 0.0;
  if (sum_nnn_jump_probabilities == 0.0)  nnn_probability_sum_delta = 0.0;
  if (sum_3nn_jump_probabilities == 0.0)  nn3_probability_sum_delta = 0.0;
  if (sum_5nn_jump_probabilities == 0.0)  nn5_probability_sum_delta = 0.0;
  if (sum_internal_probabilities == 0.0)  nn5_probability_sum_delta = 0.0;
  if (sum_external_probabilities == 0.0)  external_probability_sum_delta = 0.0;
  
  /* Check CPU time for the cycle */
  print_out_cycle_time1 = clock();
  print_out_cycle_time  = (double) (print_out_cycle_time1 - print_out_cycle_time0) / CLOCKS_PER_SEC;
  
  print_out_cycle_time0 = clock(); // Start the clock for the following cycle.
  
  
  
  /** Print out data **/
  
   
  /* Print to std */
  i = 0;
  
  if (kmc_step == 0) fprintf(stdout, "#%i:Step",i); i++; 
  if (kmc_step != 0) fprintf(stdout, "%e ",      kmc_step);                //  Step
  
  if (kmc_step == 0) fprintf(stdout, " %i:Time[s] ",i); i++; 
  if (kmc_step != 0) fprintf(stdout, " %e ",      simulation_time);        //  Time[s]  
  
  if (kmc_step == 0) fprintf(stdout, " %i:AOBj",i); i++;
  if (kmc_step != 0) fprintf(stdout, " %i",      nr.atomic_objects);       //  AOBj
  
  if (kmc_step == 0) fprintf(stdout, " %i:at",i); i++;
  if (kmc_step != 0) fprintf(stdout, " %i",      nr.atoms);                //  at
  
  if (kmc_step == 0) fprintf(stdout, " %i:ad",i); i++;
  if (kmc_step != 0) fprintf(stdout, " %i",      nr.adatoms);              //  ad

  if (kmc_step == 0) fprintf(stdout, " %i:rm_ad",i); i++;
  if (kmc_step != 0) fprintf(stdout, " %1.2le",  nr.removed_atoms);        //  rm_ad
  
  if (kmc_step == 0) fprintf(stdout, " %i:rm_V ",i); i++;
  if (kmc_step != 0) fprintf(stdout, " %1.2le ",  nr.removed_vacancies);   //  rm_V
  
  if (kmc_step == 0) fprintf(stdout, " %i:height[uc]",i); i++; 
  if (kmc_step != 0) fprintf(stdout, " %i",      height_uc);               //  height[uc]
  
  if (kmc_step == 0) fprintf(stdout, " %i:height[m]",i); i++;
  if (kmc_step != 0) fprintf(stdout, " %1.2le",  height);                  //  height[m]
  
  
  if (deposition_rate > 0.0) {
    if (kmc_step == 0) fprintf(stdout, " %i:dep",i); i++;
    if (kmc_step != 0) fprintf(stdout, " %1.2le",  nr.deposition_events);    //  dep
  
    if (kmc_step == 0) fprintf(stdout, " %i:dep[1/s]",i); i++; 
    if (kmc_step != 0) fprintf(stdout, " %1.2e",   deposition_rate_check);   //  dep[1/s]
  }
  
  if (evaporation_rate > 0.0) {
    if (kmc_step == 0) fprintf(stdout, " %i:ev",i); i++;
    if (kmc_step != 0) fprintf(stdout, " %1.2le",  nr.removed_atoms);        // ev          Effective evaporation (events + removal)
  
    if (kmc_step == 0) fprintf(stdout, " %i:ev[1/s]",i); i++;
    if (kmc_step != 0) fprintf(stdout, " %1.2e",   evaporation_rate_check);  // ev[1/s]
  }
  
  if (kmc_step == 0) fprintf(stdout, " %i:err[pc]",i); i++;
  if (kmc_step != 0) fprintf(stdout, " %1.2e",  probability_sum_delta);     // err[pc]
  
  if (tag.use_field == 1) {
    if (kmc_step == 0) fprintf(stdout, " %i:max_Efield[V/m]",i); i++;
    if (kmc_step != 0) fprintf(stdout, " %1.2e",  max_local_field);         // max_Efield[V/m]
    
    
    if(tag.use_field) if (kmc_step == 0) fprintf(stdout, " %i:max_charge[e]",i); i++;
    if (kmc_step != 0) fprintf(stdout, " %1.2lf", max_charge);              // max_charge[e]
  
    if(tag.use_field) if (kmc_step == 0) fprintf(stdout, " %i:F_solver_calls",i); i++;
    if (kmc_step != 0) fprintf(stdout, " %1.2e",  nr.calls_field_solver);     // F_solver_calls
  }
  
  if (kmc_step == 0) fprintf(stdout, " %i:Frame #d\n",i);
  if (kmc_step != 0) fprintf(stdout, " %i #d",     print_xyz_frame_nr);      // Frame
  
  if (tag.end_condition == 1){
    // Add last data line marker
    fprintf(stdout, " #end\n ");
    print_xyz_frame_nr++;
  }
  else {
      fprintf(stdout, "\n");
  }

    
    
  /* Print to objects.out */
  if (objects_out == 1) {
    i = 0;

    if (kmc_step == 0) fprintf(file_objects_out, "#%i:Step",i); i++;
    if (kmc_step != 0) fprintf(file_objects_out, "%e" ,      kmc_step);                          // Step
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:Time[s] ",i); i++;     
    if (kmc_step != 0) fprintf(file_objects_out, " %e ",      simulation_time);                  // Time[s]
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:AOBj",i); i++;    
    if (kmc_step != 0) fprintf(file_objects_out, " %i",      nr.atomic_objects);                 // AOBj
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:at",i); i++;    
    if (kmc_step != 0) fprintf(file_objects_out, " %i",      nr.atoms);                          // at
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:ad",i); i++;    
    if (kmc_step != 0) fprintf(file_objects_out, " %i",      nr.adatoms);                        // ad
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:V",i); i++;    
    if (kmc_step != 0) fprintf(file_objects_out, " %i",      nr.vacancies);                      // V
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:rm_ad",i); i++;    
    if (kmc_step != 0) fprintf(file_objects_out, " %1.2le",  nr.removed_atoms);                  // rm_ad
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:rm_V ",i); i++;
    if (kmc_step != 0) fprintf(file_objects_out, " %1.2le ",  nr.removed_vacancies);             // rm_V
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:height[uc]",i); i++;
    if (kmc_step != 0) fprintf(file_objects_out, " %i",      height_uc);                         // height[uc]
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:height[m] ",i); i++;
    if (kmc_step != 0) fprintf(file_objects_out, " %1.2le ",  height);                           // height[m]
    
    if (deposition_rate > 0.0) {
      if (kmc_step == 0) fprintf(file_objects_out, " %i:dep",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %1.2le",  nr.deposition_events);            // dep
    
      if (kmc_step == 0) fprintf(file_objects_out, " %i:dep[1/s]",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %1.2e",   deposition_rate_check);          // dep[1/s]
      
      if (kmc_step == 0) fprintf(file_objects_out, " %i:dep[s^-1 m^-2]",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %1.2le",  nr.deposition_events/(simulation_time*total_area));            // depostition rate per area
      
      if (kmc_step == 0) fprintf(file_objects_out, " %i:area[m^2] ",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %1.2le ",  total_area);                      // area of the sytem
    }
    
    if (evaporation_rate > 0.0) {
      if (kmc_step == 0) fprintf(file_objects_out, " %i:ev",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %1.2le",  nr.removed_atoms);                // ev           Effective evaporation (events + removal)
    
      if (kmc_step == 0) fprintf(file_objects_out, " %i:ev[1/s] ",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %1.2e ",   evaporation_rate_check);          // ev[1/s]
    }
    
    if (tag.cluster_evaporation == 0) {    
      if (kmc_step == 0) fprintf(file_objects_out, " %i:cl",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %i",      nr.clusters);                     // cl
    
      if (kmc_step == 0) fprintf(file_objects_out, " %i:min_cl ",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %i ",      min_cluster_size);               // min_cl
    }
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:err[pc] ",i); i++;
    if (kmc_step != 0) fprintf(file_objects_out, " %1.2e ",   probability_sum_delta);            // err[pc]
  
    if (tag.use_field == 1) {
      if (kmc_step == 0) fprintf(file_objects_out, " %i:F_solver_calls",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %1.2e",   nr.calls_field_solver);           // F_solver_calls
 
      if (kmc_step == 0) fprintf(file_objects_out, " %i:max_Efield[V/m]",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %1.2e",   max_local_field);                 // max_Efield[V/m]
    
      if (kmc_step == 0) fprintf(file_objects_out, " %i:max_charge[e]",i); i++;
      if (kmc_step != 0) fprintf(file_objects_out, " %1.2lf",  max_charge);                      // max_charge[e]     
    
    }
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:N{a<4}/N1nn",i); i++;
    if (kmc_step != 0) fprintf(file_objects_out, " %1.2le",  nr.jumps_unstable/nr.jumps_1nn);    // N{a<4}/N1nn
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:<N1nn{c-a}>",i); i++;
    if (kmc_step != 0) fprintf(file_objects_out, " %1.2le",  sum_ca_bonds/nr.jumps_1nn);         // <N1nn{c-a}>
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:<Em1nn>[eV]",i); i++;
    if (kmc_step != 0) fprintf(file_objects_out, " %1.2le",  sum_Em/nr.jumps_1nn);               // <Em1nn>[eV]

    if (kmc_step == 0) fprintf(file_objects_out, " %i:CPU",i); i++;
    if (kmc_step != 0) fprintf(file_objects_out, " %1.2le",  print_out_cycle_time);              // CPU
    
//     if (nr.no_event > 0.0) 
//       fprintf(file_objects_out, ",	noev: %e",		nr.no_event/kmc_step);
    
    if (kmc_step == 0) fprintf(file_objects_out, " %i:Frame",i); i++;
    if (kmc_step != 0) fprintf(file_objects_out, " %i",      print_xyz_frame_nr);                // Frame
    
    if (tag.end_condition == 1){
      // Add last data line marker
      fprintf(file_objects_out, " #end\n");
    }
    else {
      fprintf(file_objects_out, "\n");
    }
  }
  

  /* Print to events.out */
  if (objects_out == 1) {
    i = 0;
    
    if (kmc_step == 0)  fprintf(file_events_out, "#%i:Step",    i); i++; 
    if (kmc_step != 0)  fprintf(file_events_out, "%e" ,         kmc_step);                        // Step

    if (kmc_step == 0)  fprintf(file_events_out, " %i:Time[s] ",i); i++; 
    if (kmc_step != 0)  fprintf(file_events_out, " %e ",        simulation_time);                 // Time[s]
    
    if (kmc_step == 0)  fprintf(file_events_out, " %i:1nn",     i); i++; 
    if (kmc_step != 0)  fprintf(file_events_out, " %1.2le",     nr.jumps_1nn);
    
    if (kmc_step == 0)  fprintf(file_events_out, " %i:2nn",     i); i++; 
    if (kmc_step != 0)  fprintf(file_events_out, " %1.2le",     nr.jumps_2nn);
    
    if (kmc_step == 0)  fprintf(file_events_out, " %i:3nn",     i); i++; 
    if (kmc_step != 0)  fprintf(file_events_out, " %1.2le",     nr.jumps_3nn);
    
    if (kmc_step == 0)  fprintf(file_events_out, " %i:4nn",     i); i++; 
    if (kmc_step != 0)  fprintf(file_events_out, " %1.2le",     nr.jumps_4nn);
    
    if (kmc_step == 0)  fprintf(file_events_out, " %i:5nn ",    i); i++; 
    if (kmc_step != 0) fprintf(file_events_out, " %1.2le ",     nr.jumps_5nn);
    
    if (kmc_step == 0)  fprintf(file_events_out, " %i:dep_ev",  i); i++; 
    if (kmc_step != 0)  fprintf(file_events_out, " %1.2le",     nr.deposition_events);
    
    if (kmc_step == 0)  fprintf(file_events_out, " %i:dep_reac",i); i++; 
    if (kmc_step != 0)  fprintf(file_events_out, " %1.2le",     nr.deposition_reactions);
    
    if (kmc_step == 0)  fprintf(file_events_out, " %i:ev",      i); i++; 
    if (kmc_step != 0)  fprintf(file_events_out, " %1.2le",     nr.evaporation_events);
    
    if (kmc_step == 0)  fprintf(file_events_out, " %i:Frame",   i); i++;
    if (kmc_step != 0)  fprintf(file_events_out, " %i #d",      print_xyz_frame_nr);                // Frame
    
    if (tag.end_condition == 1){
      // Add last data line marker
      fprintf(file_events_out, " #end\n");
    }
    else {
      fprintf(file_events_out, "\n");
    } 
  }
  
  if (tag.debug == 1){
    printf("    Probability check %e %e %e\n", sum_probabilities,sum_internal_probabilities,sum_external_probabilities);
    printf("    Probability check %e Internal err: %e (%e + %e + %e + %e), ext err: %e \n",kmc_step, internal_probability_sum_delta,nn_probability_sum_delta,nnn_probability_sum_delta,nn3_probability_sum_delta,nn5_probability_sum_delta, external_probability_sum_delta);  
  }
  
  tot_points = dimension.x*dimension.y*dimension.z*dimension.p;
  if (nr_points != tot_points) {
    printf("WARNING Nr of objects + points != total points: %i != %i\n",nr_points,tot_points);
  }
  
  fflush(stdout);
  if(objects_out == 1) {
    fflush(file_objects_out);
    fflush(file_events_out);
  }
  check_system_integrity_time1 = clock();
  check_system_integrity_time += (double)(check_system_integrity_time1 - check_system_integrity_time0) / CLOCKS_PER_SEC;
}


/* Go through the whole lattice and calculate the probabilities for migration jumps
 */
void add_probabilities(){
  printf("# Add probabilities\n");
  Pvector q,w;
  int omega;
  Jump jump;
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
            /** Internal events **/
            
            /* nn jumps */
            for (omega = 0; omega < max_1nn; omega++) {
              jump.omega   = omega;
              jump.initial = q;
              w            = omega_position_of_1nn(q, omega);
              jump.final   = w;
              jump.kind    = 1; // 1nn jump
              jump.signum_4d.a = system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
              jump.signum_4d.b = system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
              jump.signum_4d.c = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
              jump.signum_4d.d = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
              system_lattice[q.x][q.y][q.z][q.p].event_1nn[omega] = transition_probability(jump);
            }
            
            /* nnn jumps */
            if (tag.allow_nnn_jumps == 1) {
              for (omega = 0; omega < max_2nn; omega++) {
                jump.omega   = omega;
                jump.initial = q;
                w            = omega_position_of_2nn(q, omega);
                jump.final   = w;
                jump.kind    = 2; // 2nn jump
                jump.signum_4d.a = system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
                jump.signum_4d.b = system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
                jump.signum_4d.c = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
                jump.signum_4d.d = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
                system_lattice[q.x][q.y][q.z][q.p].event_2nn[omega] = transition_probability(jump);
              }
            }
            
            /* 3nn jumps */
            if (tag.allow_3nn_jumps == 1) {
              for (omega = 0; omega < max_3nn; omega++) {
                jump.omega   = omega;
                jump.initial = q;
                w            = omega_position_of_3nn(q, omega);
                jump.final   = w;
                jump.kind    = 3; // 3nn jump
                jump.signum_4d.a = system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
                jump.signum_4d.b = system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
                jump.signum_4d.c = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
                jump.signum_4d.d = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;                
                system_lattice[q.x][q.y][q.z][q.p].event_3nn[omega] = transition_probability(jump);
              }
            }
            
            /* 5nn jumps */
            if (tag.allow_5nn_jumps == 1) {
              for (omega = 0; omega < max_5nn; omega++) {
                jump.omega   = omega;
                jump.initial = q;
                w            = omega_position_of_5nn(q, omega);
                jump.final   = w;
                jump.kind    = 5; // 5nn jump
                jump.signum_4d.a = system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
                jump.signum_4d.b = system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
                jump.signum_4d.c = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
                jump.signum_4d.d = system_lattice[w.x][w.y][w.z][w.p].nr_2nn; 
                system_lattice[q.x][q.y][q.z][q.p].event_5nn[omega] = transition_probability(jump);
              }
            }
            
          }
        }
        // Only adatoms considered movable at this point.
      } 
    }
  }
  
  add_external_probabilies();
  
}



/* Sum all possible event probabilities 
 */
void calculate_sum_probabilities(){
  calculate_sum_probabilities_time0 = clock();
  int x,y,z,p;
  int nn, nnn, omega3, omega5;
  
  sum_internal_probabilities  = 0.0;
  sum_nn_jump_probabilities   = 0.0;
  sum_nnn_jump_probabilities  = 0.0;
  sum_3nn_jump_probabilities  = 0.0;
  sum_5nn_jump_probabilities  = 0.0;
  sum_external_probabilities  = 0.0;
  sum_probabilities = 0.0;
  for (x = 0; x < dimension.x; x++) {
    for (y = 0; y < dimension.y; y++) {
      for (z = 0; z < dimension.z; z++) {
        for (p = 0; p < dimension.p; p++) {
          // Only considering adatoms at the moment
          if (system_lattice[x][y][z][p].kind == adatom_kind) {
            
            /* Probabilities for nn jumps */
            for (nn = 0; nn < max_1nn; nn++) {
              sum_nn_jump_probabilities += system_lattice[x][y][z][p].event_1nn[nn].probability;
              // 	      printf("%e Probability summing: %e += %e\n",kmc_step, sum_probabilities,system_lattice[x][y][z][p].probability_nn_jump[nn]);
            }
            
            /* Probabilities for nnn jumps */
            if (tag.allow_nnn_jumps == 1) {
              for (nnn = 0; nnn < max_2nn; nnn++) {
                sum_nnn_jump_probabilities += system_lattice[x][y][z][p].event_2nn[nnn].probability;
              }
            }
            
            /* Probabilities for 3nn jumps */
            if (tag.allow_3nn_jumps == 1) {
              for (omega3 = 0; omega3 < max_3nn; omega3++) {
                sum_3nn_jump_probabilities += system_lattice[x][y][z][p].event_3nn[omega3].probability;
              }
            }
            
            /* Probabilities for 5nn jumps */
            if (tag.allow_5nn_jumps == 1) {
              for (omega5 = 0; omega5 < max_5nn; omega5++) {
                sum_5nn_jump_probabilities += system_lattice[x][y][z][p].event_5nn[omega5].probability;
              }
            }
            
            /* External events for atomic objects*/
            sum_external_probabilities += system_lattice[x][y][z][p].probability_evaporation;
            
          }
          if (system_lattice[x][y][z][p].kind == vacancy_kind) {
            
            /* External events for vacancies */
            sum_external_probabilities += system_lattice[x][y][z][p].probability_deposition;
            
          }
        }
      }
    }
  }
  
  sum_internal_probabilities  = definition_sum_internal_probabilities();
  sum_probabilities           = definition_sum_probabilities();
  printf("# Probability sum: %e = %e + %e\n", sum_probabilities,sum_internal_probabilities,sum_external_probabilities); 
  
  if (sum_probabilities <= 0.0){
    printf("%e WARNING Probability sum: %lf\n",kmc_step,sum_probabilities);
  }
  
  calculate_sum_probabilities_time1 = clock();
  calculate_sum_probabilities_time += (double) (calculate_sum_probabilities_time1 - calculate_sum_probabilities_time0)/CLOCKS_PER_SEC;
}





/* Remove object atomic object q */
void remove_object(Pvector q) {
  Jump jump;
  
  if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind || system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind) {
    
    jump = system_lattice[q.x][q.y][q.z][q.p].last_transition;
    printf("# Removing object %lf\n", system_lattice[q.x][q.y][q.z][q.p].index);
    printf("# Last 4d 1nn transition: (%i %i %i %i)\n", jump.signum_4d.a, jump.signum_4d.b, jump.signum_4d.c, jump.signum_4d.d);
    
    if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) nr.adatoms--;
    else if (system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind) nr.atoms--;
    
    nr.removed_atoms++;
    new_vacancy(q);
    
  }
  
//   else if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {
//     nr.removed_vacancies += 1;
//     system_lattice[q.x][q.y][q.z][q.p].kind = adatom_kind;
//   }

  update_bond_count(q);         // Update the bound count in the unit cells next to q and including q.
  add_probabilities_locally(q); // Adds probabilities based on nr_1nn and nr_2nn to all unit cells sourrounding q, including q.
  
}



// If object, jumping from u to v, is ends up outside a non-periodic boundary, remove object.
void check_non_pbc(Pvector u, Pvector v) {
  Pvector q;
  int nn;
  int absorbed = 0;
  int r1,r2;
  
  if (pbc.z == 0) {
    
    // Check if any of the 1nn neighbours are of fixed_empty_kind
    for (nn = 0; nn < max_1nn; nn++) {
      q = omega_position_of_1nn(v, nn);
      if (system_lattice[q.x][q.y][q.z][q.p].kind == fixed_empty_kind) {
        remove_object(u);
      }   
    }
  }
  if (pbc.x == 0) {
    // If object is jumping outside the boundaries in x direction, the object is removed.
    r1 = v.x; 
    r2 = boundary_condition(v.x, 0);

    if (r1 != r2) {
//       printf("Removing %i %i %i\n",u.x, r1,r2);
      remove_object(u);
    }
  }
  if (pbc.y == 0) {
    // If object is jumping outside the beoundaries in y direction, the object is removed.
    r1 = v.y; 
    r2 = boundary_condition(v.y, 1);

    if (r1 != r2) {
//       printf("Removing %i %i %i\n",u.x, r1,r2);
      remove_object(u);
    }
  }
}


// Check the conditions for evaporating of adatoms in the local space
// and remove them if the conditions are fulfilled.
void check_for_evaporation(Pvector q) {
  check_for_evaporation_time0 = clock();
  int x,y,z,p;
  Pvector r;
  
  for (x = -1; x <= 1; x++) {
    for (y = -1; y <= 1; y++) {
      for (z = -1; z <= 1; z++) {
        for (p = 0; p < dimension.p; p++) {
          r.x = boundary_condition(q.x + x, 0);
          r.y = boundary_condition(q.y + y, 1);
          r.z = boundary_condition(q.z + z, 2);
          r.p = p;
          
          if (system_lattice[r.x][r.y][r.z][r.p].kind == adatom_kind) {
            if (system_lattice[r.x][r.y][r.z][r.p].nr_1nn == 0 && system_lattice[r.x][r.y][r.z][r.p].nr_2nn == 0){
              
              /* Adatom has evaporated */
              remove_object(r); // Will update bond count and probabilities too.
            }
          }
        }
      }
    }
  }
  check_for_evaporation_time1 = clock();
  check_for_evaporation_time += (double) (check_for_evaporation_time1 - check_for_evaporation_time0) / CLOCKS_PER_SEC;
}

/* Remove atoms belonging to clusters detached from the bulk
 */
void remove_detached_clusters(int max_cluster_index) {
  Pvector q;
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          if (system_lattice[q.x][q.y][q.z][q.p].cluster_index > 0 &&
            system_lattice[q.x][q.y][q.z][q.p].cluster_index != bulk_cluster_index &&
            (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind || 
            system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind)) {
	    
            remove_object(q);
            // 	    printf("Removed cluster atom\n");
          }
        }
      }
    }
  }
}



/* Index all atomic clusters and determine their size
 * A cluster are all groups of atoms (or adatoms) connected 
 * with a chain of 1nn bonds between the atoms.
 */
void cluster_analysis() {
  cluster_analysis_time0 = clock();
  Pvector q,v;
  int max_cluster_index = 0;
  int max_cluster_index_old = -1;
  int cluster_index = 0;
  int nn;
  int i, cluster_size;
  int joined_clusters_found = 0;
  int unit_cell_atoms;
  double spherical_radius;
  double pi = 3.141592653589793;
  int cumulative_cluster_size		= 0;
  double cumulative_spherical_radius 	= 0;
  int nr_free_clusters 			= 0;		// Clusters detached from the bulk
  double average_cluster_size		= 0.0;
  double average_spherical_radius	= 0.0;
  
  bulk_cluster_index = -1;	// Initialization
  
  //   printf("#\n# Cluster analysis started\n");
  if (tag.end_condition == 1){
    // Add last data line marker
    fprintf(file_clusters,"### ");
  }
  else {
    fprintf(file_clusters,"## ");
  }
  fprintf(file_clusters,"%e %e s", kmc_step, simulation_time);
  fprintf(file_clusters,",	Frame: %i\n",	print_xyz_frame_nr); 
  
  /* Initialize cluster indicies for all objects */
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          system_lattice[q.x][q.y][q.z][q.p].cluster_index = 0;
        }
      }
    }
  }
  
  
  while (max_cluster_index > max_cluster_index_old || joined_clusters_found == 1) {	
    max_cluster_index_old = max_cluster_index;
    joined_clusters_found = 0;
    for (q.z = 0; q.z < dimension.z; q.z++) {
      for (q.y = 0; q.y < dimension.y; q.y++) {
        for (q.x = 0; q.x < dimension.x; q.x++) {
          for (q.p = 0; q.p < dimension.p; q.p++) {
            if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind ||
              system_lattice[q.x][q.y][q.z][q.p].kind == atom_kind ||
              system_lattice[q.x][q.y][q.z][q.p].kind == fixed_atom_kind) {
	      
              if (system_lattice[q.x][q.y][q.z][q.p].cluster_index == 0) {
                /* New cluster found */
                max_cluster_index++;
                system_lattice[q.x][q.y][q.z][q.p].cluster_index = max_cluster_index;
              }
	       
              /* Find out if any of the 1nn atoms are already indexed */
              cluster_index  = system_lattice[q.x][q.y][q.z][q.p].cluster_index;
              if (system_lattice[q.x][q.y][q.z][q.p].nr_1nn > 0) {
                for (nn = 0; nn < max_1nn; nn++) {
                  v = omega_position_of_1nn(q, nn);        
                  if (system_lattice[v.x][v.y][v.z][v.p].kind == atom_kind || 
                      system_lattice[v.x][v.y][v.z][v.p].kind == adatom_kind || 
                      system_lattice[v.x][v.y][v.z][v.p].kind == fixed_atom_kind) {
                    
                    if (system_lattice[v.x][v.y][v.z][v.p].cluster_index > 0 && 
                  system_lattice[v.x][v.y][v.z][v.p].cluster_index != cluster_index ) {
                      
                      /* cluster_index takes the lowest index (>0) value of the cluster atoms*/
                      joined_clusters_found = 1;
                      if (cluster_index > system_lattice[v.x][v.y][v.z][v.p].cluster_index)
                  cluster_index = system_lattice[v.x][v.y][v.z][v.p].cluster_index;
                      
                    }		    
                  }
                }
              }

              
              system_lattice[q.x][q.y][q.z][q.p].cluster_index = cluster_index;
              
              /* Check if q belongs to the bulk cluster (which contains fixed atoms) or the label label_bulk_cluster */
              if ( system_lattice[q.x][q.y][q.z][q.p].kind == fixed_atom_kind 
                || system_lattice[q.x][q.y][q.z][q.p].label == label_bulk_cluster) 
              {
                bulk_cluster_index = cluster_index;
              }
	      
              /* Give the 1nn atoms the same cluster index as q */
              if (system_lattice[q.x][q.y][q.z][q.p].nr_1nn > 0) {
                for (nn = 0; nn < max_1nn; nn++) {
                  v = omega_position_of_1nn(q, nn);
                  if (system_lattice[v.x][v.y][v.z][v.p].kind == atom_kind || 
                      system_lattice[v.x][v.y][v.z][v.p].kind == adatom_kind || 
                      system_lattice[v.x][v.y][v.z][v.p].kind == fixed_atom_kind) {
                    
                    system_lattice[v.x][v.y][v.z][v.p].cluster_index = cluster_index;
                  
                    /* Check if any of the 1nn atoms belong to the bulk (which contains fixed atoms) */
                    if ( system_lattice[v.x][v.y][v.z][v.p].kind == fixed_atom_kind
                      || system_lattice[v.x][v.y][v.z][v.p].label == label_bulk_cluster ) 
                    {
                      
                      bulk_cluster_index = cluster_index;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  /* Analysis of the cluster size distribution */
  nr.clusters 			= 0;
  cumulative_cluster_size 	= 0;
  cumulative_spherical_radius 	= 0;
  min_cluster_size		= nr.atomic_objects;	// Theoretical maximum cluster size as initialization.
  for (cluster_index = 1; cluster_index <= max_cluster_index; cluster_index++) {
    cluster_size = 0;  
    for (q.x = 0; q.x < dimension.x; q.x++) {
      for (q.y = 0; q.y < dimension.y; q.y++) {
        for (q.z = 0; q.z < dimension.z; q.z++) {
          for (q.p = 0; q.p < dimension.p; q.p++) {
            if (system_lattice[q.x][q.y][q.z][q.p].cluster_index == cluster_index) {
              cluster_size++;
            }
          }
        }
      }
    }
    if (cluster_size > 0) {
      nr.clusters++;
      
      /* Find the smallest cluster size */
      if (cluster_size < min_cluster_size) min_cluster_size = cluster_size;
      
      
      if (crystal == fcc) {
        unit_cell_atoms = 4;
      }
      if (crystal == bcc) {
        unit_cell_atoms = 2;
      }
            
      /* Radius if the cluster is approximated as a sphere */
      spherical_radius = a0*pow(3.0*cluster_size/(4.0*unit_cell_atoms*pi),1.0/3.0);
      
      if (cluster_index == bulk_cluster_index) {
        fprintf(file_clusters,"Bulk	%i,	size %i\n",cluster_index,cluster_size);
	
      }
      else {
        fprintf(file_clusters,"Cluster	%i,	size %i,	r ~ %e m",cluster_index,cluster_size,spherical_radius);
        if (tag.cluster_evaporation == 1) fprintf(file_clusters,"	Removed\n");
        else fprintf(file_clusters,"\n");
        cumulative_cluster_size 	+= cluster_size;
        cumulative_spherical_radius 	+= spherical_radius;
        nr_free_clusters++;		// Clusters detached from the bulk
      }
    }
  }
  
  if (nr_free_clusters > 0) {
    average_cluster_size	= cumulative_cluster_size/nr_free_clusters;
    average_spherical_radius	= cumulative_spherical_radius/nr_free_clusters;
  }
  else {
    average_cluster_size	= 0.0;
    average_spherical_radius	= 0.0;
  }
  
  /* Print out data */
  if (tag.end_condition == 1){
    // Add last data line marker
    fprintf(file_clusters,"#end ");
    
  }
  else {
    fprintf(file_clusters,"# ");
  }
  
  fprintf(file_clusters,"%e %e s, Free clusters: %i,	Av. size: %lf,	r ~ %e m", kmc_step, simulation_time, nr_free_clusters, average_cluster_size, average_spherical_radius);
  if (tag.cluster_evaporation == 1) fprintf(file_clusters,"	(Removed)");
  fprintf(file_clusters,",	Frame: %i\n\n",	print_xyz_frame_nr); 
  fflush(file_clusters);
  
  if (tag.cluster_evaporation == 1 && nr_free_clusters > 0) {
    remove_detached_clusters(max_cluster_index);
  }
  
//   printf("nr_objects = %i, ad %i, at %i, V %i\n",nr.objects, nr.adatoms, nr.atoms, nr.vacancies);
  
  cluster_analysis_time1 = clock();
  cluster_analysis_time += (double) (cluster_analysis_time1 - cluster_analysis_time0) / CLOCKS_PER_SEC;
}


/* Make jump from q0 to q1. Boundary conditions already checked. Global xyzp already updated.
 * 
 * Object struct members that should not be moved or changed: .helmod_xyz and .position
 */
void make_jump(Jump jump) {
  make_jump_time0 = clock();
  
  Pvector q0 = jump.initial;
  Pvector q1 = jump.final;
  int i_1nn,i_2nn;
  Pvector nn;
  int tmp1,tmp2;
  Object ini_atom, fin_atom;
  
  ini_atom = system_lattice[q0.x][q0.y][q0.z][q0.p];
  fin_atom = system_lattice[q1.x][q1.y][q1.z][q1.p];

  int tmp_kind;
  int tmp_element;
  int tmp_label;
  int tmp_cluster;
  double tmp_nr_jumps;
  Vector tmp_initial_position;
  int tmp_index;
  double tmp_depid;
  Jump last_jump;
  int omega,omega3,omega5;
  
  
  /*** Update the values of the moving objects ***/
  
  /* Count jumps for atoms/adatoms */
  tmp_nr_jumps = system_lattice[q1.x][q1.y][q1.z][q1.p].nr_jumps;
  if (
    system_lattice[q0.x][q0.y][q0.z][q0.p].kind == adatom_kind ||
    system_lattice[q0.x][q0.y][q0.z][q0.p].kind == atom_kind) {
    // Add one jump to the counter for adatoms and atoms
    system_lattice[q1.x][q1.y][q1.z][q1.p].nr_jumps = system_lattice[q0.x][q0.y][q0.z][q0.p].nr_jumps + 1.0;
  }
  else {
    /* Jumping vacancy? */
    system_lattice[q1.x][q1.y][q1.z][q1.p].nr_jumps = system_lattice[q0.x][q0.y][q0.z][q0.p].nr_jumps;
  }
  system_lattice[q0.x][q0.y][q0.z][q0.p].nr_jumps = tmp_nr_jumps;
  
  
  /* Update kind and element for target and initial point */
  tmp_kind 					= system_lattice[q1.x][q1.y][q1.z][q1.p].kind;
  system_lattice[q1.x][q1.y][q1.z][q1.p].kind 	= system_lattice[q0.x][q0.y][q0.z][q0.p].kind;
  system_lattice[q0.x][q0.y][q0.z][q0.p].kind	= tmp_kind;
  
  tmp_element 						= system_lattice[q1.x][q1.y][q1.z][q1.p].element;
  system_lattice[q1.x][q1.y][q1.z][q1.p].element 	= system_lattice[q0.x][q0.y][q0.z][q0.p].element;
  system_lattice[q0.x][q0.y][q0.z][q0.p].element	= tmp_element;
  
  
  /* Initial_position and index values move with the object */
  tmp_index 							= system_lattice[q1.x][q1.y][q1.z][q1.p].index;
  system_lattice[q1.x][q1.y][q1.z][q1.p].index			= system_lattice[q0.x][q0.y][q0.z][q0.p].index;
  system_lattice[q0.x][q0.y][q0.z][q0.p].index			= tmp_index;
  tmp_initial_position 						= system_lattice[q1.x][q1.y][q1.z][q1.p].initial_position;
  system_lattice[q1.x][q1.y][q1.z][q1.p].initial_position	= system_lattice[q0.x][q0.y][q0.z][q0.p].initial_position;
  system_lattice[q0.x][q0.y][q0.z][q0.p].initial_position	= tmp_initial_position;
 
  /* Deposition index values move with the object */
  tmp_depid                                               = system_lattice[q1.x][q1.y][q1.z][q1.p].deposition_index;
  system_lattice[q1.x][q1.y][q1.z][q1.p].deposition_index = system_lattice[q0.x][q0.y][q0.z][q0.p].deposition_index;
  system_lattice[q0.x][q0.y][q0.z][q0.p].deposition_index = tmp_depid;  
  
  
  /* Assume the adatom jumps within the same cluster (cluster index preserved) 
   * The check for emitted adatoms is done in update_object_kind().
   */
  tmp_cluster 						= system_lattice[q1.x][q1.y][q1.z][q1.p].cluster_index;
  system_lattice[q1.x][q1.y][q1.z][q1.p].cluster_index 	= system_lattice[q0.x][q0.y][q0.z][q0.p].cluster_index;
  system_lattice[q0.x][q0.y][q0.z][q0.p].cluster_index	= tmp_cluster;
  
  /* Update labels */
  tmp_label 					= system_lattice[q1.x][q1.y][q1.z][q1.p].label;
  system_lattice[q1.x][q1.y][q1.z][q1.p].label 	= system_lattice[q0.x][q0.y][q0.z][q0.p].label;
  system_lattice[q0.x][q0.y][q0.z][q0.p].label	= tmp_label;
  
  /* Record last 4d 1nn transitions of the atom */
  system_lattice[q1.x][q1.y][q1.z][q1.p].last_transition = jump;
    
    
  
  /** Update object values related to field and charge **/
  
  /* An atom will keep its charge when jumping to a vacancy, even tough the charges chould ideally be recalculated
   * at every kmc step.
   */
  system_lattice[q1.x][q1.y][q1.z][q1.p].electric_charge = ini_atom.electric_charge;
  system_lattice[q0.x][q0.y][q0.z][q0.p].electric_charge = 0.0; // A vacancy will not have any charge.
  
  /* Update field too, even though it should be recalculated to be accurate */
  system_lattice[q0.x][q0.y][q0.z][q0.p].electric_field = ini_atom.electric_field;  // The field of the inital position will not be changed
  system_lattice[q0.x][q0.y][q0.z][q0.p].scalar_field   = ini_atom.scalar_field;    //
  system_lattice[q1.x][q1.y][q1.z][q1.p].electric_field = fin_atom.electric_field;  // The jumping atom will obtain the field of the final position.
  system_lattice[q1.x][q1.y][q1.z][q1.p].scalar_field   = fin_atom.scalar_field;    //
  
  /* Nr of jumps without calling the field solver, add_electric_field_and_charge() */
  system_lattice[q0.x][q0.y][q0.z][q0.p].nr_jumps_with_constant_field++;
  system_lattice[q1.x][q1.y][q1.z][q1.p].nr_jumps_with_constant_field++;
  
  /* Check if field should be recalculated */
  if (system_lattice[q0.x][q0.y][q0.z][q0.p].nr_jumps_with_constant_field >= max_jumps_at_constant_field || 
      system_lattice[q1.x][q1.y][q1.z][q1.p].nr_jumps_with_constant_field >= max_jumps_at_constant_field) {
      
    tag.recalculate_field = 1;  // Recalculate the field
  }
  
  /* Needed before calculating the field */
  update_bond_count(q0); // Update the bond count in the unit cells next to q0 and including q0.
  update_bond_count(q1); // Update the bond count in the unit cells next to q1 and also q1.
  
#ifdef FIELD
  /* Recalculate the field*/
  if (tag.use_field == 1){ 
    electric_field_calculation_step++;
    
    /* Optimization for the apex (Only works with LibHelmod solver)*/
    if (electric_field_calculation_step > global_electric_field_recalculation_frequency) {
      electric_field_calculation_step = 1;  // The electric field will be calculated for the whole system
    }
    
    calculate_field_for_full_system();
    
  }

  
  /* Make sure the gradients around the last event is always updated */
  if (tag.use_field == 1) {
    add_gradient(q0);
    add_gradient(q1);
  }
#endif // FIELD
  
  /* Will update sum_probabilities and its subsums using the difference between the old and the new probabilites of q0 and q1 
   * (which should therefore not be zeroed in this function)
   */
  add_probabilities_locally(q0);
  add_probabilities_locally(q1);
  

  
  make_jump_time1 = clock();
  make_jump_time += (double) (make_jump_time1 - make_jump_time0) / CLOCKS_PER_SEC;
}



/* Choose an event and carry it out. 
 */
void choose_event() {
  choose_event_time0 = clock();
  double uF;
  double tmp_sum = 0.0;
  Pvector q0,q1,q1_np;
  Pvector G0,G1,G0_old,G1_old;
  int omega,omega3,omega5;
  int omega_tmp;
  int nnn;
  int i,j;
  int event_found = 0;
  int point_counter = 0;
  int a,b,c,d;
  char xyzp_file_name[100];
  Object adatom;
  int nr_tries = 0;
  int configuration_26d;
  float barrier;
  int element;
  Jump jump;

  while (nr_tries < 2) {
    nr_tries++;
  
    /* Save the cumulative probability sum (for the time calculations) */
    sum_probabilities_before_event = sum_probabilities;
    
    uF = random_double2()*sum_probabilities;	// Random real number in the inteval [0,sum_probabilities)
    q0.x = q0.y = q0.z = q0.p = omega = 0;
    //   printf("%e uF = %lf\n",kmc_step,uF);
    
    if (uF > sum_internal_probabilities) {
      tmp_sum += sum_internal_probabilities;	// Looking for an external event
    //     printf("# Looking for an external event\n");
    }
    else if (uF <= sum_internal_probabilities) {
      /* Looking for an internal event */
      
    
      if (uF > sum_nn_jump_probabilities && uF <= sum_internal_probabilities && (tag.allow_nnn_jumps == 1 
        || tag.allow_3nn_jumps || tag.allow_5nn_jumps )) 
      {
        tmp_sum +=sum_nn_jump_probabilities;	// Looking for a nnn jump event (internal event)
      }
      
      // Otherwise, looking for a nn jump event.
    }
    
    
    while (q0.x < dimension.x && event_found == 0) {
      q0.y = 0;
      while (q0.y < dimension.y && event_found == 0) {
        q0.z = 0;
        while (q0.z < dimension.z && event_found == 0) {
          q0.p = 0;
          while (q0.p < dimension.p && event_found == 0) {
            point_counter++;
            // printf("Counter: %i; %i %i %i %i\n",point_counter,x,y,z,p);
            // Only considering adatoms at the moment
            if (system_lattice[q0.x][q0.y][q0.z][q0.p].kind == adatom_kind) {
        
              if (uF <= sum_internal_probabilities) {
          
                /** Looking for an internal an event **/
          
                if (uF <= sum_nn_jump_probabilities) {
      
                    
                  /* Looking for a nn jump event */

                  omega = 0;
                  while (omega < max_1nn && event_found == 0) {
                    tmp_sum += system_lattice[q0.x][q0.y][q0.z][q0.p].event_1nn[omega].probability;
        
                    if (tmp_sum >= uF && sum_probabilities > 0.0) {
          
                      /* nn jump event */
                      event_found = 1;	// Will stop the search for events.
                      jump = system_lattice[q0.x][q0.y][q0.z][q0.p].event_1nn[omega]; // The jump to carry out.
                      
                      /* Calculate coordinates for the target point q1 */
                      q1_np = omega_position_of_1nn_with_no_PBC(q0,omega);
                      
                      check_non_pbc(q0,q1_np);	// Check for non-periodic boundaries. Should objects be removed?
                      
                      /* Apply PBC for q1. */
                      q1 = omega_position_of_1nn(q0, omega);
                      
                      /* Calculate global coordinate of q0 and q1 (for diffusion coeffiecent calculations) */
                      G0_old = system_lattice[q0.x][q0.y][q0.z][q0.p].global_xyzp;
                      G1_old = system_lattice[q1.x][q1.y][q1.z][q1.p].global_xyzp;
                      
                      /* Change of global coordinates for atom jumping from q1 to q0 */
                      G0 = omega_position_of_1nn_with_no_PBC(G1_old,omega);
                      
                      /* Change of global coordinates for atom jumping from q0 to q1 */
                      G1 = omega_position_of_1nn_with_no_PBC(G0_old,omega);                      
                  
                      /* Check if the adatom was removed; if not make jump. */
                      if (system_lattice[q0.x][q0.y][q0.z][q0.p].kind == adatom_kind) {
                        system_lattice[q0.x][q0.y][q0.z][q0.p].global_xyzp = G0;
                        system_lattice[q1.x][q1.y][q1.z][q1.p].global_xyzp = G1;
                        a = system_lattice[q0.x][q0.y][q0.z][q0.p].nr_1nn;
                        b = system_lattice[q0.x][q0.y][q0.z][q0.p].nr_2nn;
                        c = system_lattice[q1.x][q1.y][q1.z][q1.p].nr_1nn;
                        d = system_lattice[q1.x][q1.y][q1.z][q1.p].nr_2nn;
                        
                        if (tag.debug == 1) {
                          adatom = system_lattice[q0.x][q0.y][q0.z][q0.p];
//                         if (adatom.label == label_diffusion) {
                          printf("%e Barrier %f = %f + %f eV, F0 %le V/m, gradient %e V/m^2, Gamma %e 1/s\n",kmc_step, adatom.effective_barrier[omega], adatom.barrier[omega], adatom.field_gradient_barrier[omega],adatom.scalar_field, adatom.field_gradient[omega], adatom.event_1nn[omega].probability);
                          for (i = 0; i <= max_1nn; i++) {
                            if (adatom.event_1nn[i].probability > 1.0e-50) {
                              printf("  Eff.barrier %3.3lf eV,   gradient %e V/m^2,    Gamma %e 1/s\n",adatom.effective_barrier[i], adatom.field_gradient[i], adatom.event_1nn[i].probability);
                            }
//                           }
                           //configuration_26d = adatom.configuration_26d[omega];
                           //barrier = adatom.barrier[omega];
                           //element = adatom.element;
                           //printf("%le s: %d, %lf eV, element: %i\n",kmc_step, configuration_26d, barrier, element);
//                         }
//                           printf("%le s: %26.0lf, %lf eV, element: %i\n",kmc_step,system_lattice[q0.x][q0.y][q0.z][q0.p].configuration_26d[omega], system_lattice[q0.x][q0.y][q0.z][q0.p].barrier[omega],system_lattice[q0.x][q0.y][q0.z][q0.p].element);
                          }
                        }
                        
                        if (tag.debug == 1) printf("%e 1nn jump\n",kmc_step);
                        nr.jumps_1nn++;
                        
                        /* Print transition index */
                        
                        if(tag.print_transitions == 1) {
                          if (jump.effective_barrier < 1000.0) {
                          fprintf(file_transitions_out, "%10.0lf %lf\n", jump.index, jump.effective_barrier);
                          }
                          else if(jump.effective_barrier >= 1000.0) {
                            printf("WARNING: Forbidden transition (%i %i %i %i), Em = %lf, index = %10.0lf\n", jump.signum_4d.a,jump.signum_4d.b,jump.signum_4d.c,jump.signum_4d.d,jump.effective_barrier,jump.index);
                          }
                        }
                        
                        /* Make nn jump */
                        if (q0.x == jump.initial.x && q0.y == jump.initial.y && q0.z == jump.initial.z && q0.p == jump.initial.p &&
                            q1.x == jump.final.x && q1.y == jump.final.y && q1.z == jump.final.z && q1.p == jump.final.p) {
                          
                          /* Check */
                          if (omega != jump.omega) {
                            printf("ERROR choose_event(): omega != jump.omega");
                          }
                          
                          /* Check */
                          if (jump.signum_4d.a != a || jump.signum_4d.b != b || jump.signum_4d.c != c || jump.signum_4d.d != d) { 
                            printf("WARNING Jump %inn (%i %i %i %i) != (%i %i %i %i), %4.0lf prob %le\n", jump.kind, a,b,c,d, jump.signum_4d.a, jump.signum_4d.b, jump.signum_4d.c, jump.signum_4d.d, jump.index, jump.probability);
                            printf("Possible 1nn events:\n");
                            for (omega_tmp = 0; omega_tmp < max_1nn; omega_tmp++) {
                              printf("  %i %4.0lf\n", omega_tmp, system_lattice[q0.x][q0.y][q0.z][q0.p].event_1nn[omega_tmp].index);
                            }
                          }
                          
                          make_jump(jump);	// Make jump from q0 to q1 and update all neighbours bonds (will uppdate q0 and q1). Will update fields and gradients.
                        }
                        else {
                          printf("ERROR choose_event: jump != q0 -> q1.");
                          exit(1);
                        }
                          
                        if (a < 4) nr.jumps_unstable++;
                        sum_ca_bonds 	+= c-a;
                        sum_Em 		+= j1nn_parameter_table[a][b][c][d].intrinsic_barrier;		// TODO Implement for the 26D parameterization
                        nr.jumps_1nn++;
                        
                        /* Checking if any adatom in the local space should be considered evaporated
                        * because it has been left without neighbours.
                        */
                        check_for_evaporation(q0);
                        check_for_evaporation(q1);
                        
                      }
                    }
                    omega++;
                  }
                }
                else if (uF > sum_nn_jump_probabilities && uF <= sum_internal_probabilities) {
                  // Remember: sum_internal_probabilities == sum_nn_jump_probabilities + sum_nnn_jump_probabilities + sum_3nn_jump_probabilities + sum_5nn_jump_probabilities
 
                                    
                  /* Looking for a 2nn jump event */
                  if (tag.allow_nnn_jumps == 1) {
                    nnn = 0;
                    while (nnn < max_2nn && event_found == 0) {
                      tmp_sum += system_lattice[q0.x][q0.y][q0.z][q0.p].event_2nn[nnn].probability;
                      
                      if (tmp_sum >= uF && sum_probabilities > 0.0) {
                        
                        /* nnn jump event */
                        event_found = 1;	// Will stop the search for events.
                        jump = system_lattice[q0.x][q0.y][q0.z][q0.p].event_2nn[nnn]; // The jump to carry out.
                        // printf("%e Event: nnn jump\n",kmc_step);
                        
                        /* Calculate coordinates for the target point q1 */
                        q1_np = omega_position_of_2nn_with_no_PBC(q0, nnn);
                        
                        check_non_pbc(q0,q1_np);	// Check for non-periodic boundaries. Should objects be removed?
                        
                        /* Apply PBC for q1 */
                        q1 = omega_position_of_2nn(q0, nnn);
                        
                        /* Calculate global coordinate of q0 and q1 (for diffusion coeffiecent calculations) */
                        G0_old = system_lattice[q0.x][q0.y][q0.z][q0.p].global_xyzp;
                        G1_old = system_lattice[q1.x][q1.y][q1.z][q1.p].global_xyzp;
                        
                        /* Change of global coordinates for atom jumping from q1 to q0 */
                        G0 = omega_position_of_2nn_with_no_PBC(G1_old, nnn);
                        
                        /* Change of global coordinates for atom jumping from q0 to q1 */
                        G1 = omega_position_of_2nn_with_no_PBC(G0_old, nnn);                        
                    
                        /* Check if the adatom was removed; if not make jump. */
                        if (system_lattice[q0.x][q0.y][q0.z][q0.p].kind == adatom_kind) {
                          system_lattice[q0.x][q0.y][q0.z][q0.p].global_xyzp = G0;
                          system_lattice[q1.x][q1.y][q1.z][q1.p].global_xyzp = G1;

                          if (tag.debug == 1) printf("%e 2nn jump\n",kmc_step);
                          nr.jumps_2nn++;
                          tag.recalculate_field = 1; // For jumps larger than 1nn, always recalculat the field.
                          if (q0.x == jump.initial.x && q0.y == jump.initial.y && q0.z == jump.initial.z && q0.p == jump.initial.p &&
                            q1.x == jump.final.x && q1.y == jump.final.y && q1.z == jump.final.z && q1.p == jump.final.p) {
                            make_jump(jump);  // Make jump from q0 to q1 and update all neighbours bonds (will uppdate q0 and q1). Will update fields and gradients.
                          }
                          else {
                            printf("ERROR choose_event: jump != q0 -> q1.");
                            exit(1);
                          }

                                                    
                          /* Checking if any adatom in the local space should be considered evaporated
                          * because it has been left without neighbours.
                          */
                          check_for_evaporation(q0);
                          check_for_evaporation(q1);
                        }
                      }
                      nnn++;
                    }
                  }
      
                  /* Looking for a 3nn jump event */
                  if (tag.allow_3nn_jumps == 1) {
                    omega3 = 0;
                    while (omega3 < max_3nn && event_found == 0) {
                      tmp_sum += system_lattice[q0.x][q0.y][q0.z][q0.p].event_3nn[omega3].probability;
                      
                      if (tmp_sum >= uF && sum_probabilities > 0.0) {
                        
                        /* 3nn jump event */
                        event_found = 1;  // Will stop the search for events.
                        jump = system_lattice[q0.x][q0.y][q0.z][q0.p].event_3nn[omega3]; // The jump to carry out.
                        // printf("%e Event: omega3 jump\n",kmc_step);
                        
                        /* Calculate coordinates for the target point q1 */
                        q1_np = omega_position_of_3nn_with_no_PBC(q0, omega3);
                        
                        check_non_pbc(q0,q1_np);  // Check for non-periodic boundaries. Should objects be removed?
                        
                        /* Apply PBC for q1 */
                        q1 = omega_position_of_3nn(q0, omega3);
                        
                        /* Calculate global coordinate of q0 and q1 (for diffusion coeffiecent calculations) */
                        G0_old = system_lattice[q0.x][q0.y][q0.z][q0.p].global_xyzp;
                        G1_old = system_lattice[q1.x][q1.y][q1.z][q1.p].global_xyzp;
                        
                        /* Change of global coordinates for atom jumping from q1 to q0 */
                        G0 = omega_position_of_3nn_with_no_PBC(G1_old, omega3);
                        
                        /* Change of global coordinates for atom jumping from q0 to q1 */
                        G1 = omega_position_of_3nn_with_no_PBC(G0_old, omega3);                        
                    
                        /* Check if the adatom was removed; if not make jump. */
                        if (system_lattice[q0.x][q0.y][q0.z][q0.p].kind == adatom_kind) {
                          system_lattice[q0.x][q0.y][q0.z][q0.p].global_xyzp = G0;
                          system_lattice[q1.x][q1.y][q1.z][q1.p].global_xyzp = G1;
                          if (tag.debug == 1) printf("%e 3nn jump\n",kmc_step);
                          nr.jumps_3nn++;
                          tag.recalculate_field = 1; // For jumps larger than 1nn, always recalculat the field.
                          if (q0.x == jump.initial.x && q0.y == jump.initial.y && q0.z == jump.initial.z && q0.p == jump.initial.p &&
                            q1.x == jump.final.x && q1.y == jump.final.y && q1.z == jump.final.z && q1.p == jump.final.p) {
                            make_jump(jump);  // Make jump from q0 to q1 and update all neighbours bonds (will uppdate q0 and q1). Will update fields and gradients.
                          }
                          else {
                            printf("ERROR choose_event: jump != q0 -> q1.");
                            exit(1);
                          }
                          
                          /* Checking if any adatom in the local space should be considered evaporated
                          * because it has been left without neighbours.
                          */
                          check_for_evaporation(q0);
                          check_for_evaporation(q1);
                        }
                      }
                      omega3++;
                    }
                  }
                  
                  /* Looking for a 5nn jump event */
                  if (tag.allow_5nn_jumps == 1) {
                    omega5 = 0;
                    while (omega5 < max_5nn && event_found == 0) {
                      tmp_sum += system_lattice[q0.x][q0.y][q0.z][q0.p].event_5nn[omega5].probability;
                      
                      if (tmp_sum >= uF && sum_probabilities > 0.0) {
                        
                        /* 5nn jump event */
                        event_found = 1;  // Will stop the search for events.
                        jump = system_lattice[q0.x][q0.y][q0.z][q0.p].event_5nn[omega5]; // The jump to carry out.
                        // printf("%e Event: omega5 jump\n",kmc_step);
                        
                        /* Calculate coordinates for the target point q1 */
                        q1_np = omega_position_of_5nn_with_no_PBC(q0, omega5);
                        
                        check_non_pbc(q0,q1_np);  // Check for non-periodic boundaries. Should objects be removed?
                        
                        /* Apply PBC for q1 */
                        q1 = omega_position_of_5nn(q0, omega5);
                        
                        /* Calculate global coordinate of q0 and q1 (for diffusion coeffiecent calculations) */
                        G0_old = system_lattice[q0.x][q0.y][q0.z][q0.p].global_xyzp;
                        G1_old = system_lattice[q1.x][q1.y][q1.z][q1.p].global_xyzp;
                        
                        /* Change of global coordinates for atom jumping from q1 to q0 */
                        G0 = omega_position_of_5nn_with_no_PBC(G1_old, omega5);
                        
                        /* Change of global coordinates for atom jumping from q0 to q1 */
                        G1 = omega_position_of_5nn_with_no_PBC(G0_old, omega5);                        
                    
                        /* Check if the adatom was removed; if not make jump. */
                        if (system_lattice[q0.x][q0.y][q0.z][q0.p].kind == adatom_kind) {
                          system_lattice[q0.x][q0.y][q0.z][q0.p].global_xyzp = G0;
                          system_lattice[q1.x][q1.y][q1.z][q1.p].global_xyzp = G1;
                          
                          
//                           adatom = system_lattice[q0.x][q0.y][q0.z][q0.p];
//                           if (adatom.label == label_diffusion) {
//                             printf("%e Gamma %e 1/s\n",kmc_step, adatom.probability_5nn_jump[omega]);
//                             for (i = 0; i <= max_5nn; i++) {
//                               if (adatom.probability_5nn_jump[i] > 0.0) {
//                                 printf("  Gamma %e 1/s\n", adatom.probability_5nn_jump[i]);
//                               }
//                             }
//                
//                           }
                          
                          if (tag.debug == 1) printf("%e 5nn jump\n",kmc_step);
                          nr.jumps_5nn++;
                          tag.recalculate_field = 1; // For jumps larger than 1nn, always recalculat the field.
                          if (q0.x == jump.initial.x && q0.y == jump.initial.y && q0.z == jump.initial.z && q0.p == jump.initial.p &&
                            q1.x == jump.final.x && q1.y == jump.final.y && q1.z == jump.final.z && q1.p == jump.final.p) {
                            make_jump(jump);  // Make jump from q0 to q1 and update all neighbours bonds (will uppdate q0 and q1). Will update fields and gradients.
                          }
                          else {
                            printf("ERROR choose_event: jump != q0 -> q1.");
                            exit(1);
                          }
                          
                          /* Checking if any adatom in the local space should be considered evaporated
                          * because it has been left without neighbours.
                          */
                          check_for_evaporation(q0);
                          check_for_evaporation(q1);
                        }
                      }
                      omega5++;
                    }
                  }
                  
                }
                
              }
              
              else if(uF > sum_internal_probabilities) {
          
                /** Looking for an external event **/

                /* Looking for an evaporation event (Adatom -> V) */
          
                tmp_sum += system_lattice[q0.x][q0.y][q0.z][q0.p].probability_evaporation;
                if (tmp_sum >= uF && sum_probabilities > 0.0 && event_found == 0) {
                  event_found = 1;	// Will stop the search for events.
                  
                  // printf("%e Evaporation event\n",kmc_step);
                  nr.evaporation_events++;
                  nr.removed_atoms++;
                  new_vacancy(q0);	// Convert adatom to vacancy (evaporation)
                  
                  update_bond_count(q0); // Needed before calculating the field. 
#ifdef FIELD
                  /* Recalculate the field*/
                  if (tag.use_field == 1){ 
                    electric_field_calculation_step++;
                    
                    /* Optimization for the apex (Only works with LibHelmod solver)*/
                    if (electric_field_calculation_step > global_electric_field_recalculation_frequency) {
                      electric_field_calculation_step = 1;  // The electric field will be calculated for the whole system
                    }
                    
                    calculate_field_for_full_system(); // Will update nr.adatoms and nr.vacancies by calling count_number_atoms()
                    
                  }
                  
                  /* Make sure the gradients around the last event is always updated */
                  if (tag.use_field == 1) {
                    add_gradient(q0);
                  }
#endif
      
                  add_probabilities_locally(q0);
                }
              } 
            }
      
            if (system_lattice[q0.x][q0.y][q0.z][q0.p].kind == vacancy_kind) {
        
              /* Looking for a deposition event (V -> adatom) */
        
              if(uF > sum_internal_probabilities) {
                tmp_sum += system_lattice[q0.x][q0.y][q0.z][q0.p].probability_deposition;
                if (tmp_sum >= uF && sum_probabilities > 0.0 && event_found == 0) {
                  event_found = 1;	// Will stop the search for events.
                  
//                   printf("%e Deposition event\n",kmc_step);
                  nr.deposition_events++;
                  nr.removed_vacancies++;
                  new_adatom(q0);         // Convert vacancy into adatom (deposition)
                  update_bond_count(q0);  // Neeeded before calculating the field
#ifdef FIELD
                  /* Recalculate the field*/
                  if (tag.use_field == 1){ 
                    electric_field_calculation_step++;
                    
                    /* Optimization for the apex (Only works with LibHelmod solver)*/
                    if (electric_field_calculation_step > global_electric_field_recalculation_frequency) {
                      electric_field_calculation_step = 1;  // The electric field will be calculated for the whole system
                    }
                    
                    calculate_field_for_full_system();
                    
                  }
                  
                  /* Make sure the gradients around the last event is always updated */
                  if (tag.use_field == 1) {
                    add_gradient(q0);
                  }
#endif // FIELD
                  
                  
                  add_probabilities_locally(q0);
                  
                }
              }
            }
            q0.p++;
          }
          q0.z++;
        }
        q0.y++;
      }
      q0.x++;
    }
    
    /* Check if an event was found; if not, recalculate the probability sum and try one more time */
    if (event_found == 0 && nr_tries == 1) {
      printf("%e No event found\n", kmc_step);
      printf("%e %e %e uF %e tmp_sum %e\n", sum_probabilities,sum_internal_probabilities,sum_external_probabilities, uF,tmp_sum);
      printf("Recalculating the probability sum and trying again to find an event\n");
      calculate_sum_probabilities();
    }
    else if(event_found == 0 && nr_tries > 1) {  
      printf("%e No event found on second trial\n", kmc_step);
      printf("%e %e %e uF %e tmp_sum %e\n", sum_probabilities,sum_internal_probabilities,sum_external_probabilities, uF,tmp_sum);
      snprintf(xyzp_file_name, sizeof(xyzp_file_name), "objects_error.xyzp");
      print_xyzp(xyzp_file_name);
      check_system_integrity(1);
      print_frame_xyz(1);    
      nr.no_event++;
      exit(1);
    }
  }
  
  choose_event_time1 = clock();
  choose_event_time += (double) (choose_event_time1 - choose_event_time0) / CLOCKS_PER_SEC;
}
  
/* With a fixed rate (atoms/second), deposite atoms */
void check_for_deposition_reactions(){
  int rnd = 0;
  int i;
  int Ndep;
  
  count_number_atoms();
  
  Ndep = 0;
  while ((Ndep + nr.deposition_reactions)/simulation_time < deposition_per_second){
    Ndep++;
  }
  
  if (Ndep > 0) {
    add_adatoms(Ndep);
    nr.deposition_reactions = nr.deposition_reactions + Ndep;
  }
  
}

  
  
/* Define the conditions for stopping the KMC simulation.
 * tag.end_condition = 1 stops the simulation.
 */
void check_end_conditions() {
  if (kmc_step >= max_kmc_step) {
    tag.end_condition = 1;
    printf("Maximum number of KMC steps reached. Ending simulation\n");
  }
  if (sum_probabilities <= 0.0) {
    tag.end_condition = 1;
    printf("Sum of probabilities = %e. No events are possible. Ending.\n",sum_probabilities);
  }
  if (height <= min_height && surface_z > 0) {
    tag.end_condition = 1;
    printf("Minimum height reached, height = %le <= %le [m]. Ending simulation.\n",height,min_height);
  }
  if (height >= max_height && surface_z > 0) {
    tag.end_condition = 1;
    printf("Maximum height reached, height = %le >= %le [m]. Ending simulation.\n",height,max_height);
  }
  if (simulation_time >= max_simulation_time) {
    tag.end_condition = 1;
    printf("Maximum simulation time reached. Ending simulation.\n");
  }
  if (nr.clusters > max_nr_clusters) {
    tag.end_condition = 1;
    printf("Maximum nr of clusters reached. Ending simulation.\n");
  }
  if (fabs(max_local_field) >= fabs(local_field_limit)) {
    tag.end_condition = 1;
    printf("Maximum local field reached. Ending simulation.\n");
  }
  // Effective evaporation (events + removal)
  if (nr.removed_atoms > max_nr_evaporated_atoms) {
    tag.end_condition = 1;
    printf("Maximum nr of evaporated atoms reached. Ending simulation.\n");
  }
  if (nr.atomic_objects > max_nr_atomic_objects) {
    tag.end_condition = 1;
    printf("Maximum nr of atoms reached. Ending simulation.\n");
  }
  if (CPU_time >= max_CPU_time && max_CPU_time >= 0) {
    tag.end_condition = 1;
    printf("Maximum CPU time reached. Ending simulation.\n");
  }
  
  
}


/* Define the conditions for stopping the statistical KMC runs
 * tag.kmc_runs_end_condition = 1 stops the cycle of KMC runs.
 */
void check_kmc_runs_end_conditions() {
  if (kmc_run >= max_kmc_run && max_kmc_run > 0) {
    tag.kmc_runs_end_condition = 1;
    printf("Maximum number of KMC runs reached. Ending simulation\n");
  }
}


void check_parameter_table() {
  int A1nn,A2nn,B1nn,B2nn;
  float value;
  for (A1nn=0;A1nn<max_1nn;A1nn++) {
    for (A2nn=0;A2nn<6;A2nn++) {
      for (B1nn=0;B1nn<max_1nn;B1nn++) {
        for (B2nn=0;B2nn<6;B2nn++) {
          value = j1nn_parameter_table[A1nn][A2nn][B1nn][B2nn].intrinsic_barrier;
          if (value <100.0 || value >100.0) printf("value %f %i %i %i %i\n",value,A1nn+1,A2nn+1,B1nn+1,B2nn+1);
        }
      }
    }
  }
}


/* Calculate the input parameters for PARCAS */
void calculate_parcas_input() {
  if (tag.print_parcas_files == 1) {
    int natoms;
    double box1,box2,box3, zmin;
    int ncell1,ncell2,ncell3;
    
    file_parcas_mdin = fopen("./md.in","w");
    natoms = nr.atoms + nr.adatoms + nr.fixed_atoms;
    box1 = dimension.x*unit_cell_dimension.x*a0*1e10;	// Box width in angstrom
    box2 = dimension.y*unit_cell_dimension.y*a0*1e10;
    box3 = dimension.z*unit_cell_dimension.z*a0*1e10;
    
    zmin = -box3/2.0 + min_height*1.0e10;	// Minimum height allowed in angstrom; zmin = 0 at box1/2 in Parcas.
    
    fprintf(file_parcas_mdin,"# Input parameters for Parcas md.in\n");
    fprintf(file_parcas_mdin,"# ncell is only important for CLIC simulations (with an electric field)\n");
    fprintf(file_parcas_mdin,"# and only works with the (100) surface at the moment\n\n");
    
    fprintf(file_parcas_mdin,"# Free format input file - parameters can be in any order, and comments\n");
    fprintf(file_parcas_mdin,"# can be placed freely among them, as long as the parameters hold the\n");
    fprintf(file_parcas_mdin,"# 9-characters + \"=\"-sign format. Each number must also be followed\n");
    fprintf(file_parcas_mdin,"# by at least one space (not a tab !).\n\n");
    
    fprintf(file_parcas_mdin,"debug    = 0\n");
    fprintf(file_parcas_mdin,"nsteps   = 40000000\n");
    fprintf(file_parcas_mdin,"tmax     = 200000000.0\n");
    fprintf(file_parcas_mdin,"restartt = 0.0\n");
    fprintf(file_parcas_mdin,"endtemp  = 0.0\n");
    fprintf(file_parcas_mdin,"seed     = %i\n", random_n(1000000));
    fprintf(file_parcas_mdin,"\n");

    fprintf(file_parcas_mdin,"Atom type-dependent parameters.\n");
    fprintf(file_parcas_mdin,"Time unit is 10.1805*sqrt(mass) = 81.15 fs for Cu, 52.88 for Al\n");
    fprintf(file_parcas_mdin,"-------------------------------------------------------------------\n");
    fprintf(file_parcas_mdin,"delta    = 0.050\n");			// Time step; not sure about units.
    fprintf(file_parcas_mdin,"ntype    = 3\n"); 
    fprintf(file_parcas_mdin,"mass(0)  = 83.8000\n"); 
    fprintf(file_parcas_mdin,"name(0)  = Kr\n"); 
    fprintf(file_parcas_mdin,"mass(1)  = 63.550\n"); 
    fprintf(file_parcas_mdin,"name(1)  = Cu\n"); 
    fprintf(file_parcas_mdin,"mass(2)  = 63.550\n"); 
    fprintf(file_parcas_mdin,"name(2)  = Cx\n"); 
    fprintf(file_parcas_mdin,"\n");

    fprintf(file_parcas_mdin,"substrate= Cu\n"); 
    fprintf(file_parcas_mdin,"iac(0,0) = -1\n"); 
    fprintf(file_parcas_mdin,"iac(0,1) = 2\n"); 
    fprintf(file_parcas_mdin,"iac(1,1) = 1\n"); 
    fprintf(file_parcas_mdin,"iac(0,2) = 0\n"); 
    fprintf(file_parcas_mdin,"iac(1,2) = 0\n"); 
    fprintf(file_parcas_mdin,"iac(2,2) = 0\n");  
    fprintf(file_parcas_mdin,"potmode  = 2\n"); 
    fprintf(file_parcas_mdin,"reppotcut= 10.0\n"); 
    fprintf(file_parcas_mdin,"\n");

    fprintf(file_parcas_mdin,"Simulation cell\n");
    fprintf(file_parcas_mdin,"---------------\n");
    fprintf(file_parcas_mdin,"latflag  = 1\n"); 
    fprintf(file_parcas_mdin,"mdlatxyz = 1\n"); 
    fprintf(file_parcas_mdin,"nprtbl   = 100000\n"); 
    fprintf(file_parcas_mdin,"neiskinr = 1.15\n"); 
    fprintf(file_parcas_mdin,"natoms   = %i\n",natoms);
    fprintf(file_parcas_mdin,"box(1)   = %f\n",box1);
    fprintf(file_parcas_mdin,"box(2)   = %f\n",box2);
    fprintf(file_parcas_mdin,"box(3)   = %f\n",box3);
    fprintf(file_parcas_mdin,"ncell(1) = %i\n",dimension.x);		// ncell is only needed for CLIC electric field calculations and only work for the (100) orientation.
    fprintf(file_parcas_mdin,"ncell(2) = %i\n",dimension.y);
    fprintf(file_parcas_mdin,"ncell(3) = %i\n",dimension.z);
    fprintf(file_parcas_mdin,"ncellp(1)= 2\n"); 
    fprintf(file_parcas_mdin,"ncellp(2)= 2\n"); 
    fprintf(file_parcas_mdin,"ncellp(3)= 2\n"); 
    fprintf(file_parcas_mdin,"pb(1)    = %i\n",	pbc.x); 
    fprintf(file_parcas_mdin,"pb(2)    = %i\n",	pbc.y); 
    fprintf(file_parcas_mdin,"pb(3)    = %i\n", pbc.z); 
    fprintf(file_parcas_mdin,"nvac     = 0\n"); 
    fprintf(file_parcas_mdin,"zmin     = %lf\n",zmin);			// Only works in Ville's version of Parcas.
    fprintf(file_parcas_mdin,"\n");
    
    fprintf(file_parcas_mdin,"Simulation\n"); 
    fprintf(file_parcas_mdin,"----------\n"); 
    fprintf(file_parcas_mdin,"mtemp    = 1\n");				// Temperature mode
    fprintf(file_parcas_mdin,"temp0    = 0.00\n");  
    fprintf(file_parcas_mdin,"timeini  = 0.00\n");
    fprintf(file_parcas_mdin,"ntimeini = 0\n");  
    fprintf(file_parcas_mdin,"initemp  = %f\n", Temperature);		// Initial temperature [K] (Gaussian distribution)  
    fprintf(file_parcas_mdin,"temp     = %f\n", Temperature);		// Desired temperature [K]  
    fprintf(file_parcas_mdin,"toll     = 0.00\n");  
    fprintf(file_parcas_mdin,"btctau   = 20.00\n");			// Temperature control
    fprintf(file_parcas_mdin,"trate    = 0.00\n");  
    fprintf(file_parcas_mdin,"bpcmode  = 0\n"); 
    fprintf(file_parcas_mdin,"tdebye   = 0.00\n");  
    fprintf(file_parcas_mdin,"amp      = 0.000000\n");  
    fprintf(file_parcas_mdin,"1/B: 7.3d-4 Cu\n");			// Pressure control parameter for Cu
    fprintf(file_parcas_mdin,"bpcbeta  = 1.0d-3\n");  
    fprintf(file_parcas_mdin,"bpctau   = 1000.00\n");  
    fprintf(file_parcas_mdin,"bpcP0    = 0.00\n");  
    fprintf(file_parcas_mdin,"bpcmode  = 0\n"); 			// Pressure control 
    fprintf(file_parcas_mdin,"tscaleth = 3.61500\n");  
    fprintf(file_parcas_mdin,"Emaxbrdr = 1000.0\n");  
    fprintf(file_parcas_mdin,"damp     = 0.00000\n");  
    fprintf(file_parcas_mdin,"ndump    = 1000000\n");			// Steps between std output 
    fprintf(file_parcas_mdin,"nmovie   = 1000000\n"); 			// Steps between movie frame dumps	
    fprintf(file_parcas_mdin,"moviemode= 7\n");				// Movie mode
    fprintf(file_parcas_mdin,"dslice(1)= -1.00\n");  
    fprintf(file_parcas_mdin,"dslice(2)= 3.62\n");  
    fprintf(file_parcas_mdin,"dslice(3)= -1.00\n");  
    fprintf(file_parcas_mdin,"dtsli(1) = 10000000.00\n");  
    fprintf(file_parcas_mdin,"tsli(1)  = 20000000.00\n");  
    fprintf(file_parcas_mdin,"dtsli(2) = 50000000.00\n");  
    fprintf(file_parcas_mdin,"dtsli(3) = 20000000.00\n");  
    fprintf(file_parcas_mdin,"ECM(1)   = 0.00\n");  
    fprintf(file_parcas_mdin,"ECM(2)   = 0.00\n");  
    fprintf(file_parcas_mdin,"ECM(3)   = 0.00\n");  
    fprintf(file_parcas_mdin,"ECMfix   = 1\n");  
    fprintf(file_parcas_mdin,"nliqanal = 100\n");  
    fprintf(file_parcas_mdin,"ndefmovie= 200\n");  
    fprintf(file_parcas_mdin,"nrestart = 1000\n");  
    fprintf(file_parcas_mdin,"Ekdef    = 0.220\n");  
    fprintf(file_parcas_mdin,"\n");

    fprintf(file_parcas_mdin,"CLIC simulation special parameters\n"); 
    fprintf(file_parcas_mdin,"----------------------------------\n");
    fprintf(file_parcas_mdin,"clneicut = 3.090\n");  			// Cut-off distance for Coloumb interaction (default 3.09) (ignore)
    fprintf(file_parcas_mdin,"elfield  = %lf\n",external_field*1e-10);	// [V/Å]	Maximum value of applied electric field
    fprintf(file_parcas_mdin,"timerelE = 1\n");  			// [fs]		Time to let system relax before external field is applied
    fprintf(file_parcas_mdin,"Erate    = -0.001\n");  			// [V/Å/fs]	Speed at which electric field is increased
    fprintf(file_parcas_mdin,"evpintval= 0\n"); 			// APT (ignore)
    fprintf(file_parcas_mdin,"mctemp   = 100\n"); 			// APT (ignore)
    fprintf(file_parcas_mdin,"evpatype = 2\n");				// Set atoms that evaporate to this type (default 0)
    fprintf(file_parcas_mdin,"iabove   = %i\n", dimension.z);
    fprintf(file_parcas_mdin,"\n");
    fprintf(file_parcas_mdin,"ionpot(0)= 0.0\n"); 
    fprintf(file_parcas_mdin,"ionpot(1)= 7.73\n"); 
    fprintf(file_parcas_mdin,"ionpot(2)= 7.90\n"); 
    fprintf(file_parcas_mdin,"ionpot(3)= 0.0\n"); 
    fprintf(file_parcas_mdin,"wrkfn(0) = 0.0\n"); 
    fprintf(file_parcas_mdin,"wrkfn(1) = 4.6\n"); 
    fprintf(file_parcas_mdin,"wrkfn(2) = 4.4\n"); 
    fprintf(file_parcas_mdin,"wrkfn(3) = 0.0\n"); 
    fprintf(file_parcas_mdin,"\n");
  }
}
 
 
/* Increase the simulation time according to the resident algorithm 
 * [young1966monte]  
 */
void increase_time() {
  
  simulation_time += -log(random_double3())/sum_probabilities_before_event;
  
//   printf("    sum_probabilities_before_event = %e\n",sum_probabilities_before_event);
}


/* Look for reactions (have no probability rate but happens with a fixed time rate) */
void look_for_reactions() {
  
  check_for_deposition_reactions();
  
}




/* The KMC algorithm. 
 * Carry out KMC steps until the end 
 * conditions are fulfilled.
 * 
 * Initial probabilites (including field contributions) have already been calcualted.
 */
void kmc_simulation() {
  int cycle_step	= 0;
  double print_time	= 0.0;
  int xyzp_step		= 0;
  int time_cycle	= 0;
  char xyzp_file_name[100];
  print_out_cycle_time0 = clock();	// Initialize
  double last_frame_print_cpu_time = 0.0;
  
  printf("# Starting KMC\n");
  electric_field_calculation_step = 0;
  
  /* Initial sanity check */
  if (sum_probabilities <= 0.0){
    printf("%e ERROR Probability sum: %lf\n",kmc_step,sum_probabilities);
//     exit(1);
  }
  
  /* Loop until any end conditions apply. max_kmc_step can be reached without the simulation
   * stopping if many statistical runs are made as during calculations of diffusion coefficients
   */
  while (tag.end_condition == 0) {
    kmc_step_time0 = clock();
    
    kmc_step++;
    cycle_step++;
    
    /* Carry out choosen event */
    choose_event();	// Function will change the vector changed_points
    
    /* Increase time */
    increase_time();
    
    look_for_reactions(); // E.g. deposition defined with a fixed rate.
    
    /* Get kmc step CPU time */
    kmc_step_time1 = clock();
    kmc_step_time = (double) (kmc_step_time1 - kmc_step_time0) / CLOCKS_PER_SEC;
    if (kmc_step <= 5) printf("%e CPU time per kmc step: %1.2le s\n",kmc_step, kmc_step_time);
    
    
    
    /** Analyse **/
    
    /* Print either after certain number of steps or after certain amount of time */
    if ( (print_out_cycle   > 0 && cycle_step 			>= print_out_cycle) 
      || (print_out_cycle_s > 0 && simulation_time - print_time >= print_out_cycle_s) 
      || (kmc_step >= slow_down_begin && slow_down_begin != 0.0)) {
      
      cluster_analysis();		// Might remove atoms and whole clusters
      
      if (print_out_cycle_s > 0 && simulation_time - print_time >= print_out_cycle_s) time_cycle = 1;
      else if (print_out_cycle_s <= 0)                                                time_cycle = 1;
      
      if (time_cycle == 1) print_xyz_frame_nr++;		// Nr of frames printed to objects.xyz. The number is needed in check_system_integrity().
      
      check_system_integrity(time_cycle);	// Print to std and data.out
      
      if(time_cycle == 1 && tag.print_xyz_files == 1) print_xyz();
      
      if(time_cycle == 1) {
        print_time = simulation_time;	// Record at which time the time cycle print out happens.
    
        /* Save the 10 last print out cycle steps as xyzp files */
        xyzp_step++;
        snprintf(xyzp_file_name, sizeof(xyzp_file_name), "objects_%02i.xyzp", xyzp_step);
        print_xyzp(xyzp_file_name);
        if (xyzp_step >= 10) {
          xyzp_step = 0;
        } 
      }
      
      cycle_step = 0;	// For the step cycle
      time_cycle = 0;	// For the time cycle
      
    }
    
    /* Check CPU time */
    if (max_CPU_time > 0) {
      simulation_cpu = clock();
      CPU_time  = (double)(simulation_cpu - simulation_begin) / CLOCKS_PER_SEC;
    }
    
    /* Print after every hour of CPU time */
    if (CPU_time - last_frame_print_cpu_time >= 3600){
      last_frame_print_cpu_time = CPU_time;
//       printf("# PRINTING FRAME\n");
      count_nr_objects();
      print_frame_xyz(2);
    }
    
    /* Check for end conditions */
    check_end_conditions();
    
  }
}


/* Remove all atoms with the label_diffusion */
void remove_diffusion_atoms() {
  Pvector q;
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          if (system_lattice[q.x][q.y][q.z][q.p].label == label_diffusion) {
            remove_object(q);
            printf("Removing diffusing adatom\n");
          }
        }
      }
    }
  }
}


/* Analyse the runs for calculating the 3D diffusion coeffiecient */
void diffusion_coefficient_calculation_analysis(double measured_F){
  diffusion_coefficient_calculation_analysis_time0 = clock();
  Pvector q;
  double D 			= 0.0;
  int N_diffusing_atoms   = 0;
  double average_D        = 0.0; // [m^2/s]
  double sq_displacement  = 0.0; // [m^2]
  double displacement     = 0.0; // [m]
  double velocity         = 0.0; // [m/s]
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          if (system_lattice[q.x][q.y][q.z][q.p].label == label_diffusion) {
            N_diffusing_atoms++;
            displacement = distance(system_lattice[q.x][q.y][q.z][q.p].initial_position, xyzp_to_xyz(system_lattice[q.x][q.y][q.z][q.p].global_xyzp,a0));	// m
            
            /* Check */
            if (N_diffusing_atoms > 1) printf("%e %e WARNING More than one atom with diffusion label\n",kmc_step, kmc_run);
          }
        }
      }
    }
  }
  
  /* Only print results if only one atome has been diffusing */
  if (N_diffusing_atoms == 1) {
    
    sq_displacement = displacement*displacement;
    velocity        = displacement/simulation_time;	// Average velocity of the diffusion adatom (exact if the path is straight) [m/s].
    
    if (displacement >= max_diffusion_displacement) {
      tag.end_condition = 1;
    }
    
    sum_squared_displacement += sq_displacement;
    average_D = sum_squared_displacement/(6.0*simulation_time*kmc_run);	// simulation_time assumed to be identical for all KMC runs.
    
    printf("%e run %e <D3d>: %e m^2/s\n", kmc_step, kmc_run, average_D);

    fprintf(file_diffusion_out,"%e %e %e %e %e %e %e %e %e\n",kmc_run,sq_displacement, sum_squared_displacement/kmc_run, simulation_time, kmc_step, average_D,displacement,velocity,measured_F);

    fflush(file_diffusion_out);
  }
  
  diffusion_coefficient_calculation_analysis_time1 = clock();
  diffusion_coefficient_calculation_analysis_time += (double) (diffusion_coefficient_calculation_analysis_time1 - diffusion_coefficient_calculation_analysis_time0) / CLOCKS_PER_SEC;
}


/* Add an adatom and let it diffuse max_kmc_step jumps. 
 * Calculate its diffusion coeffiecent
 * Repeat nr_statistical_kmc_runs times.
 */
void calculation_of_diffusion_coefficient() {
  Pvector q;
  int N_diffusing_atoms = 0;
  kmc_run = 0;
  double measured_F = 0.0;  // [V/m]
  while (tag.kmc_runs_end_condition == 0){
    kmc_run++;
    
    printf("#### KMC run %lg ####\n",kmc_run);
    kmc_step		= 0;
    simulation_time	= 0;
    tag.end_condition 	= 0; // Redo kmc_runs
    
    /* Initializing the global position of the diffusing adatom */
    N_diffusing_atoms = 0;
    for (q.x = 0; q.x < dimension.x; q.x++) {
      for (q.y = 0; q.y < dimension.y; q.y++) {
        for (q.z = 0; q.z < dimension.z; q.z++) {
          for (q.p = 0; q.p < dimension.p; q.p++) { 
            if (system_lattice[q.x][q.y][q.z][q.p].label == label_diffusion) {
              N_diffusing_atoms++;
              system_lattice[q.x][q.y][q.z][q.p].global_xyzp = q;
                        
              /* Check */
              if (N_diffusing_atoms > 1) printf("%e %e WARNING More than one atom with diffusion label\n",kmc_step, kmc_run);
            }
          }
        }
      }
    }
    
    remove_diffusion_atoms(); // Remove all atoms with the label_diffusion before starting a new run.
    count_all_bonds();        // Needed before calculating the field.
    
#ifdef FIELD
    if (tag.use_field == 1) {
      calculate_field_for_full_system();
      update_all_gradients();
      
      if (kmc_run == 1) {
        measured_F = system_lattice[field_measurement_point.x][field_measurement_point.y][field_measurement_point.z][field_measurement_point.p].scalar_field;
      }
    }
#endif // FIELD
    
    printf("Adding diffusing adatom\n");
    if (q_diffusion_adatom.x == 0 && q_diffusion_adatom.y == 0 && q_diffusion_adatom.z == 0 && q_diffusion_adatom.p == 0) {
      add_adatoms(1);		// Add one adatom, which will have a label label_diffusion.
    }
    else {
      add_adatom(q_diffusion_adatom, label_diffusion);		// Add one adatom at coordinates q. It will have a label label_diffusion.
    }
    print_xyz_frame_nr++;
    check_system_integrity(0);
    if (tag.print_xyz_files == 1) print_xyz();    // First frame of the diffusion
    kmc_simulation();
    diffusion_coefficient_calculation_analysis(measured_F);
    check_system_integrity(0);

    check_kmc_runs_end_conditions();
  }
}

  
void main(int argc, char *argv[]) {
  simulation_begin = clock();
  
  char parcas_initial_xyz_file_name[100];
  char parcas_end_xyz_file_name[100];
  int tmp_kind;
  char xyzp_file_name[100];
  double other_time;

  
  printf("\n");
  printf("#\n");
  printf("# Kimocs - Kinetic Monte Carlo for Surfaces\n");
  printf("#\n"); 
  printf("# Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>\n");
  printf("#\n");
  printf("# This program is free software: you can redistribute it and/or modify\n");
  printf("# it under the terms of the GNU General Public License as published by\n");
  printf("# the Free Software Foundation, either version 3 of the License, or\n");
  printf("# (at your option) any later version.\n");
  printf("#\n");
  printf("# This program is distributed in the hope that it will be useful,\n");
  printf("# but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
  printf("# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
  printf("# GNU General Public License for more details.\n");
  printf("#\n");
  printf("# You should have received a copy of the GNU General Public License\n");
  printf("# along with this program.  If not, see <https://www.gnu.org/licenses/>.\n");
  printf("#\n");
  printf("# Cite as:\n");
  printf("#\n");
  printf("#       V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.\n"); 
  printf("#       Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]\n");
  printf("#\n");
  printf("#\n");
  printf("# Contributors:\n"); 
  printf("#\n");
  printf("#   Ville Jansson, PhD\n");
  printf("#   Ekaterina Baibuz, MSc\n");
  printf("#   Jyri Kimari (né Lahtinen), MSc\n");
  printf("#\n");
  printf("#\n");
  printf("# *** Independent modules (not part of Kimocs, but can be combined) *** \n");
  printf("#\n"); 
  printf("# HELMOD (Calculations of electric field and charges)\n");
  printf("# Created by Flyura Djurabekova, Stefan Parviainen, Kai Nordlund, and Mikko Lyly (University of Helsinki, 2008).\n");
  printf("#\n");
  printf("# All rights reserved.\n");
  printf("#\n");
  printf("# Cite as:\n");
  printf("#\n");
  printf("#      Djurabekova, F., Parviainen, S., Pohjonen, A., & Nordlund, K. (2011). Atomistic\n");
  printf("#      modeling of metal surfaces under electric fields: Direct coupling of electric fields\n");
  printf("#      to a molecular dynamics algorithm. Physical Review E, 83(2), 026704.\n");
  printf("#\n");
  printf("#\n");
  printf("# FEMOCS (Calculations of electric field)\n");
  printf("# Created by Mihkel Veske and Andreas Kyritsakis (University of Helsinki, University of Tartu, 2016)\n");
  printf("# Licence: GPLv3.0\n");
  printf("#\n");
  printf("# Cite as:\n");
  printf("#\n");
  printf("#     Veske, M. et al, 2018. Dynamic coupling of a finite element solver to large-scale atomistic simulations.\n");
  printf("#     Journal of Computational Physics, 367, pp.279–294.\n");
  printf("#\n");
  printf("#\n");
  printf("#                                  * * * \n");
  printf("#\n");
  

  /* Initialize global variables */
  initialize_global_variables();
  
  /* Command line arguement to turn on debug mode with more output */
  
  if(argc > 1) {
    if (argc == 2) {
      if (strcmp("--debug", argv[1]) == 0) {
        tag.debug = 1;
        printf("# Debug mode\n");
      }
    }
  }
  
  
  /* Initial calculations */
//   estimate_memory_usage();
//   exit(0);
  

  
  /* Initiate files */
  if (tag.print_xyz_files == 1) file_xyz = fopen("./objects.xyz","w");
  snprintf(parcas_initial_xyz_file_name, sizeof(parcas_initial_xyz_file_name), "mdlat.in.xyz_initial");
  file_objects_out  = fopen("./objects.out","w");   // Output data
  file_clusters     = fopen("./clusters.out","w");  // Initiate the output file for cluster statistics.
  file_events_out   = fopen("./events.out","w");    // Output event data
  file_transitions_out = fopen("./transitions.out","w");    // Output indicies of made nn transitions
  fprintf(file_transitions_out, "# Transition index, Barrier [eV]\n");
  fprintf(file_clusters,"# Atomic clusters\n");
  file_diffusion_out	= fopen("./diffusion_coefficient.out","w");
  fprintf(file_diffusion_out,"#0:KMC_run 1:squared_displacement[m^2] 2:av_squared_displacement[m^2] 3:run_time[s] 4:nr_diffusion_jumps 5:av_3D_diffusion_coefficient[m^2/s] 6:distance[m] 7:velocity[m/s] 8:measured_F[V/m]\n");   
  
  
  /* Parameter intialization */
  unit_cell_dimension.x = 1.0;
  unit_cell_dimension.y = 1.0;
  unit_cell_dimension.z = 1.0;
  
  q_diffusion_adatom.x	= 0;
  q_diffusion_adatom.y	= 0;
  q_diffusion_adatom.z	= 0;
  q_diffusion_adatom.p	= 0;
  
  initiate_lattice_orientation_parameters(surface_orientation);

  
  
  /* Read in configurations */
  read_configuration_file();
  
  

//   check_local_configuration(); // Temporary check. Will end simulation here.
  if (tag.check_neighbour_matrices == 1) check_neighbour_matrices();       // Will end simulation here.


  count_all_bonds();
  
#ifdef FIELD
  /* Electric field and charge */
  if (tag.use_field == 1) {
    //if (femocs.tag_use_femocs == 1) 
    //  print_femocs_field_tag = 1;                   // Print field for the intial system.
    calculate_field_for_full_system();
    update_all_gradients();
  }
#endif
  
  count_nr_objects();                               // Will count the nr_objects and nr_adatoms
  add_probabilities();                              // Will need nr_adatoms
  
  
  print_xyz_frame_nr++;                             // Count number of frames in object.xyz. The number is needed in check_system_integrity().
  check_system_integrity(1);
  
  old_nr_objects = nr.objects;
  
  print_frame_xyz(0);                               // Print intitial.xyz.

  /* Print initial xyz system */
  if (tag.print_parcas_files == 1) 
    print_parcas_xyz(parcas_initial_xyz_file_name); // Print initial xyz system for Parcas.
  if (tag.print_lammps_files == 1) 
    print_lammps_xyz();
  if (tag.print_xyz_files == 1) 
    print_xyz();                                    // First frame in the xyz movie
  calculate_parcas_input();                         // Calculate input parameters for Parcas.
  
  /* Sum all event probabilities */
  calculate_sum_probabilities();
  
  /* KMC simulation */
  if (tag.calculate_diffusion_coefficient == 1) {
    printf("Diffusion calculation\n");
    calculation_of_diffusion_coefficient();
  }
  else {
    kmc_simulation();	// Normal kmc run
  }
    
  /* Analyze data and check the integrity of the system */
  
  if (tag.calculate_diffusion_coefficient == 0) {
    print_xyz_frame_nr++;
    check_system_integrity(1);
  }
//   check_random_generator();
//   check_parameter_table();
  
  /* End simulation */

#ifdef FIELD
  if (tag.use_field == 1){
    calculate_field_for_full_system();                // Final field in all points.
    update_all_gradients();
  }
#endif // FIELD
  
  print_frame_xyz(1);                                 // Print end.xyz.
  
  if (tag.print_xyz_files == 1) print_xyz();        // Last frame in the xyz movie.
  if (tag.print_parcas_files == 1) {
    // Output format for Parcas
    snprintf(parcas_end_xyz_file_name, sizeof(parcas_end_xyz_file_name), "mdlat.in.xyz_end");
    print_parcas_xyz(parcas_end_xyz_file_name);
  }
  snprintf(xyzp_file_name, sizeof(xyzp_file_name), "objects_end.xyzp");
  print_xyzp(xyzp_file_name);
  
  /* For interaction with COMSOL */
  if (tag.using_comsol == 1) {
    print_comsol_xyz();
  }

#ifdef FIELD
  /* If FEMOCS is used, end it nicely */
  call_femocs_destructor();
#endif
  
  /* CPU time estimates */
  
  simulation_end = clock();
  CPU_time	= (double)(simulation_end - simulation_begin) / CLOCKS_PER_SEC;
  other_time	= CPU_time
		  -check_system_integrity_time
		  -read_configuration_file_time
		  -count_all_bonds_time
		  -calculate_sum_probabilities_time
		  -diffusion_coefficient_calculation_analysis_time
		  -choose_event_time
		  -helmod_time
		  -print_xyz_time
		  -print_xyzp_time;
  
  printf("# End of simulation.\n");
  printf("# \n");
  printf("# check_system_integrity():      %f s, %2.1f pc\n",check_system_integrity_time, check_system_integrity_time/CPU_time*100.0);
  printf("#  cluster_analysis()            %f s, %2.1f pc\n",cluster_analysis_time, cluster_analysis_time/CPU_time*100.0);
  printf("#\n");
  printf("# read_configuration_file():     %f s, %2.1f pc\n",read_configuration_file_time, read_configuration_file_time/CPU_time*100.0);
  printf("#  read_in_parameter_file():     %f s, %2.1f pc\n",read_in_parameter_file_time, read_in_parameter_file_time/CPU_time*100.0);
  printf("#  make_bulk():                  %f s, %2.1f pc\n",make_bulk_time, make_bulk_time/CPU_time*100.0);
  printf("#\n");
  printf("# count_all_bonds():             %f s, %2.1f pc\n",count_all_bonds_time, count_all_bonds_time/CPU_time*100.0);
  printf("# calculate_sum_probabilities(): %f s, %2.1f pc\n",calculate_sum_probabilities_time, calculate_sum_probabilities_time/CPU_time*100.0);
  printf("# D_coeff_calc_analysis():       %f s, %2.1f pc\n",diffusion_coefficient_calculation_analysis_time, diffusion_coefficient_calculation_analysis_time/CPU_time*100.0);
  printf("# choose_event():                %f s, %2.1f pc\n",choose_event_time, choose_event_time/CPU_time*100.0);
  printf("#  check_for_evaporation():      %f s, %2.1f pc\n",check_for_evaporation_time, check_for_evaporation_time/CPU_time*100.0);
  printf("#  make_jump():                  %f s, %2.1f pc\n",make_jump_time, make_jump_time/CPU_time*100.0);
  printf("#   add_probabilities_locally()  %f s, %2.1f pc\n",add_probabilities_locally_time, add_probabilities_locally_time/CPU_time*100.0);
  printf("#   update_bond_count()          %f s, %2.1f pc\n",update_bond_count_time, update_bond_count_time/CPU_time*100.0);
  printf("#   make_barrier_xyz():          %f s, %2.1f pc\n",make_barrier_xyz_time, make_barrier_xyz_time/CPU_time*100.0);
  printf("#\n");
  printf("# helmod()                       %f s, %2.1f pc\n",helmod_time, helmod_time/CPU_time*100.0);
  printf("#  helmod_wrapper()              %f s, %2.1f pc\n",helmod_wrapper_time, helmod_wrapper_time/CPU_time*100.0);
  printf("#\n");
#ifdef FIELD
  printf("# call_femocs()                  %f s, %2.1f pc\n",call_femocs_time, call_femocs_time/CPU_time*100.0);
#endif // FIELD
  printf("#\n");
  printf("# print_xyz():                   %f s, %2.1f pc\n",print_xyz_time, print_xyz_time/CPU_time*100.0);
  printf("# print_xyzp():                  %f s, %2.1f pc\n",print_xyzp_time, print_xyzp_time/CPU_time*100.0);
  printf("#\n");
  printf("# Other                          %f s, %2.1f pc\n",other_time, other_time/CPU_time*100.0);
  printf("# ___________________________________________\n");
  printf("# Total CPU time:                %f s, %2.1f pc\n",CPU_time, CPU_time/CPU_time*100.0);
  printf("# Full HELMOD field was calculated:     %i times\n"		,n_full_field_calc);
  printf("# Average CPU time/step:         %1.2le s\n"		,CPU_time/kmc_step);
  printf("# Average CPU time/step/adatom:  %1.2le s\n"		,CPU_time/kmc_step/nr.adatoms);
  printf("# Average CPU time/step/object:  %1.2le s\n"		,CPU_time/kmc_step/nr.objects);
  printf("#\n");
  printf("# Have a nice day!\n");
  
  
  
}
