#
#   Kimocs - Kinetic Monte Carlo for Surfaces
# 
#   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 
#   Cite as:
# 
#      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
#      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
# 


include build/makefile.defs


all: ubuntu

ubuntu: ubuntu16

stable:
	make -f build/makefile.stable

ubuntu14: submodules femocs_ubuntu
	make -f build/makefile.ubuntu14

ubuntu16: submodules femocs_ubuntu
	make -f build/makefile.ubuntu16

taito: submodules femocs_taito
	 module load $(MODULESTAITO) && make -f build/makefile.taito

alcyone: submodules femocs_alcyone
	 module load $(MODULESALCYONE) && make -f build/makefile.alcyone


install: install_ubuntu

install_ubuntu: submodules install_femocs_ubuntu

install_taito: submodules install_femocs_taito

install_alcyone: submodules install_femocs_alcyone


submodules:


femocs_ubuntu:
	cd femocs && make
	
femocs_taito:
	module load $(MODULESTAITO) && cd femocs && make
	
femocs_alcyone:
	module load $(MODULESALCYONE) && cd femocs && make


install_femocs_ubuntu:
	cd femocs && make install-ubuntu

install_femocs_taito:
	module load $(MODULESTAITO) && cd femocs && make install-taito
	
install_femocs_alcyone:
	module load $(MODULESALCYONE) && cd femocs && make install-alcyone

	
clean:
	rm -f $(EXE1) a.out *.o *.mod
	rm -rf fann/build

clean-all:
	rm -f $(EXE1) a.out *.o *.mod
	rm -rf fann/build fann/install
	cd femocs && make clean

.ALWAYSEXEC:
