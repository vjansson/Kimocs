/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */

#ifdef FIELD

#define NCELLX_field 2*NCELLX
#define NCELLY_field 2*NCELLY
#define NCELLZ_field 2*NCELLZ

#include "Femocs_wrap.h"

typedef struct {
  int       tag_use_femocs;    // 1: Using FEMOCS to calculate the field.
  double  * Ad_position_x;     // x positions of adatoms given to FEMOCS in [ang]
  double  * Ad_position_y;     // y positions of adatoms given to FEMOCS in [ang]
  double  * Ad_position_z;     // z positions of adatoms given to FEMOCS in [ang]
  double  * field_point_x;     // x positions of adatoms or vacancies given to FEMOCS in [ang]
  double  * field_point_y;     // y positions of adatoms or vacancies given to FEMOCS in [ang]
  double  * field_point_z;     // z positions of adatoms or vacancies given to FEMOCS in [ang]
  double    external_field;    // The external field in [V/m]			
  double  * field_x;           // Fields for every surface atom (adatom_kind) in [V/ang]
  double  * field_y;           // Fields for every surface atom (adatom_kind) in [V/ang]
  double  * field_z;           // Fields for every surface atom (adatom_kind) in [V/ang]
  double  * field_norm;

  double  * force_charge;      // charge and force for every surface atom (adatom kind) in [e], eV/A. it comes in helmod xq format (charge1, Fx1, Fy1, Fz1, charge2...)
  int     * types;             // types of atoms. By default 2
  int     * flags;             // femocs interpolation flags
  int       nr_adatoms;
  int       nr_vacancies;
  int       nr_field_points;   // Nr of adatoms + vacancies
  int       error;             // 1: FEMOCS failed.
  int       Ncalls;
  FEMOCS * femocs_obj;
  
} femocs_data;


/** Global field-related variables **/

/* General global field variables */
extern double field_grid[NCELLX_field][NCELLY_field][NCELLZ_field][4];

/* Global Femocs variables */
extern femocs_data femocs;
extern clock_t  call_femocs_time0,call_femocs_time1;
extern double   call_femocs_time;

#endif // FIELD


/* Effective migration barrier for a jump from q to w under a field.
 * Periodic boundaries already accounted for.
 * Jump kind is 1,2,3 or 5 for 1nn, 2nn, 3nn and 5nn jumps, respectively 
 */
Jump effective_barrier(Jump jump);

#ifdef FIELD
/* Initialize FEMOCS variables */
void intialize_femocs_variables();

/* Update variables needed to call FEMOCS. */ 
void update_femocs_variables();

/* Call FEMOCS in order to calculate the field */
void call_femocs();

/* Update the field gradients for all adatoms and vacancies in the system */
void update_all_gradients();

/* Calculate the gradients in the (omega) directions of all possible jumps of adatom q*/
void add_gradient(Pvector q);

/* Add the electric field and charge to every lattice point */
void add_electric_field_and_charge();


/* Add the electric field and charge to every lattice point. */
void add_electric_field_and_charge();

/* Set the counter of the number of steps without any field calculation for every atom to zero. */
void initiate_field_jump_optimization();

/* Add the electric field and charge to every lattice point. Might put end_condition == 1 and stop the simulation. */
void calculate_field_for_full_system();


void free_femocs_data();

/* Clean up memory used by femocs before ending the simulation and prints end information 
 * to output directory*/
void call_femocs_destructor();

#endif //FIELD
