/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */



typedef struct {
    double charge_x,charge_y,charge_z,charge, field_x,field_y,field_z,field;
} Atom;

  
// Global variables for integration with Helmod.

extern Vector libhelmod_unit_cell_dimension;
extern Vector p0_libhelmod,p1_libhelmod,p2_libhelmod,p3_libhelmod,p4_libhelmod,p5_libhelmod;	// #D coordinates in the unit cell of the p atoms for LibHelmod
extern Vector p6_libhelmod,p7_libhelmod,p8_libhelmod,p9_libhelmod,p10_libhelmod,p11_libhelmod;

// Input for helmod:
extern int LH_iabove;			//How high above surface E is calculated
extern double LH_elfield; 			//Applied E
extern int LH_myatoms;
extern int LH_ncells[3];
extern int LH_ncellp[3];
extern double LH_pbc[3];
extern double * LH_x0; 			// Coordinates of atoms
extern int * LH_atype;			// Array of types of atoms listed in x0 [type_of_atom1, type_of_atom2,...]

// helmod output
extern double * LH_xq; 			// Charges of atoms [charge1, x1 side,y1 side ,z1 side,...]

extern int LH_isize[3];  			// ncells*ncellp
extern int LH_evpatype;			// Atom type for evaporated atoms
extern int LH_nfullsolve;			// How often to evaluate E for full system
extern int LH_full_solve;
extern int LH_aroundapx;			// Solve E around apex (probably highest point)
extern int LH_debug;
extern int LH_debug;
extern double * LH_E;


/* Applying periodic boundary conditions in the x and y dimensions for the Ivector h 
 * in the LibHelmod ijk field coordinate system. 
 * 
 */
Ivector ijk_pbc(Ivector h);

/* Calculates the distance in [m] between two field points, h0 and h1, 
 * in LibHelmod's ijk coordinate system */
double ijk_distance(Ivector h0, Ivector h1);


/* Initialize system variables needed for LibHelmod */
void initialize_libhelmod();


/* Calculate cartheisian libhelmod atom coordinates (Parcas coordinates)
 * These coordinates are normalized between -0,5 and 0.5 in all directions.
 */
Vector xyzp_to_libhelmod_coordinates(Pvector q);


/* Function for converting xyzp coordinates q to LibHelmod's truncated xyz coordinates  */
Vector xyzp_to_truncated_xyz(Pvector q);


/* Convert LibHelmod field coordinates h = (i,j,k) to xyz [Å] coordinates */
Vector libhelmod_field_ijk_to_xyz(Ivector h);

/* Convert cartheisian xyz [Å] coordinates to LibHelmod field coordinates h = (i,j,k) */
Ivector xyz_to_libhelmod_field_ijk(Vector v);

/* Check if the position h in the libhelmod lattice is a fcc or bcc position.
 * Returns 1 if yes and 0 otherwise.
 * Needed as the libhelmod lattice can not be mapped one-to-one with the 
 * Kimocs lattice.
 */
int check_for_kimocs_lattice_position_in_libhelmod_lattice(Ivector h); 


/* Convert LibHelmod field coordinates h = (i,j,k) to Kimocs coordinates. */
Pvector libhelmod_field_ijk_to_xyzp(Ivector h);


/* Convert Kimocs coordinates q to LibHelmod field coordinates i,j,k */
Ivector xyzp_to_libhelmod_field_ijk(Pvector q);


/* Read in atoms from Kimocs to LibHelmod */
void kimocs_lattice_to_x0();


/* Wrapper to call the Fortran code LibHelmod, that provides field and charges */
extern void helmod_wrapper_for_c(double* LH_elfield, double* LH_x0, int LH_ncells[], double grid_spacing[], double* LH_xq, int* LH_atype, int* LH_iabove, double** LH_E, int* LH_full_solve, int* LH_aroundapx, int* LH_debug, int* LH_evpatype, int* LH_skiplimcheck, int* LH_myatoms, int* LH_error);


/* Update in-variables, such as atom positions, needed before calling LibHelmod */
void update_libhelmod_variables();


/* Call LibHelmod and give the atom positions. 
 * Get field and charges
 * Translate the LibHelmod data to Kimocs formats
 */ 
void helmod();




