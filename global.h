
/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */

#include <stdio.h>	/* for fprintf and stderr */
#include <stdlib.h>	/* for exit */
#include <math.h>
#include <string.h>
#include "mt64.h"	/* Mersenne Twister random generator */
#include <time.h>	/* For timing functions */


/* Define the dimensions of the system in unit cells (should be dynamically defined in the future).
 * NCELLP is the nr of atoms per unit cell.
 */

#define NCELLX 32
#define NCELLY 32
#define NCELLZ 64
#define NCELLP 12
#define NEL 2		// Max number of chemical elements + the vacancy kind in the system


// Structs

/* Define an double 3D vector type */
typedef struct {
  double x, y, z;
} Vector;

typedef struct {
  int x, y, z;
} Ivector;

/* Define an int 4D vector type [x,y,z,p]*/
typedef struct {
  int x, y, z, p;
} Pvector;

/* Define the electric field parameters for the metal, as defined in [kyritsakis2018atomistic]*/
typedef struct {
  double M_sl;    // = M_s - M_l; difference in the permament system dipole between the systems with the adatom at the saddle point and lattice point
  double A_sl;    // = A_s - A_l; difference in system polarizability between the systems with the adatom at the saddle point and lattice point.
  
  double M_sr;    // = M_s - M_r; difference in the system permanent dipole between the system with the adatom at the saddle point and the reference system (adatom removed)
  double A_sr;    // = A_s - A_r; difference in the system polarizability between the system with the adatom at the saddle point and the reference system (adatom removed)
  
} Dipole;

/* Atomic process (a,b,c,d) */
typedef struct {
  int a;
  int b;
  int c;
  int d;
} Signum_4d;


/* Paramters for a single atom transition (jump or exchange process) */
typedef struct {
  Signum_4d    signum_4d;   // (a,b,c,d)
  double intrinsic_barrier; // [eV]
  double attempt_frequency; // [s^{-1}]
  double index;             // All (a,b,c,d) processes numbered from 0 to N
  
} Parameter;


typedef struct {
  /* Input */
  Signum_4d    signum_4d;
  Pvector      initial;       // Initial position
  Pvector      final;         // Final position
  int          omega;         // Direction index
  int          kind;          // 1, 2, 3, or 5 for 1nn, 2nn, 3nn, or 5nn; (0 for exchange, but not yet implemented)
  
  /* Output to be calculated */
  double       attempt_frequency; // [s^{-1}]
  double       effective_barrier; // [eV]; Barrier used in arrhenius formula
  double       intrinsic_barrier; // [eV]; The barrier in the absence of a field 
  double       field_barrier;     // [eV]; The change of the barrier due to a field. (may be negtive)
  double       probability;       // [s^{-1}; Probability rate for the jump.
  double       index;             // Read from the par file.
  
} Jump;


/* Namespace for counting things. The variables are initialized in initialize_global_variables() */
typedef struct {
  int empty;                  // Total number of empty points in the system lattice.
  int fixed_empty;            // Total number of fixed empty points (atom sinks).
  int vacancies;              // Total number of vacancy objects in the system lattice.
  int atoms;                  // Total number of atom objects in the system lattice.
  int fixed_atoms;            // Total number of edge atom objects in the system lattice.
  int adatoms;                // Total number of adatom objects in the system lattice.
  int atomic_objects;         // Nr atoms + adatoms + nr_fixed_atoms
  int objects;                // Total number of atoms, adatoms and vacancy objects 
  int initial_atomic_objects; // Number of initial atoms, adatoms and fixed atoms
  
  double removed_atoms;          // Total number of removed atoms or adatoms during simulation
  double removed_vacancies;      // Total number of removed vacancies during simulation
  int added_fixed_atoms;
  int added_fixed_empty;
  int clusters;               // Total number of atomic clusters
  
  /* Events */
  double  no_event;           // Nr of steps where no event were carried out.
  double  jumps_1nn;          // Nr of 1nn jump events in the simulation.
  double  jumps_2nn;          // Nr of 2nn jump events in the simulation.
  double  jumps_3nn;          // Nr of 3nn jump events in the simulation.
  double  jumps_4nn;          // Nr of 4nn jump events in the simulation.
  double  jumps_5nn;          // Nr of 5nn jump events in the simulation.
  double  evaporation_events; // Total number of evaporation events. Automatically removed atoms not counted.
  double  deposition_events;  // Total number of deposition events.
  double  deposition_reactions;   // Total number of atoms deposited when depostion is not treated as a MC event, but a reaction with a fixed rate.
  
  double  jumps_unstable;     // Nr of 1nn jumps with a < 4.
  double  calls_field_solver; // Nr of calls to the field solver (Helmod or Femocs)
} Nr;

/* Tags (flags). The variables are initialized in initialize_global_variables() */
typedef struct{
  int end_condition;                    // End conditions are fullfilled: 1 or not fullfilled: 0
  int kmc_runs_end_condition;           // End conditions are fullfilled: 1 or not fullfilled: 0
  int focused_deposition;               // 1: Deposition is focused on a small area.
  int print_empty_kind;
  int print_femocs_field;               // 1: Print the Field for empty objects, as calculated by Femocs.
  int using_comsol;                     // 1: Data will be exchanged with COMSOL
  int print_xyz_files;                  // 1: Print objects.xyz and end.xyz files.
  int print_xyzp_files;                 // 1: Print objects_*.xyzp files for restarts.
  int print_parcas_files;               // 1: Print md.in, mdlat.in.xyz_initial, and mdlat.in.xyz_end for Parcas.
  int print_lammps_files;               // 1: Print LAMMPS atom dump file objects.dump
  int print_par_files;                  // 1: Print used_nn_parameters.par and other files with used parameters.
  int print_transitions;                // 1: Print the index of every nn transition.
  int allow_nnn_jumps;                  // 1: Allow nnn jumps.
  int allow_3nn_jumps;                  // 1: Allow 3nn jumps.
  int allow_5nn_jumps;                  // 1: Allow 5nn jumps.
  int global_attempt_frequency_1nn;         // 1: All 1nn jumps have the same attempt frequency read in from the par file or data.in.
  int global_attempt_frequency_2nn;
  int global_attempt_frequency_3nn;
  int global_attempt_frequency_5nn;
  int use_atomic_temperature;           // 1: Tempererature set for individual objects.
  int cluster_evaporation;              // 1: Clusters detached from the bulk will be removed.
  int calculate_diffusion_coefficient;  // 1: Calculate the diffusion coefficient with multiple KMC runs.
  int using_26D_parameters;             // 1: Using a 26D parameterization (instead of the 4D parameterization)
  int using_72D_parameters;             // 1: Using a 26D parameterization (instead of the 4D parameterization)
  int using_ann;                        // 1: Using ANNs for barriers
  int using_classifier;                 // 1: Using ANN classifier for barriers
  int debug;                            // 1: Debuggin mode
  int print_xyz_temperature;            // Print object temperatures in xyz files.
  int deposition_mode;                  // 2: Field-dependent, 3: old
  int deposition_nn_threshold;          // int; Minimum number of neighbour for a vacancy position to be valid for a deposition event.
  int check_neighbour_matrices;         // 1: Check that the neighbours of any atom are correctly identified.
  
  /* Field-related tags */
  int use_field;                        // 1: Use electric field
  int print_libhelmod_field;            // 1: Print the LibHelmod field
  int recalculate_field;                // 1: Recalculate the field.
  int use_libhelmod;                    // 1: Use LibHelmod to calculate the field.
  int use_charges;                      // 1: Calculate electric charges with Femocs (or Helmod)
  int libhelmod_field_model;            // Model for how the field values are transferred from LibHelmod to Kimocs
} Tag;

enum Kind {
  empty_kind              = 0,    // nr_1nn == 0
  atom_kind               = 1,    // nr_1nn == 12
  adatom_kind             = 2,    // nr_1nn  < 12
  vacancy_kind            = 3,    // nr_1nn  > 1
  fixed_atom_kind         = 4,    // Accounts for a bottom non-pbc boundary
  fixed_empty_kind        = 5,    // Accounts for a top non-pbc boundary
};

enum Element {
  vacancy_element         = 0,
  lattice_element         = 1,    // Chemical element of the lattice
};

enum Label {
  label_none,                     // No label.
  label_diffusion,                // Label for atom that is used for calculation of its diffusion coefficient.
  label_bulk_cluster,             // The atom belongs to a cluster defined as bulk, which will not be removed by the cluster analysis of detached clusters.
  label_free_floating_atom,
  label_removed_adatom,
};

enum Crystal {
  fcc                       = 1,
  bcc                       = 2,
};


/* Definition of an object */
typedef struct {
  int     nr_1nn;                       // Nr of 1nn atoms or adatoms to the object
  int     nr_2nn;                       // Nr of 2nn atoms or adatoms to the object
  enum Kind     kind;                   // 0: empty, 1: atom, 2: adatom, 3:vacancy, 4: edge atom:
  enum Element  element;                // 0: vacancy, 1: lattice element, >=2, foreign elements.
  enum Label    label;                  // 0 for no label; integer for labealing a particular atom. 1: diffusion calculation.
  int     cluster_index;                // Default: 0. Index for the atomic clusters.
  double  index;                        // Object index for identifying how individual objects move
  double  deposition_index;             // Numbering of deposited atoms; 0 for initial atoms. 
  
  /* Event probabilities for the object */
  Jump    event_1nn[12];                // Probabilities for the nn jumps of the object in the omega directions (0..11 in fcc)
  Jump    event_2nn[6];                 // Probabilities for nnn jumps for the object
  Jump    event_3nn[12];                // Probabilities for the 3nn jumps of the object in the omega directions
  Jump    event_5nn[8];                 // Probabilities for the 5nn jumps of the object in the omega directions
  double  probability_deposition;       // Probability for a deposition event.   TODO Rename to event_deposition
  double  probability_evaporation;      // Probability for an evaporation event. TODO Rename to event_evaporation
  
  int     configuration_26d[12];        // The 26D-configurations in all 12 directions (fcc); stored as doubles. For checks.
  float   barrier[12];                  // [eV] For checks.
  float   field_gradient_barrier[12];   // [eV] The effect of the field gradient on the barrier (can be negative)
  float   effective_barrier[12];        // barrier + field_gradient_barrier
  Jump    last_transition;              // Save the last transition that the object did.
  
  Pvector global_xyzp;                  // xyzp position without PBC
  Vector  initial_position;             // [m] The initial position of an object in Carthesian coordinates for calculation of the diffusion coefficient
  Vector  electric_field;               // [V/m]
  double  scalar_field;                 // [V/m]
  double  average_field;	
  double  sum_field;
  double  field_gradient[12];           // [V/m^2] Field gradient components in the omega directions of possible jumps.
  double  field_gradient_2nn[12];       // [V/m^2] Field gradient components in the omega directions of possible jumps for 2nn jumps.
  double  field_gradient_3nn[12];       // [V/m^2] Field gradient components in the omega directions of possible jumps for 3nn jumps.
  double  field_gradient_5nn[12];       // [V/m^2] Field gradient components in the omega directions of possible jumps for 5nn jumps.
  double  max_gradient;                 // [V/m^2] Maximum field gradient
  double  nn_gradient;                  // Showing the gradient for a diffusing adatom to jump to this vacancy.
  double  electric_charge;              // [e] Electric charge
  double  nr_jumps;                     // Nr of jumps a particular atom/adatom has done during the simulation.
  double  Temperature;                  // [K] Tempereature associated with individual objects (for e.g. T gradients)
  int     nr_jumps_with_constant_field; // Nr of jumps done without calling the field solver. 
  Ivector remote_field_position;        // Libhelmod ijk position where the adatom and vacancy fields are measured.
  
  /* Features related to the lattice and will not move with the object */
  Vector  helmod_xyz;                   // xyz position in LibHelmod (PARCAS) format: the coordinates are normalized between -0,5 and 0.5 in all directions.
  Vector  position;                     // [m] Carthesian coordinates.
} Object;


// Global variables
extern Object system_lattice[NCELLX][NCELLY][NCELLZ][NCELLP];
extern double simulation_time;                              // [s]  Simulation time.
extern double max_simulation_time;                          // [s]  Maximum simulation time
extern double Temperature;                                  // [K]  Simulation temperature
extern double cloud_temperature;                            // [K]  Average temperature of particles being deposited.
extern double kmc_step;                                     //      KMC step
extern double kmc_run;                                      //      The KMC run number
extern double max_kmc_step;                                 //      Max number of KMC steps in the simulation.
extern double max_kmc_run;                                  //      Max nr of KMC runs for improved statistics; eg for calculations of the diffusion coefficient.

extern double sum_probabilities_before_event;               //      Sum of all event probabilities before any event is carried out

extern double sum_probabilities;                            //      Sum of all event probabilities
extern double sum_internal_probabilities;                   //      Sum of the internal probabilities
extern double sum_nn_jump_probabilities;                    //      Sum of the nn jump internal probabilities
extern double sum_nnn_jump_probabilities;                   //      Sum of the nnn jump internal probabilities
extern double sum_3nn_jump_probabilities;                   //      Sum of the 3nn jump internal probabilities
extern double sum_5nn_jump_probabilities;                   //      Sum of the 5nn jump internal probabilities
extern double sum_external_probabilities;                   //      Sum of the external probabilities 

extern double sum_D_coefficient;                            //      Sum of the 3D diffusion coefficients from every kmc_run.
extern double sum_squared_displacement;                     //      Sum of the squared displacements for calculations of the diffusion coefficient.
extern double height;                                       // [m]  Highest z coordinate of the system
extern double height_old;                                   // [m]  Old height
extern double min_height;                                   // [m]  Minimum height allowed before ending the simulation.
extern double max_height;                                   // [m]  Maximum height allowed before ending the simulation.
extern double evaporation_rate;                             // [adatoms/s/m^2] Rate of adatoms removed from the system.
extern double deposition_rate;                              // [adatoms/s/m^2] Rate of adatoms added to the system.
extern double deposition_per_second;                        // [adatoms/s] Average rate of adatoms deposited to the system  
extern double atomic_deposition_rate;                       // [1/s] Probability rate for one adatom to become depositioned (at the position of a vacancy)
extern double atomic_evaporation_rate;                      // [1/s] Probability rate for one adatom to evaporate

extern Parameter j1nn_parameter_table[13][7][13][7];        // 	Probabilities for atom 1nn jumps in the 4D parameterization.
extern Parameter j2nn_parameter_table[13][7][13][7];        //  Probabilities for atom 2nn jumps. Optimization: Could be defined dynamically to save memory
extern Parameter j3nn_parameter_table[13][7][13][7];        //  Probabilities for atom 3nn jumps. Optimization: Could be defined dynamically to save memory
extern Parameter j5nn_parameter_table[13][7][13][7];        //  Probabilities for atom 5nn jumps. Optimization: Could be defined dynamically to save memory
extern float atom_26D_parameter_table[2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2][2];	// Probabilities for atom nn jumps in the 26D parameterization.
extern double a0;                                           // [m]
extern double jump_distance_nn;                             // [m] Distance between nearest neighbour atom positions.
extern double jump_distance_2nn;                            // [m] 2nn jump distance.
extern double jump_distance_3nn;                            // [m] 3nn jump distance.
extern double jump_distance_4nn;                            // [m] 4nn jump distance.
extern double jump_distance_5nn;                            // [m] 5nn jump distance.
extern Pvector dimension;                                   // [unit cells]; Simulation cell dimensions.
extern Ivector pbc;                                         // Periodic boundary condition for x dimension (0: no, 1: yes)
extern int max_1nn;                                         // Max number of nn atoms. 12 for fcc and 8 for bcc.
extern int max_2nn;                                         // Max number of 2nn atoms; 6 for fcc and bcc.
extern int max_3nn;                                         // Max number of 3nn atoms; 12 for bcc.
extern int max_4nn;                                         // Max number of 4nn atoms; 12 for bcc.
extern int max_5nn;                                         // Max number of 5nn atoms; 8 for bcc.
extern int max_s;                                           // Max number of s-parameters in the 26D parameterization.

extern double max_index;                                    // Max object index.

extern Nr  nr;                                                     // Namespace for counting variables
extern Tag tag;                                                    // Namespace for tags

extern int max_nr_elements;                                 // 1 for only one metal.
extern int max_nr_clusters;                                 // Max number of clusters (end condition).
extern int max_nr_evaporated_atoms;                         // Max number of evaporated atoms (end condition)
extern double max_nr_atomic_objects;                        // Maximum number of atoms (adatoms + atoms) allowed in the system. Possibel end condition for deposition processes.
extern int min_cluster_size_limit;                          // Minimum number of atoms to be considered a cluster.
extern int min_cluster_size;                                // Size of the smallest cluster in the system.
extern double max_diffusion_displacement;                   // Maximum displacement of diffusing atom in diffusion calculations.
extern double max_CPU_time;                                 // Maximum CPU time (ignore if negative)
extern int bulk_cluster_index;                              // The cluster index of the substrate.
extern double sum_ca_bonds;                                 // Cumulative sum of c-a value for all 1nn (a,b,c,d) jumps
extern double sum_Em;                                       // Cumulative sum of all migration barriers [eV] for all 1nn jumps

extern int old_nr_objects;
extern float slow_down_begin;

extern float print_out_cycle;                               // Print out data and do checks after every step cycle.
extern double print_out_cycle_s;                            // Print out data and do checks after every time cycle.
extern int print_missing_parameters;                        // List in a file the missing parameters and make xyz files for the barrier calculations. Turned off [0] by default.
extern int colour_probabilities;                            // Colour code objects in xyz files according to their max event probability. Will slow down simulation.
extern int xyz_label_colour;                                // 1: Colour code labelled objects. 0: No colouring.

extern int print_xyz_info;                                  // Print label, 1nn, 2nn in objects.xyz
extern int print_xyz_frame_nr;                              // Count the number of frames printed to object.xyz.
extern int surface_orientation;                             // 100 or 111 or 110.
extern int lattice_option;                                  // 0: standard; 1: second alternative for fcc110 (110b for helmod)
extern enum Crystal crystal;                                // 1: fcc, 2: bcc
extern int surface_z;                                       // Higest layer of unit cells.

/* Parameters */
extern double attempt_frequency_1nn;                        // [s^-1] The attempt frequency for 1nn jumps
extern double attempt_frequency_2nn;                        // [s^-1] The attempt frequency for 2nn jumps
extern double attempt_frequency_3nn;                        // [s^-1] The attempt frequency for 3nn jumps
extern double attempt_frequency_5nn;                        // [s^-1] The attempt frequency for 5nn jumps
extern int vector_1nn[12][12][4];                           // Matrix for calculating distances between 1nn atoms with p = 4, p = 6, and p = 12.
extern int vector_2nn[12][6][4];                            // Matrix for calculating distances between 2nn atoms with p = 4, p = 6, and p = 12.
extern int vector_3nn[12][12][4];                           // Matrix for calculating distances between 3nn atoms
extern int vector_5nn[12][8][4];                            // Matrix for calculating distances between 5nn atoms
extern int matrix_26D_1nn[12][12][26][4];                   // Matrix for 1nn jumps with the 26D parameterization.
extern int matrix_72D_1nn[12][12][72][4];                   // Matrix for 1nn jumps with the 72D parameterization.
int   matrix_mirror1[72][2];                                // Matrix for the mirror1 operation of the (currently, up to) 72D LAE description
int   matrix_mirror2[72][2];                                // Matrix for the mirror2 operation of the (currently, up to) 72D LAE description
int   matrix_mirror3[72][2];                                // Matrix for the mirror3 operation of the (currently, up to) 72D LAE description
extern unsigned long long seed[4];

extern char atom_type1[100];                                // Atom type for xyz files: Cu, W, Fe, etc
extern char atom_type2[100];                                // Atom type for xyz files: Cu, W, Fe, etc

extern Vector  unit_cell_dimension;                         // Length of the unit cell in x, y and z direction in [a0]. 
extern Vector  p0,p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11;       // #D coordinates in the unit cell of the p atoms.
extern Pvector q_diffusion_adatom;                          // Initial coordinates for adatom for diffusion calculations.
extern int     inverse_omega[12][12];                       // The omega index giving the opposite jump direction
extern int     inverse_omega2[12][12];                      // The omega index giving the opposite jump direction for 2nn jumps
extern int     inverse_omega3[12][12];                      // The omega index giving the opposite jump direction for 3nn jumps
extern int     inverse_omega5[12][12];                      // The omega index giving the opposite jump direction for 5nn jumps


/* Field-related variables */
extern double external_field;                               // [V/m]        Applied external field.
extern double field_sign;                                   // +1 or -1;    Gives the direction of the external field along z: +1 for the anod and -1 for the kathod.
extern double free_polarizability;                          // [em]         Polarizability of free single atoms in vacuum.
extern Dipole W2232;                       
extern int    global_electric_field_recalculation_frequency;// How often the electric field is recalculated for the whole system
extern int    electric_field_calculation_step;              // In range [0..electric_field_calculation_step), 0 mean electric field was  calculated in the previous kmc_step
extern double cumulative_charge;                            // Stores cumulative charge for the whole system
extern double cumulative_charge_prev;
extern int    aroundapx_input;                              // Read in from data.in. The size of the box where the field is calculated, the center of the box is in the highest atom position (?)
extern double charge_threshold;
extern int    n_full_field_calc;
extern double max_local_field;                              // [V/m] Max measured local field (Positive for anode and negative for cathode fields)
extern double local_field_limit;                            // [V/m] The simulation will end if the local field limit is reached.
extern int    max_jumps_at_constant_field;                  // Maximum jumps of an idividual atom before the fields needs to be recalculated.
extern Pvector field_measurement_point;                     // Coordinate for measure the field.
extern double nr_femocs_errors;                             // Nr of times that FEMOCS returns femocs.error != 0.
extern double gradient_correction;                          // Correction factor for the estimation of the gradient in the direciton of the jump.
extern double surface_field_measurement_distance;           // [m] Perpendicular distance from the surface where the surface field is measured for adatoms and vacancies.

int *desc;
int *mir1;
int *mir2;
int *invr;
int *reve;
int *rem1;
int *rem2;
int *rein;
int *mind_barr;
int *mind_class;


extern clock_t simulation_begin, simulation_end;
extern clock_t simulation_cpu;
extern clock_t check_system_integrity_time0, check_system_integrity_time1;
extern clock_t calculate_sum_probabilities_time0, calculate_sum_probabilities_time1;
extern clock_t choose_event_time0, choose_event_time1;
extern clock_t print_xyz_time0, print_xyz_time1;
extern clock_t print_xyzp_time0, print_xyzp_time1;
extern clock_t make_jump_time0, make_jump_time1;
extern clock_t make_barrier_xyz0, make_barrier_xyz1;
extern clock_t deposition_time0,deposition_time1;
extern clock_t evaporation_time0,evaporation_time1;
extern clock_t helmod_time0,helmod_time1;
extern clock_t helmod_wrapper_time0,helmod_wrapper_time1;
extern clock_t count_all_bonds_time0, count_all_bonds_time1;
extern clock_t read_configuration_file_time0, read_configuration_file_time1;
extern clock_t read_in_parameter_file_time0, read_in_parameter_file_time1;
extern clock_t make_bulk_time0, make_bulk_time1;
extern clock_t check_for_evaporation_time0, check_for_evaporation_time1;
extern clock_t add_probabilities_locally_time0, add_probabilities_locally_time1;
extern clock_t update_bond_count_time0, update_bond_count_time1;
extern clock_t cluster_analysis_time0, cluster_analysis_time1;
extern clock_t kmc_step_time0, kmc_step_time1;
extern clock_t print_out_cycle_time0, print_out_cycle_time1;
extern clock_t diffusion_coefficient_calculation_analysis_time0, diffusion_coefficient_calculation_analysis_time1;

extern double CPU_time;
extern double check_system_integrity_time; 
extern double calculate_sum_probabilities_time;
extern double choose_event_time;
extern double print_xyz_time;
extern double print_xyzp_time;
extern double make_jump_time;
extern double make_barrier_xyz_time;
extern double deposition_time;
extern double evaporation_time;
extern double helmod_time;
extern double helmod_wrapper_time;
extern double count_all_bonds_time;
extern double read_configuration_file_time;
extern double read_in_parameter_file_time;
extern double make_bulk_time;
extern double check_for_evaporation_time;
extern double add_probabilities_locally_time;
extern double update_bond_count_time;
extern double cluster_analysis_time;
extern double kmc_step_time;
extern double print_out_cycle_time;
extern double diffusion_coefficient_calculation_analysis_time;

      
extern FILE *file_init_xyz;             // Intitial xyz input file
extern FILE *file_parameters;           // nn parameter file
extern FILE *file_data_in;              // Configuration file
extern FILE *file_objects_out;          // Output data
extern FILE *file_events_out;           // Events data
extern FILE *file_transitions_out;      // List of nn transitions made during the simulation; listes as indicies.
extern FILE *file_end_xyz;              // Final xyz file
extern FILE *file_xyz;                  // XYZ output
extern FILE *file_parcas_xyz;           // XYZ output for PARCAS
extern FILE *file_xyzp;                 // XYZP file for restarts
extern FILE *file_in_xyzp;              // XYZP input file 
extern FILE *file_2nn_p0_xyz;
extern FILE *file_2nn_p1_xyz;
extern FILE *file_2nn_p2_xyz;
extern FILE *file_2nn_p3_xyz;
extern FILE *file_2nn_p4_xyz;
extern FILE *file_2nn_p5_xyz;
extern FILE *file_2nn_p6_xyz;
extern FILE *file_2nn_p7_xyz;
extern FILE *file_2nn_p8_xyz;
extern FILE *file_2nn_p9_xyz;
extern FILE *file_2nn_p10_xyz;
extern FILE *file_2nn_p11_xyz;
extern FILE *file_local;
extern FILE *file_parcas_mdin;
extern FILE *file_E_comsol_out;
extern FILE *file_clusters;
extern FILE *file_diffusion_out;					// File for printing the diffusion coeffiecients after every KMC run.


/* Initialize the global variables defined as structs */
void initialize_global_variables();


void initiate_lattice_orientation_parameters(int surface_orientation);


/* Eulers's angle transformations.
 * Omega is a vector with the three rotation angles psi, phi and theta.
 * x is the intial xyz position and r is the returned xyz position.
 */
Vector rot(Vector x, Vector Omega);


/* Function for converting 3D coordinates to internal Kimocs coordinates */
Pvector xyz_to_xyzp(Vector v, double lattice_parameter);


/* Function for converting Kimocs coordinates to 3D */
Vector xyzp_to_xyz(Pvector q, double lattice_parameter);


// Count the number of atomic objects in the system
void count_number_atoms();

/* Applying periodic boundary conditions in x, y, z directions and
 * change unit cell coorinate q accordingly for the dimension D:
 * D = 0 for x dimension
 * D = 1 for y dimension
 * D = 2 for z dimension
 * 
 * If perodic boundary apply, the new unit cell coordinte is returned.
 * The lattice have unit cells numbered between 0 and q_max in any dimension.
 * 
 * If boundaries are not periodic in some direction, this will be handled by
 * another function. In this function pcb = 1 for all directions.
 */
int boundary_condition(int q, int D);


/* Arrhenius' formula for temperature activated event probability
 * frequencies: 
 * nu: attempt frequency [s^-1] 
 * E: activation energy [eV]
 * kb: Boltzmann's contant [eV K^-1]
 */
double arrhenius_formula(double nu, double E, double T);


void coordinate_check(Pvector q);

// Calculation of the distance in [m] between two Carthesian coordinates.
// Does not take periodic boundary conditions into account.
double distance(Vector v0, Vector v1);

/* Calculation of the carthesian distance in [m] between two xyzp coordinates.
 * Does not take periodic boundary conditions into account.
 */
double qdistance(Pvector q0, Pvector q1);

/* Initialize a new adatom */
void new_adatom(Pvector q);


/* Initialize a new vacancy (or empty)*/
void new_vacancy(Pvector q);

/* Find the 1nn coordinates q of position v, numbered 0 to omega. Boundary conditions are taken into account. */
Pvector omega_position_of_1nn(Pvector v, int omega);

/* Find the 1nn coordinates q of position v, numbered 0 to omega. Boundary conditions are not taken into account. */
Pvector omega_position_of_1nn_with_no_PBC(Pvector v, int omega);

/* Find the 2nn coordinates q of position v, numbered 0 to omega. Boundary conditions are taken into account. */
Pvector omega_position_of_2nn(Pvector v, int omega);

/* Find the 2nn coordinates q of position v, numbered 0 to omega. Boundary conditions are not taken into account. */
Pvector omega_position_of_2nn_with_no_PBC(Pvector v, int omega);

/* Find the 3nn coordinates q of position v, numbered 0 to omega. Boundary conditions are taken into account. */
Pvector omega_position_of_3nn(Pvector v, int omega);

/* Find the 3nn coordinates q of position v, numbered 0 to omega. Boundary conditions are not taken into account. */
Pvector omega_position_of_3nn_with_no_PBC(Pvector v, int omega);

/* Find the 5nn coordinates q of position v, numbered 0 to omega. Boundary conditions are taken into account. */
Pvector omega_position_of_5nn(Pvector v, int omega);

/* Find the 5nn coordinates q of position v, numbered 0 to omega. Boundary conditions are not taken into account. */
Pvector omega_position_of_5nn_with_no_PBC(Pvector v, int omega);

/* 26d: Find the coordinate q of the s neighbour of the 1nn omega neighbour of v. */
Pvector omega_26d_s_configuration(Pvector v, int omega, int s);

/* 26d: Find the coordinate q of the s neighbour of the 1nn omega neighbour of v. No periodic boundary condition */
Pvector omega_26d_s_configuration_with_no_PBC(Pvector v, int omega, int s);

/* 72d: Find the coordinate q of the s neighbour of the 1nn omega neighbour of v. */
Pvector omega_72d_s_configuration(Pvector v, int omega, int s);

/* 72d: Find the coordinate q of the s neighbour of the 1nn omega neighbour of v. No periodic boundary condition */
Pvector omega_72d_s_configuration_with_no_PBC(Pvector v, int omega, int s);

/* Definition of the global variable sum_probabilities */
double definition_sum_probabilities();

/* Definition of the global variable sum_internal_probabilities */
double definition_sum_internal_probabilities();

/* Check the neighbour matrices for 4d */
int check_4d_neighbours(int nn);

/* Check the beighbour matrices for 26d or 72d*/
int check_1nn_bonds();

void check_local_configuration();
  
void check_random_generator();

void check_neighbour_matrices();
