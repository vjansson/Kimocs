
/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */


#include <stdio.h>	/* for fprintf and stderr */
#include <stdlib.h>	/* for exit */
#include <math.h>
#include <string.h>
#include "mt64.h"	/* Mersenne Twister random generator */
#include <time.h>	/* For timing functions */

#include "global.h"
#include "probabilities.h"
#include "ann.h"

#include "efield.h"

#include "kimocs.h"


/***** Probability functions *****/


/* Distribution of neutral atoms with
 * dipole moments atom_dipole_moment in a electeric field F [V/m]
 */
double dipole_probability(double F) {
//   double polarizability	= 5.5e-21;	// [eV m^2/V^2]; for a free neutral atom. 
  double kb			= 8.6173324e-5; // [eV K^-1]
  double distribution		= 0.0;
  
  distribution = exp(free_polarizability*F*F/((double) 2.0*kb*cloud_temperature));
  
  return distribution;
  
}


/* Weight the probability rates for deposition events
 * according to the strenght of the electric field.
 */
void deposition_field_correlation() { 
  Pvector q;
  double sum_surface_field      = 0;
  double field_factor           = 0.0;
  double field                  = 0.0;
  double sum_dipole_probability = 0;
  double dip_tmp                = 0;
  double probability_tmp        = 0.0;
  double nr_dep_sites           = 0.0;
  
  
  /* Count the number of vacanies and sum the electric field of the vacanceis. */
  nr_dep_sites = 0;
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          
          /* Sum the surface electric field (i.e. the field in all vacancies) */
          if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind && tag.deposition_nn_threshold <= system_lattice[q.x][q.y][q.z][q.p].nr_1nn) {
            
            nr_dep_sites++;
            field = system_lattice[q.x][q.y][q.z][q.p].scalar_field;
            sum_surface_field += field;
            
            /* Diple moment distribution summation */
            if (tag.deposition_mode == 2 || tag.deposition_mode == 3) {
              dip_tmp = dipole_probability(field);
              sum_dipole_probability += dip_tmp;
//               if (dip_tmp > 1e10)
//                 printf("                                                                                                            field = %e, dip_tmp = %e\n", field, dip_tmp);
            }
            
          } 
        }
      } 
    }
  }
  
//   printf("                                                                                                                          sum_dipole_probability = %e, sum_surface_field = %e, nr.vacancies = %i\n", sum_dipole_probability,sum_surface_field, nr.vacancies);
  
  sum_external_probabilities = 0.0;
  
  /* Weigh the deposition probability with the surface electic field strenght */
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          
          /* Sum the surface electric field (i.e. the field in all vacancies) */
          if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind && tag.deposition_nn_threshold <= system_lattice[q.x][q.y][q.z][q.p].nr_1nn) {
            
            if (tag.use_field == 1) {
              field = system_lattice[q.x][q.y][q.z][q.p].scalar_field;
              
              if (tag.deposition_mode == 1) {
                field_factor = field*nr.vacancies/sum_surface_field;	// Should be always positive
                
              }
              else if (tag.deposition_mode == 2 || tag.deposition_mode == 3) {
                field_factor = dipole_probability(field)*nr_dep_sites/sum_dipole_probability;
              }
            
            }
            else {
              field_factor = 1.0;
            }

            if (atomic_deposition_rate*field_factor >= 0.0) {
              probability_tmp = atomic_deposition_rate*field_factor;
              if (probability_tmp > 1e-100) {
                system_lattice[q.x][q.y][q.z][q.p].probability_deposition = probability_tmp;
              }
              else {
                system_lattice[q.x][q.y][q.z][q.p].probability_deposition = 0;
              }
            }
            else if (atomic_deposition_rate*field_factor < 0.0){
              system_lattice[q.x][q.y][q.z][q.p].probability_deposition = 0;
              printf("%e ERROR Deposition probability * field factor is negative: %e * %e = %e\n", kmc_step, atomic_deposition_rate, field_factor, atomic_deposition_rate*field_factor);
              exit(1);
            }
          }
          else {
            /* Not vacancy */
            system_lattice[q.x][q.y][q.z][q.p].probability_deposition = 0;
          }
          
          /* Update the sum of probabilities for external events: depositions and evaporations */
          sum_external_probabilities += system_lattice[q.x][q.y][q.z][q.p].probability_deposition + system_lattice[q.x][q.y][q.z][q.p].probability_evaporation;
          
        }
      } 
    }
  }
  
  sum_probabilities = definition_sum_probabilities();
}





/* Read the nn parameters from the 4d parameterization table and calculate the probabilities 
 * for an atom jumping from q to w (in the omega direction).
 * jump_kind = 1,2,4 or 5 (nn, 2nn, 3nn, 5nn)
 * Will take the field into account if it is present.
 */
// double atom_4D_jump_probability(Pvector q, Pvector w, int omega, int jump_kind) {
//   double nu, E, jump_probability;
//   double T;
//   Jump jump;
//   
//   
//   /* Temperature regime */
//   if (tag.use_atomic_temperature == 1) {
//     T = system_lattice[q.x][q.y][q.z][q.p].Temperature; // Global temperature not used.
//   }
//   else {
//     T = Temperature;  // Global temperature is used.
//   }
//   
//   /* Get the barrier and the effect of the field, if present */
//     jump.initial = q;
//     jump.final   = w;
//     jump.omega   = omega;
//     jump.kind    = jump_kind;
//     jump = effective_barrier(q,w,omega,jump_kind);
// 
//   /* Find the attempt frequency nu */
//   switch (jump_kind) {
//   case 1:
//     t.attempt_frequency = attempt_frequency_1nn;
//     break;
//   case 2:
//     t.attempt_frequency = attempt_frequency_2nn;
//     break;  
//   case 3:
//     t.attempt_frequency = attempt_frequency_3nn;
//     break;
//   case 5:
//     t.attempt_frequency = attempt_frequency_5nn;
//     break;
//   default:
//     printf("ERROR Only nn, 2nn, 3nn and 5nn jumps are implemented for this surface orientation.\n");
//     exit(1);
//   }
// 
//   
//   jump_probability = arrhenius_formula(jump.attempt_frequency,jump.effective_barrier,T);                              // Calculate the probability rate.
//   
//   
//   if (jump_probability < 0.0 || jump.effective_barrier >= 1000.0) {
//     // Barriers larger than 1000 eV are considered marked as forbidden.
//     jump_probability = 0.0; // To avoid overflow.
//   }
//   
// //   printf("%e nu %lf, Em %lf, Gamma %lf, 1nn %i, 2nn %i, 1nn %i, 2nn %i\n",kmc_step,nu,Em,jump_probability,initial_1nn,initial_2nn,final_1nn,final_2nn);
//   
//   return jump_probability;
//   
// }

// /* Read the nn parameters from the 4d parameterization table and calculate the probabilities 
//  * for an atom jumping from q to w (in the omega direction).
//  */
// double atom_nn_jump_probability(Pvector q, Pvector w, int omega) {
//   double nu, Em, jump_probability;
//   double T;
//   int initial_1nn, initial_2nn, final_1nn, final_2nn;
//   // The parameters are stored in a table:
//   // atom_parameter_table[A1nn][A2nn][B1nn][B2nn] = atom_M;
//   
//   initial_1nn	= system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
//   initial_2nn	= system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
//   final_1nn	= system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
//   final_2nn	= system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
//   
//   /* Temperature regime */
//   if (tag.use_atomic_temperature == 1) {
//     T = system_lattice[q.x][q.y][q.z][q.p].Temperature; // Global temperature not used.
//   }
//   else {
//     T = Temperature;	// Global temperature is used.
//   }
//   
//   /* Check */
//   if (initial_1nn < 0 || initial_2nn < 0 || final_1nn < 0 || final_2nn < 0) {
//     printf("%e ERROR in atom_nn_jump_probability: %i %i %i %i\n",kmc_step, initial_1nn,initial_2nn,final_1nn,final_2nn);
//     exit(1);
//   }
//   
//   /* Field effects */
//   if (tag.use_field == 1) {
//     Em = effective_barrier(q,w,omega,1);
//   }
//   else {
//     /* No field */
//     nu = attempt_frequency_1nn;
//     Em = atom_parameter_table[initial_1nn][initial_2nn][final_1nn][final_2nn];  // Get the energy barrier from the 4d table.
//     system_lattice[q.x][q.y][q.z][q.p].barrier[omega] = Em;                     // Save value for later analysis.
//   }
//   
//   jump_probability = arrhenius_formula(nu,Em,T);                              // Calculate the probability rate.
// //   printf("%e nu %lf, Em %lf, Gamma %lf, 1nn %i, 2nn %i, 1nn %i, 2nn %i\n",kmc_step,nu,Em,jump_probability,initial_1nn,initial_2nn,final_1nn,final_2nn);
//   
//   return jump_probability;
// }
// 
// 
// /* Read the nnn parameters from the table and calculate the probabilities */
// double atom_nnn_jump_probability(Pvector q, Pvector w) {
//   double nu, Em, jump_probability;
//   double T;
//   int initial_1nn, initial_2nn, final_1nn, final_2nn;
//   
//   // The parameters are stored in a table:
//   // nnn_parameter_table[A1nn][A2nn][B1nn][B2nn] = atom_M;
// 
//   initial_1nn = system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
//   initial_2nn = system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
//   final_1nn   = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
//   final_2nn   = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
// 
//   /* Check */
//   if (initial_1nn < 0 || initial_2nn < 0 || final_1nn < 0 || final_2nn < 0) {
//     printf("%e ERROR inatom_nnn_jump_probability: %i %i %i %i\n",kmc_step, initial_1nn,initial_2nn,final_1nn,final_2nn);
//     exit(1);
//   }
//   
//   /* Temperature regime */
//   if (tag.use_atomic_temperature == 1) {
//     T = system_lattice[q.x][q.y][q.z][q.p].Temperature; // Global temperature not used.
//   }
//   else {
//     T = Temperature;	// Global temperature is used.
//   }
//   
//   if (attempt_frequency_2nn > 0.0) {
//     nu = attempt_frequency_2nn;
//   }
//   else {
//     nu = attempt_frequency_1nn;
//   }
//   
//   Em = nnn_parameter_table[initial_1nn][initial_2nn][final_1nn][final_2nn];
//   jump_probability = arrhenius_formula(nu,Em,T);
// //   printf("%e nu %lf, Em %lf, Gamma %lf, 1nn %i, 2nn %i, 1nn %i, 2nn %i\n",kmc_step,nu,Em,jump_probability,initial_1nn,initial_2nn,final_1nn,final_2nn);
//   
//   return jump_probability;
// }
// 
// 
// /* Read the 3nn parameters from the table and calculate the probabilities */
// double atom_3nn_jump_probability(Pvector q, Pvector w) {
//   double nu, Em, jump_probability;
//   double T;
//   int initial_1nn, initial_2nn, final_1nn, final_2nn;
//   
//   // The parameters are stored in a table:
//   // nn3_parameter_table[A1nn][A2nn][B1nn][B2nn] = atom_M;
// 
//   initial_1nn = system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
//   initial_2nn = system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
//   final_1nn   = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
//   final_2nn   = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
// 
//   /* Check */
//   if (initial_1nn < 0 || initial_2nn < 0 || final_1nn < 0 || final_2nn < 0) {
//     printf("%e ERROR inatom_3nn_jump_probability: %i %i %i %i\n",kmc_step, initial_1nn,initial_2nn,final_1nn,final_2nn);
//     exit(1);
//   }
//   
//   /* Temperature regime */
//   if (tag.use_atomic_temperature == 1) {
//     T = system_lattice[q.x][q.y][q.z][q.p].Temperature; // Global temperature not used.
//   }
//   else {
//     T = Temperature;  // Global temperature is used.
//   }
//   
//   if (attempt_frequency_3nn > 0.0) {
//     nu = attempt_frequency_3nn;
//   }
//   else {
//     nu = attempt_frequency_1nn;
//   }
//   
//   Em = nn3_parameter_table[initial_1nn][initial_2nn][final_1nn][final_2nn];
//   jump_probability = arrhenius_formula(nu,Em,T);
// //   printf("%e nu %lf, Em %lf, Gamma %lf, 1nn %i, 2nn %i, 1nn %i, 2nn %i\n",kmc_step,nu,Em,jump_probability,initial_1nn,initial_2nn,final_1nn,final_2nn);
//   
//   return jump_probability;
// }
// 
// 
// /* Read the 5nn parameters from the table and calculate the probabilities */
// double atom_5nn_jump_probability(Pvector q, Pvector w) {
//   double nu, Em, jump_probability;
//   double T;
//   int initial_1nn, initial_2nn, final_1nn, final_2nn;
//   
//   // The parameters are stored in a table:
//   // nn5_parameter_table[A1nn][A2nn][B1nn][B2nn] = atom_M;
// 
//   initial_1nn = system_lattice[q.x][q.y][q.z][q.p].nr_1nn;
//   initial_2nn = system_lattice[q.x][q.y][q.z][q.p].nr_2nn;
//   final_1nn   = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
//   final_2nn   = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
// 
//   /* Check */
//   if (initial_1nn < 0 || initial_2nn < 0 || final_1nn < 0 || final_2nn < 0) {
//     printf("%e ERROR inatom_5nn_jump_probability: %i %i %i %i\n",kmc_step, initial_1nn,initial_2nn,final_1nn,final_2nn);
//     exit(1);
//   }
//   
//   /* Temperature regime */
//   if (tag.use_atomic_temperature == 1) {
//     T = system_lattice[q.x][q.y][q.z][q.p].Temperature; // Global temperature not used.
//   }
//   else {
//     T = Temperature;  // Global temperature is used.
//   }
//   
//   if (attempt_frequency_5nn > 0.0) {
//     nu = attempt_frequency_5nn;
//   }
//   else {
//     nu = attempt_frequency_5nn;
//   }
//   
//   Em = nn5_parameter_table[initial_1nn][initial_2nn][final_1nn][final_2nn];
//   jump_probability = arrhenius_formula(nu,Em,T);
// //   printf("%e nu %lf, Em %lf, Gamma %lf, 1nn %i, 2nn %i, 1nn %i, 2nn %i\n",kmc_step,nu,Em,jump_probability,initial_1nn,initial_2nn,final_1nn,final_2nn);
//   
//   return jump_probability;
// }


/* Read the s parameters from the 26D parameterization table and calculate the probabilities 
 * for an atom jump from position q in direction omega to an vacancy (should be checked before 
 * calling the function).
 */
// double atom_26D_jump_probability(Pvector q, int omega) {
//   double jump_probability;
//   double T;
//   int s[26];	// The 26D parameters
//   int i, sn_kind;
//   Pvector sn;	// 1nn and 2nn atoms of the process
//   double s_configuration = 0;
//   int element;
//   Pvector w;
//   Transition t;
//  
//   /* Temperature regime */
//   if (tag.use_atomic_temperature == 1) {
//     T = system_lattice[q.x][q.y][q.z][q.p].Temperature; // Global temperature not used.
//   }
//   else {
//     T = Temperature;	// Global temperature is used.
//   }
//   
// //   t.attempt_frequency = attempt_frequency_1nn;
//   w = omega_position_of_1nn(q, omega);
//   
//   // Get transition barrier and attempt_frequency
//   t = effective_barrier(q,w,omega,1); // Taking into account the effect of the field (if present)
//   
//   jump_probability = arrhenius_formula(t.attempt_frequency,t.effective_barrier,T);
//   
//   return jump_probability; 
//   
// }

/* Find the probability for an atom transition (1nn, 2nn, 3nn, or 5nn jump), defined by the Jump struct jump 
 * Could include exchange processes in the future.
 * Replaces jump_probability_nn()
 */
Jump transition_probability(Jump jump) {
  double T;
  
  /* Check that the process is an atomic object moving to a vacancy */
  if (system_lattice[jump.initial.x][jump.initial.y][jump.initial.z][jump.initial.p].element > 0 &&
      system_lattice[jump.final.x][jump.final.y][jump.final.z][jump.final.p].element == 0) {
    
        /* Temperature regime */
        if (tag.use_atomic_temperature == 1) {
          T = system_lattice[jump.initial.x][jump.initial.y][jump.initial.z][jump.initial.p].Temperature; // Global temperature not used.
        }
        else {
          T = Temperature;  // Global temperature is used.
        }
        
        /* Get the effective barrier (same as the intrinsic barrier if a field is not present) */
        jump = effective_barrier(jump);
        
        
        /* Get the attempt frequency */
        
        switch (jump.kind) {
        case 1:
          if (tag.global_attempt_frequency_1nn == 1) {  
            jump.attempt_frequency = attempt_frequency_1nn; // Otherwise, the attempt frequencies for 1nn were already read from the par file, separately for every process.
          }
          break;
        case 2:
          if (tag.global_attempt_frequency_2nn == 1) {  
            jump.attempt_frequency = attempt_frequency_2nn;
          }
          break;  
        case 3:
          if (tag.global_attempt_frequency_3nn == 1) {  
            jump.attempt_frequency = attempt_frequency_3nn;
          }
          break;
        case 5:
          if (tag.global_attempt_frequency_5nn == 1) {  
            jump.attempt_frequency = attempt_frequency_5nn;
          }
          break;
        default:
          printf("ERROR Only nn, 2nn, 3nn and 5nn jumps are implemented for this surface orientation.\n");
          exit(1);
        }
        
        
        /* Calculate the probability rate */
        jump.probability = arrhenius_formula(jump.attempt_frequency,jump.effective_barrier,T);
        
        /* Sanity check */
        if (jump.probability < 0.0 || jump.effective_barrier >= 1000.0) {
          // Barriers larger than 1000 eV are considered marked as forbidden.
          jump.probability = 0.0;
        }
        
    
  }
  else { 
    jump.probability = (double) 0.0;   // Only adatoms are allowed to make jumps.
  }
  
  return jump;
}




/* Calculate the probability of an adatom jumping from q to omega ( 0 <= 1nn <= max_1nn) 
 * Will take the field into account if present.
 */ 
// double jump_probability_nn(Pvector q, int omega) {
// //   printf("%e Looking for jump possibilities\n",kmc_step);
//   Pvector w;
//   int w_nr_1nn, w_nr_2nn;
//   double probability;
//   float Em;
//   
//   if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
//     w = omega_position_of_1nn(q,omega);
//     if (system_lattice[w.x][w.y][w.z][w.p].kind == empty_kind || 
//         system_lattice[w.x][w.y][w.z][w.p].kind == vacancy_kind || 
//         system_lattice[w.x][w.y][w.z][w.p].kind == fixed_empty_kind) {
//       
//       if (tag.using_26D_parameters == 1) {
//         /* Using the 26D parameterization */
//         
//         probability = atom_26D_jump_probability(q,omega);
//       }
//       else {
//         /* The normal 4D parameterization is used */
// 
// //         w_nr_1nn = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;	
// //         w_nr_2nn = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
//         probability = atom_4D_jump_probability(q,w,omega,1);
// 
// //         if (print_missing_parameters == 1) {
// //           /* Check for missing parameters, i.e. the probability == 0.0 for an adatom to jump to an empty point. */
// //           if (probability == 0.0) {
// //             Em = atom_parameter_table[ system_lattice[q.x][q.y][q.z][q.p].nr_1nn ][ system_lattice[q.x][q.y][q.z][q.p].nr_2nn ][w_nr_1nn][w_nr_2nn].barrier;
// //             if (Em < 49.0 || Em > 71.0) {
// //               /* Make xyz file for Parcas for initial and final state of jump (if not done before). */
// //               make_barrier_xyz(q,w,system_lattice[q.x][q.y][q.z][q.p].nr_1nn, system_lattice[q.x][q.y][q.z][q.p].nr_2nn,w_nr_1nn,w_nr_2nn);
// //               /* Mark that barrier xyz file is made */
// //               atom_parameter_table[ system_lattice[q.x][q.y][q.z][q.p].nr_1nn ][ system_lattice[q.x][q.y][q.z][q.p].nr_2nn ][w_nr_1nn][w_nr_2nn].barrier = 50.0;
// //             }
// //           }
// //         }
//       }
//     
//     }
//     else {
//       probability = (double) 0.0;	// No probability for atomic exchanges.
//     }
//   }
//   else { 
//     probability = (double) 0.0;		// Only adatoms are allowed to make jumps.
//   }
//   return probability;
// }


/* Calculate the probability of an adatom making a nnn jump from q to w (omega next-nearest neighbour of q) */ 
// double jump_probability_nnn(Pvector q, int omega) {
// //   printf("%e Looking for jump possibilities\n",kmc_step);
//   Pvector w;
//   int w_nr_1nn, w_nr_2nn;
//   double probability;
//   float Em;
//   
//   if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
//     w = omega_position_of_2nn(q, omega);
//     if (system_lattice[w.x][w.y][w.z][w.p].kind == empty_kind || system_lattice[w.x][w.y][w.z][w.p].kind == vacancy_kind || system_lattice[w.x][w.y][w.z][w.p].kind == fixed_empty_kind) {
//       
// //       w_nr_1nn = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;	
// //       w_nr_2nn = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
//       probability = atom_4D_jump_probability(q,w,omega,2);;
//       
//     }
//     else {
//       probability = (double) 0.0;	// No probability for atomic exchanges.
//     }
//   }
//   else { 
//     probability = (double) 0.0;		// Only adatoms are allowed to make jumps.
//   }
//   return probability;
// }


/* Calculate the probability of an adatom making a 3nn jump from q to w (omega 3nn of q) */ 
// double jump_probability_3nn(Pvector q, int omega) {
// //   printf("%e Looking for jump possibilities\n",kmc_step);
//   Pvector w;
//   double probability;
//   float Em;
//   
//   if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
//     w = omega_position_of_3nn(q, omega);
//     if (system_lattice[w.x][w.y][w.z][w.p].kind == empty_kind || system_lattice[w.x][w.y][w.z][w.p].kind == vacancy_kind || system_lattice[w.x][w.y][w.z][w.p].kind == fixed_empty_kind) {
//       probability = atom_4D_jump_probability(q,w,omega,3);;
//     }
//     else {
//       probability = (double) 0.0; // No probability for atomic exchanges.
//     }
//   }
//   else { 
//     probability = (double) 0.0;   // Only adatoms are allowed to make jumps.
//   }
//   return probability;
// }


/* Calculate the probability of an adatom making a 5nn jump from q to w (omega 5nn of q) */ 
// double jump_probability_5nn(Pvector q, int omega) {
// //   printf("%e Looking for jump possibilities\n",kmc_step);
//   Pvector w;
//   double probability;
//   float Em;
//   
//   if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
//     w = omega_position_of_5nn(q, omega);
//     if (system_lattice[w.x][w.y][w.z][w.p].kind == empty_kind || system_lattice[w.x][w.y][w.z][w.p].kind == vacancy_kind || system_lattice[w.x][w.y][w.z][w.p].kind == fixed_empty_kind) {
//       probability = atom_4D_jump_probability(q,w,omega,5);
//     }
//     else {
//       probability = (double) 0.0; // No probability for atomic exchanges.
//     }
//   }
//   else { 
//     probability = (double) 0.0;   // Only adatoms are allowed to make jumps.
//   }
//   return probability;
// }






/* Adds internal and external probabilities based on nr_1nn and nr_2nn to all unit cells sourrounding q, including q */
void add_probabilities_locally(Pvector q) {
  add_probabilities_locally_time0 = clock();
  
  Pvector f;
  int omega;
  Pvector w;
  int x,y,z,p;
  Jump jump;
  double tmp_nn_jump_probability            = 0.0;
  double tmp_nnn_jump_probability           = 0.0;
  double tmp_3nn_jump_probability           = 0.0;
  double tmp_5nn_jump_probability           = 0.0;
  double old_internal_probability_part_sum  = 0.0;
  double old_nn_jump_probability_part_sum   = 0.0;
  double old_nnn_jump_probability_part_sum  = 0.0;
  double old_3nn_jump_probability_part_sum  = 0.0;
  double old_5nn_jump_probability_part_sum  = 0.0;
  double old_external_probability_part_sum  = 0.0;
  double new_internal_probability_part_sum  = 0.0;
  double new_nn_jump_probability_part_sum   = 0.0;
  double new_nnn_jump_probability_part_sum  = 0.0;
  double new_3nn_jump_probability_part_sum  = 0.0;
  double new_5nn_jump_probability_part_sum  = 0.0;
  double new_external_probability_part_sum  = 0.0;

  // Add probabilities to all objects w, which are 1nn objects of q
  for (x = -2; x <= 2; x++) {
    for (y = -2; y <= 2; y++) {
      for (z = -2; z <= 2; z++) {
        for (p = 0; p < dimension.p; p++) {
          w.x = boundary_condition(q.x + x, 0);
          w.y = boundary_condition(q.y + y, 1);
          w.z = boundary_condition(q.z + z, 2);
          w.p = p;
          
      // 	  printf("# Adding probabilities locally (%i %i %i %i)\n", w.x, w.y, w.z, w.p);
          
          /** Internal probabilities **/
          
          /* nn jumps from w in the omega direction*/
          for (omega = 0; omega < max_1nn; omega++) {
            jump.omega   = omega;
            jump.initial = w;
            f            = omega_position_of_1nn(w, omega);
            jump.final   = f;
            jump.kind    = 1; // 1nn jump
            jump.signum_4d.a = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
            jump.signum_4d.b = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
            jump.signum_4d.c = system_lattice[f.x][f.y][f.z][f.p].nr_1nn;
            jump.signum_4d.d = system_lattice[f.x][f.y][f.z][f.p].nr_2nn;
            jump = transition_probability(jump); // Calculate the new probability
            tmp_nn_jump_probability             = jump.probability;
            old_nn_jump_probability_part_sum    += system_lattice[w.x][w.y][w.z][w.p].event_1nn[omega].probability;
            new_nn_jump_probability_part_sum    += tmp_nn_jump_probability;
            system_lattice[w.x][w.y][w.z][w.p].event_1nn[omega] = jump;
          }
          
          /* nnn jumps */
          if (tag.allow_nnn_jumps == 1) {
            for (omega = 0; omega < max_2nn; omega++) {
              jump.omega   = omega;
              jump.initial = w;
              f            = omega_position_of_2nn(w, omega);
              jump.final   = f;
              jump.kind    = 2; // 2nn jump
              jump.signum_4d.a = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
              jump.signum_4d.b = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
              jump.signum_4d.c = system_lattice[f.x][f.y][f.z][f.p].nr_1nn;
              jump.signum_4d.d = system_lattice[f.x][f.y][f.z][f.p].nr_2nn;
              jump = transition_probability(jump); // Calculate the new probability
              tmp_nnn_jump_probability            = jump.probability;
              old_nnn_jump_probability_part_sum   += system_lattice[w.x][w.y][w.z][w.p].event_2nn[omega].probability;
              new_nnn_jump_probability_part_sum   += tmp_nnn_jump_probability;
              system_lattice[w.x][w.y][w.z][w.p].event_2nn[omega] = jump;
              
            }
          }
          
          /* 3nn jumps */
          if (tag.allow_3nn_jumps == 1) {
            for (omega = 0; omega < max_3nn; omega++) {
              jump.omega   = omega;
              jump.initial = w;
              f            = omega_position_of_3nn(w, omega);
              jump.final   = f;
              jump.kind    = 3; // 3nn jump
              jump.signum_4d.a = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
              jump.signum_4d.b = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
              jump.signum_4d.c = system_lattice[f.x][f.y][f.z][f.p].nr_1nn;
              jump.signum_4d.d = system_lattice[f.x][f.y][f.z][f.p].nr_2nn;
              jump = transition_probability(jump); // Calculate the new probability
              tmp_3nn_jump_probability            = jump.probability;
              old_3nn_jump_probability_part_sum   += system_lattice[w.x][w.y][w.z][w.p].event_3nn[omega].probability;
              new_3nn_jump_probability_part_sum   += tmp_3nn_jump_probability;
              system_lattice[w.x][w.y][w.z][w.p].event_3nn[omega] = jump;
              
            }
          }
          
          /* 5nn jumps */
          if (tag.allow_5nn_jumps == 1) {
            for (omega = 0; omega < max_5nn; omega++) {
              jump.omega   = omega;
              jump.initial = w;
              f            = omega_position_of_5nn(w, omega);
              jump.final   = f;
              jump.kind    = 5; // 5nn jump
              jump.signum_4d.a = system_lattice[w.x][w.y][w.z][w.p].nr_1nn;
              jump.signum_4d.b = system_lattice[w.x][w.y][w.z][w.p].nr_2nn;
              jump.signum_4d.c = system_lattice[f.x][f.y][f.z][f.p].nr_1nn;
              jump.signum_4d.d = system_lattice[f.x][f.y][f.z][f.p].nr_2nn;
              jump = transition_probability(jump); // Calculate the new probability
              tmp_5nn_jump_probability            = jump.probability;
              old_5nn_jump_probability_part_sum   += system_lattice[w.x][w.y][w.z][w.p].event_5nn[omega].probability;
              new_5nn_jump_probability_part_sum   += tmp_5nn_jump_probability;
              system_lattice[w.x][w.y][w.z][w.p].event_5nn[omega] = jump;
              
            }
          }
          
          
          /** External probabilities **/
          
          old_external_probability_part_sum += system_lattice[w.x][w.y][w.z][w.p].probability_deposition + system_lattice[w.x][w.y][w.z][w.p].probability_evaporation;
          
          
          
          
          /* Deposition */
          
          if(tag.use_field != 1) {
            /* Deposition event probabilities are trieted differently if a field is applied ; see deposition_field_correlation() */
            if (system_lattice[w.x][w.y][w.z][w.p].kind == vacancy_kind && tag.deposition_nn_threshold <= system_lattice[w.x][w.y][w.z][w.p].nr_1nn) {
              
              if (tag.focused_deposition == 1) {
                if (w.x >= 8 && w.x <= 12 && w.y >= 8 && w.y <= 12) {
                  system_lattice[w.x][w.y][w.z][w.p].probability_deposition = atomic_deposition_rate;
                }
                else {
                  system_lattice[w.x][w.y][w.z][w.p].probability_deposition = 0;
                  
                }
              }
              else {
                // Normal depostion: every vacancy has a rate to be turned into an (deposited) adatom
                system_lattice[w.x][w.y][w.z][w.p].probability_deposition = atomic_deposition_rate;
              }
            }
            else {
              // Deposition events are only possible at free surface postitions, i.e. vacancies.
              system_lattice[w.x][w.y][w.z][w.p].probability_deposition = 0;
            }

          }
          
          
          /* Evaporation */
          
          if (system_lattice[w.x][w.y][w.z][w.p].kind == adatom_kind && system_lattice[w.x][w.y][w.z][w.p].nr_1nn > 1){
            // Adatoms with only one NN vacancy will not evaporate (to prevent void creation in the bulk).
            system_lattice[w.x][w.y][w.z][w.p].probability_evaporation = atomic_evaporation_rate;
          }
          else {
            system_lattice[w.x][w.y][w.z][w.p].probability_evaporation = 0;
          }
          
          new_external_probability_part_sum += system_lattice[w.x][w.y][w.z][w.p].probability_deposition + system_lattice[w.x][w.y][w.z][w.p].probability_evaporation;
          
        }
      }
    } 
  }
  
  /* Update the total probability sum of the system by applying the difference
   * Sums need to be updated for 1nn of q0 and q1 to avoid counting probabilities twice.
   */
  sum_nn_jump_probabilities   += (new_nn_jump_probability_part_sum  - old_nn_jump_probability_part_sum);
  if (tag.allow_nnn_jumps == 1) sum_nnn_jump_probabilities  += (new_nnn_jump_probability_part_sum - old_nnn_jump_probability_part_sum);
  if (tag.allow_3nn_jumps == 1) sum_3nn_jump_probabilities  += (new_3nn_jump_probability_part_sum - old_3nn_jump_probability_part_sum);
  if (tag.allow_5nn_jumps == 1) sum_5nn_jump_probabilities  += (new_5nn_jump_probability_part_sum - old_5nn_jump_probability_part_sum);
  sum_external_probabilities  += (new_external_probability_part_sum - old_external_probability_part_sum);
  sum_internal_probabilities  = definition_sum_internal_probabilities();
  sum_probabilities           = definition_sum_probabilities();
  
  if (sum_external_probabilities < 0) {
    printf("ERROR sum_external_probabilities = %e < 0\n", sum_external_probabilities);
    printf("%e %e\n", new_external_probability_part_sum,old_external_probability_part_sum);
    exit(1); 
  }
  
  if (sum_internal_probabilities <= 0.0) {
    printf("    WARNING add_probabilities_locally(): sum_internal_probabilities = %e\n", sum_internal_probabilities);
    calculate_sum_probabilities();
  }
  
 
  
//   printf("# Probability sum: %e = %e + %e\n", sum_probabilities,sum_internal_probabilities,sum_external_probabilities); 
  old_nn_jump_probability_part_sum  = 0.0;
  old_nnn_jump_probability_part_sum = 0.0;
  old_3nn_jump_probability_part_sum = 0.0;
  old_5nn_jump_probability_part_sum = 0.0;
  old_internal_probability_part_sum = 0.0;
  old_external_probability_part_sum = 0.0;
  new_nn_jump_probability_part_sum  = 0.0;
  new_nnn_jump_probability_part_sum = 0.0;
  new_3nn_jump_probability_part_sum = 0.0;
  new_5nn_jump_probability_part_sum = 0.0;
  new_internal_probability_part_sum = 0.0;
  new_external_probability_part_sum = 0.0;
  
  
  add_probabilities_locally_time1 = clock();
  add_probabilities_locally_time += (double) (add_probabilities_locally_time1 - add_probabilities_locally_time0) / CLOCKS_PER_SEC;
}


// Add probabilities for external events to all objects.
void add_external_probabilies() {
  double area = 0.0; 	// [m^2]
  Pvector q;

  
  // Assuming all adatoms or vacanies are on the surface:  
  
  /* Evaporation events */
  if (evaporation_rate > 0.0) {
    atomic_evaporation_rate = evaporation_rate*a0*a0/2.0;
    printf("# Atomic evaporation rate: %e 1/s\n",atomic_evaporation_rate);
    for (q.x = 0; q.x < dimension.x; q.x++) {
      for (q.y = 0; q.y < dimension.y; q.y++) {
        for (q.z = 0; q.z < dimension.z; q.z++) {
          for (q.p = 0; q.p < dimension.p; q.p++) {
            if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind && system_lattice[q.x][q.y][q.z][q.p].nr_1nn > 1) {
              // Adatoms with only one NN vacancy will not evaporate (to prevent void creation in the bulk).
              system_lattice[q.x][q.y][q.z][q.p].probability_evaporation = atomic_evaporation_rate;
            }
          }
        }
      }
    }
  }
  
  /* Deposition events */
  if (deposition_rate > 0.0) {
    if (tag.deposition_mode == 3) {
      atomic_deposition_rate = deposition_rate*a0*a0/2.0; // Correct for a perfect fcc(100) surface. Approximate for others.
    }
    else if (tag.deposition_mode == 1 || tag.deposition_mode == 2) {
      area = dimension.x*dimension.y*unit_cell_dimension.x*unit_cell_dimension.y*a0*a0; // [m^2]
      atomic_evaporation_rate = evaporation_rate*area/nr.adatoms;
    }
    else {
      printf("WARNING deposition mode %i not defined\n", tag.deposition_mode);
    }
    
    if (tag.use_field != 1) {
//     printf("# Atomic deposition rate: %e 1/s\n",atomic_deposition_rate);
      for (q.x = 0; q.x < dimension.x; q.x++) {
        for (q.y = 0; q.y < dimension.y; q.y++) {
          for (q.z = 0; q.z < dimension.z; q.z++) {
            for (q.p = 0; q.p < dimension.p; q.p++) {
              if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind && tag.deposition_nn_threshold <= system_lattice[q.x][q.y][q.z][q.p].nr_1nn) {
                
                if (tag.focused_deposition == 1) {
                  if (q.x >= 8 && q.x <= 12 && q.y >= 8 && q.y <= 12) {
                    system_lattice[q.x][q.y][q.z][q.p].probability_deposition = atomic_deposition_rate;
                  }
                }
                else {
                  system_lattice[q.x][q.y][q.z][q.p].probability_deposition = atomic_deposition_rate;
                }
          
              }
              else {
                system_lattice[q.x][q.y][q.z][q.p].probability_deposition = 0.0;
              }
            }
          }
        }
      }
    }
    else if (tag.use_field == 1) {
      deposition_field_correlation(); // Gives the field deposition rates with a correlation to the field
    }
  }
}



