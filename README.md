
# Kimocs - Kinetic Monte Carlo for Surfaces
 
Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
Cite as:
 
    V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
    Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 
 
## In brief

Kimocs is an open source atomistic Kinetic Monte Carlo code that is freely available at https://gitlab.com/vjansson/Kimocs. 
The code is desgined for simulations of long-term evolution of atomic-scale surface structures.

For more details of usage, see the manual in doc/ or at https://gitlab.com/vjansson/Kimocs/blob/master/doc/kimocs_manual.pdf.
Below follows a brief overview of how to install the code.


## Contributors:

 - Ville Jansson, PhD
 - Ekaterina Baibuz, PhD
 - Jyri Kimari (né Lahtinen), PhD
 - Stefan Parviainen, PhD
 - Andreas Kyritsakis, PhD
     

## Download and compile Kimocs and its submodules

### Stable version ###

1. Download Kimocs (will automatically make a directory) and submodules (only fann is needed) from the Git repository

    git clone --recursive https://gitlab.com/vjansson/Kimocs.git -b stable

    When asked for a username for gitlab, press 'Crtl-c' (the libhelmod submodule is not needed for the stable version)

2. Compile with the commands

    cd Kimocs
    
   On a local Ubuntu machine:
   
    make stable
    
   On kale.grid.helsinki.fi:
   
    module load CMake/3.5.2-foss-2016b
    make stable



### All other version ###

1. Make an account on gitlab.com.

2. Ask for access to LibHelmod from Dr Jansson, <ville.b.c.jansson@gmail.com>

3. To download Kimocs, run the command

   git clone --recursive https://gitlab.com/vjansson/Kimocs.git -b master

4. To compile Kimocs for the first time (including downloading dependencies and compiling them):

   On a local Ubuntu machine:
   
    make install
    make
   
   On Ubuntu (or Cubbli) 14 you will probably need:

    make install
    make ubuntu14
   
   On taito.csc.fi:
   
    make install_taito
    make taito

   On alcyone.grid.helsinki.fi:

    make install_alcyone
    make alcyone

   During this process, it will ask you for your usuername and password for the LibHelmod 
   repository at gitlab.com.

   You will need the "install" commands only when you are
   compiling the Femocs libraries for the first time!

5. To compile Kimocs when you have already installed the Femocs libaries (and have not modified them after that):

   On a local Ubuntu machine:
   
    make
   
   On Ubuntu (or Cubbli) 14 you will probably need:

    make ubuntu14
   
   On taito.csc.fi:

    make taito

   On alcyone.grid.helsinki.fi:

    make alcyone

   These commands will be sufficient even if Femocs has been modified,
   as long as the libraries have not been changed.


## Other make options

To clean, use the command:

    make clean

To also clean submodule builds, use

    make clean-all
 

## Developer guide

Anybody is welcome to contribute to the development of Kimocs, but in order to keep the code 
at high quality and avoid forking, developers are asked to follow the following guidelines:

1. The master branch is the main branch and should only contain well-written tested code. Too keep it this way,
new code submitted or merged into this branch should be reviewed by Ville Jansson <ville.b.c.jansson@gmail.com>.
If you want to merge a branch with master, make sure that there are no conflicts by making the merge on you local mashine
and then upload it to Gitlab as a new temporary branch and make a merge request to the master branch. 

2. The stable branch is intended for new users of Kimocs. The intention is that this branch should be easy to install
and run and test out by any user. The stable branch should thus be even more tested than the master branch. The stable
branch should otherwise be just a snapshot of the master branch when the master branch is considered to be stable enough
to show to new users.

3. Developers are allowed to make as many temporary branches on Gitlab as they want (as long as there is space). When
they want to merge their work with the master branch, they may just make a merge request in Gitlab.

4. Please try to keep a coding style consistent with the exisiting code in Kimocs. Make sure that variables are not 
confused with global variables. 

5. Use SI units in all calculations, inputs and outputs and don't use prefixes. If values are printed in the scientific
notation (e.g.  4.0e-9 m), the order of magnitude is easy to see anyway. Electronvolt (eV) is however used for energies. 
xyz files use Ångström per convention. 
