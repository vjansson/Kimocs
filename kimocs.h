
/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */


/***** General functions *****/

/* Extra calcuations */
void estimate_memory_usage();




/* Read in parameters for nn atom jumps */
void read_in_parameter_file(char * file_name);


/* Read in parameters for nnn atom jumps */
void read_in_nnn_parameter_file(char * file_name);

/* Read in parameters for 3nn atom jumps */
void read_in_3nn_parameter_file(char * file_name);

/* Read in parameters for 5nn atom jumps */
void read_in_5nn_parameter_file(char * file_name);

/* Print nn atom jump parameters */
void print_1nn_parameters();

/* Print 2nn atom jump parameters */
void print_2nn_parameters();

/* Print 3nn atom jump parameters */
void print_3nn_parameters();

/* Print 5nn atom jump parameters */
void print_5nn_parameters();

/* Initiate nn atom jump parameter table */
void initiate_nn_parameter_table();
  
/* Read in 26D parameters for nn atom jumps */
void read_in_26D_parameter_file(char * file_name);

/* Initiate nnn atom jump parameter table */
void initiate_nnn_parameter_table();

/* Initiate 3nn atom jump parameter table */
void initiate_3nn_parameter_table();

/* Initiate 5nn atom jump parameter table */
void initiate_5nn_parameter_table();


/* Define random number genrator functions. The idea is that the generators should be easy to change in this function,
 * so that not the wholse code need to be altered. These functions are thus interfaces between the generator and the simulation
 * code. As generator, the Mersenne Twister will be used.
 */

/* initialization of the random number generator with seed
 */
void random_generator_initialization();

/* Generates random integer [0, 2^63-1]
 */
long long random_int();

/* Generates random double on [0,1]-real-interval */
double random_double1();

/* Generates random double on [0,1)-real-interval */
double random_double2();

/* Generates random double on (0,1)-real-interva */
double random_double3();

/* Generate integers on interval [0,N] */
int random_n(int N);


/*
 * Reads in atoms from a xyz file with the format
 * At x y z
 * 
 * Where At is the atom kind that will be read in.
 * 
 * in_xyz_file_name       = file name
 * xyz_lattice_parameter  = lattice parameter in m
 * xyz_atom_type          = atom name (e.g. 'At', 'Cu')
 * 
 * disp is the displacement Pvector, [0,0,0,0] by default.
 */
void read_initial_xyz_file(char in_xyz_file_name[100], double xyz_lattice_parameter, char xyz_atom_type[100], Pvector disp);


/* Print start or end xyz file. 
 * Only print the positions of atoms and adatoms.
 * Coordinates in [a0].
 * stage = 0 Initial system
 * stage = 1 Final system 
 */
void print_frame_xyz(int stage);


/* Print xyz file for movie. 
 * Only print the positions of atoms and adatoms.
 * Coordinates in [a0].
 */
void print_xyz();

/* Print xyz files in the LAMMPS atom dump format */
void print_lammps_xyz();


/* Print out the final state xyz file for NEB calculation of the migration
 * barrier
 */
void print_parcas_xyz_final_missing_barrier(char * file_name, Pvector qi, Pvector qf);

void print_parcas_xyz(char * file_name);

/* Make xyz file for Parcas for initial, qi, and final state, qf, of a jump, 
 * for which the migration energy is missing. */
void make_barrier_xyz(Pvector qi, Pvector qf, int qi_nn1, int qi_2nn, int qf_1nn, int qf_2nn);

/* Print file for export to COMSOL.
 */
void print_comsol_xyz();


/* Function to save the state of the simulation
 * in terms of objects positions.
 */
void print_xyzp(char * file_name);


/* Read in atom, adatom and vacancy objects from a xyzp file.
 * The objects will overwrite old objects at the same positions.
 */ 
void read_xyzp_file(char * file_name);


/* Read in atom, adatom and vacancy objects from a xyzp file.
 * The objects will overwrite old objects at the same positions.
 */ 
void read_comsol_xyz_file(char * file_name);


/* Make empty lattice with the dimensions 
 * specified in the configuration file*/
void initialize_lattice();



/* For an object at v, count and return the number of all NN atoms and adatoms. */
int object_1nn_bonds(Pvector v);


/* 
 * For an object at (x,y,z,p), count and return the number of all 2nn atoms and adatoms in a fcc lattice.
 * Should work for both (100), (111) and (110).
 */
int object_2nn_bonds(Pvector v);

// Make changes adatom <-> atom and vacancy <-> empty, depending on the nr_1nn value.
void update_object_kind(Pvector q);


// Update the bond count in the unit cells next to q0 and including q0.
void update_bond_count(Pvector q);


/* For every lattice point, count the number of 1nn and 2nn bonds 
 * to neigbour objects.
 */
void count_all_bonds();


/* Filling the plane of unit cells at z = 0,1 with atoms*/
void make_plane();

// Function to fill the system with atoms between z_min and z_max.
void make_bulk(int z_min, int z_max);

// Function to fill the system with atoms between z_min and z_max in metres.
void make_bulk_m(double min, double max);

void fix_bottom_layer();




// Make top empty layer account for non-periodic boundary.
void fix_top_empty_layer();



/* Make cube of dimension (X,Y,Z)*a^3, first layer at z = 4 */
void make_cube(int X, int Y, int Z);


/* Make an octahedron with a "diameter" d [m], truncated to diameter t<=d [m], and height h [m] above the surface */
void make_truncated_octahedron(double d, double t, double h);


/* Make cube of dimension (X,Y,Z)*a^3, first layer at z = 4 */
void make_cube_m(double x, double y, double z);


/* Make cube of dimension (X,Y,Z)*a^3, first layer at z = 4 */
void make_cubic_void(int X, int Y, int Z);


/* Make a cylinder with a diameter D [a0] and height Z, first layer at z = 4 */
void make_cylinder(double r, double h, int axis);

/* Make a sphere with a diameter d [m] and height h [m] above surface */
void make_sphere(double d, double h);

/* Make a ridge with height h [m] */
void make_ridge(double h);

/* Make cubic island of dimension (X,Y)*a^2; first layer at z = 4 */
void make_island(int X, int Y);


/* Function to add N adatoms on the surface.
 */
void add_adatoms(int N);


/* Add adatom at coordinate (x,y,z,p). */
void add_adatom(Pvector q, int label);


/* Randomly add N vacancies.
 */ 
void add_vacancies(int N);


void check_initial_configuration();

/* mark all atoms, adatoms and fixed atoms in the systems as bulk, which will 
 * not be removed as detached clusters in cluster analysis. */
void mark_free_clusters_as_bulk();

/* Read in general parameters from data.in */
void read_general_input();

/* Read in material-specific parameters from data.in */
void read_material_specific_input();

/* Read in surface parameters from data.in */
void read_system_input();

/* Read in substrate and boundary parameters from data.in */
void read_substrate_input();

/* Read in from data.in what structures to contruct in the system*/
void read_structures_input();

/* Read in parameters for external events from data.in */
void read_external_events_input();

/* Read in parameters for apex subsystem field calculation (only with Helmod) from data.in */
void read_libhelmod_optimization_input();

/* Read in parameters how to print xyz files from data.in */
void read_xyz_parameter_input();

/* Read in parameters how to print xyz files from data.in */
void read_output_files_input();

/* Read in parameters for end conditions and standard output from data.in */
void read_end_conditions_input();

/* Read in parameters for end conditions and standard output from data.in */
void read_specific_calculations_input();

/* Every particular simulation setup is defined
 * in a configuration file called data.in.
 * Rows starting with "#" are not read.
 */
void read_configuration_file();



/* Count nr_objects, nr_atomic_objects, nr_atoms, nr_adatoms, nr_fixed_atoms,
 * nr_adatoms, nr_empty, nr_fixed_empty, and nr_vacancies.
 */
void count_nr_objects();


/* Check that the system is correct */
void check_system_integrity(int data_out);



/* Calculate the jump probability frequency for a jump
 * of an adatom bond as (1nn,2nn) to an empy point bond as (1nn,2nn)
 */



/* Go through the whole lattice and calculate the probabilities for migration jumps
 */
void add_probabilities();



/* Sum all possible event probabilites 
 */
void calculate_sum_probabilities();





// Remove object q
void remove_object(Pvector q);



// If object, jumping from u to v, is ends up outside a non-periodic boundary, remove object.
void check_non_pbc(Pvector u, Pvector v);


// Check the conditions for evaporating of adatoms in the local space
// and remove them if the conditions are fulfilled.
void check_for_evaporation(Pvector q);


/* Index all atomic clusters and determine their size
 * A cluster are all groups of atoms (or adatoms) connected 
 * with a chain of 1nn bonds between the atoms.
 */
void cluster_analysis();


/* Make jump from q0 to q1. Boundary conditions already checked. Global xyzp already updated.
 */
void make_jump(Jump jump);



/* Choose an event and carry it out. 
 */
void choose_event();
  

/* Define the conditions for stopping the KMC simulation.
 * end_condition = 1 stops the simulation.
 */
void check_end_conditions();


/* Define the conditions for stopping the statistical KMC runs
 * kmc_runs_end_condition = 1 stops the cycle of KMC runs.
 */
void check_kmc_runs_end_conditions();


void check_parameter_table();
 
 
/* Increase the simulation time according to the resident algorithm 
 * [young1966monte]  
 */
void increase_time();


/* Look for reactions (have no probability rate but happens with a fixed time rate) */
void look_for_reactions();


/* The KMC algorithm. 
 * Carry out KMC steps until the end 
 * conditions are fulfilled.
 */
void kmc_simulation();

// Calculate the input parameters for PARCAS
void calculate_parcas_input();


/* Remove all atoms with the diffusion_label */
void remove_diffusion_atoms();


/* Analyse the runs for calculating the 3D diffusion coeffiecient */
void diffusion_coefficient_calculation_analysis(double measured_F);


/* Add an adatom and let it diffuse max_kmc_step jumps. 
 * Calculate its diffusion coeffiecent
 * Repeat nr_statistical_kmc_runs times.
 */
void calculation_of_diffusion_coefficient();
  

void main();
