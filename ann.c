#include "global.h"
#include "floatfann.h"

struct fann *ann_classifier[NEL-1] = {};
struct fann ***ann_regressor[NEL-1] = {};
int ann_classes[NEL-1];
int ann_committee_size[NEL-1];
const float classifier_sum_tol = 0.001;

/* Creates the reflection of the local atomic environment desc
   and saves it to mir1. The migration direction is preserved.
   Plane of reflection is parallel to a {100} surface.*/
void mirror1 (int *mir1, int desc[]) {
  int i;
  for (i = 0; i<max_s; i++){
    mir1[i] = desc[matrix_mirror1[i][1]];
  }
}

/* Creates the reflection of the local atomic environment desc
   and saves it to mir2. The migration direction is preserved.
   Plane of reflection is perpendicular to a {100} surface.*/
void mirror2 (int *mir2, int desc[]) {
  int i;
  for (i = 0; i<max_s; i++){
    mir2[i] = desc[matrix_mirror2[i][1]];
  }
}

/* Creates the reflection of the local atomic environment desc
   and saves it to mir3. The migration direction is REVERSED.
   Plane of reflection is perpendicular to the migration direction.*/
void mirror3 (int *mir3, int desc[]) {
  int i;
  for (i = 0; i<max_s; i++){
    mir3[i] = desc[matrix_mirror3[i][1]];
  }
}

/* Compares descriptions desc1 and desc2 element by element
   and returns the smaller one (first smaller element). If the
   descriptions are exactly the same, returns the (pointer to the)
   first description*/
int *mindesc(int desc1[], int desc2[]) {
  int i;
  for (i = 0; i < max_s; i++){
    if (desc1[i] < desc2[i]){
      return desc1;
    } else if(desc2[i] < desc1[i]){
      return desc2;
    }
  }
  return desc1;
}

/* Initializes the regressor network array */
void initiate_regressor(int element, int classes, int committee_size){
  ann_classes[element-1] = classes;
  ann_regressor[element-1] = malloc(classes*sizeof(struct fann *));
  ann_committee_size[element-1] = committee_size;
  int i;
  for (i = 0; i < classes; i++){
    ann_regressor[element-1][i] = malloc(committee_size*sizeof(struct fann *));
  }
}

/* Allocates the arrays needed for finding the minimum
   description in get_ann_barrier.*/
void allocate_desc_arrays(){
  desc = malloc(max_s*sizeof(int));
  mir1 = malloc(max_s*sizeof(int));
  mir2 = malloc(max_s*sizeof(int));
  invr = malloc(max_s*sizeof(int));
  reve = malloc(max_s*sizeof(int));
  rem1 = malloc(max_s*sizeof(int));
  rem2 = malloc(max_s*sizeof(int));
  rein = malloc(max_s*sizeof(int));
}

/* Reads in the the ANN regressor for the correct element and class from file.
   Returns the input dimensionality of the network. */
int read_in_ann_regressor(int element, int ann_class, int committee_member, char * file_name){
  ann_regressor[element-1][ann_class-1][committee_member-1] = fann_create_from_file(file_name);
  // Test
  fann_type input[max_s];
  fann_type *E_ann;
  int i;
  for (i = 0; i < max_s; i++){
    input[i] = 0.0;
  }
  E_ann = fann_run(ann_regressor[element-1][ann_class-1][committee_member-1], input);
  printf("# ANN %d %d %d returned %10.5f eV test value\n",element,ann_class,committee_member,E_ann[0]);
  if(ann_regressor[element-1][ann_class-1][committee_member-1]->scale_mean_in==NULL){
    printf("# No scaling included in ANN %d %d %d\n",element,ann_class,committee_member);
  }else{
    printf("# Scaling included in ANN %d %d %d\n",element,ann_class,committee_member);
  }
  return fann_get_num_input(ann_regressor[element-1][ann_class-1][committee_member-1]);
}

/* Reads in the the ANN classifier for the correct element from file
   Returns the input dimensionality of the network. */
int read_in_ann_classifier(int element, char * file_name){
  ann_classifier[element-1] = fann_create_from_file(file_name);
  return fann_get_num_input(ann_classifier[element-1]);
}

/* Returns the barrier for jump s of the correct element with ANN */
float get_ann_barrier(int element, int * s){
  /* Copy the local atomic environment s to
     integer array desc, then calculate the
     two mirror images, mir1 and mir2, and the
     "inverse" image invr (mirror1 of mir2). Then
     find out which of these four environments
     is the "minimum description" (mind) by comparing
     element by element. The minimum is fed as
     input to the regressor, to ensure identical
     response to symmetrically equivalent environments.*/
  memcpy(desc, s, max_s*sizeof(int));

  mirror1(mir1, desc);
  mirror2(mir2, desc);
  mirror1(invr, mir2);

  mirror3(reve, desc);

  mirror1(rem1, reve);
  mirror2(rem2, reve);
  mirror1(rein, rem2);

  mind_barr = mindesc( mindesc( mindesc(desc, mir1), mir2), invr);
  mind_class = mindesc( mindesc( mindesc( mindesc( mindesc( mindesc( mindesc(desc, mir1), mir2), invr), reve), rem1), rem2), rein);

  fann_type input_barr[max_s];
  fann_type input_class[max_s];
  int i;
  for (i = 0; i < max_s; i++){
    input_barr[i] = mind_barr[i];
    input_class[i] = mind_class[i];
  }

  fann_type *E_ann;
  float e_sum;
  float e_sum_committee;
  float e_pred;

  if (tag.using_classifier == 1){
    /* If using an ANN classifier, feed the input to it and
       use the proper regressors. */
    fann_type *classification_output;
    classification_output = fann_run(ann_classifier[element-1], input_class);

    /*
    // DEBUGGING CLASSIFIER:
    for (i = 0; i < ann_classes[element-1]; i++){
      printf("%f", classification_output[i]);
      printf(" ");
    }
    printf("\n");
    */

    int j;

    float classifier_sum = 0.0;
    for (i = 0; i < ann_classes[element-1]; i++){
      classifier_sum += classification_output[i];
    }
    if (classifier_sum > classifier_sum_tol){
      /* If the classifier gives sensible results,
         calculate the weighted sum of the committee
         results according to the classidfier output*/
      e_sum = 0.0;
      for (i = 0; i < ann_classes[element-1]; i++){
        /* If the classifier output for this process is
           zero, don't bother calling the corresponding
           regressors. */
        if (classification_output[i] <= 0.0){
          continue;
        }
        e_sum_committee = 0.0;
        for (j = 0; j < ann_committee_size[element-1]; j++){
          if (ann_regressor[element-1][i][j]->scale_mean_in != NULL){
            fann_scale_input(ann_regressor[element-1][i][j], input_barr);
          }
          E_ann = fann_run(ann_regressor[element-1][i][j], input_barr);
          if (ann_regressor[element-1][i][j]->scale_mean_in != NULL){
            fann_descale_input(ann_regressor[element-1][i][j], input_barr);
          }
          if (ann_regressor[element-1][i][j]->scale_mean_out != NULL){
            fann_descale_output(ann_regressor[element-1][i][j],E_ann);
          }
          e_sum_committee += E_ann[0];
        }
        e_sum += classification_output[i]*(e_sum_committee/ann_committee_size[element-1]);
      }
      e_pred = e_sum/classifier_sum;
    }else{
      /* If the classifier gives almost all zeros,
         just calculate the simple average of different
         classes. */
      e_sum = 0.0;
      for (i = 0; i < ann_classes[element-1]; i++){
        e_sum_committee = 0.0;
        for (j = 0; j < ann_committee_size[element-1]; j++){
          if (ann_regressor[element-1][i][j]->scale_mean_in != NULL){
            fann_scale_input(ann_regressor[element-1][i][j], input_barr);
          }
          E_ann = fann_run(ann_regressor[element-1][i][j], input_barr);
          if (ann_regressor[element-1][i][j]->scale_mean_in != NULL){
            fann_descale_input(ann_regressor[element-1][i][j], input_barr);
          }
          if (ann_regressor[element-1][i][j]->scale_mean_out != NULL){
            fann_descale_output(ann_regressor[element-1][i][j],E_ann);
          }
          e_sum_committee += E_ann[0];
        }
        e_sum += e_sum_committee/ann_committee_size[element-1];
      }
      e_pred = e_sum/ann_classes[element-1];
    }
  }else{
    /* If not using classifier at all, just
       take the result from the 0th (the only) class. */
    e_sum_committee = 0.0;
    for (i = 0; i < ann_committee_size[element-1]; i++){
      if (ann_regressor[element-1][0][i]->scale_mean_in != NULL){
        fann_scale_input(ann_regressor[element-1][0][i], input_barr);
      }
      E_ann = fann_run(ann_regressor[element-1][0][i], input_barr);
      if (ann_regressor[element-1][0][i]->scale_mean_in != NULL){
        fann_descale_input(ann_regressor[element-1][0][i], input_barr);
      }
      if (ann_regressor[element-1][0][i]->scale_mean_out != NULL){
        fann_descale_output(ann_regressor[element-1][0][i],E_ann);
      }
      e_sum_committee += E_ann[0];
    }
    e_pred = e_sum_committee/ann_committee_size[element-1];
  }
  if (e_pred < 0.0) e_pred = 0.0;
  return e_pred;
}
