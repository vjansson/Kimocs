/*
 *   Kimocs - Kinetic Monte Carlo for Surfaces
 * 
 *   Copyright (C) 2014 Ville Jansson, PhD, <ville.b.c.jansson@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 * 
 *   Cite as:
 * 
 *      V Jansson, E Baibuz, and F Djurabekova. Long-term stability of Cu surface nanotips.
 *      Nanotechnology, 27(26):265708, 2016, arXiv:1508.06870 [cond-mat.mtrl-sci]
 * 
 */

#include <stdio.h>	/* for fprintf and stderr */
#include <stdlib.h>	/* for exit */
#include <math.h>
#include <string.h>
#include "mt64.h"	/* Mersenne Twister random generator */
#include <time.h>	/* For timing functions */

#include "global.h"
#include "probabilities.h"
#include "kimocs.h"
#include "efield.h"
#include "ann.h"

#ifdef FIELD
#include "helmod_interface.h"
#endif // FIELD

/** Global field-related variables **/
#ifdef FIELD

/* General */
double field_grid[NCELLX_field][NCELLY_field][NCELLZ_field][4];

/* FEMOCS */
femocs_data femocs;

clock_t call_femocs_time0,call_femocs_time1;
double  call_femocs_time      = 0.0;
#endif //FIELD


/* Effective migration barrier for a jump from q to w under a field.
 * Periodic boundaries already accounted for. 
 * jump kind = 1, 2, 4, or 5 (nn, 2nn, 3nn, 5nn)
 * Using saddle point values also for longer jumps than nn jumps.
 */
Jump effective_barrier(Jump jump) {
  Pvector q = jump.initial;
  Pvector w = jump.final;
  int omega = jump.omega;
  
  double dEg = 0.0;       // Change of the barrier due to the field gradient
  double Em;              // Barrier without any field
  double Emfield;         
  double Fl;              // Field at intial position.
//   double Fs;              // Field at the saddle position.
  double DeltaF;          // Fs - Fl
  double jump_distance;   // [m] The jump distance.
  int    element;
  int    s[max_s];           // The 26D or 72D parameters
  int    i, sn_kind;
  Pvector sn;             // 1nn and 2nn atoms of the process
  Pvector q1;             // The p-vector of the target position
  double s_configuration = 0;
  Dipole system_dipole;
  int    a,b,c,d;
  Parameter par;
  
  
  
  if (tag.using_26D_parameters == 1 || tag.using_72D_parameters == 1) {
    /* Find the 26D configuration of the jump */
    for (i = 0; i < max_s; i++) {
      if (tag.using_26D_parameters == 1){
        sn = omega_26d_s_configuration(q, omega, i); 
      }else{
        sn = omega_72d_s_configuration(q, omega, i); 
      }
      
      sn_kind = system_lattice[sn.x][sn.y][sn.z][sn.p].kind;
      
      if (sn_kind == atom_kind || sn_kind == adatom_kind || sn_kind == fixed_atom_kind) {
        s[i] = system_lattice[sn.x][sn.y][sn.z][sn.p].element;
        if (tag.using_26D_parameters == 1){
          s_configuration += s[i]*pow(max_nr_elements + 1, 25 - i);
        }
      }
      else if (sn_kind == vacancy_kind || sn_kind == empty_kind || sn_kind == fixed_empty_kind) {
        s[i] = system_lattice[sn.x][sn.y][sn.z][sn.p].element;  // = vacancy_element
      }
      
    }
    
    if (tag.using_26D_parameters == 1){
      system_lattice[q.x][q.y][q.z][q.p].configuration_26d[omega] = s_configuration;  // Assumes that the corresponding jump_probability value is saved to .probability_nn_jump[omega] (which will happen later in the code). TODO: Add configuration_26d[omega] as member in the Jump struct. 
    }
  }
  
  /* The considered process q to w is (1,b,c,d) */
  a = jump.signum_4d.a;
  b = jump.signum_4d.b;
  c = jump.signum_4d.c;
  d = jump.signum_4d.d;
  
  /* Find the right jump distance and the field-independent migration energy barrier Em*/
  switch (jump.kind) {
  case 1:
    jump_distance = jump_distance_nn; // [m];
    
    if(tag.using_26D_parameters == 1 || tag.using_72D_parameters == 1) {
     
      if (tag.using_ann == 1){
        /* Check if the jump would result in evaporation.
           If it would, forbid the jump when using ANN */
//         q1 = omega_position_of_1nn(q,omega); // Final position.
        /* Less than 1 neighbour = vacuum, since the initial position is counted
           as an existing neighbour */
        if (system_lattice[jump.final.x][jump.final.y][jump.final.z][jump.final.p].nr_1nn <= 1){
          Em = 1000.0; // Em = 1000.0 eV means forbidden
        }
        else{
          /* Get the barrier from ann */
          element = system_lattice[q.x][q.y][q.z][q.p].element;
          Em = get_ann_barrier(element,s);
        }
//     printf("ANN Em = %lf eV\n",Em);
      }
      else {
        if (tag.using_26D_parameters == 1){
          Em = atom_26D_parameter_table[s[0]][s[1]][s[2]][s[3]][s[4]][s[5]][s[6]][s[7]][s[8]][s[9]][s[10]][s[11]][s[12]][s[13]][s[14]][s[15]][s[16]][s[17]][s[18]][s[19]][s[20]][s[21]][s[22]][s[23]][s[24]][s[25]];
        }
      }
      
    }
    else {
      /* Get barrier and attempt frequency for the transition */
      par = j1nn_parameter_table[a][b][c][d];
      Em = par.intrinsic_barrier;
      jump.index = par.index;
      if (tag.global_attempt_frequency_1nn == 0) {
        jump.attempt_frequency = par.attempt_frequency;
      }
      else {
        jump.attempt_frequency = attempt_frequency_1nn;
      }
    }
    system_lattice[q.x][q.y][q.z][q.p].barrier[omega] = Em; // The field-independent barrier.
    
    break;
  case 2:
    jump_distance = jump_distance_2nn;
    par = j2nn_parameter_table[a][b][c][d];
    Em = par.intrinsic_barrier;
    jump.index = par.index;
    if (tag.global_attempt_frequency_2nn == 0) {
      jump.attempt_frequency = par.attempt_frequency;
    }
    else {
      jump.attempt_frequency = attempt_frequency_2nn;
    }
    break;  
  case 3:
    jump_distance = jump_distance_3nn;
    par = j3nn_parameter_table[a][b][c][d];
    Em = par.intrinsic_barrier;
    jump.index = par.index;
    if (tag.global_attempt_frequency_3nn == 0) {
      jump.attempt_frequency = par.attempt_frequency;
    }
    else {
      jump.attempt_frequency = attempt_frequency_3nn;
    }
    break;
  case 5:
    jump_distance = jump_distance_5nn;
    par = j5nn_parameter_table[a][b][c][d];
    Em = par.intrinsic_barrier;
    jump.index = par.index;
    if (tag.global_attempt_frequency_5nn == 0) {
      jump.attempt_frequency = par.attempt_frequency;
    }
    else {
      jump.attempt_frequency = attempt_frequency_5nn;
    }
    break;
  default:
    printf("ERROR Only nn, 2nn, 3nn and 5nn jumps are implemented for this surface orientation.\n");
    exit(1);
  }
  
  jump.effective_barrier = jump.intrinsic_barrier = Em; // Effective barrier == intrinsic_barrier in absence of a field.
  
#ifdef FIELD
  /* A field will modify the jump.effective_barrier, but not the jump.intrinsic_barrier */

  if (tag.use_field == 1) {
    /* Find the scalar field at the initial position Fl and at the saddle point Fs */
    Fl = system_lattice[q.x][q.y][q.z][q.p].scalar_field;
        
    DeltaF = system_lattice[q.x][q.y][q.z][q.p].field_gradient[omega]*(jump_distance/2.0);  // Using the gradient to estimate the field at the saddle point (Fs), halfway of the jump.
    
//     dEg = - adatom_sad.mu*Fs - 0.5*adatom_sad.alpha*Fs*Fs + 0.5*substrate.alpha*Fs*Fs \
//           + adatom_lat.mu*Fl + 0.5*adatom_lat.alpha*Fl*Fl - 0.5*substrate.alpha*Fl*Fl;  // [kyritsakis2018atomistic] Eq. 9 for field gradients
    
    system_dipole = W2232;
    
    // The migration barrier in electric field gradients; Eq. 9 in [kyritsakis2018atomistic]
    Emfield = Em - system_dipole.M_sl*Fl - system_dipole.A_sl/2.0*Fl*Fl - system_dipole.M_sr*DeltaF - system_dipole.A_sr*Fl*DeltaF;
    
    dEg = Emfield - Em; // The field part of the barrier.
    system_lattice[q.x][q.y][q.z][q.p].field_gradient_barrier[omega] = dEg;  // Saving for later analysis.
    
  
    /* Effective barrier t.barrier */
  
    /* Forbidding negative barriers and correcting for forbidden barriers (> 50 eV) */
    if (Emfield > 0.0) {
      if(Em < 49.0)   // Em >= 50.00 eV barriers used for marking and are not physical. 
        jump.effective_barrier = Emfield;
      else if (Em >= 50.0) {
        jump.effective_barrier   = Em;
        dEg = 0.0; // The field does not affect forbidden barriers
        if (tag.use_field == 1) {
          system_lattice[q.x][q.y][q.z][q.p].field_gradient_barrier[omega] = dEg;  // Saving for later analysis.
          jump.field_barrier = dEg;
        }
      }
    }
    else jump.effective_barrier = 0.0;
  }
#endif // FIELD
  
//   if (Em < 1.0) {
//     printf("%e %e %f %f %f\n",F0,F1,dEm,Em,t.barrier);
//   } 
  
  if (jump.kind == 1) system_lattice[q.x][q.y][q.z][q.p].effective_barrier[omega] = jump.effective_barrier;
  
  return jump;
}


#ifdef FIELD


/* Update the field gradients for all adatoms and vacancies in the system.
 * Assumes that the field for all points has been previously updated.
 */
void update_all_gradients(){
  Pvector q;
  int omega;
  
//   printf("update_all_gradients()\n");
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind || system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {
            add_gradient(q);
          }
          else {
            for (omega = 0; omega < max_1nn; omega++) {
              system_lattice[q.x][q.y][q.z][q.p].field_gradient[omega] = 0.0;
            }
            for (omega = 0; omega < max_2nn; omega++) {
              system_lattice[q.x][q.y][q.z][q.p].field_gradient_2nn[omega] = 0.0;
            }
            for (omega = 0; omega < max_3nn; omega++) {
              system_lattice[q.x][q.y][q.z][q.p].field_gradient_3nn[omega] = 0.0;
            }
            for (omega = 0; omega < max_5nn; omega++) {
              system_lattice[q.x][q.y][q.z][q.p].field_gradient_5nn[omega] = 0.0;
            }
          }
        }
      }
    }
  }
}


/* Calculate the gradients in the (omega) directions of all possible jumps of adatom q*/
void add_gradient(Pvector q) {
  Pvector w,m;
  int omega;
  double F0,F1;
  double gradient;
  double max_gradient = 0.0;  // Max gradient for adatom q.
  int i;
  double d; // [m]; distance between field measuring points ijk
  Ivector h0,h1;

  
  /* Looking for neighbour vacancies and adatoms */
  if (system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind || system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
    
    if (tag.libhelmod_field_model == 1 || femocs.tag_use_femocs == 1) {
      for (omega = 0; omega < max_1nn; omega++) {    
      
        
        /* Forward jump */
        w = omega_position_of_1nn(q,omega);
        
        /* Backward jump */
        m = omega_position_of_1nn(q,inverse_omega[q.p][omega]);
        
        F1 = system_lattice[w.x][w.y][w.z][w.p].scalar_field;
        F0 = system_lattice[m.x][m.y][m.z][m.p].scalar_field; // m, q and w are in line, m opposit to w.
              
        /* Field gradient at q in the direction of the jump, omega */
        gradient = gradient_correction*(F1 - F0)/(2.0*jump_distance_nn); // F1 is the field at the forward lattice point. F0 is the backward jump position
        system_lattice[q.x][q.y][q.z][q.p].field_gradient[omega] = gradient;
        if (system_lattice[q.x][q.y][q.z][q.p].label == label_diffusion) {
          if (system_lattice[w.x][w.y][w.z][w.p].kind == vacancy_kind) {
            system_lattice[w.x][w.y][w.z][w.p].nn_gradient = gradient;
          }
          else {
            system_lattice[w.x][w.y][w.z][w.p].nn_gradient = 0.0;
          }
        }
        if(gradient > max_gradient) {
          max_gradient = gradient;
        }
        
      }
      system_lattice[q.x][q.y][q.z][q.p].max_gradient = max_gradient;

      
      /* Looking for 2nn vacancies and adatoms */
      for (omega = 0; omega < max_2nn; omega++) {    

          
          /* Forward jump */
          w = omega_position_of_2nn(q,omega);
          
          /* Backward jump */
          m = omega_position_of_2nn(q,inverse_omega2[q.p][omega]);
          
          F1 = system_lattice[w.x][w.y][w.z][w.p].scalar_field;
          F0 = system_lattice[m.x][m.y][m.z][m.p].scalar_field; // m, q and w are in line, m opposit to w.
                
          /* Field gradient at q in the direction of the jump, omega */
          gradient = (F1 - F0)/(2.0*jump_distance_2nn);
          system_lattice[q.x][q.y][q.z][q.p].field_gradient_2nn[omega] = gradient;
        
      }
      
      
      /* Looking for 3nn vacancies and adatoms */
      for (omega = 0; omega < max_3nn; omega++) {    

          
          /* Forward jump */
          w = omega_position_of_3nn(q,omega);
          
          /* Backward jump */
          m = omega_position_of_3nn(q,inverse_omega3[q.p][omega]);
          
          F1 = system_lattice[w.x][w.y][w.z][w.p].scalar_field;
          F0 = system_lattice[m.x][m.y][m.z][m.p].scalar_field; // m, q and w are in line, m opposit to w.
                
          /* Field gradient at q in the direction of the jump, omega */
          gradient = (F1 - F0)/(2.0*jump_distance_3nn);
          system_lattice[q.x][q.y][q.z][q.p].field_gradient_3nn[omega] = gradient;
        
      }
      
      
      /* Looking for 5nn vacancies and adatoms */
      for (omega = 0; omega < max_5nn; omega++) {    

          
          /* Forward jump */
          w = omega_position_of_5nn(q,omega);
          
          /* Backward jump */
          m = omega_position_of_5nn(q,inverse_omega5[q.p][omega]);
          
          F1 = system_lattice[w.x][w.y][w.z][w.p].scalar_field;
          F0 = system_lattice[m.x][m.y][m.z][m.p].scalar_field; // m, q and w are in line, m opposit to w.
                
          /* Field gradient at q in the direction of the jump, omega */
          gradient = (F1 - F0)/(2.0*jump_distance_5nn);
          system_lattice[q.x][q.y][q.z][q.p].field_gradient_5nn[omega] = gradient;
        
      }
    }
    
    
    
    
    else if (tag.libhelmod_field_model == 2) {
      /* The gradient is calculatead as (F1 - F0)/d, where 
       * F1 is the field at the forward jump position and
       * F0 is the initial position q; 
       * d is the length between the remote field measuring points ijk.
       */
      
      max_gradient = 0.0;
      
      for (omega = 0; omega < max_1nn; omega++) {
        
      
        
        /* Forward jump */
        w = omega_position_of_1nn(q,omega);
        
        F1 = system_lattice[w.x][w.y][w.z][w.p].scalar_field;
        F0 = system_lattice[q.x][q.y][q.z][q.p].scalar_field;
        
        if (system_lattice[w.x][w.y][w.z][w.p].kind == vacancy_kind) {
          d = ijk_distance(system_lattice[q.x][q.y][q.z][q.p].remote_field_position,system_lattice[w.x][w.y][w.z][w.p].remote_field_position); // Assuming pbc have not been applied to the remote field positions.

        
          /* Field gradient at q in the direction of the jump, omega */
          if (d > 0.0) {
            gradient = (F1 - F0)/d;
          }
          else gradient = 0.0;
        }
        else {
          d = 0.0;
          gradient = 0.0; // The gradients for jumps into other atoms should be zero.
        }
            
        system_lattice[q.x][q.y][q.z][q.p].field_gradient[omega] = gradient;
        
        
        if (system_lattice[q.x][q.y][q.z][q.p].label == label_diffusion) {
          if (system_lattice[w.x][w.y][w.z][w.p].kind == vacancy_kind) {
            system_lattice[w.x][w.y][w.z][w.p].nn_gradient = gradient;
          }
          else {
            system_lattice[w.x][w.y][w.z][w.p].nn_gradient = 0.0;
          }
        }
        
        if(fabs(gradient) > fabs(max_gradient)) {
          max_gradient = fabs(gradient);
        }
        
      }
      system_lattice[q.x][q.y][q.z][q.p].max_gradient = max_gradient;

      
      /* Looking for 2nn vacancies and adatoms */
      for (omega = 0; omega < max_2nn; omega++) {    

          
          /* Forward jump */
          w = omega_position_of_2nn(q,omega);
          
          F1 = system_lattice[w.x][w.y][w.z][w.p].scalar_field;
          F0 = system_lattice[q.x][q.y][q.z][q.p].scalar_field;
          if (system_lattice[w.x][w.y][w.z][w.p].element == vacancy_element) {
            d = ijk_distance(system_lattice[q.x][q.y][q.z][q.p].remote_field_position,system_lattice[w.x][w.y][w.z][w.p].remote_field_position); // Assuming pbc have not been applied to the remote field positions.
                  
            /* Field gradient at q in the direction of the jump, omega */
            if (d > 0.0) {
              gradient = (F1 - F0)/d;
            }
            else gradient = 0.0;
          }
          else {
            d = 0.0;
            gradient = 0.0; // The gradients for jumps into other atoms should be zero.
          }
          
          system_lattice[q.x][q.y][q.z][q.p].field_gradient_2nn[omega] = gradient;
        
      }
      
      
      /* Looking for 3nn vacancies and adatoms */
      for (omega = 0; omega < max_3nn; omega++) {    

          
          /* Forward jump */
          w = omega_position_of_3nn(q,omega);
          
          F1 = system_lattice[w.x][w.y][w.z][w.p].scalar_field;
          F0 = system_lattice[q.x][q.y][q.z][q.p].scalar_field; 
          
          if (system_lattice[w.x][w.y][w.z][w.p].element == vacancy_element) {
            d = ijk_distance(system_lattice[q.x][q.y][q.z][q.p].remote_field_position,system_lattice[w.x][w.y][w.z][w.p].remote_field_position); // Assuming pbc have not been applied to the remote field positions.
                  
            /* Field gradient at q in the direction of the jump, omega */
            if (d > 0.0) {
              gradient = (F1 - F0)/d;
            }
            else gradient = 0.0;;
          }
          else {
            d = 0.0;
            gradient = 0.0; // The gradients for jumps into other atoms should be zero.
          }
          
          system_lattice[q.x][q.y][q.z][q.p].field_gradient_3nn[omega] = gradient;
        
      }
      
      
      /* Looking for 5nn vacancies and adatoms */
      for (omega = 0; omega < max_5nn; omega++) {    

          
          /* Forward jump */
          w = omega_position_of_5nn(q,omega);
          
          F1 = system_lattice[w.x][w.y][w.z][w.p].scalar_field;
          F0 = system_lattice[q.x][q.y][q.z][q.p].scalar_field;
          
          if (system_lattice[w.x][w.y][w.z][w.p].element == vacancy_element) {
            d = ijk_distance(system_lattice[q.x][q.y][q.z][q.p].remote_field_position,system_lattice[w.x][w.y][w.z][w.p].remote_field_position); // Assuming pbc have not been applied to the remote field positions.
                  
            /* Field gradient at q in the direction of the jump, omega */
            if (d > 0.0) {
              gradient = (F1 - F0)/d;
            }
            else gradient = 0.0;
          }
          else {
            d = 0.0;
            gradient = 0.0; // The gradients for jumps into other atoms should be zero.
          }
              
          system_lattice[q.x][q.y][q.z][q.p].field_gradient_5nn[omega] = gradient;
        
      }
    }
    
  }
}




/* Initialize FEMOCS variables */
void intialize_femocs_variables() {
  femocs.Ncalls = 0;
  femocs.tag_use_femocs = 0;                        // Use FEMOCS to calculate the field.
  femocs.external_field = 1.0e-10*external_field;   // The external field in [V/ang]	
  femocs.nr_adatoms	= 0;                            // Nr of adatoms (to be sent to FEMOCS)
  femocs.nr_vacancies = 0;
  femocs.nr_field_points = 0;                       // Nr of adatoms + vacancies.
}

/* Update variables needed to call FEMOCS. */ 
void update_femocs_variables() {
  int i;
  femocs.external_field = 1.e-10 * external_field;  // The external field in [V/ang]	

  count_number_atoms();                               // Will update the nr_adatoms and nr_vacancies variables.
  femocs.nr_adatoms      = nr.adatoms;
  femocs.nr_vacancies    = nr.vacancies;
  
//   tag.print_femocs_field = 0; // DEBUG
  
  /* For the first and the last step, calculate the field in every point of the system */ 
  if (tag.print_femocs_field == 1 || (kmc_step == 0 && kmc_run == 0) || tag.end_condition == 1) {
    femocs.nr_field_points = dimension.x*dimension.y*dimension.z*dimension.p;
  }
  else {
    femocs.nr_field_points = nr.adatoms + nr.vacancies;
  }
    
  /* Memoray allocations. Done every kmc step as the number of adatoms may change */
  femocs.Ad_position_x = malloc(sizeof(double) * femocs.nr_adatoms);
  memset(femocs.Ad_position_x, 0.0, sizeof(double) * femocs.nr_adatoms);
  
  femocs.Ad_position_y = malloc(sizeof(double) * femocs.nr_adatoms);
  memset(femocs.Ad_position_y, 0.0, sizeof(double) * femocs.nr_adatoms);
  
  femocs.Ad_position_z = malloc(sizeof(double) * femocs.nr_adatoms);
  memset(femocs.Ad_position_z, 0.0, sizeof(double) * femocs.nr_adatoms);
  
  femocs.field_point_x = malloc(sizeof(double) * femocs.nr_field_points);
  memset(femocs.field_point_x, 0.0, sizeof(double) * femocs.nr_field_points);
  
  femocs.field_point_y = malloc(sizeof(double) * femocs.nr_field_points);
  memset(femocs.field_point_y, 0.0, sizeof(double) * femocs.nr_field_points);
  
  femocs.field_point_z = malloc(sizeof(double) * femocs.nr_field_points);
  memset(femocs.field_point_z, 0.0, sizeof(double) * femocs.nr_field_points);
  
  femocs.field_x = malloc(sizeof(double) * femocs.nr_field_points);
  memset(femocs.field_x, 0.0, sizeof(double) * femocs.nr_field_points);
  
  femocs.field_y = malloc(sizeof(double) * femocs.nr_field_points);
  memset(femocs.field_y, 0.0, sizeof(double) * femocs.nr_field_points);
  
  femocs.field_z = malloc(sizeof(double) * femocs.nr_field_points);
  memset(femocs.field_z, 0.0, sizeof(double) * femocs.nr_field_points);
  
  femocs.field_norm = malloc(sizeof(double) * femocs.nr_field_points);
  memset(femocs.field_norm, 0.0, sizeof(double) * femocs.nr_field_points);
  
  
  femocs.force_charge = malloc(sizeof(double) * femocs.nr_adatoms * 4);
  memset(femocs.force_charge, 0.0, sizeof(double) * femocs.nr_adatoms * 4);
  
  femocs.types = malloc(sizeof(int) * femocs.nr_adatoms);
  for (i = 0; i < femocs.nr_adatoms; i++) femocs.types[i] = 2;
  
  femocs.flags = malloc(sizeof(int) * femocs.nr_field_points);
  for (i = 0; i < femocs.nr_field_points; i++) femocs.flags[i] = 0;
}

void call_femocs() {
  call_femocs_time0 = clock();
  printf("%e Calling FEMOCS\n", kmc_step);
  Pvector q;
  int atomN = 0, success, tot_success = 0;
  int pointN = 0;
  Vector F;
  
  update_femocs_variables();
  
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          /* Make a list of all adatoms for FEMOCS */
          if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
            femocs.Ad_position_x[atomN] = 1.0e10 * system_lattice[q.x][q.y][q.z][q.p].position.x;   // [ang]		
            femocs.Ad_position_y[atomN] = 1.0e10 * system_lattice[q.x][q.y][q.z][q.p].position.y;   // [ang]
            femocs.Ad_position_z[atomN] = 1.0e10 * system_lattice[q.x][q.y][q.z][q.p].position.z;   // [ang]

            atomN++; 
          }
          
          /* Make a list of all points (adatoms and vacancies) for which FEMOCS will return the field 
           * Always done for the first and last KMC step.
           */
          if (tag.print_femocs_field == 1 || (kmc_step == 0 && kmc_run == 0) || tag.end_condition == 1) {
            femocs.field_point_x[pointN] = 1.0e10 * system_lattice[q.x][q.y][q.z][q.p].position.x;  // [ang]    
            femocs.field_point_y[pointN] = 1.0e10 * system_lattice[q.x][q.y][q.z][q.p].position.y;  // [ang]
            femocs.field_point_z[pointN] = 1.0e10 * system_lattice[q.x][q.y][q.z][q.p].position.z;  // [ang]

            pointN++; 
          }
          else {
            if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind || system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {
              femocs.field_point_x[pointN] = 1.0e10 * system_lattice[q.x][q.y][q.z][q.p].position.x;  // [ang]    
              femocs.field_point_y[pointN] = 1.0e10 * system_lattice[q.x][q.y][q.z][q.p].position.y;  // [ang]
              femocs.field_point_z[pointN] = 1.0e10 * system_lattice[q.x][q.y][q.z][q.p].position.z;  // [ang]

              pointN++; 
            }
          }
        }
      }
    }
  }
  
  if (atomN > femocs.nr_adatoms) {
    printf("ERROR The surface atoms are not counted correctly. atomN = %d, nr_adatoms = %d\n", atomN, femocs.nr_adatoms);
    exit(1);
  }
    
    
  /* Create the femocs object if first call */
  if (femocs.Ncalls == 0) femocs.femocs_obj = create_femocs("data.in");

  /* Export the atoms to FEMOCS */
  femocs_import_atoms(femocs.femocs_obj, &femocs.error, femocs.nr_adatoms, 
          femocs.Ad_position_x, femocs.Ad_position_y, femocs.Ad_position_z, femocs.types);
  
  if (femocs.error) {
    printf("%le Error in femocs importing atoms\n", kmc_step);
    nr_femocs_errors++;
  }
  
  /* Run Femocs */
  femocs_run(femocs.femocs_obj, &femocs.error, 0, 0.0);
  
  if (femocs.error) {
    printf("%le Error in femocs run\n", kmc_step);
    nr_femocs_errors++; 
  }

  /* Import the electric field on atoms from FEMOCS */
//   femocs_interpolate_elfield(femocs.femocs_obj, &femocs.error, femocs.nr_field_points,
//           femocs.field_point_x, femocs.field_point_y, femocs.field_point_z,
//           femocs.field_x, femocs.field_y, femocs.field_z, 
//           femocs.field_norm, femocs.flags);
  
  /* Import the electric field on atoms from FEMOCS */
  femocs_interpolate_surface_elfield(femocs.femocs_obj, &femocs.error, femocs.nr_field_points,
          femocs.field_point_x, femocs.field_point_y, femocs.field_point_z,
          femocs.field_x, femocs.field_y, femocs.field_z, 
          femocs.field_norm, femocs.flags);
  
  
  if(tag.use_charges == 1) {
//    femocs_export_charge_and_force(femocs.femocs_obj, &femocs.error, femocs.nr_adatoms, 
//             femocs.force_charge);
    printf("ERROR femocs_export_charge_and_force() in femocs/src/Femocs_wrap.cpp does not exist anymore and need to be reimplemented.\n");
    exit(1);
  }          

  atomN = 0;
  
  if (femocs.error == 0) {
    
    /* Get the field for every adatom and vacancy (go through them in the same order as when the femocs arrays were initialized before FEMOCS was called */
    int pointN = 0;
    for (q.x = 0; q.x < dimension.x; q.x++) {
      for (q.y = 0; q.y < dimension.y; q.y++) {
        for (q.z = 0; q.z < dimension.z; q.z++) {
          for (q.p = 0; q.p < dimension.p; q.p++) {
             
            if (tag.use_charges == 1) {
              if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind) {
                  system_lattice[q.x][q.y][q.z][q.p].electric_charge = femocs.force_charge[4 * atomN];
                  atomN++;
              }
            }
            
            /* For the first and last KMC step, the field is calculated for all points */
            if (tag.print_femocs_field == 1 || (kmc_step == 0 && kmc_run == 0) || tag.end_condition == 1) {
              F.x = 1.e10 * femocs.field_x[pointN]; // [V/m]
              F.y = 1.e10 * femocs.field_y[pointN];
              F.z = 1.e10 * femocs.field_z[pointN];

              system_lattice[q.x][q.y][q.z][q.p].electric_field = F;
              system_lattice[q.x][q.y][q.z][q.p].scalar_field = field_sign*sqrt(F.x*F.x + F.y*F.y + F.z*F.z);

              pointN++;
            }
            else {
              if (system_lattice[q.x][q.y][q.z][q.p].kind == adatom_kind || 
                  system_lattice[q.x][q.y][q.z][q.p].kind == vacancy_kind) {

                F.x = 1.e10 * femocs.field_x[pointN]; // [V/m]
                F.y = 1.e10 * femocs.field_y[pointN];
                F.z = 1.e10 * femocs.field_z[pointN];

                system_lattice[q.x][q.y][q.z][q.p].electric_field = F;
                system_lattice[q.x][q.y][q.z][q.p].scalar_field = field_sign*sqrt(F.x*F.x + F.y*F.y + F.z*F.z);

                pointN++;
                
              }
            }
          }
        }
      }
    }
    
  }
  else {
    printf("%le FEMOCS ERROR: The field could not be calculated.\n", kmc_step); // Ending the simulation\n");
//     tag.end_condition = 1;
    nr_femocs_errors++;
  }
  
  femocs.Ncalls++;
  
  
  free_femocs_data();
  
  call_femocs_time1 = clock();
  call_femocs_time += (double) (call_femocs_time1 - call_femocs_time0) / CLOCKS_PER_SEC;
}


/* Update variables needed to call FEMOCS. */ 
void free_femocs_data() {

  /* Memoray deallocations. Done every kmc step as the number of adtoms may change */
  free(femocs.Ad_position_x);
  free(femocs.Ad_position_y); 
  free(femocs.Ad_position_z);
  
  free(femocs.field_point_x);
  free(femocs.field_point_y); 
  free(femocs.field_point_z);
  
  free(femocs.field_x);    
  free(femocs.field_y);
  free(femocs.field_z);
  free(femocs.field_norm);
  
  free(femocs.force_charge);    
  
  free(femocs.flags);
  free(femocs.types); 
  
  
  
}




/* Add the electric field and charge to every lattice point */
void add_electric_field_and_charge() {
  
//   printf("%e Field calculated\n",kmc_step);
  nr.calls_field_solver++;
  
  if (tag.use_libhelmod == 1) {
    helmod();		// Use LibHelmod to calculate field and charges.
  }
  else if (femocs.tag_use_femocs == 1) {
    call_femocs();	// Use FEMOCS to calculate the field.
  }
  
  else {
    printf("ERROR No field solver is specified\n");
    exit(1);
  }  
}


/* Set the counter of the number of steps without any field calculation for every atom to zero. */
void initiate_field_jump_optimization() {
  Pvector q;
  for (q.x = 0; q.x < dimension.x; q.x++) {
    for (q.y = 0; q.y < dimension.y; q.y++) {
      for (q.z = 0; q.z < dimension.z; q.z++) {
        for (q.p = 0; q.p < dimension.p; q.p++) {
          system_lattice[q.x][q.y][q.z][q.p].nr_jumps_with_constant_field = 0;
        }
      }
    }
  }
}



/* Add the electric field and charge to every lattice point. Might put tag.end_condition == 1 and stop the simulation. */
void calculate_field_for_full_system(){
  
  /* Optimization for individual atoms */
  if (tag.recalculate_field == 1 || max_jumps_at_constant_field == 0) {
    tag.recalculate_field = 0;  // Reset tag.
    add_electric_field_and_charge();
    initiate_field_jump_optimization();
    
    if (tag.deposition_mode == 1 || tag.deposition_mode == 2 || tag.deposition_mode == 3) {
     deposition_field_correlation();
    }
  }
    
  
}


/* Clean up memory used by femocs before ending the simulation and prints end information 
 * to output directory*/
void call_femocs_destructor() {
  if (femocs.tag_use_femocs == 1 && tag.end_condition == 1) {
    delete_femocs(femocs.femocs_obj);
  }
}

#endif //FIELD
